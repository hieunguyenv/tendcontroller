//
//  ParentCrop_SuccessionController.swift
//  TendGrow
//
//  Created by Nguyen Trung on 3/24/16.
//  Copyright (c) 2016 spiraledge.com. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum CategoryEnum: Int {
    case vegetables = 1
    case herbs = 2
    case fruits = 3
    case flowers = 4
    case farmseed = 5
    case trees_ = 6
    case berries = 7
    case vines = 8
    
}

enum CreateListObjectiveAndChangeEnum: Int {
    case createListFirst = 1
    case createListDetail = 2
    
    case changeVariety = 3
    case changeVariety_FinalHarvest = 4
    
    case changeCropType = 5
    case changeCropType_FinalHarvest = 6
    
    case changePlatingMethod = 7
    
    case changeGrowingMethod = 8
    case changeGrowingMethod_FinalHarvest = 9
    
    case createTaskDetail = 10
    case changeHarvestSeason = 11
}

enum TypeSelectCropTypeEnum: Int {
    case skipVariety = 1
    case haveVariety = 2
    
}



@objc protocol ParentCrop_SuccessionControllerDelegate {
    @objc optional func ParentCrop_SuccessionControllerDelegate_ReloadTable(_ listsuccesion:[SectionForRow])
    @objc optional func ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType(_ indexPath:IndexPath, createObjectAndChangType:Int, currentvalue: String)
    @objc optional func ParentCrop_SuccessionControllerDelegate_showDatePicker(_ indexPath:IndexPath, date:Date, minDate:Date?, maxDate:Date?)
    @objc optional func ParentCrop_SuccessionControllerDelegate_updateValue(_ indexPath:IndexPath, currentValue:String, maximumNumber:Float)
    @objc optional func ParentCrop_SuccessionControllerDelegate_ShowPickerSelection(_ indexPath:IndexPath, valueSelect:String, listSelect: NSArray, actionType:AddTaskTypeAction )
    @objc optional func ParentCrop_SuccessionControllerDelegate_showDropdownColumn(_ indexPath:IndexPath, clsColumn:ClassObjectColumn)
    @objc optional func ParentCrop_SuccessionControllerDelegate_linkLocation(_ indexPath:IndexPath, fromDate:Double, toDate:Double ,plantingAmount:Int, plantingUnit:String, rowperbed:Int)
    @objc optional func ParentCrop_SuccessionControllerDelegate_linkLocation_Orchard(_ indexPath:IndexPath, lng:Double, lat:Double, orchardId:Int)
    @objc optional func didCheckCompleteTask(_ indexPath:IndexPath, dateIntervalDatePlantingLabor : Double)
    @objc optional func ParentCrop_SuccessionControllerDelegate_editNote(_ indexPath:IndexPath, noteid:Int)
    @objc optional func ParentCrop_SuccessionControllerDelegate_updateConvertRate(_ indexPath:IndexPath, fromunit:String, tounit:String, currentNumber:String, isHarvestUnit:Bool)
    
    @objc optional func ParentCrop_SuccessionControllerDelegate_editGoogleMap(_ indexPath:IndexPath, currentPoint: CLLocationCoordinate2D)
    @objc optional func statusCompleteTask(_ status: Int)
    @objc optional func setUserProfile(_ userAssign: NSString, imageUser: NSString, date: NSString, labor: NSString)
    @objc optional func gotoTimelineCrop(_ successionid: Int, varietyName: String, parentName: String)
}
class ParentCrop_SuccessionController: NSObject {
    weak var delegate: ParentCrop_SuccessionControllerDelegate? = nil
    //MARK: - PUBLIC
    var listEvent: [TaskTimeline] = [TaskTimeline]()
    var listUserAssign: [User] = [User]()
    var listUserAssigned: [User] = [User]()
    var clsTaskDetail:SW_Task = SW_Task()
    var clsSuccessionDetail:CSuccession = CSuccession()
    var clsMasterCropSuccession:CMasterCropSuccession = CMasterCropSuccession()
    var combinationDataForShow: CMasterCropCombinationData = CMasterCropCombinationData()
    var combiKey: String = ""
    var combiId: Int = 0
    var successionSeedingMethoTypeCL:SuccessionSeedingMethodType = SuccessionSeedingMethodType.NONE_SEEDINGMETHOD //GREENHOUSE_SOW_S
    var listFieldLocation:[ClassFields] = [ClassFields]()
    var listGreenhouseLocation:[ClassFields] = [ClassFields]()
    var isSelectItem:Bool = false
    var plantingMethodFirstLoad:String = ""
    var listSectionSuccession = [SectionForRow]()
    var typeSelectCropTypeEnum: TypeSelectCropTypeEnum = TypeSelectCropTypeEnum.haveVariety
    var firstharvestcompleted:Bool = false
    var isFinishCrop:Bool = false
    var growingmethod:String = ""
    var previousPlatingMethod:SuccessionSeedingMethodType = SuccessionSeedingMethodType.NONE_SEEDINGMETHOD
    var location:DetailClassField? = nil
    var selectedBedList : [SelectedBed] = [SelectedBed]()
    var categoryID = 0
    var fieldid = 0
    var listConvertRate:[CConvertTrates] = [CConvertTrates]()
    var isSingleTree = false
    var createCellObject = CreateCellForGroup()
    var treeLocation = CLLocationCoordinate2DMake(0, 0)
    var isNoBedSelected:Bool = false
    var clsTrackingSuccession :ClassTrackingSuccession? = nil
    var stringMaturity: String = ""
    var haveCombiUser: Bool = false
    var isEditedFields: Bool = false
    var isHassuccession: Bool = false
    var statusOfTask: Int = 0
    var listComment: NSMutableArray = NSMutableArray()
    var listSeedSource = [SW_SeedSource]()
    var totalTime: Int = 0
    var listUnitParentCrop: NSArray = []
    let saveSuccession: SaveObject = SaveObject()
    let saveTask: SaveObject = SaveObject()
    var stringDesLocation: String = ""
    var stringCropTypeEdit: String = ""
    
    var listEventExpensesCreate: [eventMixpanel] = [eventMixpanel]()
    var typeSuccession: String = ""
    var evenSuccession: String = ""
    
    var shouldChangeValueEdit: Bool = false
    var isHasUnitSuccession: Bool = false
    //MARK: - PRIVATE 
    fileprivate let _kWidthOfIphone = UIScreen.main.bounds.size.width
    fileprivate var previousPlantingDate_Interval:Double = 0
    var dateIntervalSeedingDate:Double = 0
    var dateIntervalDayToTransplant:Double = 0
    var dateIntervalDayToFirstHarvest:Double = 0
    fileprivate var dateIntervalDayToLastHarvest:Double = 0
    
    fileprivate var numberDayToTransplant:Double = 0
    fileprivate var numberDayToFirstHarvest:Double = 0
    fileprivate var numberDayToLastHarvest:Double = 0
    fileprivate var daytoTransplantLow:Double = 0
    fileprivate var daytoTransplantHigh:Double = 0
    var dateIntervalDatePlantingLabor:Double = 0
    fileprivate var laborTime:Int = 0
    fileprivate var fieldName = ""
    
    fileprivate var changeTypeEnum:CreateListObjectiveAndChangeEnum = CreateListObjectiveAndChangeEnum.createListFirst
    var keeptrackEditField:ClassEditKeepTrack = ClassEditKeepTrack()
    
    fileprivate var listCropType:[CCropType] = [CCropType]()
    
    fileprivate var rolePermissions: RLMResults<RLMObject>?
    fileprivate var rolePermission: RolePermissionDatabase = RolePermissionDatabase()
    fileprivate var rolePermissionNote: RolePermissionDatabase = RolePermissionDatabase()
//    private var dateIntervalDay_maxLastHarvestForTree:Double = 0
    fileprivate var numberYearForFruistTree:Int = 0
    fileprivate var yearForFruitTree: Int = 0
    fileprivate var listyieldrateunit = [String]()
    var growingMethodTitle:String = ""
    
    var previousNumber : String = ""
    var isChangeVariety: Bool = false
    var isChangeCropType: Bool = false
    
     //MARK: - PUBLIC METHOD
    func createListObjectiveSuccession(_ type: CreateListObjectiveAndChangeEnum){
        changeTypeEnum  = type
        
        listyieldrateunit = listYieldRateUnitAmount_Normal
        if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
            listyieldrateunit = listYieldRateUnitAmount_Tree
        }
        else if categoryID == CategoryEnum.berries.rawValue {
            listyieldrateunit = listBerriesUnitAmount
        }
        
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.roundingMode = NumberFormatter.RoundingMode.halfUp
        nf.maximumFractionDigits = 2
        
        if clsSuccessionDetail.id > 0 {
            typeSuccession = "Edit Successions"
            if evenSuccession == "" {
                if clsTaskDetail.id > 0 {
                    evenSuccession = "Task Detail"
                } else {
                    evenSuccession = "Succession Detail"
                }
            }
        } else {
            typeSuccession = "Create Successions"
            if evenSuccession == "" {
                evenSuccession = "Adding succession"
            }
        }
        
        if clsMasterCropSuccession.links.parentcrop.name == "Tomatoes" || clsMasterCropSuccession.links.parentcrop.name == "Rice"{
            growingMethodTitle = "Growing method"
        }
        else {
            growingMethodTitle = "Harvest stage"
        }
        
        switch (changeTypeEnum){
        case .createListFirst: // new succession
            dateIntervalSeedingDate = Utilities.convertDateTime(toTimeStamp: Utilities.resetHourMinSec(of: Date())).doubleValue
            previousPlatingMethod = SuccessionSeedingMethodType.NONE_SEEDINGMETHOD
            previousPlantingDate_Interval = 0
            // add new
            listSectionSuccession = self.createFirstData()
            listSectionSuccession.append(self.createNote())
            listSectionSuccession.append(self.createPhoto())
            
            // add crop type
            listCropType.removeAll(keepingCapacity: false)
            if clsMasterCropSuccession.links.listcroptype != nil && clsMasterCropSuccession.links.listcroptype.count >= 1 {
                for k in 0  ..< clsMasterCropSuccession.links.listcroptype.count  {
                    listCropType.append(clsMasterCropSuccession.links.listcroptype[k] as! CCropType)
                }
            }

            break
        case .createListDetail: // succession detail
            
            dateIntervalSeedingDate = clsSuccessionDetail.startdate
            previousPlatingMethod = SuccessionSeedingMethodType.NONE_SEEDINGMETHOD
            previousPlantingDate_Interval = 0
            
            //keep track
            //keeptrackEditField.oneworddescriptionEdit = self.clsSuccessionDetail.oneworddesc
            //keeptrackEditField.plantingAmountEdit = nf.stringFromNumber(NSNumber(double: clsSuccessionDetail.plantingamount) )!
            if clsTrackingSuccession != nil && clsTrackingSuccession!.inrowspacing {
                //keeptrackEditField.inrowspacingNumberEdit = nf.stringFromNumber(NSNumber(float: clsSuccessionDetail.inrowspacing))!
            }
            if clsTrackingSuccession != nil && clsTrackingSuccession!.rowperbed {
                //keeptrackEditField.rowperbedNumverEdit = nf.stringFromNumber(NSNumber(float: clsSuccessionDetail.rowperbed) )!
            }
            
            //keeptrackEditField.betweenrowspacingNumberEdit = nf.stringFromNumber(NSNumber(float: clsSuccessionDetail.rowspacinglow) )!
            
            //keeptrackEditField.flatpot_value = nf.stringFromNumber(NSNumber(double: clsSuccessionDetail.numofflats))!
            
            //keeptrackEditField.flatpot_unit = nf.stringFromNumber(NSNumber(int: Int32(clsSuccessionDetail.numofflatsunitid)))!
            
            //keeptrackEditField.totalofTransplent = nf.stringFromNumber(NSNumber(float: clsSuccessionDetail.numofplants))!
            
            if clsTrackingSuccession != nil && (clsTrackingSuccession!.seedingrate || clsTrackingSuccession!.seedingrateunitid) {
                //keeptrackEditField.inrowspacingNumberEdit = nf.stringFromNumber(NSNumber(double: clsSuccessionDetail.seedingratevalue))!
                //keeptrackEditField.seedingRateUnitEdit = listSeedingRateUnit_ful[clsSuccessionDetail.seedingratevalueunitid - 1]
            }
            
            // add crop type
            listCropType.removeAll(keepingCapacity: false)
            if clsMasterCropSuccession.links.listcroptype != nil && clsMasterCropSuccession.links.listcroptype.count >= 1 {
                for k in 0  ..< clsMasterCropSuccession.links.listcroptype.count  {
                    listCropType.append(clsMasterCropSuccession.links.listcroptype[k] as! CCropType)
                }
            }
            
            if categoryID == CategoryEnum.trees_.rawValue {
                if clsSuccessionDetail.extendProp.fieldid > 0 {
                    location = DetailClassField()
                    location!.id = clsSuccessionDetail.extendProp.fieldid
                    location!.name =  clsSuccessionDetail.extendProp.fieldname
                }
            }
            
            // edit
            listSectionSuccession = self.createDetailData()
            listSectionSuccession.append(self.createPhoto())
            fieldName = clsSuccessionDetail.extendProp.fieldname
            break
        
        case .changeVariety:
            previousPlantingDate_Interval = 0
            isChangeVariety = true
            // add crop type
            listCropType.removeAll(keepingCapacity: false)
            if clsMasterCropSuccession.links.listcroptype != nil && clsMasterCropSuccession.links.listcroptype.count >= 1 {
                for k in 0  ..< clsMasterCropSuccession.links.listcroptype.count {
                    listCropType.append(clsMasterCropSuccession.links.listcroptype[k] as! CCropType)
                }
            }
            
            // update Crop Variety
            for i in 0 ..< listSectionSuccession.count {
                let sectionforShow:SectionForRow = listSectionSuccession[i]
                switch(sectionforShow.sectionName){
                    
                case .sectionCropType:
                    self.createCrop_Variety(sectionforShow, parentcropname: self.clsMasterCropSuccession.links.parentcrop.name, imgName: self.clsMasterCropSuccession.imagename, varietyname: self.clsMasterCropSuccession.varietyname, cropTypename: self.clsMasterCropSuccession.links.croptype.name)
                    break
                
                default:
                    continue
                    
                }
            }
            self.didChangePlantingMethod_GrowingMethod()
            
            break
        case .changeVariety_FinalHarvest :
            listCropType.removeAll(keepingCapacity: false)
            if clsMasterCropSuccession.links.listcroptype != nil && clsMasterCropSuccession.links.listcroptype.count >= 1 {
                for k in 0  ..< clsMasterCropSuccession.links.listcroptype.count {
                    listCropType.append(clsMasterCropSuccession.links.listcroptype[k] as! CCropType)
                }
            }
            for i in 0  ..< listSectionSuccession.count {
                let sectionforShow:SectionForRow = listSectionSuccession[i]
                switch(sectionforShow.sectionName){
                    
                case .sectionCropType:
                    self.createCrop_Variety(sectionforShow, parentcropname: self.clsMasterCropSuccession.links.parentcrop.name, imgName: self.clsMasterCropSuccession.imagename, varietyname: self.clsMasterCropSuccession.varietyname, cropTypename: self.clsMasterCropSuccession.links.croptype.name)
                    break
                    
                default:
                    continue
                    
                }
            }
            break
        case .changeCropType:
            isChangeCropType = true
            previousPlantingDate_Interval = 0
            
            self.didChangePlantingMethod_GrowingMethod()
            break
        case .changeCropType_FinalHarvest :
            listCropType.removeAll(keepingCapacity: false)
            if clsMasterCropSuccession.links.listcroptype != nil && clsMasterCropSuccession.links.listcroptype.count >= 1 {
                for k in 0  ..< clsMasterCropSuccession.links.listcroptype.count {
                    listCropType.append(clsMasterCropSuccession.links.listcroptype[k] as! CCropType)
                }
            }
            for i in 0 ..< listSectionSuccession.count {
                let sectionforShow:SectionForRow = listSectionSuccession[i]
                switch(sectionforShow.sectionName){
                    
                case .sectionCropType:
                    self.createCrop_Variety(sectionforShow, parentcropname: self.clsMasterCropSuccession.links.parentcrop.name, imgName: self.clsMasterCropSuccession.imagename, varietyname: self.clsMasterCropSuccession.varietyname, cropTypename: self.clsMasterCropSuccession.links.croptype.name)
                    break
                    
                default:
                    continue
                    
                }
            }
            break
        case .changePlatingMethod:
            //growingmethod = self.combinationDataForShow.growingmethod
//            if self.keeptrackEditField.growingMethodEdit != "" {
//                self.growingmethod = self.keeptrackEditField.growingMethodEdit
//            }
            self.didChangePlantingMethod_GrowingMethod()
            break
        case .changeGrowingMethod:
            //growingmethod = self.combinationDataForShow.growingmethod
            self.didChangePlantingMethod_GrowingMethod()
            break
            
        case .createTaskDetail:
            fieldName = clsTaskDetail.extendProp.fieldname
            dateIntervalSeedingDate = clsTaskDetail.dateplanted
//            dateIntervalDayToTransplant = clsTaskDetail.dateplanted
            numberDayToTransplant = Double(clsSuccessionDetail.daytotransplant)
            dateIntervalDayToTransplant = clsTaskDetail.taskdate
            
            previousPlatingMethod = SuccessionSeedingMethodType.NONE_SEEDINGMETHOD
            previousPlantingDate_Interval = 0
            
            //keep track
            //keeptrackEditField.plantingAmountEdit = nf.stringFromNumber(NSNumber(double: clsSuccessionDetail.plantingamount) )!
            
            if clsTrackingSuccession != nil && clsTrackingSuccession!.inrowspacing {
                //keeptrackEditField.inrowspacingNumberEdit = nf.stringFromNumber(NSNumber(float: clsSuccessionDetail.inrowspacing))!
            }
            if clsTrackingSuccession != nil && clsTrackingSuccession!.rowperbed {
                //keeptrackEditField.rowperbedNumverEdit = nf.stringFromNumber(NSNumber(float: clsSuccessionDetail.rowperbed) )!
            }
            //keeptrackEditField.betweenrowspacingNumberEdit = nf.stringFromNumber(NSNumber(float: clsSuccessionDetail.rowspacinglow) )!
            
            if clsTrackingSuccession != nil && (clsTrackingSuccession!.seedingrate || clsTrackingSuccession!.seedingrateunitid) {
                //keeptrackEditField.inrowspacingNumberEdit = nf.stringFromNumber(NSNumber(double: clsSuccessionDetail.seedingratevalue))!
                //keeptrackEditField.seedingRateUnitEdit = listSeedingRateUnit_ful[clsSuccessionDetail.seedingratevalueunitid - 1]
            }
            
            // add crop type
            listCropType.removeAll(keepingCapacity: false)
            if clsMasterCropSuccession.links.listcroptype != nil && clsMasterCropSuccession.links.listcroptype.count >= 1 {
                
                for k in 0 ..< clsMasterCropSuccession.links.listcroptype.count {
                    listCropType.append(clsMasterCropSuccession.links.listcroptype[k] as! CCropType)
                }
            }
        
            if categoryID == CategoryEnum.trees_.rawValue {
                if clsTaskDetail.extendProp.fieldid > 0 {
                    location = DetailClassField()
                    location!.id = clsTaskDetail.extendProp.fieldid
                    location!.name =  clsTaskDetail.extendProp.fieldname
                }
                isSingleTree = !clsSuccessionDetail.grouptrees
                
                if clsSuccessionDetail.lnglat != "" {
                    let jsonData: Data = clsSuccessionDetail.lnglat.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                    let myDictionary:NSDictionary = (try! JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    
                    let json = JSON(myDictionary)
                    var lat:Double = 0, lng:Double = 0
                    if let tmp = json["lat"].double {
                        lat = tmp
                    }
                    if let tmp = json["lng"].double {
                        lng = tmp
                    }
                    treeLocation = CLLocationCoordinate2DMake(lat, lng)
                }
            }
            
            listSectionSuccession = self.createDetailData()
            
            listSectionSuccession.append(self.createHeaderTimeline())
            
            //listSectionSuccession.append(self.createTimelineForTask())
            //listSectionSuccession.append(self.createAddComment())
            
            //listSectionSuccession.append(self.createListNote())
            //listSectionSuccession.append(self.createPhoto())
            break
            
        case .changeHarvestSeason:

            self.didChangePlantingMethod_GrowingMethod()
            
            break
        default:
            
            break
        }
        
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
    }
    
    func didSelectAtIndex(_ indexPath:IndexPath){
        
        let realm = RLMRealm.default()
        rolePermissions = RolePermissionDatabase.allObjects(in: realm)
        if rolePermissions?.count > 0 {
            rolePermission = rolePermissions!.object(at: 1) as! RolePermissionDatabase
            rolePermissionNote = rolePermissions!.object(at: 3) as! RolePermissionDatabase
        }
        
        
        let section = listSectionSuccession[indexPath.section]
        var cellForShow = section.listCellForRow[indexPath.row]
        
        if cellForShow.isDontAllowSelected == true
        {
            return
        }
        
        switch(cellForShow.typeCell_AT){
        case .CELL_AT_SHOWSUCCESSION:
            switch cellForShow.typeAction_AT {
            case .ACTION_AT_SUCCESSIONTIMELINE:
                let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                self.delegate?.gotoTimelineCrop!(((dictShow.value(forKey: "successionid") as AnyObject).intValue)!, varietyName: dictShow.value(forKey: "varietyname") as! String, parentName: dictShow.value(forKey: "parentname") as! String)
                
                break
            default:
                break
            }
            break
        case .CELL_AT_SUCCESSONTIMELINE:
            switch cellForShow.typeAction_AT {
            case .ACTION_AT_SUCCESSIONTIMELINE:
                let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                self.delegate?.gotoTimelineCrop!(((dictShow.value(forKey: "successionid") as AnyObject).intValue)!, varietyName: dictShow.value(forKey: "varietyname") as! String, parentName: dictShow.value(forKey: "parentname") as! String)
                                
                break
            default:
                break
            }
            
        case .Cell_NameType_ID:
            switch(cellForShow.typeAction_AT){
                
            case .ACTION_AT_ADD_PLANTINGMETHOD:
                if self.firstharvestcompleted || self.isFinishCrop {
                    return
                }
                if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    previousPlantingDate_Interval = dateIntervalSeedingDate
                }
                else {
                    previousPlantingDate_Interval  = dateIntervalDayToTransplant
                }
                
                let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                if ((self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType) != nil) {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType!(indexPath, createObjectAndChangType:CreateListObjectiveAndChangeEnum.changePlatingMethod.rawValue, currentvalue: dictShow.value(forKey: "rightitem") as! String)
                }
                break
            case .ACTION_AT_ADD_GROWINGMETHOD:
                let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                if ((self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType) != nil) {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType!(indexPath, createObjectAndChangType:CreateListObjectiveAndChangeEnum.changeGrowingMethod.rawValue, currentvalue: dictShow.value(forKey: "rightitem") as! String)}
                break
            case .ACTION_AT_ADD_GH_STARTDATE:
                let date:Date = Utilities.date(fromTimestamp: dateIntervalSeedingDate)
                
                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    let maxdate:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: maxdate)
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted) {
                    let maxdate:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: maxdate)
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                } else {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                }
                
                break
            case .ACTION_AT_ADD_DAY_IN_GH:
                self.delegate?.ParentCrop_SuccessionControllerDelegate_updateValue!(indexPath, currentValue:(NSString(format: "%.0f", numberDayToTransplant) as String), maximumNumber: 100000)
                
                break
            case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                let date:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                
                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || self.clsTaskDetail.id > 0 {
                        let mindate:Date = Utilities.date(fromTimestamp: dateIntervalSeedingDate)
                        let maxdate:Date = Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: maxdate)
                    }
                    else {
                        let maxdate:Date = Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: maxdate)
                    }
                    
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                    
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || self.clsTaskDetail.id > 0 {
                        let mindate:Date = Utilities.date(fromTimestamp: dateIntervalSeedingDate)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
                    }
                    else {
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                    }
                    
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || self.clsTaskDetail.id > 0 {
                        let mindate:Date = Utilities.date(fromTimestamp: dateIntervalSeedingDate)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
                    }
                    else {
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                    }
                } else {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                }
                
//                if clsTaskDetail.id > 0 {
//                    if clsSuccessionDetail.id > 0 && clsSuccessionDetail.links.subtasktype.id == SuccessionSeedingMethodType.GREENHOUSE_SOW_S.rawValue{
//                        let mindate:NSDate = Utilities.dateFromTimestamp(dateIntervalSeedingDate)
//                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
//                    }
//                    else{
//                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
//                    }
//                }
//                else {
//                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
//                }
                break
            case .ACTION_AT_ADD_HARVEST_UNIT:
                if cellForShow.valueSelected_AT != "" {
                    let indexValue = Int( Utilities.idMethodCompare(cellForShow.valueSelected_AT, andList: listHarvestUnit))
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: listHarvestUnit[indexValue], listSelect: listHarvestUnit as NSArray, actionType:cellForShow.typeAction_AT)
                }
                else {
                    if listUnitParentCrop.count > 0 {
                        
                        let indexValue = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                        
                        let stringShow = listHarvestUnit[indexValue]
                        
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: stringShow, listSelect: listHarvestUnit as NSArray, actionType:cellForShow.typeAction_AT)
                    } else {
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: "", listSelect: listHarvestUnit as NSArray, actionType:cellForShow.typeAction_AT)
                    }
                    
                }
                
                break
            case .ACTION_AT_ACTIONSEEDSOURCE:
                //get name
                var listseed = [String]()
                for i in 0..<listSeedSource.count {
                    listseed.append(listSeedSource[i].name)
                }
                listseed.append("Add New Seed Source")
                
                if cellForShow.isHaveData_AT {
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: listseed as NSArray, actionType:cellForShow.typeAction_AT)
                }
                else{
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: "", listSelect: listseed as NSArray, actionType:cellForShow.typeAction_AT)
                }
                
                break
            case .ACTION_AT_ADD_SEED_COATING:
                self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: listSeedType as NSArray, actionType:cellForShow.typeAction_AT)
                
                break
            case .ACTION_AT_ADD_GROWING_CYCLE:
                if categoryID == CategoryEnum.trees_.rawValue {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: listGrowingCicle_Tree as NSArray, actionType:cellForShow.typeAction_AT)
                }
                else {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: listGrowingCicle as NSArray, actionType:cellForShow.typeAction_AT)
                }
                
                
                break
            case .ACTION_AT_ADD_HARVEST_FREQUENCY:
                let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                if dicShow.value(forKey: "valuenumber") as? String != nil {
                    let columnNumberValues = dicShow.value(forKey: "valuenumber") as! String
                    let valueunit1 = dicShow.value(forKey: "valueunit1") as! String
                    let clsObject = ClassObjectColumn(columnName1s: "Harvest frequency", columnName2s: "", placeholderColumns: "Add frequency", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listFrequency, listDropDown2s: nil, columnIndex:1)
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                }
                else {
                    let clsObject = ClassObjectColumn(columnName1s: "Harvest frequency", columnName2s: "", placeholderColumns: "Add frequency", columnNumberValues: "0", columnDropdown1s: "", columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listFrequency, listDropDown2s: nil, columnIndex:1)
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                }
                
                break
                
            case .ACTION_AT_ADD_AVG_SALE_PRICE:
                
                let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                if dicShow.value(forKey: "valuenumber") as? String != nil {
                    if dicShow.value(forKey: "valuenumber") as! String != "" && dicShow.value(forKey: "valuenumber") as! String != "0" {
                        let columnNumberValues = dicShow.value(forKey: "valuenumber") as! String
                        
                        var valueunit1 = dicShow.value(forKey: "valueunit1") as! String
                        
                        isHasUnitSuccession = true
                        
                        if valueunit1 == "" {
                            isHasUnitSuccession = false
                            if listUnitParentCrop.count > 0 {
                                isHasUnitSuccession = true
                                let indexValue = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                                
                                valueunit1 = listSalePrice_perfull[indexValue]
                                
                            }
                        } else {
                            let indexValue = Int( Utilities.idMethodCompare(valueunit1, andList: listSalePrice_per))
                            valueunit1 = listSalePrice_perfull[indexValue]
                        }
                        
                        let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    } else {
                        var salepriceindex = 8
                        isHasUnitSuccession = false
                        
                        if listUnitParentCrop.count > 0 {
                            isHasUnitSuccession = true
                            salepriceindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                        }
                        
                        let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: "0", columnDropdown1s: listSalePrice_perfull[salepriceindex], columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
                        
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    }
                }
                else {
                    var salepriceindex = 8
                    isHasUnitSuccession = false
                    
                    if listUnitParentCrop.count > 0 {
                        isHasUnitSuccession = true
                        salepriceindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                    }
                    
                    let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: "0", columnDropdown1s: listSalePrice_per[salepriceindex], columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                }
                
                
                break
            case .ACTION_AT_ADD_YIELD_RATE:
                let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                
//                var yieldrateplantingindex = 0
//                if categoryID == CategoryEnum.TREES_.rawValue || categoryID == CategoryEnum.VINES.rawValue{
//                    yieldrateplantingindex = 1
//                }
                
                if dicShow.value(forKey: "valuenumber") as? String != nil{
                    if dicShow.value(forKey: "valuenumber") as! String != "" && dicShow.value(forKey: "valuenumber") as! String != "0" {
                        let columnNumberValues = dicShow.value(forKey: "valuenumber") as! String
                        let valueunit1 = dicShow.value(forKey: "valueunit1") as! String
                        let valueunit2 = dicShow.value(forKey: "valueunit2") as! String
                        isHasUnitSuccession = true
                        if valueunit2 == "" {
                            isHasUnitSuccession = false
                        }
                        
                        let indexValue = Int( Utilities.idMethodCompare(valueunit2, andList: listYieldRate_Brief))
                        var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: listHarvestUnit[indexValue], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit, listDropDown2s: listHarvestUnit, columnIndex:1)
                        if categoryID == CategoryEnum.berries.rawValue {
                            clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: listHarvestUnit[indexValue], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount, listDropDown2s: listHarvestUnit, columnIndex:1)
                        }
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    } else {
                        var yieldrateunitrowIndex = 2
                        if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
                            yieldrateunitrowIndex = 0
                        }
                        
                        var yieldrateindex = 8
                        isHasUnitSuccession = false
                        
                        if listUnitParentCrop.count > 0 {
                            isHasUnitSuccession = true
                            yieldrateindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                        }
                        
                        var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listyieldrateunit[yieldrateunitrowIndex], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit , listDropDown2s: listHarvestUnit, columnIndex:1)
                        if categoryID == CategoryEnum.berries.rawValue {
                            clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listBerriesUnitAmount[2], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount , listDropDown2s: listHarvestUnit, columnIndex:1)
                        }
                        
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    }
                    
                }
                else {
                    var yieldrateunitrowIndex = 2
                    if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
                        yieldrateunitrowIndex = 0
                    }
                    
                    var yieldrateindex = 8
                    isHasUnitSuccession = false
                    
                    if listUnitParentCrop.count > 0 {
                        isHasUnitSuccession = true
                        yieldrateindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                    }
                    
                    var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listyieldrateunit[yieldrateunitrowIndex], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit , listDropDown2s: listHarvestUnit, columnIndex:1)
                    if categoryID == CategoryEnum.berries.rawValue {
                        clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listBerriesUnitAmount[2], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount , listDropDown2s: listHarvestUnit, columnIndex:1)
                    }
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                }
                
                break
                
            case .ACTION_AT_ADD_CROP_TYPE:
//                let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                if self.firstharvestcompleted == false && self.isFinishCrop == false {
                    let cellVariety = section.listCellForRow[indexPath.row - 1]
                    let dicShowVariety = cellVariety.dictionaryShow_AT as NSDictionary
                    if (dicShowVariety.value(forKey: "variety") as? String == "Generic Variety" || self.clsMasterCropSuccession.farmid > 0) && listCropType.count > 1 {
                        var listCropTypeString = [String]()
                        for k:Int in 0 ..< listCropType.count {
                            listCropTypeString.append(listCropType[k].name)
                        }
                        let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: dictShow.value(forKey: "rightitem") as! String , listSelect: listCropTypeString as NSArray, actionType:cellForShow.typeAction_AT)
                    }
                } else if listCropType.count > 1 {
                    var listCropTypeString = [String]()
                    for k:Int in 0 ..< listCropType.count {
                        listCropTypeString.append(listCropType[k].name)
                    }
                    let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: dictShow.value(forKey: "rightitem") as! String , listSelect: listCropTypeString as NSArray, actionType:cellForShow.typeAction_AT)
                }
                
                break
            case .ACTION_AT_ADD_ROOTSTOCK:
                self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: listRestock as NSArray, actionType:cellForShow.typeAction_AT)
                
                break
                
            case .ACTION_AT_ADD_TAKE_DOWNDATE:
                
                let date:Date = Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)
                self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant), maxDate: nil)
                
                break
                
            case .ACTION_HARVERT_SEASION:
                // listHarvestSeason
                
                if cellForShow.valueSelected_AT != "" && cellForShow.valueSelected_AT != "0" {
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType!(indexPath, createObjectAndChangType: CreateListObjectiveAndChangeEnum.changeHarvestSeason.rawValue, currentvalue: listHarvestSeason[cellForShow.valueSelected_AT.integerValue])
                }
                else{
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType!(indexPath, createObjectAndChangType: CreateListObjectiveAndChangeEnum.changeHarvestSeason.rawValue, currentvalue: listHarvestSeason[0])
                }
                break
            case .ACTION_AT_ADD_PLANTINGDATE:
                
                let date:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                }
                else {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest))
                }
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST_TREE:
                
                let date:Date = Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)
                
                self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: Utilities.date(fromTimestamp: dateIntervalSeedingDate), maxDate: Utilities.date(fromTimestamp: dateIntervalDayToLastHarvest))
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST_CELL:
                
                let date:Date = Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)

                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    let mindate:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                    let mindate:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    let mindate:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
                } else {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                }
                
                break
            case .ACTION_AT_ADD_LAST_HARVEST_TREE:
                
                let date:Date = Utilities.date(fromTimestamp: dateIntervalDayToLastHarvest)
                
//                self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: Utilities.dateFromTimestamp(dateIntervalDayToFirstHarvest), maxDate: Utilities.dateFromTimestamp(Utilities.setLastDateFrom_datetime_ForTree(Int32(yearForFruitTree))) )
                self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest), maxDate: nil )
                
                break
            case .ACTION_AT_ADD_FIST_FRUIT_HARVEST:
                
                let yearharvest = Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToTransplant)
                var arrayString = [String]()
                for i:Int32 in 0 ..< 200 {
                    let firstharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearharvest + i, currentDate:dateIntervalDayToFirstHarvest)
                    if firstharvestcheck >= dateIntervalDayToTransplant {
                        arrayString.append("\(yearharvest + i)")
                    }
                }
                self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: arrayString as NSArray, actionType:cellForShow.typeAction_AT)
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST:
                stringMaturity = "Maturity"
                self.delegate?.ParentCrop_SuccessionControllerDelegate_updateValue!(indexPath, currentValue:(NSString(format: "%.0f", numberDayToFirstHarvest) as String), maximumNumber: 10000)
                
                break
            default:
                break
            }
            
            break
        case .Cell_NameType_TwoRight:
            switch(cellForShow.typeAction_AT){
                
            case .ACTION_AT_ADD_DAY_IN_GH:
                self.delegate?.ParentCrop_SuccessionControllerDelegate_updateValue!(indexPath, currentValue:(NSString(format: "%.0f", numberDayToTransplant) as String), maximumNumber: 100000)
                
                break
            case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                let date:Date = Utilities.date(fromTimestamp: dateIntervalDayToTransplant)
                
                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || self.clsTaskDetail.id > 0 {
                        let mindate:Date = Utilities.date(fromTimestamp: dateIntervalSeedingDate)
                        let maxdate:Date = Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: maxdate)
                    }
                    else {
                        let maxdate:Date = Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: maxdate)
                    }
                    
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S)  {
                    
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || self.clsTaskDetail.id > 0 {
                        let mindate:Date = Utilities.date(fromTimestamp: dateIntervalSeedingDate)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
                    }
                    else {
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                    }
                    
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || self.clsTaskDetail.id > 0 {
                        let mindate:Date = Utilities.date(fromTimestamp: dateIntervalSeedingDate)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: mindate, maxDate: nil)
                    }
                    else {
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                    }
                } else {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDatePicker!(indexPath, date: date, minDate: nil, maxDate: nil)
                }
                
                break
            
            case .ACTION_AT_ADD_LAST_HARVEST:
                self.delegate?.ParentCrop_SuccessionControllerDelegate_updateValue!(indexPath, currentValue:(NSString(format: "%.0f", numberDayToLastHarvest) as String), maximumNumber: 1000)
                
                break
            case .ACTION_AT_ADD_AVG_SALE_PRICE:
                
//                let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
//                if dicShow.valueForKey("valuenumber") as? String != nil {
//                    
//                    let columnNumberValues = dicShow.valueForKey("valuenumber") as! String
//                    let valueunit1 = dicShow.valueForKey("valueunit1") as! String
//                    let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
//                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
//                }
//                else {
//                    
//                    var salepriceindex = 0
//                    if categoryID == CategoryEnum.HERBS.rawValue || categoryID == CategoryEnum.FLOWERS.rawValue {
//                        salepriceindex = 2
//                    }
//                    
//                    let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: "0", columnDropdown1s: listSalePrice_perfull[salepriceindex], columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
//                    
//                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
//                }
                let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                if dicShow.value(forKey: "valuenumber") as? String != nil {
                    if dicShow.value(forKey: "valuenumber") as! String != "" && dicShow.value(forKey: "valuenumber") as! String != "0" {
                        let columnNumberValues = dicShow.value(forKey: "valuenumber") as! String
                        
                        var valueunit1 = dicShow.value(forKey: "valueunit1") as! String
                        
                        isHasUnitSuccession = true
                        
                        if valueunit1 == "" {
                            isHasUnitSuccession = false
                            if listUnitParentCrop.count > 0 {
                                isHasUnitSuccession = true
                                let indexValue = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                                
                                valueunit1 = listSalePrice_perfull[indexValue]
                                
                            }
                        } else {
                            let indexValue = Int( Utilities.idMethodCompare(valueunit1, andList: listSalePrice_per))
                            valueunit1 = listSalePrice_perfull[indexValue]
                        }
                        
                        let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    } else {
                        var salepriceindex = 8
                        isHasUnitSuccession = false
                        
                        if listUnitParentCrop.count > 0 {
                            isHasUnitSuccession = true
                            salepriceindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                        }
                        
                        let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: "0", columnDropdown1s: listSalePrice_perfull[salepriceindex], columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
                        
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    }
                }
                else {
                    var salepriceindex = 8
                    isHasUnitSuccession = false
                    
                    if listUnitParentCrop.count > 0 {
                        isHasUnitSuccession = true
                        salepriceindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                    }
                    
                    let clsObject = ClassObjectColumn(columnName1s: "Avg. sales price", columnName2s: "", placeholderColumns: "Add avg. sales price", columnNumberValues: "0", columnDropdown1s: listSalePrice_per[salepriceindex], columnDropdown2s: "", haveNumberDropdowns: 1, listDropDown1s:listSalePrice_perfull, listDropDown2s: nil, columnIndex:1)
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                }

                
                break
            case .ACTION_AT_ADD_GROWING_CYCLE:
                if categoryID == CategoryEnum.trees_.rawValue {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: listGrowingCicle_Tree as NSArray, actionType:cellForShow.typeAction_AT)
                }
                else {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_ShowPickerSelection!(indexPath, valueSelect: cellForShow.valueSelected_AT, listSelect: listGrowingCicle as NSArray, actionType:cellForShow.typeAction_AT)
                }
                
                
                break
            case .ACTION_AT_ADD_YIELD_RATE:
                let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                
                
//                if dicShow.valueForKey("valueunit2") as? String != "" {
//                    let columnNumberValues = dicShow.valueForKey("valuenumber") as! String
//                    let valueunit1 = dicShow.valueForKey("valueunit1") as! String
//                    let valueunit2 = dicShow.valueForKey("valueunit2") as! String
//                    
//                    let indexValue = Int( Utilities.idMethodCompare(valueunit2, andList: listYieldRate_Brief))
//                    
//                    var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: listHarvestUnit[indexValue], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit, listDropDown2s: listHarvestUnit, columnIndex:1)
//                    if categoryID == CategoryEnum.BERRIES.rawValue {
//                        clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: listHarvestUnit[indexValue], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount, listDropDown2s: listHarvestUnit, columnIndex:1)
//                    }
//                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
//                    
//                }
//                else {
//                    
//                    var yieldrateunitrowIndex = 2
//                    if categoryID == CategoryEnum.TREES_.rawValue || categoryID == CategoryEnum.VINES.rawValue{
//                        yieldrateunitrowIndex = 0
//                    }
//                    
//                    var yieldrateindex = 0
//                    if categoryID == CategoryEnum.HERBS.rawValue || categoryID == CategoryEnum.FLOWERS.rawValue {
//                        yieldrateindex = 0
//                    }
//                
//                    var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listyieldrateunit[yieldrateunitrowIndex], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit , listDropDown2s: listHarvestUnit, columnIndex:1)
//                    if categoryID == CategoryEnum.BERRIES.rawValue {
//                        clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listBerriesUnitAmount[0], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount, listDropDown2s: listHarvestUnit, columnIndex:1)
//                    }
//                    
//                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
//                }
                if dicShow.value(forKey: "valuenumber") as? String != nil{
                    if dicShow.value(forKey: "valuenumber") as! String != "" && dicShow.value(forKey: "valuenumber") as! String != "0" {
                        let columnNumberValues = dicShow.value(forKey: "valuenumber") as! String
                        let valueunit1 = dicShow.value(forKey: "valueunit1") as! String
                        let valueunit2 = dicShow.value(forKey: "valueunit2") as! String
                        isHasUnitSuccession = true
                        if valueunit2 == "" {
                            isHasUnitSuccession = false
                        }
                        
                        let indexValue = Int( Utilities.idMethodCompare(valueunit2, andList: listYieldRate_Brief))
                        var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: listHarvestUnit[indexValue], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit, listDropDown2s: listHarvestUnit, columnIndex:1)
                        if categoryID == CategoryEnum.berries.rawValue {
                            clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: columnNumberValues, columnDropdown1s: valueunit1, columnDropdown2s: listHarvestUnit[indexValue], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount, listDropDown2s: listHarvestUnit, columnIndex:1)
                        }
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    } else {
                        var yieldrateunitrowIndex = 2
                        if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
                            yieldrateunitrowIndex = 0
                        }
                        
                        var yieldrateindex = 8
                        isHasUnitSuccession = false
                        
                        if listUnitParentCrop.count > 0 {
                            isHasUnitSuccession = true
                            yieldrateindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                        }
                        
                        var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listyieldrateunit[yieldrateunitrowIndex], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit , listDropDown2s: listHarvestUnit, columnIndex:1)
                        if categoryID == CategoryEnum.berries.rawValue {
                            clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listBerriesUnitAmount[2], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount , listDropDown2s: listHarvestUnit, columnIndex:1)
                        }
                        
                        self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                    }
                    
                }
                else {
                    var yieldrateunitrowIndex = 2
                    if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
                        yieldrateunitrowIndex = 0
                    }
                    
                    var yieldrateindex = 8
                    isHasUnitSuccession = false
                    
                    if listUnitParentCrop.count > 0 {
                        isHasUnitSuccession = true
                        yieldrateindex = Int( Utilities.idMethodCompare(listUnitParentCrop[listUnitParentCrop.count - 1] as? String, andList: listarvest_Unit_Single))
                    }
                    
                    var clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listyieldrateunit[yieldrateunitrowIndex], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listyieldrateunit , listDropDown2s: listHarvestUnit, columnIndex:1)
                    if categoryID == CategoryEnum.berries.rawValue {
                        clsObject = ClassObjectColumn(columnName1s: "Avg. yield rate", columnName2s: "", placeholderColumns: "Add yield rate", columnNumberValues: "0", columnDropdown1s: listBerriesUnitAmount[2], columnDropdown2s: listHarvestUnit[yieldrateindex], haveNumberDropdowns: 2, listDropDown1s:listBerriesUnitAmount , listDropDown2s: listHarvestUnit, columnIndex:1)
                    }
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_showDropdownColumn!(indexPath, clsColumn: clsObject)
                }

                
                break
                
            default:
                break
            }
            break
        case .CELL_AT_CROP_VARIETY:
            self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType!(indexPath, createObjectAndChangType: CreateListObjectiveAndChangeEnum.changeVariety.rawValue, currentvalue: "")
            
            break
        case .CELL_AT_HIDEMORE:
            if cellForShow.groupShowHide == GROUPSHOWHIDE.HARVESTINFORMATION{
                if cellForShow.isShowCell == false {
                    section.listCellForRow.remove(at: indexPath.row)
                    
                    
                    for i:Int in 0 ..< section.listCellForRow.count {
                        let cellshow = section.listCellForRow[i]
                        if cellshow.groupShowHide == GROUPSHOWHIDE.HARVESTINFORMATION && cellshow.typeCell_AT != .CELL_AT_HIDEMORE {
                            cellshow.isHiddenGroup = false
                        }
                        
                    }
                    
                    cellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_HIDEMORE
                    let dictShow = NSDictionary(objects: ["","Hide additional crop details"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    cellForShow.groupShowHide = GROUPSHOWHIDE.HARVESTINFORMATION
                    cellForShow.isShowCell = true
                    section.listCellForRow.append(cellForShow)

                }
                else {
                    section.listCellForRow.remove(at: indexPath.row)
                    
                    
                    for i:Int in 0 ..< section.listCellForRow.count {
                        let cellshow = section.listCellForRow[i]
                        if cellshow.groupShowHide == GROUPSHOWHIDE.HARVESTINFORMATION && cellshow.typeCell_AT != .CELL_AT_HIDEMORE {
                            cellshow.isHiddenGroup = true
                        }
                    }
                    
                    cellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_HIDEMORE
                    let dictShow = NSDictionary(objects: ["","Show additional crop details"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    cellForShow.groupShowHide = GROUPSHOWHIDE.HARVESTINFORMATION
                    section.listCellForRow.insert(cellForShow, at: 3)
                }
                
                
                self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
            }
            break
        case .CELL_AT_ADD_NOTE_ITEM:
            self.delegate?.ParentCrop_SuccessionControllerDelegate_editNote!(indexPath, noteid: cellForShow.note!.id)
            break
        case .CELL_AT_SHOWLOCATION:
            if categoryID == CategoryEnum.trees_.rawValue {
                if location != nil
                {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_linkLocation_Orchard!(indexPath, lng: treeLocation.longitude, lat: treeLocation.latitude, orchardId: location!.id)
                }
                else
                {
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_linkLocation_Orchard!(indexPath, lng: treeLocation.longitude, lat: treeLocation.latitude, orchardId: 0)
                }
            }
            else {
                var plantingamount:Int = 0
                var plantingunit = ""
                var rowperbed:Int = 0
                (plantingamount,plantingunit, rowperbed) = self.getPlantingAmount()
//                self.delegate?.ParentCrop_SuccessionControllerDelegate_linkLocation!(indexPath, fromDate: dateIntervalDayToTransplant, toDate: dateIntervalDayToLastHarvest, plantingAmount : self.getPlantingAmount())
                self.delegate?.ParentCrop_SuccessionControllerDelegate_linkLocation!(indexPath, fromDate: dateIntervalDayToTransplant, toDate: dateIntervalDayToLastHarvest, plantingAmount : plantingamount, plantingUnit:plantingunit, rowperbed:rowperbed)
            }
            break
        case .CELL_AT_GoogleMapLOCATION:
            self.delegate?.ParentCrop_SuccessionControllerDelegate_editGoogleMap!(indexPath, currentPoint: CLLocationCoordinate2DMake(treeLocation.latitude, treeLocation.longitude))
            break
        case .CellCompletePlatingTask_ID:
            if rolePermission.completefunc {
                self.isSelectItem = true
                let typeDate : String = cellForShow.dictionaryShow_AT.value(forKey: "typeDate") as! String
                if(typeDate.integerValue == 2 || typeDate.integerValue  == 3)
                {
                    let dic : NSDictionary = NSDictionary(objects: ["1"], forKeys: ["typeDate" as NSCopying])
                    cellForShow.dictionaryShow_AT = dic
                }
                else
                {
                    let date : Date = Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor)
                    if((date as NSDate).isToday() || (date as NSDate).isInFuture())
                    {
                        let dic : NSDictionary = NSDictionary(objects: ["3"], forKeys: ["typeDate" as NSCopying])
                        cellForShow.dictionaryShow_AT = dic
                    }
                    else
                    {
                        let dic : NSDictionary = NSDictionary(objects: ["2"], forKeys: ["typeDate" as NSCopying])
                        cellForShow.dictionaryShow_AT = dic
                    }
                }
                self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
            } else {
                
            }
            break
        default:
            break
        }
    }
    fileprivate func didChangePlantingMethod_GrowingMethod(){
        
        
        
        // reload all field when change planting method
        // remove section some section
        var i: Int = 0
        while i < listSectionSuccession.count {
            
            print("i: \(i), count: \(listSectionSuccession.count)")
            
            let sectionforShow:SectionForRow = listSectionSuccession[i]
            switch(sectionforShow.sectionName){
                
            case .sectionPlantingAmount:
                listSectionSuccession.remove(at: i)
                i -= 1
                break
            case .sectionPlantingMethod:
                listSectionSuccession.remove(at: i)
                i -= 1
                break
            case .sectionFlat_Pot:
                listSectionSuccession.remove(at: i)
                i -= 1
                break
            case .sectionHarvest:
                listSectionSuccession.remove(at: i)
                i -= 1
                break
            default:
                i+=1
                continue
                
            }
        }
        //check index location
        var indexInsert = 3
        for i:Int in 0 ..< listSectionSuccession.count {
            let sectionforShow:SectionForRow = listSectionSuccession[i]
            switch(sectionforShow.sectionName){
                
            case .sectionLocation:
                indexInsert = i + 1
                break
                
            default:
                continue
                
            }
        }
        
        
        
        // add some section
        
        // section haverst information
        var sectionforShow = SectionForRow()
        self.createHarvestInformation(sectionforShow)
        listSectionSuccession.insert(sectionforShow, at: indexInsert)
        
        var isCalculateTranslate = false
        
        //section greenhouse
        if ( categoryID == CategoryEnum.vegetables.rawValue || categoryID == CategoryEnum.flowers.rawValue || categoryID == CategoryEnum.herbs.rawValue || categoryID == CategoryEnum.berries.rawValue ) {
            if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                sectionforShow = SectionForRow()
                self.createGH_Flat(sectionforShow)
                listSectionSuccession.insert(sectionforShow, at: indexInsert)
                isCalculateTranslate = true
            } else if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S {
                sectionforShow = SectionForRow()
                self.createTPP_Flat(sectionforShow)
                listSectionSuccession.insert(sectionforShow, at: indexInsert)
                isCalculateTranslate = true
            }
        }
        
        //section planting
        sectionforShow = SectionForRow()
        self.createGroup_Planting_GetDate()

        if combinationDataForShow.growingmethod != "" && changeTypeEnum != .changeGrowingMethod {
            growingmethod = combinationDataForShow.growingmethod
        }
        
        self.createGroup_Planting_Growing(sectionforShow, growingMethod: growingmethod)
        self.createGroup_Planting_Growing_Child(sectionforShow, isDetail: false)
        listSectionSuccession.insert(sectionforShow, at: indexInsert)
        
        //section three column

        if categoryID != CategoryEnum.trees_.rawValue || isSingleTree == false {
            if clsTaskDetail.id == 0 || successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                sectionforShow = SectionForRow()
                if clsSuccessionDetail.id > 0 {
                    self.createThreeColumn_Planting_Detail(sectionforShow)
                } else {
                    self.createThreeColumn_Planting(sectionforShow)
                }
                listSectionSuccession.insert(sectionforShow, at: indexInsert)
            }
        }
        
        self.updateChangeDate_AtCellName(listSectionSuccession)
        
        if isCalculateTranslate{
            self.updateTranslate()
        }
        
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
        
    }
    
    fileprivate func updateTranslate(){
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.roundingMode = NumberFormatter.RoundingMode.halfUp
        nf.maximumFractionDigits = 2
        var rowperbed:Double = 0
        var plantingamount:Double = 0
        var inrowspacing:Double = 0
        for m in 0..<listSectionSuccession.count{
            let sections = listSectionSuccession[m]
            for n in 0..<sections.listCellForRow.count{
                let cellFor = sections.listCellForRow[n]
                if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN {
                    let dictShow = cellFor.dictionaryShow_AT as NSDictionary
                    let column11 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                    if column11.columnName1 == "Planting amount" {
                        plantingamount = column11.columnNumberValue.doubleValue
                        
                        let column21 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                        inrowspacing = column21.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                        
                        let column31 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                        rowperbed = column31.columnNumberValue.doubleValue
                    }
                    else {
                        break
                    }
                }
                else if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN_FLAT{
                    let dictShow = cellFor.dictionaryShow_AT as NSDictionary
                    let  column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                    let column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                    let column3 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                    
                    
                    if inrowspacing == 0 {
                        keeptrackEditField.numberOfPlant = 0
                    }
                    else  {
                        keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column3.columnNumberValue.doubleValue / 100 )) / inrowspacing
                        
                    }
                    
                    let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                    if flatObj == 0 {
                        keeptrackEditField.numberOfFlat = 0
                    }
                    else {
                        keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                    }
                    cellFor.dictionaryShow_AT = NSDictionary(objects: [ column1, column2, column3, nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "column3" as NSCopying,"numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                }
                else if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_TWOCOLUMN_FLAT{
                    let dictShow = cellFor.dictionaryShow_AT as NSDictionary
                    let  column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                    let column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                    
                    
                    if inrowspacing == 0 {
                        keeptrackEditField.numberOfPlant = 0
                    }
                    else  {
                        keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column2.columnNumberValue.doubleValue / 100 )) / inrowspacing
                        
                    }
                    
                    let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                    if flatObj == 0 {
                        keeptrackEditField.numberOfFlat = 0
                    }
                    else {
                        keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                    }
                    cellFor.dictionaryShow_AT = NSDictionary(objects: [ column1, column2,  nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                    
                }
            }
        }
    }
    
    func cancelConvertRate(_ indexPath:IndexPath, oldText:String, oldUnit: String)
    {
        
        
        let section = listSectionSuccession[indexPath.section]
        let cellForShow = section.listCellForRow[indexPath.row]
        let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
        
        switch(cellForShow.typeAction_AT){
            
        case .ACTION_AT_ADD_HARVEST_UNIT:
            let dictShow = NSDictionary(objects: ["Harvest unit", "\(oldText)"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
            cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
            if oldText == "Select unit" {
                cellForShow.valueSelected_AT = ""
            }
            else {
                cellForShow.valueSelected_AT = oldText
            }
            keeptrackEditField.harvestUnitEdit = cellForShow.valueSelected_AT
            break
        case .ACTION_AT_ADD_YIELD_RATE:
            
            let valueunit1 = dictShow.value(forKey: "valueunit1") as! String
            
            let dictShow = NSDictionary(objects: ["Avg. yield rate", "\(oldText)", "\(self.previousNumber)", "\(valueunit1)", "\(oldUnit)"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying, "valueunit2" as NSCopying])
            keeptrackEditField.avgYieldRateEditValue = "\(oldText)"
            keeptrackEditField.avgYieldRateEditValueNumber = "\(self.previousNumber)"
            keeptrackEditField.avgYieldRateEditUnit1 = "\(valueunit1)"
            keeptrackEditField.avgYieldRateEditUnit2 = "\(oldUnit)"
            cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
            
            break
        case .ACTION_AT_ADD_AVG_SALE_PRICE:
            
            let dictShow = NSDictionary(objects: ["Avg. sales price", "\(oldText)", "\(self.previousNumber)", "\(oldUnit)"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying])
            
            keeptrackEditField.avgSalesPriceEditValue = "\(oldText)"
            keeptrackEditField.avgSalesPriceEditValueNumber = "\(self.previousNumber)"
            keeptrackEditField.avgSalesPriceEditUnit = "\(oldUnit)"
            
            cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
            
            break
        default:
            break
        }
        
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)

    }
    
    func checkExistUnit() -> Bool
    {
        var isExistUnit = false
        
        // update Crop Variety
        for i in 0 ..< listSectionSuccession.count {
            let sectionforShow:SectionForRow = listSectionSuccession[i]
            switch(sectionforShow.sectionName){
                
            case .sectionHarvest:
                for j in 0 ..< sectionforShow.listCellForRow.count {
                    let cellForShow = sectionforShow.listCellForRow[j]
                    switch(cellForShow.typeAction_AT){
                        
                    case .ACTION_AT_ADD_HARVEST_UNIT:
                        if cellForShow.valueSelected_AT != "" {
                            isExistUnit = true
                        }
                        break
                    case .ACTION_AT_ADD_AVG_SALE_PRICE:
                        
                        let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                        if dicShow.value(forKey: "valuenumber") as? String != nil {
                            
                            let columnNumberValues = dicShow.value(forKey: "valuenumber") as! String
                            if columnNumberValues.doubleValue > 0 {
                                isExistUnit = true
                            }
                            
                        }
                        
                        break
                    case .ACTION_AT_ADD_YIELD_RATE:
                        let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                        if dicShow.value(forKey: "valueunit2") as? String != ""{
                            let columnNumberValues = dicShow.value(forKey: "valuenumber") as! String
                            if columnNumberValues.doubleValue > 0 {
                                isExistUnit = true
                            }
                            
                        }
                        break
                    default:
                        break
                    }
                }
                
                break
                
            default:
                continue
                
            }
        }
        return isExistUnit
    }
    
    func getExistFromUnit() -> String
    {
        var unitFrom : String = ""
        for i in 0 ..< listSectionSuccession.count
        {
            let sectionforShow = listSectionSuccession [i]
            for j in 0 ..< sectionforShow.listCellForRow.count
            {
                let cellForShow = sectionforShow.listCellForRow[j]
                if (cellForShow.typeAction_AT == .ACTION_AT_ADD_HARVEST_UNIT && cellForShow.dictionaryShow_AT.value(forKey: "rightitem") != nil && cellForShow.dictionaryShow_AT.value(forKey: "rightitem") as! String != "Select unit")
                {
                    let index_unit = Int( Utilities.idMethodCompare(cellForShow.dictionaryShow_AT.value(forKey: "rightitem") as? String, andList: listHarvestUnit))
                    unitFrom = listHarvest_Unit_Plural[index_unit]
                    break
                }
                else if (cellForShow.typeAction_AT == .ACTION_AT_ADD_YIELD_RATE)
                {
                    let rightItem : String = "rightitem"
//                    if(cellForShow.typeCell_AT == AddTaskTypeCell.Cell_NameType_TwoRight)
//                    {
//                        rightItem = "rightitem1"
//                        
//                    }
                    if(cellForShow.dictionaryShow_AT.value(forKey: rightItem) != nil && cellForShow.dictionaryShow_AT.value(forKey: rightItem) as! String != "Add yield rate")
                    {
                        let index_unit = Int( Utilities.idMethodCompare(cellForShow.dictionaryShow_AT.value(forKey: "valueunit2") as? String, andList: listYieldRate_Brief))
                        unitFrom = listHarvest_Unit_Plural[index_unit]
                        break
                    }
                    
                }
                else if(cellForShow.typeAction_AT == .ACTION_AT_ADD_AVG_SALE_PRICE)
                {
                    let rightItem : String = "rightitem"
//                    if(cellForShow.typeCell_AT == AddTaskTypeCell.Cell_NameType_TwoRight)
//                    {
//                        rightItem = "rightitem1"
//                        
//                    }
                    if(cellForShow.dictionaryShow_AT.value(forKey: rightItem) != nil && cellForShow.dictionaryShow_AT.value(forKey: rightItem) as! String != "Add avg. sales price")
                    {
                        let index_unit = Int( Utilities.idMethodCompare(cellForShow.dictionaryShow_AT.value(forKey: "valueunit1") as? String, andList: listSalePrice_per))
                        unitFrom = listHarvest_Unit_Plural[index_unit]
                        break
                    }
                    
                }
            }
            if(unitFrom != "")
            {
                break
            }
        }
        return unitFrom
    }
    
    func updateDataForFlat(_ indexPath:IndexPath, valueChange:String, fieldname:String){
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.roundingMode = NumberFormatter.RoundingMode.halfUp
        nf.maximumFractionDigits = 2
        
        isSelectItem = true
        let section = listSectionSuccession[indexPath.section]
        let cellForShow = section.listCellForRow[indexPath.row]
        let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
        let column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
        let column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
        
        var  numofflat = dictShow.object(forKey: "numberofflat") as! String
        var  numberofplant = dictShow.object(forKey: "numberofplant") as! String
        
        if cellForShow.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN_FLAT {
            let column3 = dictShow.object(forKey: "column3") as! ClassObjectColumn
            if fieldname == "numberofflat" {
                keeptrackEditField.numberOfFlat = valueChange.doubleValue
                numofflat = nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!
        
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                if flatObj != 0 {
                    keeptrackEditField.numberOfPlant = Double(flatObj) * keeptrackEditField.numberOfFlat
                    if keeptrackEditField.numberOfPlant > 99999.99{
                        keeptrackEditField.numberOfPlant = 99999.99
                    }
                    numberofplant = nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!
                }
                
                cellForShow.dictionaryShow_AT = NSDictionary(objects: [ column1, column2, column3, numofflat, numberofplant], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "column3" as NSCopying,"numberofflat" as NSCopying,"numberofplant" as NSCopying ])
            }
            else {
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                keeptrackEditField.numberOfPlant = valueChange.doubleValue
                numberofplant = nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!
                
                if flatObj != 0 {
                    keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant / Double(flatObj)
                    numofflat = nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!
                }
                
                
                cellForShow.dictionaryShow_AT = NSDictionary(objects: [ column1, column2, column3, numofflat, numberofplant], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "column3" as NSCopying,"numberofflat" as NSCopying,"numberofplant" as NSCopying ])
            }
        }
        else if cellForShow.typeCell_AT == AddTaskTypeCell.CELL_AT_TWOCOLUMN_FLAT{
            if fieldname == "numberofflat" {
                keeptrackEditField.numberOfFlat = valueChange.doubleValue
                numofflat = nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!
                
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                if flatObj != 0 {
                    keeptrackEditField.numberOfPlant = Double(flatObj) * keeptrackEditField.numberOfFlat
                    if keeptrackEditField.numberOfPlant > 99999.99{
                        keeptrackEditField.numberOfPlant = 99999.99
                    }
                    numberofplant = nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!
                    
                }
                
                cellForShow.dictionaryShow_AT = NSDictionary(objects: [ column1, column2, numofflat, numberofplant], forKeys: [ "column1" as NSCopying, "column2" as NSCopying, "numberofflat" as NSCopying, "numberofplant" as NSCopying ])
            }
            else {
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                keeptrackEditField.numberOfPlant = valueChange.doubleValue
                numberofplant = nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!
                
                if flatObj != 0 {
                    keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant / Double(flatObj)
                    numofflat = nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!
                }
                
                
                cellForShow.dictionaryShow_AT = NSDictionary(objects: [ column1, column2, numofflat, numberofplant], forKeys: [ "column1" as NSCopying, "column2" as NSCopying, "numberofflat" as NSCopying, "numberofplant" as NSCopying ])
            }
        }
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
    }
    
    func setValueData(_ indexPath:IndexPath, valueString:String, dicData:NSDictionary?){
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.roundingMode = NumberFormatter.RoundingMode.halfUp
        nf.maximumFractionDigits = 2
        
        isSelectItem = true
        let section = listSectionSuccession[indexPath.section]
        let cellForShow = section.listCellForRow[indexPath.row]
        
        switch(cellForShow.typeCell_AT){
            
        case .Cell_NameType_ID:
            switch(cellForShow.typeAction_AT){
            case .ACTION_AT_ADD_GH_STARTDATE:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                break
            case .ACTION_AT_ADD_DAY_IN_GH:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                break
            case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_HARVEST_UNIT:
                isEditedFields = true
                let index_unit_new = Int( Utilities.idMethodCompare(valueString, andList: listHarvestUnit))
                var oldUnit : String = ""
                if(cellForShow.valueSelected_AT != "")
                {
                    let index_unit_old = Int( Utilities.idMethodCompare(cellForShow.dictionaryShow_AT.value(forKey: "rightitem") as? String, andList: listHarvestUnit))
                    oldUnit = Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_old])
                }
                let toUnit = Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_new])
                
                self.delegate?.ParentCrop_SuccessionControllerDelegate_updateConvertRate!(indexPath, fromunit:oldUnit, tounit: toUnit, currentNumber:"0", isHarvestUnit: true)
                
                cellForShow.valueSelected_AT = valueString
                
                let dictShow = NSDictionary(objects: ["Harvest unit", "\(listHarvestUnit[index_unit_new])"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                keeptrackEditField.harvestUnitEdit = listHarvestUnit[index_unit_new]
                
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                break
            case .ACTION_AT_ACTIONSEEDSOURCE:
                isEditedFields = true
                cellForShow.isHaveData_AT = true
                cellForShow.valueSelected_AT = valueString
                let dictShow = NSDictionary(objects: ["Seed source", cellForShow.valueSelected_AT], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                keeptrackEditField.seedSourceEdit = cellForShow.valueSelected_AT
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                break
            case .ACTION_AT_ADD_SEED_COATING:
                isEditedFields = true
                cellForShow.valueSelected_AT = valueString
                let dictShow = NSDictionary(objects: ["Seed coating", cellForShow.valueSelected_AT], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                keeptrackEditField.seedCoatingEdit = cellForShow.valueSelected_AT
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                
                break
            case .ACTION_AT_ADD_GROWING_CYCLE:
                isEditedFields = true
                cellForShow.valueSelected_AT = valueString
                var dictShow = NSDictionary(objects: ["Growing cycle", cellForShow.valueSelected_AT], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                if valueString == "Biennial" {
                    cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
                    dictShow = NSDictionary(objects: ["Growing cycle", cellForShow.valueSelected_AT, "Finishes in \(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToLastHarvest) + 1)"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                } else {
                    cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_ID
                }
                
                keeptrackEditField.growingCycleEdit = cellForShow.valueSelected_AT
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                
                break
            case .ACTION_AT_ADD_HARVEST_FREQUENCY:
                isEditedFields = true
                let nf = NumberFormatter()
                nf.numberStyle = NumberFormatter.Style.decimal
                nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                nf.maximumFractionDigits = 2
                
                let column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                let doubleValue = column1.columnNumberValue.doubleValue
                let dictShow = NSDictionary(objects: ["Harvest frequency", "\(nf.string(from: NSNumber(value: doubleValue as Double))!)\(column1.columnDropdown1)",column1.columnNumberValue,"\(column1.columnDropdown1)"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying])
                keeptrackEditField.harvestFrequencyEditValue = "\(nf.string(from: NSNumber(value: doubleValue as Double))!)\(column1.columnDropdown1)"
                keeptrackEditField.harvestFrequencyEditValueNumber = column1.columnNumberValue
                keeptrackEditField.harvestFrequencyEditUnit = "\(column1.columnDropdown1)"
                
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                
                break
            case .ACTION_AT_ADD_AVG_SALE_PRICE:
                isEditedFields = true
                let nf = NumberFormatter()
                nf.numberStyle = NumberFormatter.Style.decimal
                nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                nf.maximumFractionDigits = 2
                
                let dicShow_old = cellForShow.dictionaryShow_AT as NSDictionary
                var unit_old = dicShow_old.value(forKey: "valueunit1") as! String
                self.previousNumber = dicShow_old.value(forKey: "valuenumber") as! String
                
                let column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                
                let doubleValue = column1.columnNumberValue.floatValue
                var value = "Add avg. sales price"
                
                let index_unit_new = Int( Utilities.idMethodCompare(column1.columnDropdown1, andList: listSalePrice_per))
                if(doubleValue > 0 && column1.columnDropdown1 != "")
                {
                    let index_unit_old = Int( Utilities.idMethodCompare(unit_old, andList: listSalePrice_per))
                    
                    unit_old = (unit_old != "") ? Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_old])  : ""
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_updateConvertRate!(indexPath, fromunit: unit_old, tounit: Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_new]), currentNumber:column1.columnNumberValue, isHarvestUnit:false)
                
                    value = "$\(nf.string(from: NSNumber(value: doubleValue as Float))!) \(listSalePrice_per[index_unit_new])"
                }
  
                var dictShow = NSDictionary(objects: ["Avg. sales price",value ,column1.columnNumberValue,"\(listSalePrice_per[index_unit_new])"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying])
                
                if column1.columnDropdown1 == "" {
                    dictShow = NSDictionary(objects: ["Avg. sales price",value ,column1.columnNumberValue,""], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying])
                }
                
                keeptrackEditField.avgSalesPriceEditValue = value
                keeptrackEditField.avgSalesPriceEditValueNumber = column1.columnNumberValue
                keeptrackEditField.avgSalesPriceEditUnit = "\(listSalePrice_per[index_unit_new])"
            
                if doubleValue > 0  &&
                    doubleValue == self.combinationDataForShow.saleprice &&
                    self.combinationDataForShow.salepriceunit != nil &&
                    keeptrackEditField.avgSalesPriceEditUnit != "" {
                        if Utilities.idMethodCompare(self.combinationDataForShow.salepriceunit, andList: listarvest_Unit_Single ) == Utilities.idMethodCompare(keeptrackEditField.avgSalesPriceEditUnit, andList: listSalePrice_per) {
                            dictShow = NSDictionary(objects: ["Avg. sales price", keeptrackEditField.avgSalesPriceEditValue,"Estimated figure", keeptrackEditField.avgSalesPriceEditValueNumber, keeptrackEditField.avgSalesPriceEditUnit], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying, "valuenumber" as NSCopying, "valueunit1" as NSCopying])
                            
                            cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
                        }
                    
                }
                
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                break
                
            case .ACTION_AT_ADD_YIELD_RATE:
                isEditedFields = true
                let nf = NumberFormatter()
                nf.numberStyle = NumberFormatter.Style.decimal
                nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                nf.maximumFractionDigits = 3
                
                let dicShow_old = cellForShow.dictionaryShow_AT as NSDictionary
                var unit_old = dicShow_old.value(forKey: "valueunit2") as! String
                self.previousNumber = dicShow_old.value(forKey: "valuenumber") as! String
                
                let column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                let doubleValue = column1.columnNumberValue.floatValue

                var value : String = "Add yield rate"
                let index_unit_new = Int( Utilities.idMethodCompare(column1.columnDropdown2, andList: listHarvestUnit))
                if(doubleValue > 0 && column1.columnDropdown2 != "")
                {
                    
                    let index_unit_old = Int( Utilities.idMethodCompare(unit_old, andList: listYieldRate_Brief))
                    
                    unit_old = (unit_old != "") ? Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_old])  : ""
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_updateConvertRate!(indexPath, fromunit: unit_old, tounit: Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_new]), currentNumber:column1.columnNumberValue, isHarvestUnit:false)
                    
                    value = "\(nf.string(from: NSNumber(value: doubleValue as Float))!) \(listYieldRate_Brief[index_unit_new])/\(column1.columnDropdown1)"
                }

                var dictShow = NSDictionary(objects: ["Avg. yield rate", value,column1.columnNumberValue,column1.columnDropdown1, "\(listYieldRate_Brief[index_unit_new])"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying, "valueunit2" as NSCopying])
                
                if column1.columnDropdown2 == "" {
                    dictShow = NSDictionary(objects: ["Avg. yield rate", value,column1.columnNumberValue,column1.columnDropdown1, ""], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying, "valueunit2" as NSCopying])
                }
                
                keeptrackEditField.avgYieldRateEditValue = value
                keeptrackEditField.avgYieldRateEditValueNumber = column1.columnNumberValue
                keeptrackEditField.avgYieldRateEditUnit1 = column1.columnDropdown1
                keeptrackEditField.avgYieldRateEditUnit2 = "\(listYieldRate_Brief[index_unit_new])"
                
                
                if doubleValue > 0 &&
                    doubleValue == self.combinationDataForShow.firstyieldrate &&
                    keeptrackEditField.avgYieldRateEditUnit1 == self.combinationDataForShow.firstyieldrateunitplanting &&
                    self.combinationDataForShow.firstyieldrateunit != nil &&
                    keeptrackEditField.avgYieldRateEditUnit2 != ""
                {
                    
                    if  Utilities.idMethodCompare(self.combinationDataForShow.firstyieldrateunit, andList: listarvest_Unit_Single ) == Utilities.idMethodCompare(keeptrackEditField.avgYieldRateEditUnit2, andList: listYieldRate_Brief){
                        dictShow = NSDictionary(objects: ["Avg. yield rate", keeptrackEditField.avgYieldRateEditValue, "Estimated figure", keeptrackEditField.avgYieldRateEditValueNumber, keeptrackEditField.avgYieldRateEditUnit1, keeptrackEditField.avgYieldRateEditUnit2], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying, "valueunit2" as NSCopying])
                        cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
                    }
                    
                }
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                
                break
            case .ACTION_AT_ADD_CROP_TYPE:
                // reload combidation data

                stringCropTypeEdit = valueString
                if self.listCropType.count > 1 {
                    
                    for k in 0 ..< self.listCropType.count {
                        if self.listCropType[k].name == valueString {
                            let croptype:CCropType = self.listCropType[k] as CCropType
                            if ((self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType) != nil) {
                                self.delegate?.ParentCrop_SuccessionControllerDelegate_ChangeSuccessionType!(indexPath, createObjectAndChangType:CreateListObjectiveAndChangeEnum.changeCropType.rawValue, currentvalue: "\(croptype.id)")
                                break
                            }
                        }
                        
                    }
                    cellForShow.valueSelected_AT = valueString
                    let dictShow = NSDictionary(objects: ["Crop type", cellForShow.valueSelected_AT], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                }
                
                break
            case .ACTION_AT_ADD_ROOTSTOCK:
                isEditedFields = true
                cellForShow.valueSelected_AT = valueString
                let dictShow = NSDictionary(objects: ["Rootstock", valueString], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                keeptrackEditField.rootstockEdit = valueString
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                cellForShow.isHaveData_AT = true
                break
            case .ACTION_HARVERT_SEASION:
                isEditedFields = true
                let indexValue = Int( Utilities.idMethodCompare(valueString, andList: listHarvestSeason))
                cellForShow.valueSelected_AT = "\(indexValue)"
                let dictShow = NSDictionary(objects: ["Harvest Season", valueString], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                keeptrackEditField.harvestSeasonEdit = valueString
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                
                // 
                break
                
            case .ACTION_AT_ADD_PLANTINGDATE:
                isEditedFields = true
                
                
                if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
                    
                    
                    dateIntervalSeedingDate = valueString.doubleValue
                    dateIntervalDayToTransplant = dateIntervalSeedingDate
                    dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                    
                    if dateIntervalDayToFirstHarvest < dateIntervalDayToTransplant {
                        var  yearUpdate = Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToTransplant)
                        
                        var i:Int32 = 1
                        var datefirstharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToFirstHarvest)
                        
                        while datefirstharvestcheck < dateIntervalDayToTransplant {
                            i += 1
                            datefirstharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToFirstHarvest)
                        }
                        dateIntervalDayToFirstHarvest = datefirstharvestcheck
                        keeptrackEditField.dateIntervalDayToFirstHarvestEdit = datefirstharvestcheck
                        
                        yearForFruitTree = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest))
                        keeptrackEditField.numberYearForFruistTreeEdit = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest))
                        
                        if dateIntervalDayToLastHarvest < dateIntervalDayToFirstHarvest{
                            yearUpdate = Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest)
                            i = 1
                            var datelastharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToLastHarvest)
                            
                            while datelastharvestcheck < dateIntervalDayToFirstHarvest {
                                i += 1
                                datelastharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToLastHarvest)
                            }
                            dateIntervalDayToLastHarvest = datelastharvestcheck
                            keeptrackEditField.dateIntervalDayToLastHarvestEdit = datelastharvestcheck
                        }
                    }
                    
                    
                }
                else {
                    dateIntervalSeedingDate = valueString.doubleValue
                    dateIntervalDayToTransplant = dateIntervalSeedingDate
                    dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                }
                
                if clsTaskDetail.id != 0 {
                    
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.maximumFractionDigits = 2
                    let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
                    var totalLabor = "Labor Time"
                    if (self.totalTime != 0) {
                        totalLabor = stringTemp! + "h"
                    }
                    
                    var stringDate = ""
                    if dateIntervalDatePlantingLabor == 0 {
                        stringDate = "Due Date"
                    } else {
                        stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
                    }
                    var assignee = "Unassigned";
                    var stringImage = "";
                    if (listUserAssigned.count > 0) {
                        if (listUserAssigned.count == 1) {
                            assignee = listUserAssigned[0].firstname;
                            stringImage = listUserAssigned[0].linkPhotoName;
                        } else {
                            assignee = "Multiple";
                        }
                    }
                    
                    self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
                }
                
                keeptrackEditField.dateIntervalSeedingDateEdit = dateIntervalSeedingDate
                keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                self.updateChangeDateLayout(section)
                self.updateChangeDate_AtCellUserInformation(listSectionSuccession)
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST:
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
                
            case .ACTION_AT_ADD_LAST_HARVEST:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST_TREE:
                isEditedFields = true
                dateIntervalDayToFirstHarvest = valueString.doubleValue
                keeptrackEditField.dateIntervalDayToFirstHarvestTree = dateIntervalDayToFirstHarvest
                
//                dateIntervalDayToLastHarvest = Utilities.setDateTimeFrom_2_datetime_ForTree(dateIntervalDayToFirstHarvest, datetime2: dateIntervalDayToLastHarvest)
                
                let yearUpdate = Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest)
                
                if yearUpdate != Int32(yearForFruitTree) {
                    yearForFruitTree = Int(yearUpdate)
                    keeptrackEditField.numberYearForFruistTreeEdit = Int(yearUpdate)
                }

                numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                numberDayToLastHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest), andDate: Utilities.date(fromTimestamp: dateIntervalDayToLastHarvest)))
                
//                dateIntervalDay_maxLastHarvestForTree = Utilities.setLastDateOfYearFrom_datetime_ForTree(dateIntervalDayToLastHarvest)
                
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST_CELL:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_LAST_HARVEST_TREE:
                isEditedFields = true
                dateIntervalDayToLastHarvest = valueString.doubleValue
                keeptrackEditField.dateIntervalDayToLastHarvestTree = dateIntervalDayToLastHarvest
                
                numberDayToLastHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest), andDate: Utilities.date(fromTimestamp: dateIntervalDayToLastHarvest)))
                
                let yearUpdate = Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest)
                
                if yearUpdate != Int32(yearForFruitTree) {
                    yearForFruitTree = Int(yearUpdate)
                    keeptrackEditField.numberYearForFruistTreeEdit = Int(yearUpdate)
                }
                
//                dateIntervalDay_maxLastHarvestForTree = Utilities.setLastDateOfYearFrom_datetime_ForTree(dateIntervalDayToLastHarvest)
                
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_TAKE_DOWNDATE:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                
                self.updateChangeDateLayout(section)
                break
                
              case .ACTION_AT_ADD_GROWINGMETHOD:
                isEditedFields = true
                growingmethod = valueString
                
                let dictShow = NSDictionary(objects: [growingMethodTitle, growingmethod], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                
                break
            case .ACTION_AT_ADD_FIST_FRUIT_HARVEST:
                if valueString.integerValue != yearForFruitTree {
                    isEditedFields = true
                    let yearPlanting = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToTransplant))
                    numberYearForFruistTree = valueString.integerValue - yearPlanting
                    yearForFruitTree = valueString.integerValue
                    
                    keeptrackEditField.numberYearForFruistTreeEdit = valueString.integerValue
                    
                    //AQuoc do it
                    
//                    dateIntervalDayToLastHarvest = Utilities.setYearForDateFrom_datetime_ForTree(Int32(yearForFruitTree), currentDate:dateIntervalDayToLastHarvest)
//                    
//                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                    
                    dateIntervalDayToFirstHarvest = Utilities.setYearForDateFrom_datetime_ForTree(Int32(yearForFruitTree), currentDate:dateIntervalDayToFirstHarvest)
                    
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    
//                    if dateIntervalDayToLastHarvest < dateIntervalSeedingDate {
//                        dateIntervalDayToLastHarvest = Utilities.setLastDateFrom_datetime_ForTree(Int32(yearForFruitTree))
//                        keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
//                    }
                    if dateIntervalDayToFirstHarvest < dateIntervalSeedingDate{
                        dateIntervalDayToFirstHarvest = dateIntervalDayToLastHarvest
                        keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    }
                    if dateIntervalDayToFirstHarvest > dateIntervalDayToLastHarvest {
                        var i = 1
                        var datelastharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(Int32(yearForFruitTree + i), currentDate:dateIntervalDayToLastHarvest)
                        
                        while dateIntervalDayToFirstHarvest > datelastharvestcheck {
                            i += 1
                            datelastharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(Int32(yearForFruitTree + i), currentDate:dateIntervalDayToLastHarvest)
                        }
                        dateIntervalDayToLastHarvest = datelastharvestcheck
                        keeptrackEditField.dateIntervalDayToLastHarvestEdit = datelastharvestcheck
                    }
                    
                    //                if yearForFruitTree == yearPlanting {
                    //                    // do nothing
                    //                } else {
                    //
                    //                }
                    
                    self.updateChangeDateLayout(section)
                    
                    let dictShow = NSDictionary(objects: ["First fruiting year", valueString], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    cellForShow.valueSelected_AT = valueString
                }
                
                break
                
            default:
                break
            }
            
            break
        case .Cell_NameType_TwoRight:
            switch(cellForShow.typeAction_AT){
                
            case .ACTION_AT_ADD_DAY_IN_GH:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST:
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_LAST_HARVEST:
                isEditedFields = true
                self.updateChangeDateValue(cellForShow.typeCell_AT, typeAction_AT: cellForShow.typeAction_AT, valueChange: valueString.doubleValue)
                self.updateChangeDateLayout(section)
                
                break
            case .ACTION_AT_ADD_AVG_SALE_PRICE:
//                isEditedFields = true
//                cellForShow.typeCell_AT = .Cell_NameType_ID
//                let nf = NSNumberFormatter()
//                nf.numberStyle = NSNumberFormatterStyle.DecimalStyle
//                nf.roundingMode = NSNumberFormatterRoundingMode.RoundHalfEven
//                nf.maximumFractionDigits = 2
//                
//                let dicShow_old = cellForShow.dictionaryShow_AT as NSDictionary
//                var unit_old = dicShow_old.valueForKey("valueunit1") as! String
//                self.previousNumber = dicShow_old.valueForKey("valuenumber") as! String
//                
//                
//                let column1 = dicData?.objectForKey("column1") as! ClassObjectColumn
//
//                let doubleValue = column1.columnNumberValue.floatValue
//                var value = "Add avg. sales price"
//                
//                let index_unit_new = Int( Utilities.idMethodCompare(column1.columnDropdown1, andList: listSalePrice_per))
//                if(doubleValue > 0)
//                {
//                    let index_unit_old = Int( Utilities.idMethodCompare(unit_old, andList: listSalePrice_per))
//                    
//                    unit_old = (unit_old != "") ? Utilities.returnFormatSingularUnitForClient(listHarvest_Unit_Plural[index_unit_old])  : ""
//                    
//                    self.delegate?.ParentCrop_SuccessionControllerDelegate_updateConvertRate!(indexPath, fromunit: unit_old, tounit: Utilities.returnFormatSingularUnitForClient(listHarvest_Unit_Plural[index_unit_new]), currentNumber:column1.columnNumberValue, isHarvestUnit:false)
//
//                    value = "$\(nf.stringFromNumber(NSNumber(float: doubleValue))!) \(listSalePrice_per[index_unit_new])"
//                }
//
//                var dictShow = NSDictionary(objects: ["Avg. sales price", value,column1.columnNumberValue,"\(listSalePrice_per[index_unit_new])"], forKeys: [ "leftitem", "rightitem","valuenumber", "valueunit1"])
//                keeptrackEditField.avgSalesPriceEditValue = value
//                keeptrackEditField.avgSalesPriceEditValueNumber = column1.columnNumberValue
//                keeptrackEditField.avgSalesPriceEditUnit = "\(listSalePrice_per[index_unit_new])"
//                
//                if doubleValue > 0  &&
//                    doubleValue == self.combinationDataForShow.saleprice &&
//                    self.combinationDataForShow.salepriceunit != nil &&
//                    keeptrackEditField.avgSalesPriceEditUnit != "" {
//                    if Utilities.idMethodCompare(self.combinationDataForShow.salepriceunit, andList: listarvest_Unit_Single ) == Utilities.idMethodCompare(keeptrackEditField.avgSalesPriceEditUnit, andList: listSalePrice_per) {
//                        dictShow = NSDictionary(objects: ["Avg. sales price", keeptrackEditField.avgSalesPriceEditValue,"Estimated Figure", keeptrackEditField.avgSalesPriceEditValueNumber, keeptrackEditField.avgSalesPriceEditUnit], forKeys: [ "leftitem", "rightitem1", "rightitem2", "valuenumber", "valueunit1"])
//                        
//                        cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
//                    }
//                    
//                }
//                
//                
//                cellForShow.dictionaryShow_AT = dictShow as [NSObject : AnyObject]
                
                cellForShow.typeCell_AT = .Cell_NameType_ID
                isEditedFields = true
                let nf = NumberFormatter()
                nf.numberStyle = NumberFormatter.Style.decimal
                nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                nf.maximumFractionDigits = 2
                
                let dicShow_old = cellForShow.dictionaryShow_AT as NSDictionary
                var unit_old = dicShow_old.value(forKey: "valueunit1") as! String
                self.previousNumber = dicShow_old.value(forKey: "valuenumber") as! String
                
                let column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                
                let doubleValue = column1.columnNumberValue.floatValue
                var value = "Add avg. sales price"
                
                let index_unit_new = Int( Utilities.idMethodCompare(column1.columnDropdown1, andList: listSalePrice_per))
                if(doubleValue > 0 && column1.columnDropdown1 != "")
                {
                    let index_unit_old = Int( Utilities.idMethodCompare(unit_old, andList: listSalePrice_per))
                    
                    unit_old = (unit_old != "") ? Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_old])  : ""
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_updateConvertRate!(indexPath, fromunit: unit_old, tounit: Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_new]), currentNumber:column1.columnNumberValue, isHarvestUnit:false)
                    
                    value = "$\(nf.string(from: NSNumber(value: doubleValue as Float))!) \(listSalePrice_per[index_unit_new])"
                }
                
                var dictShow = NSDictionary(objects: ["Avg. sales price",value ,column1.columnNumberValue,"\(listSalePrice_per[index_unit_new])"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying])
                
                if column1.columnDropdown1 == "" {
                    dictShow = NSDictionary(objects: ["Avg. sales price",value ,column1.columnNumberValue,""], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying])
                }
                
                keeptrackEditField.avgSalesPriceEditValue = value
                keeptrackEditField.avgSalesPriceEditValueNumber = column1.columnNumberValue
                keeptrackEditField.avgSalesPriceEditUnit = "\(listSalePrice_per[index_unit_new])"
                
                if doubleValue > 0  &&
                    doubleValue == self.combinationDataForShow.saleprice &&
                    self.combinationDataForShow.salepriceunit != nil &&
                    keeptrackEditField.avgSalesPriceEditUnit != "" {
                    if Utilities.idMethodCompare(self.combinationDataForShow.salepriceunit, andList: listarvest_Unit_Single ) == Utilities.idMethodCompare(keeptrackEditField.avgSalesPriceEditUnit, andList: listSalePrice_per) {
                        dictShow = NSDictionary(objects: ["Avg. sales price", keeptrackEditField.avgSalesPriceEditValue,"Estimated figure", keeptrackEditField.avgSalesPriceEditValueNumber, keeptrackEditField.avgSalesPriceEditUnit], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying, "valuenumber" as NSCopying, "valueunit1" as NSCopying])
                        
                        cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
                    }
                    
                }
                
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                break
            case .ACTION_AT_ADD_GROWING_CYCLE:
                isEditedFields = true
                cellForShow.valueSelected_AT = valueString
                var dictShow = NSDictionary(objects: ["Growing cycle", cellForShow.valueSelected_AT], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                if valueString == "Biennial" {
                    cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
                    dictShow = NSDictionary(objects: ["Growing cycle", cellForShow.valueSelected_AT, "Finishes in \(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToLastHarvest) + 1)"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                } else {
                    cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_ID
                }
                
                keeptrackEditField.growingCycleEdit = cellForShow.valueSelected_AT
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                
                break
            case .ACTION_AT_ADD_YIELD_RATE:
//                isEditedFields = true
//                cellForShow.typeCell_AT = .Cell_NameType_ID
//                let nf = NSNumberFormatter()
//                nf.numberStyle = NSNumberFormatterStyle.DecimalStyle
//                nf.roundingMode = NSNumberFormatterRoundingMode.RoundHalfEven
//                nf.maximumFractionDigits = 3
//                
//                let dicShow_old = cellForShow.dictionaryShow_AT as NSDictionary
//                var unit_old = dicShow_old.valueForKey("valueunit2") as! String
//                self.previousNumber = dicShow_old.valueForKey("valuenumber") as! String
//                
//                let column1 = dicData?.objectForKey("column1") as! ClassObjectColumn
//                
//                let doubleValue = column1.columnNumberValue.floatValue
//                
//                var value : String = "Add yield rate"
//                let index_unit_new = Int( Utilities.idMethodCompare(column1.columnDropdown2, andList: listHarvestUnit))
//                if(doubleValue > 0)
//                {
//                    let index_unit_old = Int( Utilities.idMethodCompare(unit_old, andList: listYieldRate_Brief))
//
//                    unit_old = (unit_old != "") ? Utilities.returnFormatSingularUnitForClient(listHarvest_Unit_Plural[index_unit_old])  : ""
//                    
//                    self.delegate?.ParentCrop_SuccessionControllerDelegate_updateConvertRate!(indexPath, fromunit: unit_old, tounit: Utilities.returnFormatSingularUnitForClient(listHarvest_Unit_Plural[index_unit_new]), currentNumber:column1.columnNumberValue, isHarvestUnit:false)
//                    value = "\(nf.stringFromNumber(NSNumber(float: doubleValue))!) \(listYieldRate_Brief[index_unit_new])/\(column1.columnDropdown1)"
//                    
//                }
//                
//                var dictShow = NSDictionary(objects: ["Avg. yield rate", value,column1.columnNumberValue,column1.columnDropdown1, "\(listYieldRate_Brief[index_unit_new])"], forKeys: [ "leftitem", "rightitem","valuenumber", "valueunit1", "valueunit2"])
//                keeptrackEditField.avgYieldRateEditValue = value
//                keeptrackEditField.avgYieldRateEditValueNumber = column1.columnNumberValue
//                keeptrackEditField.avgYieldRateEditUnit1 = column1.columnDropdown1
//                keeptrackEditField.avgYieldRateEditUnit2 = "\(listYieldRate_Brief[index_unit_new])"
//                if doubleValue > 0 &&
//                    doubleValue == self.combinationDataForShow.firstyieldrate &&
//                    keeptrackEditField.avgYieldRateEditUnit1 == self.combinationDataForShow.firstyieldrateunitplanting &&
//                    self.combinationDataForShow.firstyieldrateunit != nil &&
//                    keeptrackEditField.avgYieldRateEditUnit2 != ""
//                {
//                    
//                    if  Utilities.idMethodCompare(self.combinationDataForShow.firstyieldrateunit, andList: listarvest_Unit_Single ) == Utilities.idMethodCompare(keeptrackEditField.avgYieldRateEditUnit2, andList: listYieldRate_Brief){
//                        dictShow = NSDictionary(objects: ["Avg. yield rate", keeptrackEditField.avgYieldRateEditValue, "Estimated Figure", keeptrackEditField.avgYieldRateEditValueNumber, keeptrackEditField.avgYieldRateEditUnit1, keeptrackEditField.avgYieldRateEditUnit2], forKeys: [ "leftitem", "rightitem1", "rightitem2","valuenumber", "valueunit1", "valueunit2"])
//                        cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
//                    }
//                    
//                }
//                
//                cellForShow.dictionaryShow_AT = dictShow as [NSObject : AnyObject]
                
                cellForShow.typeCell_AT = .Cell_NameType_ID
                isEditedFields = true
                let nf = NumberFormatter()
                nf.numberStyle = NumberFormatter.Style.decimal
                nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                nf.maximumFractionDigits = 3
                
                let dicShow_old = cellForShow.dictionaryShow_AT as NSDictionary
                var unit_old = dicShow_old.value(forKey: "valueunit2") as! String
                self.previousNumber = dicShow_old.value(forKey: "valuenumber") as! String
                
                let column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                let doubleValue = column1.columnNumberValue.floatValue
                
                var value : String = "Add yield rate"
                let index_unit_new = Int( Utilities.idMethodCompare(column1.columnDropdown2, andList: listHarvestUnit))
                if(doubleValue > 0 && column1.columnDropdown2 != "")
                {
                    
                    let index_unit_old = Int( Utilities.idMethodCompare(unit_old, andList: listYieldRate_Brief))
                    
                    unit_old = (unit_old != "") ? Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_old])  : ""
                    
                    self.delegate?.ParentCrop_SuccessionControllerDelegate_updateConvertRate!(indexPath, fromunit: unit_old, tounit: Utilities.returnFormatSingularUnit(forClient: listHarvest_Unit_Plural[index_unit_new]), currentNumber:column1.columnNumberValue, isHarvestUnit:false)
                    
                    value = "\(nf.string(from: NSNumber(value: doubleValue as Float))!) \(listYieldRate_Brief[index_unit_new])/\(column1.columnDropdown1)"
                }
                
                var dictShow = NSDictionary(objects: ["Avg. yield rate", value,column1.columnNumberValue,column1.columnDropdown1, "\(listYieldRate_Brief[index_unit_new])"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying, "valueunit2" as NSCopying])
                
                if column1.columnDropdown2 == "" {
                    dictShow = NSDictionary(objects: ["Avg. yield rate", value,column1.columnNumberValue,column1.columnDropdown1, ""], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying, "valueunit2" as NSCopying])
                }
                
                keeptrackEditField.avgYieldRateEditValue = value
                keeptrackEditField.avgYieldRateEditValueNumber = column1.columnNumberValue
                keeptrackEditField.avgYieldRateEditUnit1 = column1.columnDropdown1
                keeptrackEditField.avgYieldRateEditUnit2 = "\(listYieldRate_Brief[index_unit_new])"
                
                
                if doubleValue > 0 &&
                    doubleValue == self.combinationDataForShow.firstyieldrate &&
                    keeptrackEditField.avgYieldRateEditUnit1 == self.combinationDataForShow.firstyieldrateunitplanting &&
                    self.combinationDataForShow.firstyieldrateunit != nil &&
                    keeptrackEditField.avgYieldRateEditUnit2 != ""
                {
                    
                    if  Utilities.idMethodCompare(self.combinationDataForShow.firstyieldrateunit, andList: listarvest_Unit_Single ) == Utilities.idMethodCompare(keeptrackEditField.avgYieldRateEditUnit2, andList: listYieldRate_Brief){
                        dictShow = NSDictionary(objects: ["Avg. yield rate", keeptrackEditField.avgYieldRateEditValue, "Estimated figure", keeptrackEditField.avgYieldRateEditValueNumber, keeptrackEditField.avgYieldRateEditUnit1, keeptrackEditField.avgYieldRateEditUnit2], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying,"valuenumber" as NSCopying, "valueunit1" as NSCopying, "valueunit2" as NSCopying])
                        cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
                    }
                    
                }
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                break
            default:
                break
            }
            break
        case .CELL_AT_SWITCH_CONTROL:
            isEditedFields = true
            
            switch(cellForShow.typeAction_AT){
                
            case .ACTION_AT_ADD_ORGANIC_SEED:
                let dictShow = NSDictionary(objects: ["Organic seed?", valueString], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                keeptrackEditField.organicSeedEidt = valueString
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                cellForShow.valueSelected_AT = valueString
                cellForShow.isHaveData_AT = true
                return
            case .ACTION_AT_ADD_GROWN_IN_HOOPHOUSE:
                let dictShow = NSDictionary(objects: ["Grown in a hoophouse", valueString], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                keeptrackEditField.grownInaHoophouseEdit = valueString
                cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                cellForShow.valueSelected_AT = valueString
                cellForShow.isHaveData_AT = true
                return
            default:
                break
            }
            break
        case .CELL_AT_ADDDESCRIPTIONONTASK:
            
            cellForShow.valueSelected_AT = valueString
            keeptrackEditField.noteInTaskEdit = valueString
            cellForShow.isHaveData_AT = true
            return
        case .CELL_AT_NOTE:
            cellForShow.valueSelected_AT = valueString
            cellForShow.isHaveData_AT = true
            return
        case .CELL_AT_THREECOLUMN_FLAT:
            let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
            var column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
            var column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
            var column3 = dictShow.object(forKey: "column3") as! ClassObjectColumn
            
            // dictShow = NSDictionary(objects: [ column1, column2, column3, numofflat, numberofplant], forKeys: [ "column1","column2", "column3","numberofflat","numberofplant" ])
            var  numofflat = dictShow.object(forKey: "numberofflat") as! String
            var  numberofplant = dictShow.object(forKey: "numberofplant") as! String
            
            if valueString == "1" {
                column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                keeptrackEditField.flatpot_unit = column1.columnDropdown1
                
                // calculate numofplant and then update numofflat
                var rowperbed:Double = 0
                var plantingamount:Double = 0
                var inrowspacing:Double = 0
                for m in 0..<listSectionSuccession.count{
                    let sections = listSectionSuccession[m]
                    for n in 0..<sections.listCellForRow.count{
                        let cellFor = sections.listCellForRow[n]
                        if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN {
                            let dictShow = cellFor.dictionaryShow_AT as NSDictionary
                            let column11 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                            if column11.columnName1 == "Planting amount" {
                                plantingamount = column11.columnNumberValue.doubleValue
                                
                                let column21 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                                inrowspacing = column21.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                                
                                let column31 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                                rowperbed = column31.columnNumberValue.doubleValue
                            }
                            else {
                                break
                            }
                        }
                    }
                }
                var  total:Double = 0
                if inrowspacing == 0 {
                    numberofplant = "0"
                }
                else  {
                    total = ((rowperbed * plantingamount * 12)*(1 + column3.columnNumberValue.doubleValue / 100 )) / inrowspacing
                    if total > 99999.99 {
                        total = 99999.99
                    }
                    numberofplant = nf.string(from: NSNumber(value: total as Double))!
                }
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                if flatObj == 0 {
                    numofflat = "0"
                }
                else {
                    numofflat = nf.string(from: NSNumber(value: total/Double(flatObj) as Double))!
                }
            }
            else if valueString == "2"{
                let newcolumnPlantingAmount = dicData?.object(forKey: "column2") as! ClassObjectColumn
                if(newcolumnPlantingAmount.columnNumberValue.integerValue > 1)
                {
                    newcolumnPlantingAmount.columnDropdown1 = "seeds"
                    keeptrackEditField.seedPerCell_value = newcolumnPlantingAmount.columnNumberValue
                    keeptrackEditField.seedPerCell_unit = newcolumnPlantingAmount.columnDropdown1
                }
                else
                {
                    newcolumnPlantingAmount.columnDropdown1 = "seed"
                    keeptrackEditField.seedPerCell_value = newcolumnPlantingAmount.columnNumberValue
                    keeptrackEditField.seedPerCell_unit = newcolumnPlantingAmount.columnDropdown1
                }
                column2 = newcolumnPlantingAmount
            }
            else if valueString == "3"{
                column3 = dicData?.object(forKey: "column3") as! ClassObjectColumn
                keeptrackEditField.estimatedloss = column3.columnNumberValue.doubleValue
                // calculate numofplant and then update numofflat
                var rowperbed:Double = 0
                var plantingamount:Double = 0
                var inrowspacing:Double = 0
                for m in 0..<listSectionSuccession.count{
                    let sections = listSectionSuccession[m]
                    for n in 0..<sections.listCellForRow.count{
                        let cellFor = sections.listCellForRow[n]
                        if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN {
                            let dictShow = cellFor.dictionaryShow_AT as NSDictionary
                            let column11 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                            if column11.columnName1 == "Planting amount" {
                                plantingamount = column11.columnNumberValue.doubleValue
                                
                                let column21 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                                inrowspacing = column21.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                                
                                let column31 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                                rowperbed = column31.columnNumberValue.doubleValue
                            }
                            else {
                                break
                            }
                        }
                    }
                }
                var  total:Double = 0
                if inrowspacing == 0 {
                    numberofplant = "0"
                }
                else  {
                    total = ((rowperbed * plantingamount * 12)*(1 + column3.columnNumberValue.doubleValue / 100 )) / inrowspacing
                    if total > 99999.99 {
                        total = 99999.99
                    }
                    numberofplant = nf.string(from: NSNumber(value: total as Double))!
                }
                
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                if flatObj == 0 {
                    numofflat = "0"
                }
                else {
                    numofflat = nf.string(from: NSNumber(value: total/Double(flatObj) as Double))!
                }
            }
            
            keeptrackEditField.numberOfFlat = numofflat.replace(",", withString: "").doubleValue
            keeptrackEditField.numberOfPlant = numberofplant.replace(",", withString: "").doubleValue
            
            cellForShow.dictionaryShow_AT = NSDictionary(objects: [ column1, column2, column3, numofflat, numberofplant], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "column3" as NSCopying,"numberofflat" as NSCopying,"numberofplant" as NSCopying ])
            break
        case .CELL_AT_THREECOLUMN:
            isEditedFields = true
            let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
            var column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
            
            //check user change planting amount
            if (column1.columnName1 == "Planting amount" && valueString == "1")
            {
                let newcolumnPlantingAmount = dicData?.object(forKey: "column1") as! ClassObjectColumn
                if (newcolumnPlantingAmount.columnNumberValue != column1.columnNumberValue && column1.columnDropdown1 != "trees" )
                {
                    if self.location != nil {
                        // reset selected bed
                        for i in 0..<self.location!.links.locations.count
                        {
                            let block : DetailClassField = self.location!.links.locations[i]
                            if (block.locationtype == "BLOCK")
                            {
                                block.totalSelectedBedBlock = 0
                                for j in 0..<block.links.locations.count
                                {
                                    block.links.locations[j].extendProp.selectedbedfeet = 0
                                }
                            }
                            else if (block.locationtype == "BED")
                            {
                                block.extendProp.selectedbedfeet = 0
                            }
                        }
                        
                        // update planting amount
                        selectedBedList  = [SelectedBed]()
                        for i:Int in 0 ..< listSectionSuccession.count
                        {
                            let sectionforShow = listSectionSuccession [i]
                            for j in 0 ..< sectionforShow.listCellForRow.count
                            {
                                let cellForShow = sectionforShow.listCellForRow[j]
                                if (cellForShow.typeCell_AT == .CELL_AT_SHOWLOCATION)
                                {
                                    let name : String = self.location!.name + " - no beds selected"
                                    let dictShow = NSDictionary(objects: [name], forKeys: ["leftitem" as NSCopying])
                                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                                    
                                    isNoBedSelected = true
                                    
                                    self.location!.links.locations = [DetailClassField]()
                                }
                            }
                        }

                    
                    }
                    else if selectedBedList.count > 0 {
                        selectedBedList  = [SelectedBed]()
                        for i in 0  ..< listSectionSuccession.count
                        {
                            let sectionforShow = listSectionSuccession [i]
                            for j in 0  ..< sectionforShow.listCellForRow.count
                            {
                                let cellForShow = sectionforShow.listCellForRow[j]
                                if (cellForShow.typeCell_AT == .CELL_AT_SHOWLOCATION)
                                {
                                    let name : String = fieldName + " - no beds selected"
                                    let dictShow = NSDictionary(objects: [name], forKeys: ["leftitem" as NSCopying])
                                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                                    
                                    isNoBedSelected = true
                                    if(self.location != nil)
                                    {
                                        self.location!.links.locations = [DetailClassField]()
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
                
                let column3 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                if column3.columnName1 == "Rows per bed" {
                    if column3.columnNumberValue.floatValue > 0 {
                        if keeptrackEditField.plantingAmountEdit != "" {
                            if keeptrackEditField.plantingAmountUnitEdit != "" && keeptrackEditField.plantingAmountUnitEdit != "plants" {
                                if newcolumnPlantingAmount.columnDropdown1 != keeptrackEditField.plantingAmountUnitEdit {
                                    if newcolumnPlantingAmount.columnDropdown1 == "row ft." {
                                        newcolumnPlantingAmount.columnNumberValue = "\(newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue * column3.columnNumberValue.doubleValue)"
                                        nf.maximumFractionDigits = 0
                                        newcolumnPlantingAmount.columnNumberValue = nf.string(from: NSNumber(value: newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue as Double))!
                                    } else if newcolumnPlantingAmount.columnDropdown1 == "bed ft." {
                                        newcolumnPlantingAmount.columnNumberValue = "\(newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue / column3.columnNumberValue.doubleValue)"
                                        nf.maximumFractionDigits = 0
                                        newcolumnPlantingAmount.columnNumberValue = nf.string(from: NSNumber(value: newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue as Double))!
                                    }
                                }
                            }
                        } else {
                            if keeptrackEditField.plantingAmountUnitEdit != "" && keeptrackEditField.plantingAmountUnitEdit != "plants" {
                                if newcolumnPlantingAmount.columnDropdown1 != keeptrackEditField.plantingAmountUnitEdit {
                                    if newcolumnPlantingAmount.columnDropdown1 == "row ft." {
                                        newcolumnPlantingAmount.columnNumberValue = "\(newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue * column3.columnNumberValue.doubleValue)"
                                        nf.maximumFractionDigits = 0
                                        newcolumnPlantingAmount.columnNumberValue = nf.string(from: NSNumber(value: newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue as Double))!
                                    } else if newcolumnPlantingAmount.columnDropdown1 == "bed ft." {
                                        newcolumnPlantingAmount.columnNumberValue = "\(newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue / column3.columnNumberValue.doubleValue)"
                                        nf.maximumFractionDigits = 0
                                        newcolumnPlantingAmount.columnNumberValue = nf.string(from: NSNumber(value: newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue as Double))!
                                    }
                                }
                            }
                        }
                    }
                }
                
                keeptrackEditField.plantingAmountEdit = newcolumnPlantingAmount.columnNumberValue
                keeptrackEditField.plantingAmountUnitEdit = newcolumnPlantingAmount.columnDropdown1
            }
            
            
            var column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
            var column3 = dictShow.object(forKey: "column3") as! ClassObjectColumn
            if valueString == "1" {
                column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                if column1.columnName1 == "Planting amount"{
                    // calculate numofplant and then update numofflat
                    let  rowperbed:Double = column3.columnNumberValue.doubleValue
                    var plantingamount:Double = column1.columnNumberValue.doubleValue
                    if column1.columnDropdown1 == "row ft." && column3.columnNumberValue.doubleValue > 0 {
                        plantingamount = column1.columnNumberValue.replace(",", withString: "").doubleValue / column3.columnNumberValue.doubleValue
                    }
                    else if column1.columnDropdown1 == "plants"{
                        plantingamount = (column1.columnNumberValue.replace(",", withString: "").doubleValue * column2.columnNumberValue.replace(",", withString: "").doubleValue) / 12
                    }
                    let inrowspacing:Double = column2.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                    
                    // draw data
                    for m in 0..<listSectionSuccession.count{
                        let sections = listSectionSuccession[m]
                        for n in 0..<sections.listCellForRow.count{
                            let cellFor = sections.listCellForRow[n]
                            if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN_FLAT {
                                
                                let dictShows = cellFor.dictionaryShow_AT as NSDictionary
                                let column11 = dictShows.object(forKey: "column1") as! ClassObjectColumn
                                let column22 = dictShows.object(forKey: "column2") as! ClassObjectColumn
                                let column33 = dictShows.object(forKey: "column3") as! ClassObjectColumn
                                
                                if inrowspacing == 0 {
                                    keeptrackEditField.numberOfPlant = 0
                                }
                                else  {
                                    keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column33.columnNumberValue.doubleValue / 100 )) / inrowspacing
                                    if keeptrackEditField.numberOfPlant > 99999.99 {
                                        keeptrackEditField.numberOfPlant = 99999.99
                                    }
                                }
                                
                                let flatObj = Utilities.convertTray_Pot_(to_Number: column11.columnDropdown1)
                                if flatObj == 0 {
                                    keeptrackEditField.numberOfFlat = 0
                                }
                                else {
                                    keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                                    
                                }
                                
                                nf.maximumFractionDigits = 2
                                
                                cellFor.dictionaryShow_AT = NSDictionary(objects: [ column11, column22, column33, nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "column3" as NSCopying,"numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                            }
                            else if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_TWOCOLUMN_FLAT {
                                let dictShows = cellFor.dictionaryShow_AT as NSDictionary
                                let column11 = dictShows.object(forKey: "column1") as! ClassObjectColumn
                                let column22 = dictShows.object(forKey: "column2") as! ClassObjectColumn
                                
                                if inrowspacing == 0 {
                                    keeptrackEditField.numberOfPlant = 0
                                }
                                else  {
                                    keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column22.columnNumberValue.doubleValue / 100 )) / inrowspacing
                                    if keeptrackEditField.numberOfPlant > 99999.99 {
                                        keeptrackEditField.numberOfPlant = 99999.99
                                    }
                                }
                                
                                let flatObj = Utilities.convertTray_Pot_(to_Number: column11.columnDropdown1)
                                if flatObj == 0 {
                                    keeptrackEditField.numberOfFlat = 0
                                }
                                else {
                                    keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                                    
                                }
                                
                                nf.maximumFractionDigits = 2
                                
                                cellFor.dictionaryShow_AT = NSDictionary(objects: [ column11, column22, nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                            }
                        }
                    }
                }
            }
            else if valueString == "2" {
                let newcolumnPlantingAmount = dicData?.object(forKey: "column2") as! ClassObjectColumn
                if newcolumnPlantingAmount.columnNumberValue != column2.columnNumberValue {
                    switch(newcolumnPlantingAmount.columnName1)
                    {

                    case "In-row spacing":
                        keeptrackEditField.inrowspacingNumberEdit = newcolumnPlantingAmount.columnNumberValue
                        // calculate numofplant and then update numofflat
                        let  rowperbed:Double = column3.columnNumberValue.doubleValue
                        var plantingamount:Double = column1.columnNumberValue.doubleValue
                        if column1.columnDropdown1 == "row ft." && column3.columnNumberValue.doubleValue > 0 {
                            plantingamount = column1.columnNumberValue.replace(",", withString: "").doubleValue / column3.columnNumberValue.doubleValue
                        }
                        else if column1.columnDropdown1 == "plants"{
                            plantingamount = (column1.columnNumberValue.replace(",", withString: "").doubleValue * newcolumnPlantingAmount.columnNumberValue.replace(",", withString: "").doubleValue) / 12
                        }
                        let inrowspacing:Double = newcolumnPlantingAmount.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                      
                        // draw data
                        
                        for m in 0..<listSectionSuccession.count{
                            let sections = listSectionSuccession[m]
                            for n in 0..<sections.listCellForRow.count{
                                let cellFor = sections.listCellForRow[n]
                                if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN_FLAT {
                                    let dictShows = cellFor.dictionaryShow_AT as NSDictionary
                                    let column11 = dictShows.object(forKey: "column1") as! ClassObjectColumn
                                    let column22 = dictShows.object(forKey: "column2") as! ClassObjectColumn
                                    let column33 = dictShows.object(forKey: "column3") as! ClassObjectColumn
                                    
                                    if inrowspacing == 0 {
                                        keeptrackEditField.numberOfPlant = 0
                                    }
                                    else  {
                                        keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column33.columnNumberValue.doubleValue / 100 )) / inrowspacing
                                        if keeptrackEditField.numberOfPlant > 99999.99 {
                                            keeptrackEditField.numberOfPlant = 99999.99
                                        }
                                    }
                                    
                                    let flatObj = Utilities.convertTray_Pot_(to_Number: column11.columnDropdown1)
                                    if flatObj == 0 {
                                        keeptrackEditField.numberOfFlat = 0
                                    }
                                    else {
                                        keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                                        
                                    }
                                    
                                    cellFor.dictionaryShow_AT = NSDictionary(objects: [ column11, column22, column33, nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "column3" as NSCopying,"numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                                }
                                else if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_TWOCOLUMN_FLAT {
                                    let dictShows = cellFor.dictionaryShow_AT as NSDictionary
                                    let column11 = dictShows.object(forKey: "column1") as! ClassObjectColumn
                                    let column22 = dictShows.object(forKey: "column2") as! ClassObjectColumn
                                    
                                    if inrowspacing == 0 {
                                        keeptrackEditField.numberOfPlant = 0
                                    }
                                    else  {
                                        keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column22.columnNumberValue.doubleValue / 100 )) / inrowspacing
                                        if keeptrackEditField.numberOfPlant > 99999.99 {
                                            keeptrackEditField.numberOfPlant = 99999.99
                                        }
                                    }
                                    
                                    let flatObj = Utilities.convertTray_Pot_(to_Number: column11.columnDropdown1)
                                    if flatObj == 0 {
                                        keeptrackEditField.numberOfFlat = 0
                                    }
                                    else {
                                        keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                                        
                                    }
                                    
                                    
                                    
                                    cellFor.dictionaryShow_AT = NSDictionary(objects: [ column11, column22, nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                                }
                            }
                        }
                        
                        
                        
                        break
                        
                    case "Seeding rate":
                        keeptrackEditField.inrowspacingNumberEdit = newcolumnPlantingAmount.columnNumberValue
                        keeptrackEditField.seedingRateUnitEdit = newcolumnPlantingAmount.columnDropdown1
                        if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.DIRECT_SOW_BAND_SWATH {
                            keeptrackEditField.inrowspacingNumberEdit = newcolumnPlantingAmount.columnNumberValue
                            keeptrackEditField.seedingRateBSUnitKeepEdit = newcolumnPlantingAmount.columnDropdown1
                        }
                        break

                        
                    default:
                        break
                        
                    }

                }
                
                if(newcolumnPlantingAmount.columnName1 == "Seeds per cell")
                {
                    if(newcolumnPlantingAmount.columnNumberValue.integerValue > 1)
                    {
                        newcolumnPlantingAmount.columnDropdown1 = "seeds"
                        keeptrackEditField.seedPerCell_value = newcolumnPlantingAmount.columnNumberValue
                        keeptrackEditField.seedPerCell_unit = newcolumnPlantingAmount.columnDropdown1
                    }
                    else
                    {
                        newcolumnPlantingAmount.columnDropdown1 = "seed"
                        keeptrackEditField.seedPerCell_value = newcolumnPlantingAmount.columnNumberValue
                        keeptrackEditField.seedPerCell_unit = newcolumnPlantingAmount.columnDropdown1
                    }
                }
                
                column2 = newcolumnPlantingAmount
            }
            else if valueString == "3" {
                
                let newcolumnPlantingAmount = dicData?.object(forKey: "column3") as! ClassObjectColumn
                
                if newcolumnPlantingAmount.columnNumberValue != column3.columnNumberValue {
                    switch(newcolumnPlantingAmount.columnName1)
                    {
                     
                    case "Rows per bed":
                        keeptrackEditField.rowperbedNumverEdit = newcolumnPlantingAmount.columnNumberValue
                        
//                        var rowperbed_old = column3.columnNumberValue.integerValue
                        var rowperbed_new = newcolumnPlantingAmount.columnNumberValue.integerValue
                        
                        
                        if rowperbed_new == 0 {
                            rowperbed_new = 1
                        }
 
                        
                        if rowperbed_new == 1 {
                            newcolumnPlantingAmount.columnDropdown1 = "row"
                        } else {
                            newcolumnPlantingAmount.columnDropdown1 = "rows"
                        }
                        
                        // calculate numofplant and then update numofflat
                        let  rowperbed:Double = keeptrackEditField.rowperbedNumverEdit.doubleValue
                        var plantingamount:Double = column1.columnNumberValue.doubleValue
                        if column1.columnDropdown1 == "row ft." && column3.columnNumberValue.doubleValue > 0 {
                            plantingamount = column1.columnNumberValue.replace(",", withString: "").doubleValue / column3.columnNumberValue.doubleValue
                        }
                        let inrowspacing:Double = column2.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                        
                        
                        // draw data
                        
                        for m in 0..<listSectionSuccession.count{
                            let sections = listSectionSuccession[m]
                            for n in 0..<sections.listCellForRow.count{
                                let cellFor = sections.listCellForRow[n]
                                if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN_FLAT {
                                    let dictShows = cellFor.dictionaryShow_AT as NSDictionary
                                    let column11 = dictShows.object(forKey: "column1") as! ClassObjectColumn
                                    let column22 = dictShows.object(forKey: "column2") as! ClassObjectColumn
                                    let column33 = dictShows.object(forKey: "column3") as! ClassObjectColumn
                                    if inrowspacing == 0 {
                                        keeptrackEditField.numberOfPlant = 0
                                    }
                                    else  {
                                        keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column33.columnNumberValue.doubleValue / 100 )) / inrowspacing
                                        if keeptrackEditField.numberOfPlant > 99999.99 {
                                            keeptrackEditField.numberOfPlant = 99999.99
                                        }
                                    }
                                    
                                    let flatObj = Utilities.convertTray_Pot_(to_Number: column11.columnDropdown1)
                                    if flatObj == 0 {
                                        keeptrackEditField.numberOfFlat = 0
                                    }
                                    else {
                                        keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                                        
                                    }
                                    cellFor.dictionaryShow_AT = NSDictionary(objects: [ column11, column22, column33, nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "column3" as NSCopying,"numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                                }
                                else if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_TWOCOLUMN_FLAT {
                                    let dictShows = cellFor.dictionaryShow_AT as NSDictionary
                                    let column11 = dictShows.object(forKey: "column1") as! ClassObjectColumn
                                    let column22 = dictShows.object(forKey: "column2") as! ClassObjectColumn
                                    
                                    if inrowspacing == 0 {
                                        keeptrackEditField.numberOfPlant = 0
                                    }
                                    else  {
                                        keeptrackEditField.numberOfPlant = ((rowperbed * plantingamount * 12)*(1 + column22.columnNumberValue.doubleValue / 100 )) / inrowspacing
                                        if keeptrackEditField.numberOfPlant > 99999.99 {
                                            keeptrackEditField.numberOfPlant = 99999.99
                                        }
                                    }
                                    
                                    let flatObj = Utilities.convertTray_Pot_(to_Number: column11.columnDropdown1)
                                    if flatObj == 0 {
                                        keeptrackEditField.numberOfFlat = 0
                                    }
                                    else {
                                        keeptrackEditField.numberOfFlat = keeptrackEditField.numberOfPlant/Double(flatObj)
                                        
                                    }
                                    
                                    
                                    
                                    cellFor.dictionaryShow_AT = NSDictionary(objects: [ column11, column22, nf.string(from: NSNumber(value: keeptrackEditField.numberOfFlat as Double))!, nf.string(from: NSNumber(value: keeptrackEditField.numberOfPlant as Double))!], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "numberofflat" as NSCopying,"numberofplant" as NSCopying ])
                                }
                            }
                        }
                        
                        break
                        
                    case "Btwn-row spacing":
                        keeptrackEditField.betweenrowspacingNumberEdit = newcolumnPlantingAmount.columnNumberValue
                        break
                        
                    default:
                        break
                    }
                }
                
                column3 = newcolumnPlantingAmount
                
                if column3.columnName1 == "Total transplants" {
                    keeptrackEditField.totalofTransplent = column3.columnNumberValue
                }
                
            }
            cellForShow.dictionaryShow_AT = NSDictionary(objects: [column1, column2, column3], forKeys: [ "column1" as NSCopying, "column2" as NSCopying, "column3" as NSCopying]) as! [AnyHashable : Any] as [AnyHashable: Any] as NSDictionary
            break
        case .CELL_AT_TWOCOLUMN:
            isEditedFields = true
            let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
            var column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
            
            //check user change planting amount
            if (column1.columnName1 == "Planting amount" && valueString == "1")
            {
                let newcolumnPlantingAmount = dicData?.object(forKey: "column1") as! ClassObjectColumn
                if (newcolumnPlantingAmount.columnNumberValue != column1.columnNumberValue && column1.columnDropdown1 != "trees" )
                {
                    if self.location != nil {
                        // reset selected bed
                        for i in 0..<self.location!.links.locations.count
                        {
                            let block : DetailClassField = self.location!.links.locations[i]
                            if (block.locationtype == "BLOCK")
                            {
                                block.totalSelectedBedBlock = 0
                                for j in 0..<block.links.locations.count
                                {
                                    block.links.locations[j].extendProp.selectedbedfeet = 0
                                }
                            }
                            else if (block.locationtype == "BED")
                            {
                                block.extendProp.selectedbedfeet = 0
                            }
                        }
                        
                        // update planting amount
                        selectedBedList  = [SelectedBed]()
                        for i in 0  ..< listSectionSuccession.count
                        {
                            let sectionforShow = listSectionSuccession [i]
                            for j in 0  ..< sectionforShow.listCellForRow.count
                            {
                                let cellForShow = sectionforShow.listCellForRow[j]
                                if (cellForShow.typeCell_AT == .CELL_AT_SHOWLOCATION)
                                {
                                    let name : String = self.location!.name + " - no beds selected"
                                    let dictShow = NSDictionary(objects: [name], forKeys: ["leftitem" as NSCopying])
                                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                                    
                                    isNoBedSelected = true
                                    
                                    self.location!.links.locations = [DetailClassField]()
                                }
                            }
                        }
                    }
                    else if selectedBedList.count > 0 {
                        selectedBedList  = [SelectedBed]()
                        for i in 0  ..< listSectionSuccession.count
                        {
                            let sectionforShow = listSectionSuccession [i]
                            for j in 0  ..< sectionforShow.listCellForRow.count
                            {
                                let cellForShow = sectionforShow.listCellForRow[j]
                                if (cellForShow.typeCell_AT == .CELL_AT_SHOWLOCATION)
                                {
                                    let name : String = fieldName + " - no beds selected"
                                    let dictShow = NSDictionary(objects: [name], forKeys: ["leftitem" as NSCopying])
                                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                                    
                                    isNoBedSelected = true
                                    if(self.location != nil)
                                    {
                                        self.location!.links.locations = [DetailClassField]()
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
                keeptrackEditField.plantingAmountEdit = newcolumnPlantingAmount.columnNumberValue
                keeptrackEditField.plantingAmountUnitEdit = newcolumnPlantingAmount.columnDropdown1
            }
            
            
            var column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
            if valueString == "1" {
                column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                
            }
            else if valueString == "2" {
                
                let newcolumnPlantingAmount = dicData?.object(forKey: "column2") as! ClassObjectColumn
                if newcolumnPlantingAmount.columnNumberValue != column2.columnNumberValue {
                    switch(newcolumnPlantingAmount.columnName1)
                    {
                        
                    case "In-row spacing":
                        keeptrackEditField.inrowspacingNumberEdit = newcolumnPlantingAmount.columnNumberValue
                        
                        break
                        
                    case "Seeding rate":
                        keeptrackEditField.inrowspacingNumberEdit = newcolumnPlantingAmount.columnNumberValue
                        keeptrackEditField.seedingRateUnitEdit = newcolumnPlantingAmount.columnDropdown1
                        if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.DIRECT_SOW_BAND_SWATH {
                            keeptrackEditField.inrowspacingNumberEdit = newcolumnPlantingAmount.columnNumberValue
                            keeptrackEditField.seedingRateBSUnitKeepEdit = newcolumnPlantingAmount.columnDropdown1
                        }
                        break
                        
                        
                    default:
                        break
                        
                    }
                    
                }
                
                if(newcolumnPlantingAmount.columnName1 == "Seeds per cell")
                {
                    if(newcolumnPlantingAmount.columnNumberValue.integerValue > 1)
                    {
                        newcolumnPlantingAmount.columnDropdown1 = "seeds"
                        keeptrackEditField.seedPerCell_value = newcolumnPlantingAmount.columnNumberValue
                        keeptrackEditField.seedPerCell_unit = newcolumnPlantingAmount.columnDropdown1
                    }
                    else
                    {
                        newcolumnPlantingAmount.columnDropdown1 = "seed"
                        keeptrackEditField.seedPerCell_value = newcolumnPlantingAmount.columnNumberValue
                        keeptrackEditField.seedPerCell_unit = newcolumnPlantingAmount.columnDropdown1
                    }
                }
                
                column2 = newcolumnPlantingAmount
                
                
            }
            
            cellForShow.dictionaryShow_AT = NSDictionary(objects: [column1, column2], forKeys: [ "column1" as NSCopying, "column2" as NSCopying]) as! [AnyHashable : Any] as [AnyHashable: Any] as NSDictionary
            break
        case .CELL_AT_TWOCOLUMN_FLAT:
            let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
            var column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
            var column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
            
            
            var  numofflat = dictShow.object(forKey: "numberofflat") as! String
            var  numberofplant = dictShow.object(forKey: "numberofplant") as! String
            
            if valueString == "1" {
                column1 = dicData?.object(forKey: "column1") as! ClassObjectColumn
                keeptrackEditField.flatpot_unit = column1.columnDropdown1
                
                // calculate numofplant and then update numofflat
                var rowperbed:Double = 0
                var plantingamount:Double = 0
                var inrowspacing:Double = 0
                for m in 0..<listSectionSuccession.count{
                    let sections = listSectionSuccession[m]
                    for n in 0..<sections.listCellForRow.count{
                        let cellFor = sections.listCellForRow[n]
                        if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN {
                            let dictShow = cellFor.dictionaryShow_AT as NSDictionary
                            let column11 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                            if column11.columnName1 == "Planting amount" {
                                plantingamount = column11.columnNumberValue.doubleValue
                                 let column31 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                                if column11.columnDropdown1 == "row ft." && column31.columnNumberValue.doubleValue > 0 {
                                    plantingamount = column1.columnNumberValue.replace(",", withString: "").doubleValue / column31.columnNumberValue.doubleValue
                                }
                                let column21 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                                inrowspacing = column21.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                                
                                rowperbed = column31.columnNumberValue.doubleValue
                            }
                            else {
                                break
                            }
                        }
                    }
                }
                var  total:Double = 0
                if inrowspacing == 0 {
                    numberofplant = "0"
                }
                else  {
                    total = ((rowperbed * plantingamount * 12)*(1 + keeptrackEditField.estimatedloss / 100 )) / inrowspacing
                    if total > 99999.99 {
                        total = 99999.99
                    }
                    numberofplant = nf.string(from: NSNumber(value: total as Double))!
                }
                
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                if flatObj == 0 {
                    numofflat = "0"
                }
                else {
                    numofflat = nf.string(from: NSNumber(value: total/Double(flatObj) as Double))!
                }
            }
            else if valueString == "2"{
                column2 = dicData?.object(forKey: "column2") as! ClassObjectColumn
                keeptrackEditField.estimatedloss = column2.columnNumberValue.doubleValue
                // calculate numofplant and then update numofflat
                var rowperbed:Double = 0
                var plantingamount:Double = 0
                var inrowspacing:Double = 0
                for m in 0..<listSectionSuccession.count{
                    let sections = listSectionSuccession[m]
                    for n in 0..<sections.listCellForRow.count{
                        let cellFor = sections.listCellForRow[n]
                        if cellFor.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN {
                            let dictShow = cellFor.dictionaryShow_AT as NSDictionary
                            let column11 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                            if column11.columnName1 == "Planting amount" {
                                let column31 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                                plantingamount = column11.columnNumberValue.doubleValue
                                if column11.columnDropdown1 == "row ft." && column31.columnNumberValue.doubleValue > 0 {
                                    plantingamount = column1.columnNumberValue.replace(",", withString: "").doubleValue / column31.columnNumberValue.doubleValue
                                }
                                let column21 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                                inrowspacing = column21.columnNumberValue.replacingOccurrences(of: ",", with: "").doubleValue
                                
                                rowperbed = column31.columnNumberValue.doubleValue
                            }
                            else {
                                break
                            }
                        }
                    }
                }
                var  total:Double = 0
                if inrowspacing == 0 {
                    numberofplant = "0"
                }
                else  {
                    total = ((rowperbed * plantingamount * 12)*(1 + keeptrackEditField.estimatedloss / 100 )) / inrowspacing
                    if total > 99999.99 {
                        total = 99999.99
                    }
                    numberofplant = nf.string(from: NSNumber(value: total as Double))!
                }
                
                let flatObj = Utilities.convertTray_Pot_(to_Number: column1.columnDropdown1)
                if flatObj == 0 {
                    numofflat = "0"
                }
                else {
                    numofflat = nf.string(from: NSNumber(value: total/Double(flatObj) as Double))!
                }
            }
            
            keeptrackEditField.numberOfFlat = numofflat.replace(",", withString: "").doubleValue
            keeptrackEditField.numberOfPlant = numberofplant.replace(",", withString: "").doubleValue
            
            cellForShow.dictionaryShow_AT = NSDictionary(objects: [ column1, column2, numofflat, numberofplant], forKeys: [ "column1" as NSCopying,"column2" as NSCopying, "numberofflat" as NSCopying,"numberofplant" as NSCopying ])
            break
        case .CELL_AT_TEXTFIELD_CONTROL:
            isEditedFields = true
            switch(cellForShow.typeAction_AT){
                
            case .ACTION_AT_ADD_ONE_DESCRIPTION:
                cellForShow.valueSelected_AT = valueString
                cellForShow.isHaveData_AT = true
                keeptrackEditField.oneworddescriptionEdit = valueString
                if dicData == nil {
                    return
                } else {
                    
                }

            default:
                break
            }
            break
        default:
            break
        }
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
    }

//    func calculateNumOfPlant(typeof:Int, rowperbed:Int, plantingamount:Int, inrowspacing:Int, estimatedloass:Double, flatObj:Int) -> Double {
//        if typeof == 1 { // change planting amount , in-row spacing , rows per bed
//            if inrowspacing == 0 {
//                return 0
//            }
//            return Double(( rowperbed * plantingamount * 12 * (1 + Double((estimatedloass)/100)))/inrowspacing)
//        }
//        else if typeof == 2{
//            
//        }
//    }
    
    func checkValidate() -> String {
        let stringMessage = "";
//        
////        if clsTaskDetail.id == 0 {
////            // check location
////            // date current
////            let datecurrent = Utilities.convertDateTimeToTimeStamp(Utilities.resetHourMinSecOfDate(NSDate())).doubleValue
////            var isHaveLocation = false
////            
////            if let t = self.location {
////                if categoryID == CategoryEnum.TREES_.rawValue {
////                    isHaveLocation = true
////                }
////                else {
////                    if t.links.locations.count > 0 {
////                        isHaveLocation = true
////                    }
////                    
////                }
////            }
////            else
////            {
////                if categoryID == CategoryEnum.TREES_.rawValue {
////                    if treeLocation.latitude != 0 || treeLocation.longitude != 0 {
////                        isHaveLocation = true
////                    }
////                }
////                else if(self.selectedBedList.count > 0)
////                {
////                    isHaveLocation = true
////                }
////            }
////            
//////            if dateIntervalDayToTransplant < datecurrent && isHaveLocation == false {
//////                //location
//////                
//////                return "Location is required to save this succession."
//////            }
////        }
//        
//        
//        for i in 0  ..< listSectionSuccession.count{
//            let section = listSectionSuccession[i]
//            for j in 0  ..< section.listCellForRow.count {
//                let cellForShow = section.listCellForRow[j]
//                
//                switch(cellForShow.typeCell_AT){
////                case .CellCompletePlatingTask_ID:
////                    let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
////                    if clsTaskDetail.id != 0 && successionSeedingMethoTypeCL  != SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
////                        if dictShow.valueForKey("typeDate") as! String == "1" || dictShow.valueForKey("typeDate") as! String == "4" {
////                            // check location
////                            var isHaveLocation = false
////                            
////                            if let t = self.location {
////                                
////                                if categoryID == CategoryEnum.TREES_.rawValue {
////                                    isHaveLocation = true
////                                }
////                                else {
////                                    if t.links.locations.count > 0 {
////                                        isHaveLocation = true
////                                    }
////                                    
////                                }
////                                
////                            }
////                            else {
////                                if categoryID == CategoryEnum.TREES_.rawValue {
////                                    
////                                    if treeLocation.latitude != 0 || treeLocation.longitude != 0 {
////                                        isHaveLocation = true
////                                    }
////                                }
////                                else if(self.selectedBedList.count > 0)
////                                {
////                                    isHaveLocation = true
////                                }
////                            }
////                            
////                            
////                            //TODO: remove logic *require location when complete planting task*
//////                            if isHaveLocation == false || isNoBedSelected {
//////                                stringMessage = kMessageLocationComplete
//////                                return stringMessage
//////                            }
////                            
////                        }
////                    }
////                    
////                    break
////                case .CELL_AT_THREECOLUMN:
////                    let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
////                    for i in 1  ..< 2 {
////                        let column = dictShow.objectForKey("column\(i)") as! ClassObjectColumn
////                        switch(column.columnName1){
////                            
//////                        case "In-row spacing":
//////                            if column.columnNumberValue.floatValue == 0 {
//////
//////                                if categoryID != CategoryEnum.BERRIES.rawValue &&  categoryID != CategoryEnum.VINES.rawValue && categoryID != CategoryEnum.TREES_.rawValue && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.DIRECT_SOW_BAND_SWATH {
//////                                    stringMessage = "Please input In-row spacing."
//////                                    return stringMessage
//////                                }
//////                            }
//////                            
//////                            break
////                            
//////                        case "Rows per bed":
//////                            if column.columnNumberValue.floatValue == 0 {
//////                                stringMessage = "Please input Rows per bed."
//////                                return stringMessage
//////                            }
//////                            
//////                            break
////                            
////                        default:
////                            break
////                            
////                        }
////                        
////                    }
////                    
////                    
////                    
////                    break
//                
//                default:
//                    break
//                }
//            }
//            
//        }
        return stringMessage
    }
    
    func removeDuplicatedUnit()
    {
        for i in 0  ..< listConvertRate.count
        {
            let convertRate = listConvertRate[i]
            if(convertRate.fromunit == convertRate.tounit && convertRate.value == 0)
            {
                listConvertRate.remove(at: i)
                break
            }
        }
    }
    
    func dictReturn() -> NSDictionary{
        
        saveSuccession.clsTaskDetail = clsTaskDetail
        saveSuccession.successionSeedingMethoTypeCL = successionSeedingMethoTypeCL
        saveSuccession.dateIntervalSeedingDate = dateIntervalSeedingDate
        saveSuccession.dateIntervalDayToTransplant = dateIntervalDayToTransplant
        saveSuccession.dateIntervalDatePlantingLabor = dateIntervalDatePlantingLabor
        
        let eventPropertise: eventMixpanel = eventMixpanel()
        eventPropertise.eventName = typeSuccession
        eventPropertise.eventPropertise = "Planting method"
        if successionSeedingMethoTypeCL.rawValue != clsSuccessionDetail.extendProp.plantingid && clsSuccessionDetail.extendProp.plantingid != 0 {
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
        } else {
            eventPropertise.eventPropertiseValue = kMixpanelEditedNo
        }
        
        eventPropertise.eventFeature = evenSuccession
        listEventExpensesCreate.append(eventPropertise)
        
        if clsMasterCropSuccession.links.parentcrop.name == "Tomatoes" || clsMasterCropSuccession.links.parentcrop.name == "Rice"{
            growingMethodTitle = "Growing method"
        }
        else {
            growingMethodTitle = "Harvest stage"
        }
        
        if growingmethod != "" {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = growingMethodTitle
            if growingmethod.capitalized != clsSuccessionDetail.extendProp.growingmethodname.capitalized && clsSuccessionDetail.id > 0 {
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            } else {
                eventPropertise.eventPropertiseValue = kMixpanelEditedNo
            }
            
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        }
        
        saveSuccession.clsSuccessionDetail = clsSuccessionDetail
        if (categoryID == CategoryEnum.vegetables.rawValue || categoryID == CategoryEnum.flowers.rawValue || categoryID == CategoryEnum.herbs.rawValue) && numberDayToTransplant == 0 && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
            if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                saveSuccession.numberDayToTransplant = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                saveSuccession.numberDayToTransplant = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                saveSuccession.numberDayToTransplant = 0
            } else {
                //saveSuccession.numberDayToTransplant = 7
            }
            
        } else {
            saveSuccession.numberDayToTransplant = numberDayToTransplant
        }
        
        if (categoryID == CategoryEnum.vegetables.rawValue || categoryID == CategoryEnum.flowers.rawValue || categoryID == CategoryEnum.herbs.rawValue || categoryID == CategoryEnum.farmseed.rawValue) && numberDayToFirstHarvest == 0 {
            
            if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                saveSuccession.numberDayToFirstHarvest = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                saveSuccession.numberDayToFirstHarvest = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                saveSuccession.numberDayToFirstHarvest = 0
            } else {
                saveSuccession.dateIntervalDayToFirstHarvest = dateIntervalDayToFirstHarvest + 24*3600000
                saveSuccession.numberDayToFirstHarvest = 1
            }
        } else {
            saveSuccession.dateIntervalDayToFirstHarvest = dateIntervalDayToFirstHarvest
            saveSuccession.numberDayToFirstHarvest = numberDayToFirstHarvest
        }
        
        saveSuccession.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest
        saveSuccession.numberDayToLastHarvest = numberDayToLastHarvest
        saveSuccession.combiId = combiId
        saveSuccession.combikey = combiKey
        
//        if (categoryID == CategoryEnum.VEGETABLES.rawValue || categoryID == CategoryEnum.FLOWERS.rawValue || categoryID == CategoryEnum.HERBS.rawValue || categoryID == CategoryEnum.FARMSEED.rawValue) && numberDayToLastHarvest == 0 {
//            saveSuccession.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest + 24*3600
//            saveSuccession.numberDayToLastHarvest = 1
//        } else {
//            saveSuccession.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest
//            saveSuccession.numberDayToLastHarvest = numberDayToLastHarvest
//        }
        
        if (categoryID == CategoryEnum.trees_.rawValue) {
            saveSuccession.numberDayToLastHarvest = numberDayToLastHarvest + 1
        }
        
//        saveSuccession.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest
//        saveSuccession.numberDayToLastHarvest = numberDayToLastHarvest
        saveSuccession.listSectionSuccession = listSectionSuccession
        saveSuccession.location = location
        
        if stringDesLocation != "" {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Location"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Location"
            eventPropertise.eventPropertiseValue = kMixpanelEditedNo
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        }
        
        if selectedBedList.count > 0 && clsSuccessionDetail.locations.count > 0 {
            var listBed = selectedBedList
            listBed = listBed.sorted(by: { (c1:SelectedBed, c2: SelectedBed) -> Bool in
                let strS1 = "\(c1.id)"
                let strS2 = "\(c2.id)"
                return strS1.localizedCaseInsensitiveCompare(strS2) == ComparisonResult.orderedAscending
            })
            var listBedOld = clsSuccessionDetail.locations
            listBedOld = listBedOld.sorted(by: { (c1:ClassFields, c2: ClassFields) -> Bool in
                let strS1 = "\(c1.id)"
                let strS2 = "\(c2.id)"
                return strS1.localizedCaseInsensitiveCompare(strS2) == ComparisonResult.orderedAscending
            })
            
        }
        
        if clsMasterCropSuccession.id != Int32(clsSuccessionDetail.extendProp.mastercropid) && clsSuccessionDetail.extendProp.mastercropid != 0 {
            if stringCropTypeEdit == "" {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "Variety"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
                
                if listCropType.count > 0 {
                    for k in 0 ..< listCropType.count {
                        if listCropType[k].id == Int(clsMasterCropSuccession.croptypeid) && listCropType[k].name != "None Selected" {
                            let eventPropertise: eventMixpanel = eventMixpanel()
                            eventPropertise.eventName = typeSuccession
                            eventPropertise.eventPropertise = "Crop type"
                            if listCropType[k].name != clsSuccessionDetail.extendProp.croptype || isChangeCropType {
                                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                            }else {
                                eventPropertise.eventPropertiseValue = kMixpanelEditedNo
                            }
                            
                            eventPropertise.eventFeature = evenSuccession
                            listEventExpensesCreate.append(eventPropertise)
                            
                            break
                        }
                    }
                } else if clsMasterCropSuccession.links.croptype.name != "" && clsMasterCropSuccession.links.croptype.name != "None Selected" {
                    if isChangeCropType {
                        let eventPropertise: eventMixpanel = eventMixpanel()
                        eventPropertise.eventName = typeSuccession
                        eventPropertise.eventPropertise = "Crop type"
                        eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                        eventPropertise.eventFeature = evenSuccession
                        listEventExpensesCreate.append(eventPropertise)
                    }
                    
                }
                
            } else {
                var eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "Variety"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
                
                eventPropertise = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "Crop type"
                if clsMasterCropSuccession.links.croptype.name != clsMasterCropSuccession.links.parentcrop.name || isChangeCropType {
                    eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                } else {
                    eventPropertise.eventPropertiseValue = kMixpanelEditedNo
                }
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
                
                
            }
        } else {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Variety"
            if isChangeVariety {
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            } else {
                eventPropertise.eventPropertiseValue = kMixpanelEditedNo
            }
            
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
            if listCropType.count > 0 {
                for k in 0 ..< listCropType.count {
                    if listCropType[k].id == Int(clsMasterCropSuccession.croptypeid) && listCropType[k].name != "None Selected" {
                        let eventPropertise: eventMixpanel = eventMixpanel()
                        eventPropertise.eventName = typeSuccession
                        eventPropertise.eventPropertise = "Crop type"
                        if clsMasterCropSuccession.links.croptype.name != listCropType[k].name || isChangeCropType {
                            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                        } else {
                            eventPropertise.eventPropertiseValue = kMixpanelEditedNo
                        }
                        
                        eventPropertise.eventFeature = evenSuccession
                        listEventExpensesCreate.append(eventPropertise)
                        
                        
                        break
                    }
                }
            } else if clsMasterCropSuccession.links.croptype.name != "" && clsMasterCropSuccession.links.croptype.name != "None Selected" {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "Crop type"
                if isChangeCropType {
                    eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                } else {
                    eventPropertise.eventPropertiseValue = kMixpanelEditedNo
                }
                
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
                
            }
        }
        
        if keeptrackEditField.oneworddescriptionEdit != "" {
            if keeptrackEditField.oneworddescriptionEdit != clsSuccessionDetail.oneworddesc {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "One-word description"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            } else {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "One-word description"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            }
        } else {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "One-word description"
            eventPropertise.eventPropertiseValue = kMixpanelEditedNo
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        }
        
        saveSuccession.combinationDataForShow = combinationDataForShow
        saveSuccession.categoryID = categoryID
        saveSuccession.clsMasterCropSuccession = clsMasterCropSuccession
        saveSuccession.selectedBedList = selectedBedList
        self.removeDuplicatedUnit()
        saveSuccession.listConvertRate = listConvertRate
        saveSuccession.treeLocation = CLLocationCoordinate2DMake(treeLocation.latitude, treeLocation.longitude)
        saveSuccession.isSingleTree = isSingleTree
        saveSuccession.isNoBedSelected = isNoBedSelected
        saveSuccession.listSeedSource = self.listSeedSource
        saveSuccession.listEventExpensesCreate = listEventExpensesCreate
        saveSuccession.eventFeature = evenSuccession
        return saveSuccession.saveSuccession()
    }
    
    func dictSaveTaskPlantReturn() -> NSDictionary{
        saveTask.dateIntervalDatePlantingLabor = dateIntervalDatePlantingLabor
        saveTask.clsTaskDetail = clsTaskDetail
        saveTask.successionSeedingMethoTypeCL = successionSeedingMethoTypeCL
        saveTask.dateIntervalSeedingDate = dateIntervalSeedingDate
        saveTask.totalLaborTask = totalTime
        saveTask.eventFeature = evenSuccession
        if clsMasterCropSuccession.links.parentcrop.name == "Tomatoes" || clsMasterCropSuccession.links.parentcrop.name == "Rice"{
            growingMethodTitle = "Growing method"
        }
        else {
            growingMethodTitle = "Harvest stage"
        }
        
        if growingmethod != "" {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = growingMethodTitle
            if growingmethod != clsSuccessionDetail.extendProp.growingmethodname {
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            } else {
                eventPropertise.eventPropertiseValue = kMixpanelEditedNo
            }
            
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        }
        
        saveTask.dateIntervalDayToTransplant = dateIntervalDayToTransplant
        saveTask.clsSuccessionDetail = clsSuccessionDetail
        if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S && numberDayToTransplant == 0 && !firstharvestcompleted && !isFinishCrop {
            if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                saveTask.numberDayToTransplant = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                saveTask.numberDayToTransplant = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                saveTask.numberDayToTransplant = 0
            } else {
                //saveTask.numberDayToTransplant = 7
            }
        } else {
            saveTask.numberDayToTransplant = numberDayToTransplant
            
        }
        
        if (categoryID == CategoryEnum.vegetables.rawValue || categoryID == CategoryEnum.flowers.rawValue || categoryID == CategoryEnum.herbs.rawValue || categoryID == CategoryEnum.farmseed.rawValue) && numberDayToFirstHarvest == 0 && !firstharvestcompleted && !isFinishCrop {
            if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                saveTask.numberDayToFirstHarvest = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                saveTask.numberDayToFirstHarvest = 0
            } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate)  as NSDate).days(before: Date()) > 0 {
                saveTask.numberDayToFirstHarvest = 0
            } else {
                saveTask.dateIntervalDayToFirstHarvest = dateIntervalDayToFirstHarvest + 24*3600000
                saveTask.numberDayToFirstHarvest = 1
            }
        } else {
            saveTask.dateIntervalDayToFirstHarvest = dateIntervalDayToFirstHarvest
            saveTask.numberDayToFirstHarvest = numberDayToFirstHarvest
        }
        
        if stringDesLocation != "" {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Location"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Location"
            eventPropertise.eventPropertiseValue = kMixpanelEditedNo
            listEventExpensesCreate.append(eventPropertise)
        }
        
        saveTask.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest
        saveTask.numberDayToLastHarvest = numberDayToLastHarvest
        saveTask.combiId = combiId
        saveTask.combikey = combiKey
        
//        if (categoryID == CategoryEnum.VEGETABLES.rawValue || categoryID == CategoryEnum.FLOWERS.rawValue || categoryID == CategoryEnum.HERBS.rawValue || categoryID == CategoryEnum.FARMSEED.rawValue) && numberDayToLastHarvest == 0 {
//            saveTask.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest + 24*3600
//            saveTask.numberDayToLastHarvest = 1
//        } else {
//            saveTask.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest
//            saveTask.numberDayToLastHarvest = numberDayToLastHarvest
//        }
        
        if (categoryID == CategoryEnum.trees_.rawValue) {
            saveTask.numberDayToLastHarvest = numberDayToLastHarvest + 1
        }
        
        saveTask.listSectionSuccession = listSectionSuccession
        saveTask.location = location
        saveTask.combinationDataForShow = combinationDataForShow
        saveTask.categoryID = categoryID
        saveTask.clsMasterCropSuccession = clsMasterCropSuccession
        saveTask.selectedBedList = selectedBedList
        self.removeDuplicatedUnit()
        saveTask.listConvertRate = listConvertRate
        saveTask.isSingleTree = isSingleTree
        saveTask.isNoBedSelected = isNoBedSelected
        if statusOfTask == 1 || statusOfTask == 4 {
            saveTask.isCompleteTask = true
        } else {
            saveTask.isCompleteTask = false
        }
        
        
        if keeptrackEditField.oneworddescriptionEdit != "" {
            if keeptrackEditField.oneworddescriptionEdit != clsSuccessionDetail.oneworddesc {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "One-word description"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            } else {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "One-word description"
                eventPropertise.eventPropertiseValue = kMixpanelEditedNo
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            }
        }
        
        
        saveTask.listAssignee = listUserAssigned
        saveTask.listComment = listComment
        saveTask.treeLocation = CLLocationCoordinate2DMake(treeLocation.latitude, treeLocation.longitude)
        saveTask.listSeedSource = self.listSeedSource
        saveTask.listEventExpensesCreate = listEventExpensesCreate
        return saveTask.SaveTaskOjbect()
    }
    
    func showImage(_ indexPath:IndexPath, image:UIImage){
        self.isSelectItem = true;
        let section = listSectionSuccession[indexPath.section]
        let cellForShow = section.listCellForRow[indexPath.row]
        
        
        if cellForShow.totalCurrentImage == 0 {
            cellForShow.img1 = image;
            cellForShow.totalCurrentImage = 1
        }
        else if cellForShow.totalCurrentImage == 1 {
            cellForShow.img2 = image;
            cellForShow.totalCurrentImage = 2;
        }
        else if cellForShow.totalCurrentImage == 2 {
            cellForShow.img3 = image
            cellForShow.totalCurrentImage = 3
        }
        else if cellForShow.totalCurrentImage == 3{
            cellForShow.img4 = image
            cellForShow.totalCurrentImage = 4;
        }
        else if (cellForShow.totalCurrentImage == 4){
            cellForShow.img5 = image
            cellForShow.totalCurrentImage = 5
        }
        
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
    }
    
    func getPhotoFromTable() -> [UIImage]? {
        var arrayList:[UIImage] =  [UIImage]()
        for i in 0  ..< listSectionSuccession.count{
            let sectionforShow:SectionForRow = listSectionSuccession[i]
            
            for j in 0  ..< sectionforShow.listCellForRow.count{
                let cellForShow = sectionforShow.listCellForRow[j]
                
                
                switch(cellForShow.typeCell_AT){
                    
                case .CELL_AT_TAKEPHOTO:
                    
                    if cellForShow.img1 != nil {
                        arrayList.append(cellForShow.img1!)
                    }
                    if cellForShow.img2 != nil {
                        arrayList.append(cellForShow.img2!)
                    }
                    if cellForShow.img3 != nil {
                        arrayList.append(cellForShow.img3!)
                    }
                    if cellForShow.img4 != nil {
                        arrayList.append(cellForShow.img4!)
                    }
                    if cellForShow.img5 != nil {
                        arrayList.append(cellForShow.img5!)
                    }
                    
                    break
                
                default:
                    continue
                    
                }
            }

        }
        if arrayList.count > 0 {
            return arrayList
        }
        else {
            return nil
        }
        
    }
    
    func reloadLocation (_ location: DetailClassField?, descString: String, indexPath: IndexPath, platingamount :Int, plantingUnit:String, rowperbed:Int) {
        isSelectItem = true
        // update planting amount 
        for i in 0  ..< listSectionSuccession.count {
            let sectionforShow = listSectionSuccession [i]
            for j in 0  ..< sectionforShow.listCellForRow.count
            {
                let cellForShow = sectionforShow.listCellForRow[j]
                if cellForShow.typeCell_AT == AddTaskTypeCell.CELL_AT_THREECOLUMN && cellForShow.typeAction_AT == AddTaskTypeAction.ACTION_AT_ADD_THREECOLUMN_PLANTING {
                    
                    let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                    let column1 = dictShow.object(forKey: "column1") as! ClassObjectColumn
                    let column2 = dictShow.object(forKey: "column2") as! ClassObjectColumn
                    let column3 = dictShow.object(forKey: "column3") as! ClassObjectColumn
                    var plantingAmount = platingamount
                    if column1.columnDropdown1 == "plants" {
                        if column2.columnNumberValue.doubleValue == 0 {
                            plantingAmount = plantingAmount*12
                        } else {
                            plantingAmount = Int(Double(plantingAmount)*12/column2.columnNumberValue.doubleValue)
                        }
                    }
                    
                    keeptrackEditField.plantingAmountEdit = "\(plantingAmount)"
                    keeptrackEditField.plantingAmountUnitEdit = column1.columnDropdown1
                    column1.columnNumberValue = keeptrackEditField.plantingAmountEdit
                    cellForShow.dictionaryShow_AT = NSDictionary(objects: [column1, column2, column3], forKeys: [ "column1" as NSCopying, "column2" as NSCopying, "column3" as NSCopying]) as! [AnyHashable : Any] as [AnyHashable: Any] as NSDictionary
                    
                }
            }
            
            
        }
        
        if platingamount == 0 && location?.id > 0 {
            self.isNoBedSelected = true
        }
        else {
            self.isNoBedSelected = false
        }
        
        self.location = location
        
        let section = listSectionSuccession[indexPath.section]
        let cellForShow = section.listCellForRow[indexPath.row]
        let dictShow = NSDictionary(objects: [descString], forKeys: ["leftitem" as NSCopying])
        
        cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
    }
    
    func didDeleteLocation(_ indexPath: IndexPath)
    {
        var plantingamount:Int = 0
        var plantingunit = ""
        var rowperbed:Int = 0
        (plantingamount, plantingunit, rowperbed) = self.getPlantingAmount()
        self.isNoBedSelected = false
        self.selectedBedList = [SelectedBed]()
        self.location = nil
//        self.reloadLocation(nil, descString: "Select Location", indexPath: indexPath, platingamount: self.getPlantingAmount())
        self.reloadLocation(nil, descString: "Select Location", indexPath: indexPath, platingamount: plantingamount, plantingUnit: plantingunit, rowperbed: rowperbed)
    }
    
    func getPlantingAmount() -> (Int,String,Int)
    {
        var numberPlating = 0
        var stringUnitPlanting = ""
        var rowperbed:Int = 0
        for i in 0  ..< listSectionSuccession.count {
            let section = listSectionSuccession[i]
            for j in 0  ..< section.listCellForRow.count {
                let cellForShow = section.listCellForRow[j]
                
                switch(cellForShow.typeCell_AT){
                    
                    
                case .CELL_AT_THREECOLUMN:
                    let dictShow = cellForShow.dictionaryShow_AT as NSDictionary
                    var isNeedToUpdateUnit = false
                    for i in 1  ..< 4 {
                        let column = dictShow.object(forKey: "column\(i)") as! ClassObjectColumn
                        switch(column.columnName1){
                            
                        case "Planting amount":
                            if column.columnDropdown1 == "plants" {
                                isNeedToUpdateUnit = true
                            }
                            numberPlating = column.columnNumberValue.replace(",", withString: "").integerValue
                            stringUnitPlanting = column.columnDropdown1
                            break
                        case "In-row spacing":
                            if isNeedToUpdateUnit {
                                if column.columnNumberValue.doubleValue == 0 {
                                    numberPlating = Int(numberPlating/12)
                                } else {
                                    numberPlating = Int(Double(numberPlating)*column.columnNumberValue.doubleValue/12)
                                }
                            }
                            break
                        case "Rows per bed":
                            rowperbed = column.columnNumberValue.replace(",", withString: "").integerValue
                            if rowperbed == 0 {
                                rowperbed = 1
                            }
                            break
                        default:
                            break
                            
                        }
                        
                    }
                    
                    break
                    
                default:
                    break
                }
            }
            
        }
        return (numberPlating, stringUnitPlanting, rowperbed)

    }
    
    func updateCellInfo(_ index:Int, listUserNew: [User]) {
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.maximumFractionDigits = 2
        let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
        var totalLabor = "Labor Time"
        if (self.totalTime != 0) {
            totalLabor = stringTemp! + "h"
        }
        var stringDate = ""
        if dateIntervalDatePlantingLabor == 0 {
            stringDate = "Due Date"
        } else {
            stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
        }
        var assignee = "Unassigned";
        var stringImage = "";
        if (listUserAssigned.count > 0) {
            if (listUserAssigned.count == 1) {
                assignee = listUserAssigned[0].firstname;
                stringImage = listUserAssigned[0].linkPhotoName;
            } else {
                assignee = "Multiple";
            }
        }
        
        self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
    }
    
    func addCommentToTimeline(_ indexPath:IndexPath, stringText:String, typeAction:AddTaskTypeAction){
        listComment.add(stringText)
        
        var sectionForShow: SectionForRow = SectionForRow()
        
        for i in 0 ..< listSectionSuccession.count {
            if listSectionSuccession[i].isTimelineSection{
                sectionForShow = listSectionSuccession[i]
                break
            }
        }
        
        let cellForShow:CellForShow = CellForShow()
        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_COMMENTINTIMELINE
        
        let dictShow = NSDictionary(objects: ["ADACA9", "You", Utilities.convertDateTimeToFullStyleString(forTask: Date()), "updated", "Planting", stringText], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying, "comment" as NSCopying])
        cellForShow.dictionaryShow_AT = dictShow as NSDictionary
        cellForShow.isShowLineBottomFull = true
        cellForShow.isShowLineTop = true
        sectionForShow.listCellForRow.insert(cellForShow, at: 1)
        
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
    }

    func updateInfoUser(_ timeString: String, indexPath: IndexPath, columnIndex: Int, numberOfPepole: Int, percentOfArea: String)
    {
        isSelectItem = true
        if(columnIndex == 1)
        {
            let nf = NumberFormatter()
            nf.numberStyle = NumberFormatter.Style.decimal
            nf.maximumFractionDigits = 2
            let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
            var totalLabor = "Labor Time"
            if (self.totalTime != 0) {
                totalLabor = stringTemp! + "h"
            }
            dateIntervalDatePlantingLabor = timeString.doubleValue;
            let stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
            var assignee = "Unassigned";
            var stringImage = "";
            if (listUserAssigned.count > 0) {
                if (listUserAssigned.count == 1) {
                    assignee = listUserAssigned[0].firstname;
                    stringImage = listUserAssigned[0].linkPhotoName;
                } else {
                    assignee = "Multiple";
                }
            }
            
            self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
            
//            dateIntervalDatePlantingLabor = timeString.doubleValue
//            var section = listSectionSuccession[indexPath.section]
//            let cellItem = section.listCellForRow[indexPath.row]
//            let stringDate : String = Utilities.convertDateTimeToString(Utilities.dateFromTimestamp(dateIntervalDatePlantingLabor))
//            let hour : Int = laborTime / 3600
//            let min : Int = (laborTime - (hour * 3600))/60
//            let stringLabor : String = "\(hour)h \(min)m"
//            let numberOfPepoleAfter : String = cellItem.dictSelected_AT.valueForKey("numberOfPepole") as! String
//            let percentOfAreaAfter : String = cellItem.dictSelected_AT.valueForKey("percentOfArea") as! String
//            
//            let dictShow : NSDictionary = NSDictionary(objects: ["\(dateIntervalDatePlantingLabor)", stringDate, "\(laborTime)", stringLabor, numberOfPepoleAfter, percentOfAreaAfter], forKeys: ["dateTimetamp", "dateShow", "laborValue", "laborShow" ,"numberOfPepole", "percentOfArea"])
//            cellItem.dictionaryShow_AT = dictShow
//            cellItem.dictSelected_AT = dictShow
//            cellItem.isHaveData_AT = true
            
            
            // date again 
            if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                dateIntervalSeedingDate = dateIntervalDatePlantingLabor
                dateIntervalDayToTransplant = dateIntervalSeedingDate + numberDayToTransplant*86400000
                
            }
            else {
                dateIntervalDayToTransplant = dateIntervalDatePlantingLabor
//
                if clsSuccessionDetail.links.subtasktype.id != SuccessionSeedingMethodType.GREENHOUSE_SOW_S.rawValue {
                    dateIntervalSeedingDate = dateIntervalDatePlantingLabor
                }
            }
            
            if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue{
                if dateIntervalDayToFirstHarvest < dateIntervalDayToTransplant {
                    var  yearUpdate = Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToTransplant)
                    
                    var i:Int32 = 1
                    var datefirstharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToFirstHarvest)
                    
                    while datefirstharvestcheck < dateIntervalDayToTransplant {
                        i += 1
                        datefirstharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToFirstHarvest)
                    }
                    dateIntervalDayToFirstHarvest = datefirstharvestcheck
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = datefirstharvestcheck
                    
                    yearForFruitTree = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest))
                    keeptrackEditField.numberYearForFruistTreeEdit = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest))
                    
                    if dateIntervalDayToLastHarvest < dateIntervalDayToFirstHarvest{
                        yearUpdate = Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToFirstHarvest)
                        i = 1
                        var datelastharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToLastHarvest)
                        
                        while datelastharvestcheck < dateIntervalDayToFirstHarvest {
                            i += 1
                            datelastharvestcheck = Utilities.setYearForDateFrom_datetime_ForTree(yearUpdate + i, currentDate:dateIntervalDayToLastHarvest)
                        }
                        dateIntervalDayToLastHarvest = datelastharvestcheck
                        keeptrackEditField.dateIntervalDayToLastHarvestEdit = datelastharvestcheck
                    }
                }
            
            }
            else {
                dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    
                }
                keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
            }
            
            
            for i in 0  ..< listSectionSuccession.count {
                let section = listSectionSuccession[i]
                if section.sectionName == .sectionPlantingMethod {
                    self.updateChangeDateLayout(section)
                }
            }
            self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)

        }
        if(columnIndex == 2)
        {
            let section = listSectionSuccession[indexPath.section]
            let cellItem = section.listCellForRow[indexPath.row]
            
            laborTime = timeString.integerValue
            let stringDate : String = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
            let hour : Int = laborTime / 3600
            let min : Int = (laborTime - (hour * 3600))/60
            let stringLabor : String = "\(hour)h \(min)m"
            let dictShow : NSDictionary = NSDictionary(objects: ["\(dateIntervalDatePlantingLabor)", stringDate, "\(laborTime)", stringLabor, "\(numberOfPepole)", "\(percentOfArea)"], forKeys: ["dateTimetamp" as NSCopying, "dateShow" as NSCopying, "laborValue" as NSCopying, "laborShow" as NSCopying ,"numberOfPepole" as NSCopying, "percentOfArea" as NSCopying])
            cellItem.dictionaryShow_AT = dictShow
            cellItem.dictSelected_AT = dictShow
            cellItem.isHaveData_AT = true
            
            self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
        }
    }
    
    func changeTreeLocation(_ indexPath: IndexPath, orchardlocation:DetailClassField?, orchardTypeAction:OrchardActionType){
        location = orchardlocation
        
        let sectionforShow = listSectionSuccession[indexPath.section]
        sectionforShow.listCellForRow.removeAll(keepingCapacity: false)
        
        createCellObject.treeLocation = treeLocation
        createCellObject.location = location
        createCellObject.isSingleTree = isSingleTree
        createCellObject.createGroup_Location_Tree(sectionforShow, orchardTypeAction:orchardTypeAction)
        
        self.delegate?.ParentCrop_SuccessionControllerDelegate_ReloadTable!(listSectionSuccession)
    }
    
    func checkUpdateTracking(_ successionid:Int) -> NSMutableDictionary?
    {
//        if successionid == 0 {
//            return nil
//        }
        
        let dic = NSMutableDictionary()
        
        if keeptrackEditField.noteInTaskEdit != "" {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Description"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        }
    
        if keeptrackEditField.estimatedloss != 0 {
            dic.setValue(true, forKey: "estimatedloss")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Estimated Loss"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "estimatedloss")
        }
        
        if keeptrackEditField.seedPerCell_value != "" {
            dic.setValue(true, forKey: "seedpercell")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Seeds per cell"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "seedpercell")
        }
        
        if keeptrackEditField.flatpot_unit != "" {
            dic.setValue(true, forKey: "numofflatsunitid")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Flat/pot type"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "numofflatsunitid")
        }
        
        if keeptrackEditField.numberOfPlant != 0 {
            dic.setValue(true, forKey: "numofplants")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Total # of transplants"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "numofplants")
        }
        
        if keeptrackEditField.numberOfFlat != 0 {
            dic.setValue(true, forKey: "numberofflat")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "# flats"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "numberofflat")
        }
        
        if keeptrackEditField.inrowspacingNumberEdit != "" || keeptrackEditField.seedingRateUnitEdit != ""{
            if plantingMethodFirstLoad != "Direct Sow" {
                if categoryID != CategoryEnum.trees_.rawValue {
                    dic.setValue(false, forKey: "seedingrate")
                    dic.setValue(false, forKey: "seedingrateunitid")
                    if self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.DIRECT_SOW_BAND_SWATH {
                        let eventPropertise: eventMixpanel = eventMixpanel()
                        eventPropertise.eventName = typeSuccession
                        eventPropertise.eventPropertise = "Seeding rate"
                        eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                        eventPropertise.eventFeature = evenSuccession
                        listEventExpensesCreate.append(eventPropertise)
                    }
                }
            }
        } else {
            dic.setValue(false, forKey: "seedingrate")
            dic.setValue(false, forKey: "seedingrateunitid")
        }
        if keeptrackEditField.inrowspacingNumberEdit != ""{
            if self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S || self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_GREENHOUSE || self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_FIELD || self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_S || self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.DIRECT_SOW_S || self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.DIRECT_SOW_BAND_SWATH {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "In-row spacing"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            }
            dic.setValue(true, forKey: "inrowspacing")
            
        } else {
            dic.setValue(false, forKey: "inrowspacing")
        }
        if keeptrackEditField.rowperbedNumverEdit != ""{
            dic.setValue(true, forKey: "rowperbed")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Rows per bed"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "rowperbed")
        }
        if keeptrackEditField.plantingAmountEdit != "" || keeptrackEditField.plantingAmountUnitEdit != "" {
            dic.setValue(true, forKey: "plantingamount")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Planting amount"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "plantingamount")
        }
        if keeptrackEditField.betweenrowspacingNumberEdit != "" {
            dic.setValue(true, forKey: "rowspacinglow")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Btwn-row spacing"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "rowspacinglow")
        }
        if keeptrackEditField.rootstockEdit != "" {
            dic.setValue(true, forKey: "rootstock")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Rootstock"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "rootstock")
        }
        if keeptrackEditField.harvestSeasonEdit != "" {
            dic.setValue(true, forKey: "harvestseasonid")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Harvest Season"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "harvestseasonid")
        }
        if keeptrackEditField.dateIntervalDayToLastHarvestTree != 0 || keeptrackEditField.numberDayToLastHarvestEdit != 0 || keeptrackEditField.dateIntervalDayToLastHarvestEdit != 0 {
            dic.setValue(true, forKey: "lastharvest")
            if self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S && clsTaskDetail.id > 0 {
                
            } else{
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "Last Harvest"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            }
        } else {
            dic.setValue(false, forKey: "lastharvest")
        }
        if keeptrackEditField.dateIntervalDayToFirstHarvestTree != 0 || keeptrackEditField.numberDayToFirstHarvestEdit != 0 || keeptrackEditField.dateIntervalDayToFirstHarvestEdit != 0 {
            dic.setValue(true, forKey: "firstharvest")
            if self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S && clsTaskDetail.id > 0 {
                
            } else {
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "First Harvest"
                if self.growingmethod == "Cover Crop" && clsTaskDetail.id == 0 {
                    eventPropertise.eventPropertise = "Take Down Date"
                }
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            }
            
        } else {
            dic.setValue(false, forKey: "firstharvest")
        }
        if keeptrackEditField.numberYearForFruistTreeEdit != 0 {
            dic.setValue(true, forKey: "firstfruitingyear")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "First fruiting year"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "firstfruitingyear")
        }
        if keeptrackEditField.harvestUnitEdit != "" {
            dic.setValue(true, forKey: "harvestunit")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Harvest unit"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "harvestunit")
        }
        if keeptrackEditField.avgYieldRateEditValue != "" || keeptrackEditField.avgYieldRateEditUnit1 != "" || keeptrackEditField.avgYieldRateEditUnit2 != "" {
            dic.setValue(true, forKey: "yieldrate")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Avg. yield rate"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "yieldrate")
        }
        if keeptrackEditField.avgSalesPriceEditValue != "" || keeptrackEditField.avgSalesPriceEditUnit != "" {
            dic.setValue(true, forKey: "saleprice")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Avg. sales price"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "saleprice")
        }
        if keeptrackEditField.harvestFrequencyEditValue != "" || keeptrackEditField.harvestFrequencyEditUnit != "" {
            dic.setValue(true, forKey: "harvestfrequency")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Harvest frequency"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "harvestfrequency")
        }
        if keeptrackEditField.seedSourceEdit != "" {
            dic.setValue(true, forKey: "seedsourceid")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Seed source"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "seedsourceid")
        }
        if keeptrackEditField.growingCycleEdit != "" {
            dic.setValue(true, forKey: "annualperennial")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Growing cycle"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "annualperennial")
        }
        if keeptrackEditField.dateIntervalSeedingDateEdit != 0 && (categoryID != CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.fruits.rawValue || categoryID == CategoryEnum.vines.rawValue) {
            if self.successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                dic.setValue(true, forKey: "startdate")
                let eventPropertise: eventMixpanel = eventMixpanel()
                eventPropertise.eventName = typeSuccession
                eventPropertise.eventPropertise = "GH start date"
                eventPropertise.eventPropertiseValue = kMixpanelEditedYes
                eventPropertise.eventFeature = evenSuccession
                listEventExpensesCreate.append(eventPropertise)
            } else {
                dic.setValue(false, forKey: "startdate")
            }
        } else {
            dic.setValue(false, forKey: "startdate")
        }
        if keeptrackEditField.numberDayToTransplantEdit != 0 {
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Days in GH"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        }
        if keeptrackEditField.dateIntervalDayToTransplantEdit != 0 {
            dic.setValue(true, forKey: "dateplanted")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            
            if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_S || successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S || successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_GREENHOUSE {
                eventPropertise.eventPropertise = "Field planting date"
            } else {
                eventPropertise.eventPropertise = "Field sowing date"
            }
            if categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue {
                eventPropertise.eventPropertise = "Planting date"
            }
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "dateplanted")
        }
        if keeptrackEditField.numberDayToFirstHarvestEdit != 0 {
            dic.setValue(true, forKey: "adjusteddtmlow")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Days to maturity"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "adjusteddtmlow")
        }
        if keeptrackEditField.numberDayToLastHarvestEdit != 0 {
            dic.setValue(true, forKey: "daytomaturityll")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Harvest window"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "daytomaturityll")
        }
        if keeptrackEditField.organicSeedEidt != "" {
            dic.setValue(true, forKey: "organicseed")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Oganic seed?"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "organicseed")
        }
        if keeptrackEditField.seedCoatingEdit != "" {
            dic.setValue(true, forKey: "pelletedoption")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Seed coating"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "pelletedoption")
        }
        if keeptrackEditField.grownInaHoophouseEdit != "" {
            dic.setValue(true, forKey: "growinginahoophouse")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Grown in a hoophouse"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "growinginahoophouse")
        }
        
        if keeptrackEditField.dateIntervalTakeDownEdit != 0 {
            dic.setValue(true, forKey: "takedowndate")
        } else {
            dic.setValue(false, forKey: "takedowndate")
        }
        
        if keeptrackEditField.flatpot_value != "" || keeptrackEditField.flatpot_unit != "" {
            dic.setValue(true, forKey: "numofflats")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "# flats"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "numofflats")
        }
        
        if keeptrackEditField.totalofTransplent != "" {
            dic.setValue(true, forKey: "numofplants")
            let eventPropertise: eventMixpanel = eventMixpanel()
            eventPropertise.eventName = typeSuccession
            eventPropertise.eventPropertise = "Total # of transplant"
            eventPropertise.eventPropertiseValue = kMixpanelEditedYes
            eventPropertise.eventFeature = evenSuccession
            listEventExpensesCreate.append(eventPropertise)
        } else {
            dic.setValue(false, forKey: "numofplants")
        }
        
        if dic.allKeys.count > 0 {
            dic.setValue(successionid, forKey: "successionid")
//            if clsTrackingSuccession != nil {
//                dic.setValue(clsTrackingSuccession!.id, forKey: "id")
//            }
        }
        
        keeptrackEditField = ClassEditKeepTrack()
        return dic
        
    }
    
    //MARK: - PRIVATE METHOD
    fileprivate func createFirstData() -> [SectionForRow]{
        growingmethod = self.combinationDataForShow.growingmethod
        var successionDataShow:[SectionForRow] = [SectionForRow]()
        for i in 0  ..< 10{
            let sectionforShow:SectionForRow = SectionForRow()
            switch(i){
                
            case 0:
                self.createParentCropObject(sectionforShow)
                break
            case 1:
                self.createCrop_Variety(sectionforShow, parentcropname: self.clsMasterCropSuccession.links.parentcrop.name, imgName: self.clsMasterCropSuccession.imagename, varietyname: self.clsMasterCropSuccession.varietyname, cropTypename: self.clsMasterCropSuccession.links.croptype.name)
                break
            case 2:
                self.createGroup_Location(sectionforShow)
                break
            case 3:
                if clsTaskDetail.id > 0 && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    continue
                }
                if categoryID == CategoryEnum.trees_.rawValue && isSingleTree == true{
                    continue
                }
                if clsSuccessionDetail.id > 0 {
                    self.createThreeColumn_Planting_Detail(sectionforShow)
                } else {
                    self.createThreeColumn_Planting(sectionforShow)
                }
                
                break
            case 4:
                self.createGroup_Planting_GetDate()
                if combinationDataForShow.growingmethod != "" && changeTypeEnum != .changeGrowingMethod  {
                    growingmethod = combinationDataForShow.growingmethod
                }
                self.createGroup_Planting_Growing(sectionforShow, growingMethod: growingmethod)
                self.createGroup_Planting_Growing_Child(sectionforShow, isDetail: false)
                    
                break
            case 5:
                if ( categoryID == CategoryEnum.vegetables.rawValue || categoryID == CategoryEnum.flowers.rawValue || categoryID == CategoryEnum.herbs.rawValue || categoryID == CategoryEnum.berries.rawValue  ) {
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                        self.createGH_Flat(sectionforShow)
                    } else if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S {
                        self.createTPP_Flat(sectionforShow)
                    }
                    
                }
                else {
                    continue
                }
                break
            case 6:
                self.createHarvestInformation(sectionforShow)
                break
            default:
                continue
                
            }
            
            successionDataShow.append(sectionforShow)
        }
        self.updateChangeDate_AtCellName(successionDataShow)
        
        return successionDataShow
    }
    fileprivate func createDetailData() -> [SectionForRow]{
        var successionDataShow:[SectionForRow] = [SectionForRow]()
        for i in 0  ..< 14{
            let sectionforShow:SectionForRow = SectionForRow()
            switch(i){
            case 0:
                if clsTaskDetail.id > 0 {
                    sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.HideAll_Header_Footer
                    self.totalTime = clsTaskDetail.links.labor.values

                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.maximumFractionDigits = 2
                    let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
                    var totalLabor = "Labor Time"
                    if (self.totalTime != 0) {
                        totalLabor = stringTemp! + "h"
                    }
                    
                    dateIntervalDatePlantingLabor = clsTaskDetail.taskdate;
                    var stringDate = ""
                    if dateIntervalDatePlantingLabor == 0 {
                        stringDate = "Due Date"
                    } else {
                        stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
                    }
                    
                    var assignee = "Unassigned";
                    var stringImage = "";
                    if (listUserAssigned.count > 0) {
                        if (listUserAssigned.count == 1) {
                            assignee = listUserAssigned[0].firstname;
                            stringImage = listUserAssigned[0].linkPhotoName;
                        } else {
                            assignee = "Multiple";
                        }
                    }
                    
                    self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
                    
//                    self.createCellInfor(sectionforShow)
                }
                else {
                    continue
                }
                break
            case 1:
                if clsTaskDetail.id > 0 {
                    sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.HideAll_Header_Footer
                    if (clsTaskDetail.finalharvest) {
                        self.delegate?.statusCompleteTask!(4)
                    }
                    else {
                        let dateTask = Utilities.date(fromTimestamp: clsTaskDetail.taskdate) as NSDate
                        if dateTask.isToday() || dateTask.isInFuture() {
                            self.delegate?.statusCompleteTask!(3)
                        } else {
                            self.delegate?.statusCompleteTask!(2)
                        }
                    }
                    //self.createCompleteHarvest(sectionforShow)
                }
                else {
                    continue
                }
                break
            case 2:
                sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.JustShowFooter
                if clsTaskDetail.id > 0 {
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                    nf.maximumFractionDigits = 2
                    let stringAmount = nf.string(from: NSNumber(value: clsSuccessionDetail.plantingamount as Double))!
                    
                    var name = "\(Utilities.convertDate(toString: clsSuccessionDetail.startdate))" + " - " + "\(Utilities.convertDate(toString: clsSuccessionDetail.enddate))" + "・" + "\(clsSuccessionDetail.extendProp.location)・" + stringAmount + " \(clsSuccessionDetail.plantingamountunit)"

                    if clsSuccessionDetail.extendProp.location == "" {
                        name = "\(Utilities.convertDate(toString: clsSuccessionDetail.startdate))" + " - " + "\(Utilities.convertDate(toString: clsSuccessionDetail.enddate))" + "・Field not assigned・" + stringAmount + " \(clsSuccessionDetail.plantingamountunit)"
                    }
                    
                    //[[[[[[clsTaskDetail.links.croptypes objectAtIndex:0] crops] objectAtIndex:0]
                    let stringCropType = clsTaskDetail.links.croptypes[0].crops[0].groupname
                    
                    let stringImage = NSString(format: "%@%@%@", Utilities.getkServer(), kPhotoPath, Utilities.generateImageName(withName: clsSuccessionDetail.imagename, andWSize: 100, andHSize: 100))
                    
                    let dictShow = NSDictionary(objects: [stringCropType, clsSuccessionDetail.varietyname, name, stringImage, "\(clsSuccessionDetail.id)", clsSuccessionDetail.varietyname, clsSuccessionDetail.croptypename], forKeys: [ "cropTypeName" as NSCopying, "masterVarietyName" as NSCopying, "locationSuccession" as NSCopying, "image" as NSCopying, "successionid" as NSCopying, "varietyname" as NSCopying, "parentname" as NSCopying])

                    let cellForShowSuc = CellForShow()
                    cellForShowSuc.typeCell_AT = AddTaskTypeCell.CELL_AT_SHOWSUCCESSION
                    cellForShowSuc.typeAction_AT = .ACTION_AT_SUCCESSIONTIMELINE
                    cellForShowSuc.valueSelected_AT = clsTaskDetail.notes
                    if keeptrackEditField.noteInTaskEdit != "" {
                        cellForShowSuc.valueSelected_AT = keeptrackEditField.noteInTaskEdit
                    }
                    cellForShowSuc.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShowSuc.isShowLineBottom = true
                    sectionforShow.listCellForRow.append(cellForShowSuc)

                    let cellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_ADDDESCRIPTIONONTASK
                    cellForShow.valueSelected_AT = clsTaskDetail.notes
                    if keeptrackEditField.noteInTaskEdit != "" {
                        cellForShow.valueSelected_AT = keeptrackEditField.noteInTaskEdit
                    }
                    cellForShow.isShowLineBottom = true
                    sectionforShow.listCellForRow.append(cellForShow)
                } else {
                    self.createCrop_Variety(sectionforShow, parentcropname: self.clsMasterCropSuccession.links.parentcrop.name, imgName: self.clsMasterCropSuccession.imagename, varietyname: self.clsMasterCropSuccession.varietyname, cropTypename: self.clsMasterCropSuccession.links.croptype.name)
                }
                break
            case 3:
                if clsTaskDetail.id > 0 && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    continue
                }
                self.createGroup_Location(sectionforShow)
                break
            case 4:
                if clsTaskDetail.id > 0 && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    continue
                }
                if categoryID == CategoryEnum.trees_.rawValue && isSingleTree == true{
                    continue
                }
                if clsSuccessionDetail.id > 0 {
                    self.createThreeColumn_Planting_Detail(sectionforShow)
                } else {
                    self.createThreeColumn_Planting(sectionforShow)
                }
                break
            case 5:
                
                self.createGroup_Planting_GetDate_Detail()
                
                if combinationDataForShow.growingmethod != "" && changeTypeEnum != .changeGrowingMethod  {
                    growingmethod = combinationDataForShow.growingmethod
                }
                
                self.createGroup_Planting_Growing(sectionforShow, growingMethod: growingmethod)
                
                self.createGroup_Planting_Growing_Child(sectionforShow, isDetail: true)
                
                break
            case 6:
                if ( categoryID == CategoryEnum.vegetables.rawValue || categoryID == CategoryEnum.flowers.rawValue || categoryID == CategoryEnum.herbs.rawValue || categoryID == CategoryEnum.berries.rawValue  ) {
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                        self.createGH_Flat_Detail(sectionforShow)
                    } else if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S {
                        self.createTPP_Flat_Detail(sectionforShow)
                    }
                    
                }
                else {
                    continue
                }
                break
            case 7:
                self.createHarvestInformation_Detail(sectionforShow)
                break
                
            default:
                continue
                
            }
            
            successionDataShow.append(sectionforShow)
        }
        
        return successionDataShow
    }
    
    fileprivate func createCellInfor(_ sectionforShow:SectionForRow){
        if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
            dateIntervalDatePlantingLabor = dateIntervalSeedingDate
        }
        else {
            dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
        }
        laborTime = clsTaskDetail.links.labor.values;
        
        createCellObject.dateIntervalDatePlantingLabor = dateIntervalDatePlantingLabor
        createCellObject.clsTaskDetail = clsTaskDetail
        createCellObject.successionSeedingMethoTypeCL = successionSeedingMethoTypeCL
        createCellObject.laborTime = laborTime
        createCellObject.listSeedSource = self.listSeedSource
        createCellObject.createCellInfor(sectionforShow)
    }

    fileprivate func createCompleteHarvest(_ sectionforShow:SectionForRow){
        createCellObject.clsTaskDetail = clsTaskDetail
        createCellObject.listSeedSource = self.listSeedSource
        createCellObject.createCompleteHarvest(sectionforShow)
    }
    
    fileprivate func createParentCropObject(_ sectionforShow:SectionForRow){
        createCellObject.clsMasterCropSuccession = clsMasterCropSuccession
        createCellObject.createParentCropObject(sectionforShow)
    }
    
    fileprivate func createCrop_Variety(_ sectionforShow:SectionForRow, parentcropname:String, imgName:String, varietyname:String, cropTypename:String){
        sectionforShow.listCellForRow.removeAll(keepingCapacity: true)
        
        createCellObject.listCropType = listCropType
        createCellObject.clsSuccessionDetail = clsSuccessionDetail
        createCellObject.createCrop_Variety(sectionforShow, parentcropname: parentcropname, imgName: imgName, varietyname: varietyname, cropTypename: cropTypename)
        
    }
    
    fileprivate func createGroup_Location(_ sectionforShow:SectionForRow){
        
        createCellObject.categoryID = categoryID
        createCellObject.treeLocation = self.treeLocation
        createCellObject.location = self.location
        createCellObject.isSingleTree = self.isSingleTree
        createCellObject.clsSuccessionDetail = clsSuccessionDetail
        createCellObject.changeTypeEnum = self.changeTypeEnum
        createCellObject.createGroup_Location(sectionforShow)
        
    }
    
    fileprivate func createThreeColumn_Planting(_ sectionforShow:SectionForRow){
        
        createCellObject.categoryID = categoryID
        createCellObject.combinationDataForShow = combinationDataForShow
        createCellObject.keeptrackEditField = keeptrackEditField
        createCellObject.successionSeedingMethoTypeCL = successionSeedingMethoTypeCL
        createCellObject.clsMasterCropSuccession = clsMasterCropSuccession
        createCellObject.isSingleTree = isSingleTree
        createCellObject.createThreeColumn_Planting(sectionforShow)
        createCellObject.shouldChangeValue = shouldChangeValueEdit
    }
    
    fileprivate func createThreeColumn_Planting_Detail(_ sectionforShow:SectionForRow){
        
        createCellObject.categoryID = categoryID
        createCellObject.combinationDataForShow = combinationDataForShow
        createCellObject.keeptrackEditField = keeptrackEditField
        createCellObject.clsSuccessionDetail = clsSuccessionDetail
        createCellObject.successionSeedingMethoTypeCL = successionSeedingMethoTypeCL
        createCellObject.clsMasterCropSuccession = clsMasterCropSuccession
        createCellObject.isSingleTree = isSingleTree
        createCellObject.createThreeColumn_Planting_Detail(sectionforShow)
        createCellObject.shouldChangeValue = shouldChangeValueEdit
        
    }
    
    fileprivate func createGroup_Planting_Growing(_ sectionforShow:SectionForRow, growingMethod:String){
        
        if self.plantingMethodFirstLoad.lowercased() == "broadcast" {
            self.plantingMethodFirstLoad = "Broadcast Sow"
        }
        else if self.plantingMethodFirstLoad.lowercased() == "drill" || self.plantingMethodFirstLoad.lowercased() == "drill down" {
            self.plantingMethodFirstLoad = "Drill Sow"
        }
        
        sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.JustShowFooter
        sectionforShow.sectionName = .sectionPlantingMethod
        
        if self.clsTaskDetail.id == 0 && self.plantingMethodFirstLoad != "" && (categoryID != CategoryEnum.fruits.rawValue &&  categoryID != CategoryEnum.trees_.rawValue &&  categoryID != CategoryEnum.vines.rawValue) {
            let cellForShow = CellForShow()
            cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_ID
            cellForShow.typeAction_AT = AddTaskTypeAction.ACTION_AT_ADD_PLANTINGMETHOD
            let dictShow = NSDictionary(objects: ["Planting method", self.plantingMethodFirstLoad], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
            cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
            sectionforShow.listCellForRow.append(cellForShow)
        }
        if self.clsMasterCropSuccession.parentcropid == 109{
            // don't show growing method  for Tomato
            return
        }
        
        if growingMethod != "" {
            let cellForShow = CellForShow()
            cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_ID
            cellForShow.typeAction_AT = AddTaskTypeAction.ACTION_AT_ADD_GROWINGMETHOD
            let dictShow = NSDictionary(objects: [growingMethodTitle, growingMethod], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
            cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
            sectionforShow.listCellForRow.append(cellForShow)
        }
    }
    
    fileprivate func createGroup_Planting_GetDate(){
        
        if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S{
            if clsTaskDetail.id > 0 {
                
                numberDayToTransplant = Double(combinationDataForShow.averagedaysgreenhouse)
                if keeptrackEditField.numberDayToTransplantEdit != 0 {
                    numberDayToTransplant = keeptrackEditField.numberDayToTransplantEdit
                }
                dateIntervalDayToTransplant =  dateIntervalSeedingDate + numberDayToTransplant*86400000
            }
            else  {
                numberDayToTransplant = Double(combinationDataForShow.averagedaysgreenhouse)
                if keeptrackEditField.numberDayToTransplantEdit != 0 {
                    numberDayToTransplant = keeptrackEditField.numberDayToTransplantEdit
                }
                
                if dateIntervalDayToTransplant == 0 {
                    dateIntervalDayToTransplant =  dateIntervalSeedingDate + numberDayToTransplant*86400000
                }
                else if previousPlatingMethod != SuccessionSeedingMethodType.NONE_SEEDINGMETHOD && previousPlatingMethod == SuccessionSeedingMethodType.GREENHOUSE_SOW_S{
                    dateIntervalDayToTransplant =  dateIntervalSeedingDate + numberDayToTransplant*86400000
                }
                else {
                    dateIntervalSeedingDate = dateIntervalDayToTransplant - numberDayToTransplant*86400000
                }
            }
        }
        else{
            if clsTaskDetail.id > 0 {
                numberDayToTransplant = Double(combinationDataForShow.averagedaysgreenhouse)
                if keeptrackEditField.numberDayToTransplantEdit != 0 {
                    numberDayToTransplant = keeptrackEditField.numberDayToTransplantEdit
                }
            }
            else {
                numberDayToTransplant = 0
                if dateIntervalDayToTransplant == 0 {
                    dateIntervalDayToTransplant =  dateIntervalSeedingDate
                }
                if keeptrackEditField.dateIntervalDayToTransplantEdit != 0 {
                    dateIntervalDayToTransplant = keeptrackEditField.dateIntervalDayToTransplantEdit
                }
                dateIntervalSeedingDate = dateIntervalDayToTransplant
                
            }
        }
        numberYearForFruistTree = Int(self.combinationDataForShow.firstfruitingyear)
        if keeptrackEditField.numberYearForFruistTreeEdit != 0 {
            numberYearForFruistTree = keeptrackEditField.numberYearForFruistTreeEdit
        }
        yearForFruitTree = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToTransplant) + numberYearForFruistTree)
        numberDayToFirstHarvest = Double(self.combinationDataForShow.daymaturityaverage)
        numberDayToLastHarvest = Double(self.combinationDataForShow.harvestwindow)
        if keeptrackEditField.numberDayToLastHarvestEdit != 0 {
            numberDayToLastHarvest = keeptrackEditField.numberDayToLastHarvestEdit
        }
        
        //set default dayto maturity
        if (categoryID == CategoryEnum.vegetables.rawValue || categoryID == CategoryEnum.flowers.rawValue || categoryID == CategoryEnum.herbs.rawValue || categoryID == CategoryEnum.farmseed.rawValue) && numberDayToFirstHarvest == 0 {
            numberDayToFirstHarvest = 0
        }
        
        dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
        if numberDayToLastHarvest > 0 {
             dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
        } else {
//            numberDayToLastHarvest = 1
//            dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest - 1)*86400000
            dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
        }
        
        if (categoryID == CategoryEnum.berries.rawValue){
            if numberDayToFirstHarvest <= 0 {
                numberDayToFirstHarvest = 90
            }
            
            dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
            
            if numberDayToLastHarvest > 0 {
                dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
            } else {
                dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
            }
            if Double(self.combinationDataForShow.firstharvest) > 0 && Double(self.combinationDataForShow.lastharvest) > 0 && Double(self.combinationDataForShow.harvestwindow) == 0 {
                numberDayToLastHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: Double(self.combinationDataForShow.firstharvest)), andDate: Utilities.date(fromTimestamp: Double(self.combinationDataForShow.lastharvest))))
            }
            
            
        }
        
        if (categoryID == CategoryEnum.fruits.rawValue || categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue){
            numberDayToTransplant = 0
            
            dateIntervalDayToFirstHarvest = Double(self.combinationDataForShow.firstharvest)
            dateIntervalDayToLastHarvest = Double(self.combinationDataForShow.lastharvest)
            if dateIntervalDayToFirstHarvest == 0 {
                dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant
                dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + 14*86400000
            }
            
            dateIntervalDayToFirstHarvest = Utilities.setDateTimeFrom_2_datetime_ForTree(dateIntervalDayToTransplant, datetime2: dateIntervalDayToFirstHarvest)
            dateIntervalDayToLastHarvest = Utilities.setDateTimeFrom_2_datetime_ForTree(dateIntervalDayToFirstHarvest, datetime2: dateIntervalDayToLastHarvest)

            numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
            numberDayToLastHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest), andDate: Utilities.date(fromTimestamp: dateIntervalDayToLastHarvest)))
            
//            dateIntervalDay_maxLastHarvestForTree = Utilities.setLastDateOfYearFrom_datetime_ForTree(dateIntervalDayToLastHarvest)
        }
    }
    
    fileprivate func createGroup_Planting_GetDate_Detail(){
        
        if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S{
            numberDayToTransplant = Double(clsSuccessionDetail.daytotransplant)
            dateIntervalDayToTransplant = dateIntervalSeedingDate + numberDayToTransplant*86400000
            
        }
        else if clsTaskDetail.id > 0 {
            numberDayToTransplant = Double(clsSuccessionDetail.daytotransplant)
            dateIntervalDayToTransplant = clsTaskDetail.taskdate
        }
        else{
            numberDayToTransplant = 0
            dateIntervalDayToTransplant = dateIntervalSeedingDate
        }
        numberYearForFruistTree = Int(self.clsSuccessionDetail.firstfruitingyear)
        
        yearForFruitTree = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToTransplant) + numberYearForFruistTree)
        numberDayToFirstHarvest = Double(self.clsSuccessionDetail.adjusteddtmlow)
        numberDayToLastHarvest = Double(self.clsSuccessionDetail.daytomaturityll)
        
        dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
        if numberDayToLastHarvest > 0 {
            dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
        } else {
//            numberDayToLastHarvest = 1
//            dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest - 1)*86400000
            dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
        }
        
        if (categoryID == CategoryEnum.berries.rawValue){
            if numberDayToFirstHarvest <= 0 {
                numberDayToFirstHarvest = 90
            }
            
            dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
            
            if numberDayToLastHarvest > 0 {
                dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
            } else {
                dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
            }
            if Double(self.clsSuccessionDetail.firstharvest) > 0 && Double(self.clsSuccessionDetail.lastharvest) > 0 && Double(self.clsSuccessionDetail.daytomaturityll) == 0 {
                numberDayToLastHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: Double(self.combinationDataForShow.firstharvest)), andDate: Utilities.date(fromTimestamp: Double(self.combinationDataForShow.lastharvest))))
            }
            
        }
        
        if (categoryID == CategoryEnum.fruits.rawValue || categoryID == CategoryEnum.trees_.rawValue || categoryID == CategoryEnum.vines.rawValue){
            numberDayToTransplant = 0
            dateIntervalDayToFirstHarvest = self.clsSuccessionDetail.firstharvest
            dateIntervalDayToLastHarvest = self.clsSuccessionDetail.lastharvest
            
            numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
            numberDayToLastHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest), andDate: Utilities.date(fromTimestamp: dateIntervalDayToLastHarvest)))
            
//            dateIntervalDay_maxLastHarvestForTree = Utilities.setLastDateOfYearFrom_datetime_ForTree(dateIntervalDayToLastHarvest)
        }
    }
    fileprivate func createGroup_Planting_Growing_Child(_ sectionforShow:SectionForRow, isDetail:Bool){
        createCellObject.dateIntervalSeedingDate = dateIntervalSeedingDate
        createCellObject.numberDayToTransplant = numberDayToTransplant
        createCellObject.dateIntervalDayToTransplant = dateIntervalDayToTransplant
        createCellObject.numberDayToFirstHarvest = numberDayToFirstHarvest
        createCellObject.dateIntervalDayToFirstHarvest = dateIntervalDayToFirstHarvest
        createCellObject.growingmethod = growingmethod
        createCellObject.numberDayToLastHarvest = numberDayToLastHarvest
        createCellObject.dateIntervalDayToLastHarvest = dateIntervalDayToLastHarvest
        createCellObject.numberYearForFruistTree = numberYearForFruistTree
        createCellObject.createGroup_Planting_Growing_Child(sectionforShow: sectionforShow, isDetail: isDetail)
    }
    
    fileprivate func createGH_Flat(_ sectionforShow:SectionForRow){
        
        createCellObject.keeptrackEditField = keeptrackEditField
        createCellObject.combinationDataForShow = combinationDataForShow
        createCellObject.createGreenhouseSowing_Flat(sectionforShow: sectionforShow)
        
    }
    fileprivate func createGH_Flat_Detail(_ sectionforShow:SectionForRow){
        createCellObject.keeptrackEditField = keeptrackEditField
        createCellObject.createGreenhouseSowing_Flat_Detail(sectionforShow: sectionforShow)
        
    }
    
    fileprivate func createTPP_Flat(_ sectionforShow:SectionForRow){
        createCellObject.keeptrackEditField = keeptrackEditField
        createCellObject.combinationDataForShow = combinationDataForShow
        createCellObject.createTPP_Flat(sectionforShow: sectionforShow)
        
    }
    
    fileprivate func createTPP_Flat_Detail(_ sectionforShow:SectionForRow){
        createCellObject.keeptrackEditField = keeptrackEditField
        createCellObject.createTPP_Flat_Detail(sectionforShow: sectionforShow)
        

    }
    
    fileprivate func createHarvestInformation(_ sectionforShow:SectionForRow){
        createCellObject.combinationDataForShow = combinationDataForShow
        createCellObject.keeptrackEditField = keeptrackEditField
        createCellObject.successionSeedingMethoTypeCL = successionSeedingMethoTypeCL
        createCellObject.listSeedSource = self.listSeedSource
        createCellObject.yearFirstHarvest = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToLastHarvest)) + 1
        createCellObject.createHarvestInformation(sectionforShow)

    }
    
    fileprivate func createHarvestInformation_Detail(_ sectionforShow:SectionForRow){
        createCellObject.combinationDataForShow = combinationDataForShow
        createCellObject.clsSuccessionDetail = clsSuccessionDetail
        createCellObject.clsTaskDetail = clsTaskDetail
        createCellObject.successionSeedingMethoTypeCL = successionSeedingMethoTypeCL
        createCellObject.listSeedSource = self.listSeedSource
        createCellObject.yearFirstHarvest = Int(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToLastHarvest)) + 1
        createCellObject.createHarvestInformation_Detail(sectionforShow)
        
    }
    
    fileprivate func createNote() -> SectionForRow {
        let realm = RLMRealm.default()
        rolePermissions = RolePermissionDatabase.allObjects(in: realm)
        if rolePermissions?.count > 0 {
            rolePermissionNote = rolePermissions!.object(at: 3) as! RolePermissionDatabase
        }
        
        let sectionforShow:SectionForRow = SectionForRow()
        if rolePermissionNote.addfunc {
            
            sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.ShowAll_Header_Footer
            sectionforShow.iconSectionTitle = NameIconSectionTitle.NONE
            sectionforShow.titleName = "NOTES"
            sectionforShow.sectionName = .sectionNote
            
            let cellForShow = CellForShow()
            cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_NOTE
            cellForShow.valueSelected_AT = ""
            
            
            sectionforShow.listCellForRow.append(cellForShow)
            
        }
        
        return sectionforShow
        
    }
    fileprivate func createListNote() -> SectionForRow {
        
        let realm = RLMRealm.default()
        rolePermissions = RolePermissionDatabase.allObjects(in: realm)
        rolePermissionNote = rolePermissions!.object(at: 3) as! RolePermissionDatabase
        
        let sectionforShow:SectionForRow = SectionForRow()
        sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.ShowAll_Header_Footer
        sectionforShow.iconSectionTitle = NameIconSectionTitle.NONE
        sectionforShow.titleName = "NOTES"
        sectionforShow.sectionName = .sectionNoteList_Task
        
        
        if self.clsTaskDetail.links.listingresults.count > 0 {
            for j in 0 ..< self.clsTaskDetail.links.listingresults.count {
                let cellForShow = CellForShow()
                cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_ADD_NOTE_ITEM
                cellForShow.typeAction_AT = .ACTION_AT_ADD_NOTE
                cellForShow.note = self.clsTaskDetail.links.listingresults[j]
                
                // check note
                var height:CGFloat = 0
                if  cellForShow.note!.links.image.url != "" {
                    //height = cellForShow.note!.note.heightWithConstrainedWidth(_kWidthOfIphone - 120, font: UIFont(name: kFontLatoRegular, size: 14)!)
                    height = cellForShow.note!.note.heightWithConstrainedWidth(_kWidthOfIphone - 120, font: UIFont.init(name: kFontLatoRegular, size: 14)!)
                }
                else {
                    height = cellForShow.note!.note.heightWithConstrainedWidth(_kWidthOfIphone - 40, font: UIFont.init(name: kFontLatoRegular, size: 14)!)
                }
                
                if cellForShow.note!.note != "" {
                    if height > 18 {
                        height = 2
                    }
                    else if height > 5 {
                        height = 1
                    }
                }
                else{
                    height = 0
                }
                // check succession
                if cellForShow.note!.extendProp.successionname.count > 0 {
                    height += 1
                }
                
                if cellForShow.note!.links.task.id > 0 || cellForShow.note!.location != ""{
                    height += 1
                }
                
                if cellForShow.note!.links.image.url != "" && height < 2 {
                    height = 2
                }
                
                cellForShow.heightForRow = 58 + (height * 15)
                
                sectionforShow.listCellForRow.append(cellForShow)
            }
        }
        
        let cellForShow = CellForShow()
        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_ADD_ITEM_VIEW_ITEM
        cellForShow.typeAction_AT = .ACTION_ADD_ITEM_VIEW_ITEM
        
        
        if rolePermissionNote.addfunc {
            let dictShow = NSDictionary(objects: ["Add a Note", "View all Notes"], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
            cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
            sectionforShow.listCellForRow.append(cellForShow)
        } else {
            let dictShow = NSDictionary(objects: ["View all Notes"], forKeys: ["rightitem" as NSCopying])
            cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
            sectionforShow.listCellForRow.append(cellForShow)
        }
        
        return sectionforShow
    }
    
    fileprivate func createHeaderTimeline() -> SectionForRow {
        let sectionforShow:SectionForRow = SectionForRow()
        sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.JustShowFooter
        sectionforShow.isTimelineSection = true
        var cellForShow:CellForShow = CellForShow()
        cellForShow = CellForShow()
        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEHEADER
        
        sectionforShow.listCellForRow.append(cellForShow)
        
        if listEvent.count > 0 {
            for i in 0..<listEvent.count {
                
                if (self.listEvent[i].eventtype == "CREATED") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "created", "Planting", self.listEvent[i].fromuser.firstname + " created " + " Task" ], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "ASSIGNED") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    
                    
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    
                    var toUser = self.listEvent[i].touser.firstname
                    if (self.listEvent[i].touser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        toUser = "you";
                        if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                            toUser = "yourself";
                        }
                    }
                    
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "assigned", "Planting", self.listEvent[i].fromuser.firstname + " assigned " + "this" + " task to " + toUser , toUser], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying, "touser" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "UPDATED") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    var stringNewValue = ""
                    if self.listEvent[i].contentupdate.newValueUnit != ""{
                        stringNewValue = "\(self.listEvent[i].contentupdate.newValue) \(self.listEvent[i].contentupdate.newValueUnit)"
                    }
                    else {
                        stringNewValue = self.listEvent[i].contentupdate.newValue
                    }
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "updated", "Planting", self.listEvent[i].fromuser.firstname + " updated " + "\(self.listEvent[i].contentupdate.fieldName)" + " to" + "\(stringNewValue)", self.listEvent[i].contentupdate.fieldName, stringNewValue], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying, "fieldName" as NSCopying, "newValue" as NSCopying])
                    
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "COMMENT") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_COMMENTINTIMELINE
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["ADACA9", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "updated", "Planting", self.listEvent[i].content], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying, "comment" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "ADD_SUCCESSION") {
                    
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["ADACA9", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "addsuccession", "Planting", self.listEvent[i].fromuser.firstname + " added Succession to this task" ], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = false
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)

                    for k in 0..<self.listEvent[i].successions.count {
                        let cellForShow:CellForShow = CellForShow()
                        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_SUCCESSONTIMELINE
                        cellForShow.typeAction_AT = .ACTION_AT_SUCCESSIONTIMELINE
                        var detailSuccession = ""
                        if (self.listEvent[i].successions[k].dateplanted != 0) {
                            detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted))
                        }
                        if self.listEvent[i].successions[k].locationformat != "" {
                            if self.listEvent[i].successions[k].extendProp.sitename != "" {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + self.listEvent[i].successions[k].extendProp.sitename + ", " + self.listEvent[i].successions[k].locationformat
                            } else {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + self.listEvent[i].successions[k].locationformat
                            }
                        } else {
                            if (self.listEvent[i].successions[k].bedft > 0) {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + "\(self.listEvent[i].successions[k].bedft)" + " Bed ft"
                            }
                        }
                        let dictShow = NSDictionary(objects: ["ADACA9", self.listEvent[i].successions[k].imagename, self.listEvent[i].successions[k].parentname, self.listEvent[i].successions[k].varietyname, "Planting", detailSuccession, "\(self.listEvent[i].successions[k].id)"], forKeys: [ "color" as NSCopying, "imgsuccession" as NSCopying,"parentname" as NSCopying, "varietyname" as NSCopying, "tasktype" as NSCopying,"detail" as NSCopying, "successionid" as NSCopying])
                        cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                        cellForShow.isShowLineBottomFull = false
                        if k == self.listEvent[i].successions.count {
                            cellForShow.isShowLineBottomFull = true
                        }
                        sectionforShow.listCellForRow.append(cellForShow)
                        
                    }
                    
                } else if (self.listEvent[i].eventtype == "UNASSIGNED") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    
                    var toUser = self.listEvent[i].touser.firstname
                    if (self.listEvent[i].touser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        toUser = "you";
                        if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                            toUser = "yourself";
                        }
                    }
                    
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "unassigned", "Planting", self.listEvent[i].fromuser.firstname + " unassigned " + "Planting" + " from this task" , toUser], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying, "touser" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "REMOVE_SUCCESSION") {
                    
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["ADACA9", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "removesuccession", "Planting", self.listEvent[i].fromuser.firstname + " removed Succession from this task" ], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = false
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                    for k in 0..<self.listEvent[i].successions.count {
                        let cellForShow:CellForShow = CellForShow()
                        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_SUCCESSONTIMELINE
                        cellForShow.typeAction_AT = .ACTION_AT_SUCCESSIONTIMELINE
                        var detailSuccession = ""
                        if (self.listEvent[i].successions[k].dateplanted != 0) {
                            detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted))
                        }
                        if self.listEvent[i].successions[k].locationformat != "" {
                            if self.listEvent[i].successions[k].extendProp.sitename != "" {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + self.listEvent[i].successions[k].extendProp.sitename + ", " + self.listEvent[i].successions[k].locationformat
                            } else {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + self.listEvent[i].successions[k].locationformat
                            }
                        } else {
                            if (self.listEvent[i].successions[k].bedft > 0) {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + "\(self.listEvent[i].successions[k].bedft)" + " Bed ft"
                            }
                        }
                        let dictShow = NSDictionary(objects: ["ADACA9", self.listEvent[i].successions[k].imagename, self.listEvent[i].successions[k].parentname, self.listEvent[i].successions[k].varietyname, "Planting", detailSuccession, "\(self.listEvent[i].successions[k].id)"], forKeys: [ "color" as NSCopying, "imgsuccession" as NSCopying,"parentname" as NSCopying, "varietyname" as NSCopying, "tasktype" as NSCopying,"detail" as NSCopying, "successionid" as NSCopying])
                        cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                        cellForShow.isShowLineBottomFull = false
                        if k == self.listEvent[i].successions.count {
                            cellForShow.isShowLineBottomFull = true
                        }
                        sectionforShow.listCellForRow.append(cellForShow)
                        
                    }
                    
                    
                } else if (self.listEvent[i].eventtype == "LINKED_TASK") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["ADACA9", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "linkedsuccession", "Planting", self.listEvent[i].fromuser.firstname + " linked this task with a succession" ], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = false
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                    for k in 0..<self.listEvent[i].successions.count {
                        let cellForShow:CellForShow = CellForShow()
                        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_SUCCESSONTIMELINE
                        cellForShow.typeAction_AT = .ACTION_AT_SUCCESSIONTIMELINE
                        var detailSuccession = ""
                        if (self.listEvent[i].successions[k].dateplanted != 0) {
                            detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted))
                        }
                        if self.listEvent[i].successions[k].locationformat != "" {
                            if self.listEvent[i].successions[k].extendProp.sitename != "" {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + self.listEvent[i].successions[k].extendProp.sitename + ", " + self.listEvent[i].successions[k].locationformat
                            } else {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + self.listEvent[i].successions[k].locationformat
                            }
                        } else {
                            if (self.listEvent[i].successions[k].bedft > 0) {
                                detailSuccession = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].successions[k].dateplanted)) + ", " + "\(self.listEvent[i].successions[k].bedft)" + " Bed ft"
                            }
                        }
                        let dictShow = NSDictionary(objects: ["ADACA9", self.listEvent[i].successions[k].imagename, self.listEvent[i].successions[k].parentname, self.listEvent[i].successions[k].varietyname, "Planting", detailSuccession, "\(self.listEvent[i].successions[k].id)"], forKeys: [ "color" as NSCopying, "imgsuccession" as NSCopying,"parentname" as NSCopying, "varietyname" as NSCopying, "tasktype" as NSCopying,"detail" as NSCopying, "successionid" as NSCopying])
                        cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                        cellForShow.isShowLineBottomFull = false
                        if k == self.listEvent[i].successions.count {
                            cellForShow.isShowLineBottomFull = true
                        }
                        sectionforShow.listCellForRow.append(cellForShow)
                        
                    }
                   
                } else if (self.listEvent[i].eventtype == "CHANGED_DATE") {
                    
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    
                    let toDate = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].todate))
                    let fromDate = Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].fromdate))
                    
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "changedate", "Planting", self.listEvent[i].fromuser.firstname + " changed the due date from " + fromDate + " to " + toDate , toDate, fromDate], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying, "todate" as NSCopying, "fromdate" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "PLANT_DATE") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "plantdate", "Planting", self.listEvent[i].fromuser.firstname + " updated due to date because the task is linked with the succession's planting date and the planting date was updated"], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "PHOTO_ADDED") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "addphoto", "Planting", self.listEvent[i].fromuser.firstname + " added a photo"], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                    if (self.listEvent[i].content != "") {
                        let cellForShow:CellForShow = CellForShow()
                        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TAKEPHOTO
                        cellForShow.listImages = NSMutableArray();
                        let image: CImage = CImage();
                        image.url = self.listEvent[i].thumbnail.size379x379
                        cellForShow.listImages.add(image);
                        cellForShow.totalCurrentImage = cellForShow.listImages.count;
                        let dictShow = NSDictionary(objects: ["7DA040", "F5F3EF"], forKeys: [ "color" as NSCopying, "background" as NSCopying])
                        
                        cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                        cellForShow.isShowLineBottomFull = true
                        sectionforShow.listCellForRow.append(cellForShow)
                    }
                    
                } else if (self.listEvent[i].eventtype == "TEMPLATE") {
                    
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    
                    let dictShow = NSDictionary(objects: ["7DA040", self.listEvent[i].fromuser.firstname, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "template", "Planting",  "This task is part of" + self.listEvent[i].fromuser.firstname], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "COMPLETED_TASK") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "completed", "Planting", self.listEvent[i].fromuser.firstname + " completed this task" ], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "UNCOMPLETED_TASK") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "uncompleted", "Planting", self.listEvent[i].fromuser.firstname + " uncompleted this task" ], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "ADD_LABOR_TIME") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "addlabor", "Planting", self.listEvent[i].fromuser.firstname + self.listEvent[i].content, self.listEvent[i].content], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying, "content" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                } else if (self.listEvent[i].eventtype == "CHANGE_LABOR_TIME") {
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    var fromUser = self.listEvent[i].fromuser.firstname
                    if (self.listEvent[i].fromuser.id == ClassGlobal.sharedInstance().tendUser.id) {
                        fromUser = "You";
                    }
                    let dictShow = NSDictionary(objects: ["7DA040", fromUser, Utilities.convertDateTimeToFullStyleString(forTask: Utilities.date(fromTimestamp: self.listEvent[i].createddate)), "addlabor", "Planting", self.listEvent[i].fromuser.firstname + self.listEvent[i].content, self.listEvent[i].content], forKeys: [ "color" as NSCopying, "fromuser" as NSCopying,"date" as NSCopying, "eventType" as NSCopying, "tasktype" as NSCopying,"fullstring" as NSCopying, "content" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                }
                else if (self.listEvent[i].eventtype == "NOTE_ADDED"){
                    let cellForShow:CellForShow = CellForShow()
                    cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TIMELINEONTASK
                    
                    cellForShow.isShowLineBottomFull = true
                    cellForShow.isShowLineTop = true
                    sectionforShow.listCellForRow.append(cellForShow)
                    
                    let note = SW_Note();
                    note?.createdby = self.listEvent[i].fromuser.firstname;
                    note?.note = self.listEvent[i].content;
                    note?.createdbyuserid = self.listEvent[i].fromuser.id;
                    note?.action = self.listEvent[i].eventtype;
                    
                    if (self.listEvent[i].thumbnail.size379x379 != "") {
                        note?.links.image.url = self.listEvent[i].thumbnail.size379x379;
                        
                    }
                    note?.date = self.listEvent[i].createddate;
                }
            }
        }
        
        
        
        
        
        
//        var cellForShowComment:CellForShow = CellForShow()
//        cellForShowComment = CellForShow()
//        cellForShowComment.typeCell_AT = AddTaskTypeCell.CELL_AT_ADDCOMMENTONTASK
//        cellForShowComment.typeAction_AT = .ACTION_AT_ADDCOMMENTONTASK
//        let dictShowComment = NSDictionary(objects: [""], forKeys: [ "comment"])
//        cellForShowComment.dictionaryShow_AT = dictShowComment as [NSObject : AnyObject]
//        
//        sectionforShow.listCellForRow.append(cellForShowComment)

        return sectionforShow
    }
    
    fileprivate func createTimelineForTask() -> SectionForRow {
        let sectionforShow:SectionForRow = SectionForRow()
        sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.JustShowFooter
        var cellForShow:CellForShow = CellForShow()
        cellForShow = CellForShow()
        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_ADDCOMMENTONTASK
        cellForShow.typeAction_AT = .ACTION_AT_ADDCOMMENTONTASK
        let dictShow = NSDictionary(objects: [""], forKeys: ["comment" as NSCopying])
        cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
        
        sectionforShow.listCellForRow.append(cellForShow)
        
        return sectionforShow
    }
    
    fileprivate func createAddComment() -> SectionForRow {
        let sectionforShow:SectionForRow = SectionForRow()
        sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.JustShowFooter
        var cellForShow:CellForShow = CellForShow()
        cellForShow = CellForShow()
        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_ADDCOMMENTONTASK
        cellForShow.typeAction_AT = .ACTION_AT_ADDCOMMENTONTASK
        let dictShow = NSDictionary(objects: [""], forKeys: [ "comment" as NSCopying])
        cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
        
        sectionforShow.listCellForRow.append(cellForShow)
        
        return sectionforShow
    }
    
    fileprivate func createPhoto() -> SectionForRow {
        let sectionforShow:SectionForRow = SectionForRow()
        sectionforShow.showHideHeader_Footer_Tye = Show_Hide_Header_FooterType.ShowAll_Header_Footer
        sectionforShow.iconSectionTitle = NameIconSectionTitle.NONE
        sectionforShow.titleName = "PHOTOS"
        sectionforShow.sectionName = .sectionPhoto
        
        let cellForShow = CellForShow()
        cellForShow.typeCell_AT = AddTaskTypeCell.CELL_AT_TAKEPHOTO
        cellForShow.valueSelected_AT = ""
        if clsTaskDetail.id > 0 && clsTaskDetail.links.images.count > 0 {
            cellForShow.listImages = NSMutableArray()
            for  i in 0..<self.clsTaskDetail.links.images.count {
                cellForShow.listImages.add(self.clsTaskDetail.links.images[i])
            }
            cellForShow.totalCurrentImage = cellForShow.listImages.count
        }
        else if clsSuccessionDetail.id > 0 && self.clsSuccessionDetail.links.images.count > 0{
            cellForShow.listImages = NSMutableArray()
            for  i in 0  ..< self.clsSuccessionDetail.links.images.count {
                cellForShow.listImages.add(self.clsSuccessionDetail.links.images[i])
            }
            cellForShow.totalCurrentImage = cellForShow.listImages.count
            
        }
        sectionforShow.listCellForRow.append(cellForShow)
        return sectionforShow
    }
    
    fileprivate func updateChangeDateValue(_ typeCell_AT:AddTaskTypeCell, typeAction_AT:AddTaskTypeAction, valueChange:Double){
        switch(typeCell_AT){
            
        case .Cell_NameType_ID:
            switch(typeAction_AT){
                
            case .ACTION_AT_ADD_GH_STARTDATE:
                
                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    dateIntervalSeedingDate = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    keeptrackEditField.numberDayToTransplantEdit = numberDayToTransplant
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                    dateIntervalSeedingDate = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    keeptrackEditField.numberDayToTransplantEdit = numberDayToTransplant
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    dateIntervalSeedingDate = valueChange
                    dateIntervalDayToTransplant = dateIntervalSeedingDate
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S{
                        dateIntervalDayToTransplant = dateIntervalSeedingDate + numberDayToTransplant*86400000
                        
                    }
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                        dateIntervalDatePlantingLabor = dateIntervalSeedingDate
                    }
                    else {
                        dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                    }
                    
                    
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                } else {
                    dateIntervalSeedingDate = valueChange
                    dateIntervalDayToTransplant = dateIntervalSeedingDate
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S{
                        dateIntervalDayToTransplant = dateIntervalSeedingDate + numberDayToTransplant*86400000
                        
                    }
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                        dateIntervalDatePlantingLabor = dateIntervalSeedingDate
                    }
                    else {
                        dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                    }
                    
                    
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                }
                
                dateIntervalDatePlantingLabor = dateIntervalSeedingDate
                
                if clsTaskDetail.id != 0 {
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.maximumFractionDigits = 2
                    let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
                    var totalLabor = "Labor Time"
                    if (self.totalTime != 0) {
                        totalLabor = stringTemp! + "h"
                    }
                    var stringDate = ""
                    if dateIntervalDatePlantingLabor == 0 {
                        stringDate = "Due Date"
                    } else {
                        stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
                    }
                    var assignee = "Unassigned";
                    var stringImage = "";
                    if (listUserAssigned.count > 0) {
                        if (listUserAssigned.count == 1) {
                            assignee = listUserAssigned[0].firstname;
                            stringImage = listUserAssigned[0].linkPhotoName;
                        } else {
                            assignee = "Multiple";
                        }
                    }
                    
                    self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
                }
                
                keeptrackEditField.dateIntervalSeedingDateEdit = dateIntervalSeedingDate
                
                break
            case .ACTION_AT_ADD_DAY_IN_GH:
                numberDayToTransplant = valueChange
                
                if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S{
                    dateIntervalDayToTransplant = dateIntervalSeedingDate + numberDayToTransplant*86400000
                    
                }
                if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    dateIntervalDatePlantingLabor = dateIntervalSeedingDate
                }
                else {
                    dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                }
                dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                }
                keeptrackEditField.numberDayToTransplantEdit = numberDayToTransplant
                keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                break
            case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                
                
                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    dateIntervalDayToTransplant = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                    dateIntervalDayToTransplant = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    dateIntervalDayToTransplant = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                } else {
                    dateIntervalDayToTransplant = valueChange
                    dateIntervalSeedingDate = dateIntervalDayToTransplant - numberDayToTransplant*86400000
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalSeedingDateEdit = dateIntervalSeedingDate
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                }
                
                keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                
                if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    dateIntervalDatePlantingLabor = dateIntervalSeedingDate
                }
                else {
                    dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                }
                
                if clsTaskDetail.id != 0 {
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.maximumFractionDigits = 2
                    let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
                    var totalLabor = "Labor Time"
                    if (self.totalTime != 0) {
                        totalLabor = stringTemp! + "h"
                    }
                    var stringDate = ""
                    if dateIntervalDatePlantingLabor == 0 {
                        stringDate = "Due Date"
                    } else {
                        stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
                    }
                    var assignee = "Unassigned";
                    var stringImage = "";
                    if (listUserAssigned.count > 0) {
                        if (listUserAssigned.count == 1) {
                            assignee = listUserAssigned[0].firstname;
                            stringImage = listUserAssigned[0].linkPhotoName;
                        } else {
                            assignee = "Multiple";
                        }
                    }
                    //self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
                    
                    self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
                }
                
                
                break
            case .ACTION_AT_ADD_PLANTINGDATE:
                
                dateIntervalDayToTransplant = valueChange
                if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    dateIntervalDatePlantingLabor = dateIntervalSeedingDate
                }
                else {
                    dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                }
                dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                }
                
                if clsTaskDetail.id != 0 {
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.maximumFractionDigits = 2
                    let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
                    var totalLabor = "Labor Time"
                    if (self.totalTime != 0) {
                        totalLabor = stringTemp! + "h"
                    }
                    var stringDate = ""
                    if dateIntervalDatePlantingLabor == 0 {
                        stringDate = "Due Date"
                    } else {
                        stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
                    }
                    var assignee = "Unassigned";
                    var stringImage = "";
                    if (listUserAssigned.count > 0) {
                        if (listUserAssigned.count == 1) {
                            assignee = listUserAssigned[0].firstname;
                            stringImage = listUserAssigned[0].linkPhotoName;
                        } else {
                            assignee = "Multiple";
                        }
                    }
                    
                    self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
                }
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST:
                numberDayToFirstHarvest = valueChange
                dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                }
                
                keeptrackEditField.numberDayToFirstHarvestEdit = numberDayToFirstHarvest
                keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                
                
                break
            case .ACTION_AT_ADD_FIRST_HARVEST_CELL:
                
                
                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    dateIntervalDayToFirstHarvest = valueChange
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                    dateIntervalDayToFirstHarvest = valueChange
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                } else if  (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    dateIntervalDayToFirstHarvest = valueChange
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                } else  {
                    dateIntervalDayToFirstHarvest = valueChange
                    dateIntervalDayToTransplant = dateIntervalDayToFirstHarvest - numberDayToFirstHarvest*86400000
                    dateIntervalSeedingDate = dateIntervalDayToTransplant - numberDayToTransplant*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalSeedingDateEdit = dateIntervalSeedingDate
                    keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                }
                keeptrackEditField.numberDayToFirstHarvestEdit = numberDayToFirstHarvest
                keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                break
            case .ACTION_AT_ADD_LAST_HARVEST:
                numberDayToLastHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToLastHarvest), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                dateIntervalDayToLastHarvest = valueChange
                keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                break
                
            case .ACTION_AT_ADD_TAKE_DOWNDATE:
                dateIntervalDayToFirstHarvest = valueChange
                keeptrackEditField.dateIntervalTakeDownEdit = dateIntervalDayToFirstHarvest
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                }
                keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                break
            default:
                break
            }
        case .Cell_NameType_TwoRight:
            switch(typeAction_AT){
                
            case .ACTION_AT_ADD_DAY_IN_GH:
                numberDayToTransplant = valueChange
                if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S || successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S{
                    dateIntervalDayToTransplant = dateIntervalSeedingDate + numberDayToTransplant*86400000
                    keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                    
                }
                dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                }
                keeptrackEditField.numberDayToTransplantEdit = numberDayToTransplant
                keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                break
            case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                
                
                if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                    dateIntervalDayToTransplant = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    numberDayToFirstHarvest = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalDayToTransplant), andDate: Utilities.date(fromTimestamp: dateIntervalDayToFirstHarvest)))
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                    dateIntervalDayToTransplant = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                    dateIntervalDayToTransplant = valueChange
                    numberDayToTransplant = Double( Utilities.daysBetweenDate(Utilities.date(fromTimestamp: dateIntervalSeedingDate), andDate: Utilities.date(fromTimestamp: dateIntervalDayToTransplant)))
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                } else {
                    dateIntervalDayToTransplant = valueChange
                    dateIntervalSeedingDate = dateIntervalDayToTransplant - numberDayToTransplant*86400000
                    dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                    if numberDayToLastHarvest > 0 {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                    } else {
                        dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                    }
                    keeptrackEditField.dateIntervalSeedingDateEdit = dateIntervalSeedingDate
                    keeptrackEditField.dateIntervalDayToFirstHarvestEdit = dateIntervalDayToFirstHarvest
                    keeptrackEditField.dateIntervalDayToLastHarvestEdit = dateIntervalDayToLastHarvest
                }
                keeptrackEditField.dateIntervalDayToTransplantEdit = dateIntervalDayToTransplant
                
                if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                    dateIntervalDatePlantingLabor = dateIntervalSeedingDate
                }
                else {
                    dateIntervalDatePlantingLabor = dateIntervalDayToTransplant
                }
                if clsTaskDetail.id != 0 {
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.maximumFractionDigits = 2
                    let stringTemp = nf.string(from: NSNumber.init(value: Double(self.totalTime)/60 as Double))
                    var totalLabor = "Labor Time"
                    if (self.totalTime != 0) {
                        totalLabor = stringTemp! + "h"
                    }
                    var stringDate = ""
                    if dateIntervalDatePlantingLabor == 0 {
                        stringDate = "Due Date"
                    } else {
                        stringDate = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
                    }
                    var assignee = "Unassigned";
                    var stringImage = "";
                    if (listUserAssigned.count > 0) {
                        if (listUserAssigned.count == 1) {
                            assignee = listUserAssigned[0].firstname;
                            stringImage = listUserAssigned[0].linkPhotoName;
                        } else {
                            assignee = "Multiple";
                        }
                    }
                    
                    self.delegate?.setUserProfile!(assignee as NSString, imageUser: stringImage as NSString, date: stringDate as NSString, labor: totalLabor as NSString)
                }
                break
            case .ACTION_AT_ADD_FIRST_HARVEST:
                numberDayToFirstHarvest = valueChange
                dateIntervalDayToFirstHarvest = dateIntervalDayToTransplant + numberDayToFirstHarvest*86400000
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                }
                keeptrackEditField.numberDayToFirstHarvestEdit = numberDayToFirstHarvest
                break
            case .ACTION_AT_ADD_LAST_HARVEST:
                numberDayToLastHarvest = valueChange
                if numberDayToLastHarvest > 0 {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + (numberDayToLastHarvest)*86400000
                } else {
                    dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest
                }
                keeptrackEditField.numberDayToLastHarvestEdit = numberDayToLastHarvest
                break
            default:
                break
            }
        default:
            break
            
        }
        
    }
    fileprivate func updateChangeDateLayout(_ section: SectionForRow){
        
        for i in 0  ..< section.listCellForRow.count
        {
            let cellForShow = section.listCellForRow[i]
            
            switch(cellForShow.typeCell_AT){
                
            case .Cell_NameType_ID:
                switch(cellForShow.typeAction_AT){
                    
                case .ACTION_AT_ADD_GH_STARTDATE:
                    
//                    if clsSuccessionDetail.id == 0 || dateIntervalSeedingDate >= Utilities.convertDateTimeToTimeStamp(NSDate()).doubleValue || clsTaskDetail.id > 0 {
//                        cellForShow.isDontAllowSelected = false
//                    } else {
//                        cellForShow.isDontAllowSelected = true
//                    }
                    
                    let dictShow = NSDictionary(objects: ["GH start date", Utilities.formatDate_Month_Day_Week(dateIntervalSeedingDate)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                
                    if successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_GREENHOUSE || successionSeedingMethoTypeCL == SuccessionSeedingMethodType.TRANSPLANT_PURCHASED_S {
                        let dictShow = NSDictionary(objects: ["Field planting date", Utilities.formatDate_Month_Day_Week(dateIntervalDayToTransplant)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                        cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    }
                    else {
                        let dictShow = NSDictionary(objects: ["Field sowing date", Utilities.formatDate_Month_Day_Week(dateIntervalDayToTransplant)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                        cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                        
                    }
                    
                    
                    break
                
                case .ACTION_AT_ADD_PLANTINGDATE:
                    var dictShow = NSDictionary(objects: ["Planting date", Utilities.formatDate_Month_Day_Week(dateIntervalSeedingDate)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    if successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S {
                        dictShow = NSDictionary(objects: ["Planting date", Utilities.formatDate_Month_Day_Week(dateIntervalDayToTransplant)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    }
                    
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    
                    break
                
                case .ACTION_AT_ADD_FIRST_HARVEST:
                    
                    var stringDays = ""
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                    nf.maximumFractionDigits = 2
                    if numberDayToFirstHarvest == 0{
                        stringDays = "0 days"
                    }
                    else if numberDayToFirstHarvest == 1 {
                        stringDays = "1 day"
                    }
                    else {
                        //                        stringDays = NSString(format: "%.0f days", numberDayToFirstHarvest) as String
                        stringDays = nf.string(from: NSNumber(value: numberDayToFirstHarvest as Double))! + " days"
                    }
                    
                    
                    if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                        cellForShow.isDontAllowSelected = true
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                        cellForShow.isDontAllowSelected = false
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                        cellForShow.isDontAllowSelected = false
                    } else {
                        cellForShow.isDontAllowSelected = false
                    }
                    
                    let dictShow = NSDictionary(objects: ["Days to maturity", stringDays], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_LAST_HARVEST:
                    
                    let dictShow = NSDictionary(objects: ["Last Harvest", Utilities.formatDate_Month_Day_Week(dateIntervalDayToLastHarvest)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    
                    break
                case .ACTION_AT_ADD_FIRST_HARVEST_CELL:
                    let dictShow = NSDictionary(objects: ["First Harvest", Utilities.formatDate_Month_Day_Week(dateIntervalDayToFirstHarvest)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_FIRST_HARVEST_TREE:
                    let dictShow = NSDictionary(objects: ["First Harvest", Utilities.formatDate_Month_Day(dateIntervalDayToFirstHarvest)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_LAST_HARVEST_TREE:
                    
                    let dictShow = NSDictionary(objects: ["Last Harvest", Utilities.formatDate_Month_Day(dateIntervalDayToLastHarvest)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    
                    break
                case .ACTION_AT_ADD_TAKE_DOWNDATE:
                    let dictShow = NSDictionary(objects: ["Take Down Date", Utilities.formatDate_Month_Day_Week(dateIntervalDayToFirstHarvest)], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_DAY_IN_GH:
                    
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                    nf.maximumFractionDigits = 2
                    
                    var stringDays = ""
                    if numberDayToTransplant == 0 {
                        stringDays = "0 days"
                    }
                    else if numberDayToTransplant == 1 {
                        stringDays = "1 day"
                    }
                    else {
//                        stringDays = NSString(format: "%.0f days", numberDayToTransplant) as String
                        stringDays = nf.string(from: NSNumber(value: numberDayToTransplant as Double))! + " days"
                    }
                    
                    
                    if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                        cellForShow.isDontAllowSelected = false
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S)  {
                        cellForShow.isDontAllowSelected = true
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                        cellForShow.isDontAllowSelected = false
                    } else {
                        cellForShow.isDontAllowSelected = false
                    }
                    
                    let dictShow = NSDictionary(objects: ["Days in GH", stringDays], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_FIST_FRUIT_HARVEST:
                    cellForShow.valueSelected_AT = "\(yearForFruitTree)"
                    let dictShow = NSDictionary(objects: ["First fruiting year", cellForShow.valueSelected_AT ], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                default:
                    break
                }
                break
            case .Cell_NameType_TwoRight:
                switch(cellForShow.typeAction_AT){
                    
                case .ACTION_AT_ADD_DAY_IN_GH:
                    var stringDays = ""
                    
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                    nf.maximumFractionDigits = 2
                    
                    if numberDayToTransplant == 0{
                        stringDays = "0 days"
                    }
                    else if numberDayToTransplant == 1 {
                        stringDays = "1 day"
                    }
                    else {
//                        stringDays = NSString(format: "%.0f days", numberDayToTransplant) as String
                        stringDays = nf.string(from: NSNumber(value: numberDayToTransplant as Double))! + " days"
                    }
                    
                    if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                        cellForShow.isDontAllowSelected = false
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                        cellForShow.isDontAllowSelected = true
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                        cellForShow.isDontAllowSelected = false
                    } else {
                        cellForShow.isDontAllowSelected = false
                    }
                    
                    var dictShow = NSDictionary(objects: ["Days in GH", stringDays, "Common: \(self.combinationDataForShow.daystotransplantdatefromghl) - \(self.combinationDataForShow.daystotransplantdatefromghh) days"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                    if self.combinationDataForShow.daystotransplantdatefromghh < self.combinationDataForShow.daystotransplantdatefromghl {
                        dictShow = NSDictionary(objects: ["Days in GH", stringDays, "Common: \(self.combinationDataForShow.daystotransplantdatefromghl) days"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                    }
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    
                    break
                case .ACTION_AT_ADD_FIELD_PLANTINGDATE:
                    let dictShow = NSDictionary(objects: ["Field planting date", Utilities.formatDate_Month_Day_Week(dateIntervalDayToTransplant), "Base on \(Utilities.stringShortDate(fromTimestamp: dateIntervalSeedingDate)) GH start date"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_FIRST_HARVEST:
                    var stringDays = ""
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                    nf.maximumFractionDigits = 2
                    if numberDayToFirstHarvest == 0{
                        stringDays = "0 days"
                    }
                    else if numberDayToFirstHarvest == 1 {
                        stringDays = "1 day"
                    }
                    else {
//                        stringDays = NSString(format: "%.0f days", numberDayToFirstHarvest) as String
                        stringDays = nf.string(from: NSNumber(value: numberDayToFirstHarvest as Double))! + " days"
                    }
                    if clsSuccessionDetail.plantingcompleted && clsSuccessionDetail.extendProp.firstharvestcompleted {
                        cellForShow.isDontAllowSelected = true
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) > 0 || (clsSuccessionDetail.extendProp.sowingcompleted && clsSuccessionDetail.extendProp.transplantghcompleted && successionSeedingMethoTypeCL == SuccessionSeedingMethodType.GREENHOUSE_SOW_S) || (clsSuccessionDetail.extendProp.sowingcompleted && successionSeedingMethoTypeCL != SuccessionSeedingMethodType.GREENHOUSE_SOW_S) {
                        cellForShow.isDontAllowSelected = false
                    } else if (Utilities.date(fromTimestamp: dateIntervalDayToTransplant) as NSDate).days(before: Date()) <= 0 && (Utilities.date(fromTimestamp: dateIntervalSeedingDate) as NSDate).days(before: Date()) > 0 {
                        cellForShow.isDontAllowSelected = false
                    } else {
                        cellForShow.isDontAllowSelected = false
                    }
                    let dictShow = NSDictionary(objects: ["Days to maturity", stringDays, "First harvest: \(Utilities.formatDate_Month_Day_Week(dateIntervalDayToFirstHarvest))"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                case .ACTION_AT_ADD_LAST_HARVEST:
                    var stringDays = ""
                    let nf = NumberFormatter()
                    nf.numberStyle = NumberFormatter.Style.decimal
                    nf.roundingMode = NumberFormatter.RoundingMode.halfUp
                    nf.maximumFractionDigits = 2
                    if numberDayToLastHarvest == 0{
                        stringDays = "0 days"
                    }
                    else if numberDayToLastHarvest == 1 {
                        stringDays = "1 day"
                    }
                    else {
//                        stringDays = NSString(format: "%.0f days", numberDayToLastHarvest) as String
                        stringDays = nf.string(from: NSNumber(value: numberDayToLastHarvest as Double))! + " days"
                    }
                    
                    let dictShow = NSDictionary(objects: ["Harvest window", stringDays, "Last harvest: \(Utilities.formatDate_Month_Day_Week(dateIntervalDayToLastHarvest))"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                    break
                default:
                    break
                }
                break
                
            default:
                break
                
            
            }
            
            
            
        }
        self.updateChangeDate_AtCellName(self.listSectionSuccession)
        self.updateChangeDate_AtCellUserInformation(self.listSectionSuccession)
    }
    
    fileprivate func updateChangeDate_AtCellName(_ listSecction:[SectionForRow]){
        for i in 0  ..< listSecction.count {
            let sectionforShow = listSecction [i]
            for j in 0  ..< sectionforShow.listCellForRow.count
            {
                let cellForShow = sectionforShow.listCellForRow[j]
                if cellForShow.typeCell_AT == AddTaskTypeCell.CELL_AT_NAME {
                    var stringDate = "\(Utilities.stringShortDate(fromTimestamp: dateIntervalDayToTransplant)) - \(Utilities.stringShortDate(fromTimestamp: dateIntervalDayToLastHarvest))"
                    if categoryID == CategoryEnum.fruits.rawValue || categoryID == CategoryEnum.trees_.rawValue ||  categoryID == CategoryEnum.vines.rawValue {
                        stringDate = ""
                    }
                    let dicShow = cellForShow.dictionaryShow_AT as NSDictionary
                    
                    cellForShow.dictionaryShow_AT =  NSDictionary(objects: [dicShow.value(forKey: "name") as! String, dicShow.value(forKey: "acronym")  as! String, stringDate], forKeys: ["name" as NSCopying,"acronym" as NSCopying,"date" as NSCopying]) as! [AnyHashable : Any] as [AnyHashable: Any] as NSDictionary
                }
                if cellForShow.typeAction_AT == AddTaskTypeAction.ACTION_AT_ADD_GROWING_CYCLE {
                    var dictShow = NSDictionary(objects: ["Growing cycle", cellForShow.valueSelected_AT], forKeys: [ "leftitem" as NSCopying, "rightitem" as NSCopying])
                    if cellForShow.valueSelected_AT == "Biennial" {
                        cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_TwoRight
                        dictShow = NSDictionary(objects: ["Growing cycle", cellForShow.valueSelected_AT, "Finishes in \(Utilities.getYearFrom_datetime_(forTree: dateIntervalDayToLastHarvest) + 1)"], forKeys: [ "leftitem" as NSCopying, "rightitem1" as NSCopying, "rightitem2" as NSCopying])
                    } else {
                        cellForShow.typeCell_AT = AddTaskTypeCell.Cell_NameType_ID
                    }
                    
                    cellForShow.dictionaryShow_AT = dictShow as NSDictionary as NSDictionary
                }
            }
        }
        
    }
    
    fileprivate func updateChangeDate_AtCellUserInformation(_ listSecction:[SectionForRow]){
        for i in 0  ..< listSecction.count {
            let sectionforShow = listSecction [i]
            for j in 0  ..< sectionforShow.listCellForRow.count
            {
                let cellForShow = sectionforShow.listCellForRow[j]
                if cellForShow.typeCell_AT == AddTaskTypeCell.CELL_AT_INFOMATION {
                    
                    let stringDate : String = Utilities.convertDateTime(toString: Utilities.date(fromTimestamp: dateIntervalDatePlantingLabor))
                    let hour : Int = laborTime / 3600
                    let min : Int = (laborTime - (hour * 3600))/60
                    let stringLabor : String = "\(hour)h \(min)m"
                    let numberOfPepoleAfter : String = cellForShow.dictSelected_AT.value(forKey: "numberOfPepole") as! String
                    let percentOfAreaAfter : String = cellForShow.dictSelected_AT.value(forKey: "percentOfArea") as! String
                    
                    let dictShow : NSDictionary = NSDictionary(objects: ["\(dateIntervalDatePlantingLabor)", stringDate, "\(laborTime)", stringLabor, numberOfPepoleAfter, percentOfAreaAfter], forKeys: ["dateTimetamp" as NSCopying, "dateShow" as NSCopying, "laborValue" as NSCopying, "laborShow" as NSCopying ,"numberOfPepole" as NSCopying, "percentOfArea" as NSCopying])
                    cellForShow.dictionaryShow_AT = dictShow
                    cellForShow.dictSelected_AT = dictShow
                    cellForShow.isHaveData_AT = true
                }
            }
        }
    }
    
    func checkExistSeedCompanyName(_ seedSourceName:String) -> Bool{
        if listSeedSource.count == 0{
            return false
        }
        else {
            for i in 0..<listSeedSource.count {
                if seedSourceName.lowercased() == listSeedSource[i].name.lowercased() {
                    return true
                }
            }
        }
        return false
    }

    func dictSaveSeedCompany(_ inputname:String) -> NSDictionary? {
        return NSDictionary(objects: [ClassGlobal.sharedInstance().tendUser.farmID, inputname], forKeys: ["farmid" as NSCopying, "name" as NSCopying ])
    }
}

class ClassObjectColumn:NSObject{
    var columnName1: String = ""
    var columnName2: String = ""
    var placeholderColumn: String = ""
    var columnNumberValue: String = ""
    var columnDropdown1:String = ""
    var columnDropdown2:String = ""
    var haveNumberDropdown = 0
    var columnIndex:Int = 1
    var listDropDown1:[String]? = [String]()
    var listDropDown2:[String]? = [String]()
    
    init (columnName1s: String, columnName2s: String, placeholderColumns: String, columnNumberValues: String, columnDropdown1s:String, columnDropdown2s:String, haveNumberDropdowns:Int, listDropDown1s:[String]?, listDropDown2s:[String]?, columnIndex:Int){
        self.columnName1 = columnName1s
        self.columnName2 = columnName2s
        self.placeholderColumn = placeholderColumns
        self.columnNumberValue = columnNumberValues
        self.columnDropdown1 = columnDropdown1s
        self.columnDropdown2 = columnDropdown2s
        self.haveNumberDropdown = haveNumberDropdowns
        self.listDropDown1 = listDropDown1s
        self.listDropDown2 = listDropDown2s
        self.columnIndex = columnIndex
    }
}

class ClassEditKeepTrack: NSObject{
    var noteInTaskEdit: String = ""
    var plantingAmountEdit:String = ""
    var plantingAmountUnitEdit:String = ""
    var seedingRateNumberEdit:String = ""
    var seedingRateUnitEdit:String = ""
    var inrowspacingNumberEdit:String = ""
    var rowperbedNumverEdit:String = ""
    var betweenrowspacingNumberEdit:String = ""
    var oneworddescriptionEdit:String = ""
    var flatpot_value: String = ""
    var flatpot_unit: String = ""
    var totalofTransplent: String = ""
    var dateIntervalDayToFirstHarvestTree: Double = 0
    var dateIntervalDayToLastHarvestTree: Double = 0
    var dateIntervalDayToFirstHarvestEdit: Double = 0
    var dateIntervalDayToLastHarvestEdit: Double = 0
    var dateIntervalTakeDownEdit: Double = 0
    
    var seedingRateBSKeepEdit: String = ""
    var seedingRateBSUnitKeepEdit: String = ""
    
    //Flat
    var numberOfFlat:Double = 0
    var numberOfPlant:Double = 0
    var estimatedloss:Double = 0
    
    // add more to keep track
    var numberYearForFruistTreeEdit: Int = 0
    var seedPerCell_value: String = ""
    var seedPerCell_unit: String = ""
    var dateIntervalSeedingDateEdit: Double = 0
    var numberDayToTransplantEdit: Double = 0
    var dateIntervalDayToTransplantEdit: Double = 0
    var numberDayToLastHarvestEdit: Double = 0
    var numberDayToFirstHarvestEdit: Double = 0
    
    //Harvest section
    var harvestUnitEdit: String = ""
    var avgYieldRateEditValue: String = ""
    var avgYieldRateEditValueNumber: String = ""
    var avgYieldRateEditUnit1: String = ""
    var avgYieldRateEditUnit2: String = ""
    var avgSalesPriceEditValue: String = ""
    var avgSalesPriceEditValueNumber: String = ""
    var avgSalesPriceEditUnit: String = ""
    
    //Harvest section expend
    var harvestFrequencyEditValue: String = ""
    var harvestFrequencyEditValueNumber: String = ""
    var harvestFrequencyEditUnit: String = ""
    var seedSourceEdit: String = ""
    var organicSeedEidt: String = ""
    var seedCoatingEdit: String = ""
    var growingCycleEdit: String = ""
    var grownInaHoophouseEdit: String = ""
    var rootstockEdit: String = ""
    var rootstockSuccession: String = ""
    var harvestSeasonEdit: String = ""
    
    var plantingMethodEdit: SuccessionSeedingMethodType = SuccessionSeedingMethodType.NONE_SEEDINGMETHOD
    var growingMethodEdit: String = ""
    var harvestStageEdit: String = ""
}

@objc class ClassTrackingSuccession: MTLModel, MTLJSONSerializing{
    var id:Int = 0
    var rowperbed:Bool = false
    var seedingrate:Bool = false
    var seedingrateunitid:Bool = false
    var inrowspacing:Bool = false
    static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        return Dictionary()
    }
    
    //MARK: NSObjectProtocol hack
    override func isEqual(_ object: Any?) -> Bool {
        return super.isEqual(object)
    }
    
    override func `self`() -> Self {
        return self
    }
}

