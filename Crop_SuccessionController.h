//
//  Crop_SuccessionController.h
//  TendGrow
//
//  Created by hungnguyeniOS on 9/23/15.
//  Copyright (c) 2015 spiraledge.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMasterCropSuccession.h"

@class CSuccession;
@protocol Crop_SuccessionEventHandler<NSObject>

@optional;
-(void)didEndReturnListObjectTaskPlant:(NSMutableArray *)listObject;

-(void)showListCropVariety:(int)atIndex;

-(void)showListLocation:(int)atIndex andIdType:(int)idType andisGH:(BOOL)isGreenHouse andStartDate:(double)startDate andEndDate:(double)endDate;

-(void)showDatePicker:(NSDate*)dateField atIndex:(int)atIndex mimDate:(NSDate*)minDate maxDate:(NSDate*)maxDate;

-(void)reloadTableViewWithNewData:(NSArray *)listData;

-(void)showPickerGrowingMethod:(int)atIndex andId:(NSString*)valueSelected;

-(void)showPickerPlantingMethod:(int)atIndex andId:(NSString*)valueSelected;

-(void)loadCombination:(NSString *)cropId andPlantingID:(NSString *)idMethod;

-(void)showPicker:(int)atIndex andStringSelect:(NSString *)valueSelect andStringSelect:(NSArray *)listSelect;

-(void)editNumber:(int)atIndex andTitle:(NSString *)titleNav andShowHeader:(BOOL)isShowHeader;

-(void)showMessage:(NSString *)stringShow;

-(void)editNote:(int)atIndex andNoteId:(int)noteid;
-(void)showSeedingRate:(int)atCellIndex atValue:(NSString *)valueString andRateId:(NSString *)unitid andSeedingRateLow:(float)seedinglow andSeedingRateHigh:(float)seedinghigh;

@end
@interface Crop_SuccessionController : NSObject

@property (nonatomic, weak) NSObject <Crop_SuccessionEventHandler> *eventHandler;
@property (nonatomic, strong) CMasterCropCombinationData * combinationDataForShow;
@property (nonatomic, assign) SuccessionSeedingMethodType successionSeedingMethoTypeCL;

@property (nonatomic, strong)  NSString * varietyNameCL;
@property (nonatomic, strong)  NSString * imageVarietyNameCL;
@property (nonatomic, strong)  NSString * plantingMethodFirstLoad;
@property (nonatomic, strong)  NSString * masterCropId;
@property (nonatomic, strong)  NSString * croptypeId;
@property (nonatomic, assign)  BOOL setDefault;
@property (nonatomic, retain)  CSuccession* clsSuccessionDetail;
@property (nonatomic, retain)  NSMutableArray * listLocationSelected;
@property (nonatomic, retain)  NSMutableArray * listLocationSelectedGreenHouse;
@property (nonatomic, assign) BOOL  isSelectItem;
@property (nonatomic, assign) BOOL  isPopulate;
@property (nonatomic, assign)  int categoryId;



-(void)createListObjectiveAddSuccession:(NSString *)nameCrop;

-(void)didSelectedAtIndex:(int)index;

-(void)setDataForIndex:(int)atIndex withData:(NSString *)valueString andDict:(NSDictionary *)valueDict andStringReturn:(NSString *)stringReturn;

-(void)setDataWhenChangeSeedingMethod:(SuccessionSeedingMethodType )newMethod andIndex:(int)atIndex withData:(NSString *)valueString andDict:(NSDictionary *)valueDict andStringReturn:(NSString *)stringReturn;

-(void)setDataWhenChangeGrowingMethod:(int) atIndex withData:(NSString *)valueString;

-(NSArray *)returnListLocation;

-(NSArray *)returnListLocationGreenHouse;
-(void)setDataWhenSelectedLocation:(NSArray *)listField andAtIndex:(int)index;

-(void)setDataWhenSelectedLocationGH:(NSArray *)listField andAtIndex:(int)index;

-(void)setDataWhenEditCombination:(NSString *)keyEdit andString:(NSString *)stringEdit andAtIndex:(int)atIndex;
-(void)validateRequiredField:(void(^)(BOOL success, int index))complete;

-(CSuccession *) successionReturn;
-(NSDictionary *)dictReturn;
-(void)reloadWithNewData;
-(void) showImage:(UIImage *)img atIndex:(int)index;
-(NSMutableArray*)getPhoto;
-(int)indexWithTypeAction:(AddTaskTypeAction)typeAction;
-(void)clearDataShow;
-(void)removeNoteId:(int)noteid;
@end
