
//
//  CropController.swift
//  TendGrow
//
//  Created by nvhieu on 7/13/15.
//  Copyright (c) 2015 spiraledge.com. All rights reserved.
//

import Foundation
import UIKit

class DateChartByCrop {
    var value: Int = 0
    var date: Date = Date()
}

@objc protocol CropControllerDelegate {
    @objc optional func CropControllerDelegate_returnedQSResults (_ returnedValue: QuickSearchResultsModel?)

    @objc optional func CropControllerDelegate_returnedChartData (_ returnedValue: [PointValue], detailType: Int, message: String)
//    optional func CropControllerGetChartViewSuccess(datesChart: [DateChartByCrop]?)
    @objc optional func CropControllerGetChartViewFail(_ error: NSError?)
    @objc optional func CropControllerGetChartViewEmpty()
    @objc optional func CropControllerDelegate_returnedDeleteCropSuccess()

    @objc optional func CropControllerDelegate_returnedAddedMasterCrop (_ returnedValue: CMasterCrop, message: String)

    @objc optional func CropControllerDelegate_returnedParentCropList (_ returnedValue: [CParentCrop]?, message: String)
    @objc optional func CropControllerDelegate_returnedMasterCropList (_ returnedValue: [CCropType]?, message: String)

    @objc optional func CropControllerDelegate_returnedCropList (_ returnedValue: [CCropType]?, message: String)
    @objc optional func CropControllerDelegate_returnedSearchedCropList (_ returnedValue: [CCropType]?, message: String)
    @objc optional func CropControllerDelegate_returnedCropTypeList (_ returnedValue: [CCropType]?, message: String)

    @objc optional func CropControllerDelegate_returnedSavedCrop (_ returnedValue: [CCrop]?, message: String)

    @objc optional func CropControllerDelegate_returnedError (_ error: NSError?, message: String)

    @objc optional func CropControllerDelegate_returnedCropTimeline (_ returnedValue: [CCropTimeline]?, message: String)

    @objc optional func CropControllerDelegate_returnedOverview (_ returnedValue: CCrop?, message:String)

    @objc optional func CropControllerDelegate_returnedCropInfo (_ returnedValue: CCrop)

    @objc optional func CropControllerDelegate_returnedYields (_ returnedValue: CGroupSuccessions?, message: String)

    @objc optional func CropControllerDelegate_returnedIrrigation (_ returnedValue: CGroupSuccessions?, message: String)

    @objc optional func CropControllerDelegate_returnedLabor (_ returnedValue: CGroupSuccessions?, message: String)

    @objc optional func CropControllerDelegate_returnedProfit (_ returnedValue: CGroupSuccessions?, message: String)

    @objc optional func CropControllerDelegate_returnedSearchCrop (_ returnedValue: CropSearchResult?, message: String)

    @objc optional func didEndCompleteCrop_Success(_ result: Dictionary<String, AnyObject>?, message:String)
    @objc optional func didEndCompleteCrop_Fail(_ error: NSError?)

    @objc optional func didEndDeleteCrop_Success(_ result: Dictionary<String, AnyObject>?, message:String)
    @objc optional func didEndUndoDeleteCrop_Success(_ result: Dictionary<String, AnyObject>?, message:String)
    @objc optional func didEndDeleteCrop_Fail(_ error: NSError?)
    @objc optional func didEndUndoDeleteCrop_Fail(_ error: NSError?)
    
    @objc optional func didEndCropSuccession_Success (_ returnedValue: ClassCropSuccession?, message: String)
    
    @objc optional func didEndCropSuccession_Fail(_ error: NSError?)
    
    @objc optional func didEndCropVarietyInformation_Success (_ listReturnedValue: [ClassCropVarietyInformation]?, message: String)
    @objc optional func didEndCropVarietyInformation_Fail (_ error: NSError?)

    //updated
    @objc optional func CropControllerDelegate_returnedListParentCropType (_ listParentCropType: [CParentCropType]?)
    @objc optional func CropControllerDelegate_returnedListMasterCropOfParentCropType (_ listMasterCrop: CCropTypeFromGetLoadListMasterCrop?)
    
    @objc optional func CropControllerDelegate_returnedParentCropListResults (_ returnedValue: [CParentCrop]?)
    @objc optional func CropControllerDelegate_returnedSuccessionListItemResults (_ returnedValue: [CSuccessionSimple]?)

    
    
    //parent crop view
    @objc optional func CropControllerDelegate_returnedSearchParentCropListResults (_ isSuccess: Bool, returnedValue: CFakeMasterCrop?)

    
    @objc optional func CropControllerDelegate_gruopDataSuccessionList(_ returnedValue: NSMutableDictionary)

}

//MARK: -

@objc class CropController: NSObject {

    fileprivate final var maxResults = 10

    var strParentCrop = "PARENTCROP"
    var strMasterCrop = "MASTER_CROP"
    weak var delegate: CropControllerDelegate? = nil
    
    var c: Communicator = Communicator.sharedCommunicator() as! Communicator
    var cropTypeId: Int = 0
    
    
    //MARK: - parse result of parent crop list
    func parseParentCropList (_ returnedParentCropListInDictionaryForm: Dictionary<String, AnyObject>) {
        let json = JSON(returnedParentCropListInDictionaryForm)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kloadlistparentcropSuccessfully) {
            var returnedValue = [CParentCrop]()

            if let tmp = json["data"].array {
                for i in 0..<tmp.count {
                    let pc: CParentCrop = CParentCrop()
                    if let t = json["data"][i]["id"].int {
                        pc.id = t
                    }
                    if let t = json["data"][i]["name"].string {
                        pc.name = t
                    }
                    if let t = json["data"][i]["description"].string {
                        pc.desc = t
                    }
                    if let t = json["data"][i]["imagename"].string {
                        pc.imagename = t
                    }
                    if let t = json["data"][i]["imagesource"].string {
                        pc.imagesource = t
                    }

                    returnedValue.append(pc)
                }
                self.delegate?.CropControllerDelegate_returnedParentCropList! (returnedValue, message: message)
            }
            else {
                self.delegate?.CropControllerDelegate_returnedError!(nil, message: message)
            }


        }

        else {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: message)
        }
    }
    
    func parseCropVarieties (_ returnedValue: Dictionary<String, AnyObject>) {
        let json = JSON(returnedValue)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kloadlistmastercropbyparentcropSuccess || code == kloadListMasterCropEmpty) {
            var returnedValue = [CCropType]()
            
            if let tmp = json["data"].array {
                for i in 0..<tmp.count {
                    let cropType: CCropType = CCropType()
                    
                    if let t = json["data"][i]["id"].int {
                        cropType.id = t
                    }
                    if let t = json["data"][i]["name"].string {
                        cropType.name = t
                    }
                    
                    if let t = json["data"][i]["links"]["mastercrops"].array {
                        var mastercrops: [CMasterCrop] = [CMasterCrop]()
                        
                        for j in 0..<t.count {
                            let mc: CMasterCrop = CMasterCrop()
                            
                            if let t = json["data"][i]["links"]["mastercrops"][j]["id"].int {
                                mc.id = t
                            }
                            
                            if let t = json["data"][i]["links"]["mastercrops"][j]["varietyname"].string {
                                mc.varietyname = t
                            }
                            if let t = json["data"][i]["links"]["mastercrops"][j]["naturalname"].string {
                                mc.naturalname = t
                            }
                            if let t = json["data"][i]["links"]["mastercrops"][j]["imagesource"].string {
                                mc.imagesource = t
                            }
                            if let t = json["data"][i]["links"]["mastercrops"][j]["imagename"].string {
                                mc.imagename = t
                            }

                            if let t = json["data"][i]["links"]["mastercrops"][j]["status"].string {
                                mc.status = t
                            }
                            if let t = json["data"][i]["links"]["mastercrops"][j]["modifieddate"].double {
                                mc.modifieddate = t
                            }
                            
                            mastercrops.append(mc)
                        }
                        
                        cropType.listMasterCrop = mastercrops
                    }
                    returnedValue.append(cropType)
                }
                self.delegate?.CropControllerDelegate_returnedMasterCropList! (returnedValue, message: message)
            }


            else {
                self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Cannot receive data from server.")
            }
        }

        else {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Load master crop failed.")
        }
    }
    
    
    func handleReturnedCropList (_ returnedCropListInDictionaryForm: Dictionary<String, AnyObject>) {
        let json = JSON(returnedCropListInDictionaryForm)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kGetCropListOfUserSuccessfully) {
            var returnedValue = [CCropType]()
            
            if let tmp = json["data"].array {
                
                //crop type
                var ct: CCropType
                //crop
                var c: CCrop
                
                for i in 0..<tmp.count {
                    ct = CCropType()
                    if let tmp = json["data"][i]["id"].int {
                        ct.id = tmp
                    }
                    
                    //crop
                    if let tmpCrops = json["data"][i]["links"]["crops"].array {
                        var crops = [CCrop]()
                        for j in 0..<tmpCrops.count {
                            
                            c = CCrop()
                            if let tmp = json["data"][i]["links"]["crops"][j]["id"].int {
                                c.id = tmp
                            }
                            c.active = json["data"][i]["links"]["crops"][j]["extendProp"]["active"].boolValue
                            if let tmp = json["data"][i]["links"]["crops"][j]["extendProp"]["taskassociated"].int {
                                c.taskassociated = tmp
                            }
                            if let tmp = json["data"][i]["links"]["crops"][j]["extendProp"]["totalsuccession"].int {
                                c.totalSuccession = tmp
                            }
                            
                            //variety
                            c.variety = CVariety()
                            if let tmp = json["data"][i]["links"]["crops"][j]["links"]["variety"]["id"].int {
                                c.variety.id = tmp
                            }
                            if let tmp = json["data"][i]["links"]["crops"][j]["links"]["variety"]["name"].string {
                                c.variety.name = tmp
                            }
                            if let tmp = json["data"][i]["links"]["crops"][j]["links"]["variety"]["status"].string {
                                c.variety.status = tmp
                            }
                            if let tmp = json["data"][i]["links"]["crops"][j]["links"]["variety"]["cropTypeId"].int {
                                c.variety.cropTypeId = tmp
                            }
                            
                            //succession
                            c.listSuccessions = [CSuccession]()
                            var s: CSuccession
                            if let successions = json["data"][i]["links"]["crops"][j]["links"]["successions"].array {
                                for k in 0..<successions.count {
                                    s = CSuccession()
                                    let successionI = json["data"][i]["links"]["crops"][j]["links"]["successions"][k]
                                    if let t = successionI["id"].int {
                                        s.id = t
                                    }
                                    if let t = successionI["cropid"].int {
                                        s.cropid = t
                                    }
                                    if let t = successionI["status"].string {
                                        s.status = t
                                    }
                                    if let t = successionI["createddate"].double {
                                        s.createddate = t
                                    }
                                    if let t = successionI["year"].int {
                                        s.year = t
                                    }
                                    if let t = successionI["successionno"].int {
                                        s.successionno = t
                                    }
                                    if let locations = successionI["extendProp"]["locations"].array {
                                        var listLocation = [ClassFields]()
                                        for l in 0..<locations.count {
                                            let loc: ClassFields = ClassFields()
                                            if let locInfo = successionI["extendProp"]["locations"][l].array {
                                                if let t = locInfo[0].int {
                                                    loc.id = t
                                                }
                                                if let t = locInfo[1].string {
                                                    loc.name = t
                                                }
                                            }
                                            listLocation.append(loc)
                                        }
                                        s.locations = listLocation
                                    }
                                    
                                    
                                    c.listSuccessions.append(s)
                                }
                                
                            }
                            
                            crops.append(c)
                        }
                        ct.crops = crops
                    }
                    
                    
                    
                    if let tmp = json["data"][i]["name"].string {
                        ct.name = tmp
                    }
                    ct.issystem = json["data"][i]["issystem"].boolValue
                    if let tmp = json["data"][i]["status"].string {
                        ct.status = tmp
                    }
                    if let tmp = json["data"][i]["note"].string {
                        ct.note = tmp
                    }
                    if let tmp = json["data"][i]["farmid"].int {
                        ct.farmid = tmp
                    }
                    returnedValue.append(ct)
                }
            }
            
            self.delegate?.CropControllerDelegate_returnedCropList!(returnedValue, message: message)
            
        }
        else if (code == kGetCropListOfUserEmpty) {
            swiftlog("Crop list is empty")
            
            self.delegate?.CropControllerDelegate_returnedCropList!(nil, message: "Crop list is empty")
            
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Crop list is empty")
            
        }
        else {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: message)
        }
    }
    
    //MARK: - get user's crops (updated)
    func getUserCrop (_ farmId: Int, categoryId: Int, plantTypeId: Int, hasSuccession : Bool, isNoLocation: Bool, tasktypeId : Int) {
//        println("start getting crop list: \(NSDate())")

        var url: String = ""
        
        if (categoryId < 0 || plantTypeId < 0) {
            if (plantTypeId > 0 && categoryId < 0){
                url = "\(Utilities.getkServer())\(kGetListCropFull)?farmId=\(farmId)&plantTypeId=\(plantTypeId)&hasSuccession=\(hasSuccession)&nolocation=\(isNoLocation)"
            }else if(plantTypeId < 0 && categoryId > 0){
                url = "\(Utilities.getkServer())\(kGetListCropFull)?categoryId=\(categoryId)&farmId=\(farmId)&hasSuccession=\(hasSuccession)&nolocation=\(isNoLocation)"
            }else{
                //url = "\(kServer)\(kGetListCropOfUser)?farmId=\(farmId)&hasSuccession=\(hasSuccession)&nolocation=\(isNoLocation)"
                url = "\(Utilities.getkServer())\(kGetListCropFull)?farmId=\(farmId)&hasSuccession=\(hasSuccession)&tasktypeid=\(tasktypeId)"
            }
            
            
        }
        else {
            url = "\(Utilities.getkServer())\(kGetListCropBasicOfUser)?categoryId=\(categoryId)&farmId=\(farmId)&plantTypeId=\(plantTypeId)&hasSuccession=\(hasSuccession)&nolocation=\(isNoLocation)"
        }
        
        
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
//            println("end getting crop list: \(NSDate())")
//            println("->start1 \(NSDate())")
            self.parseUserCropList((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.CropControllerDelegate_returnedError!(error, message: "")
                Utilities.evenAnswer(Utilities.getNameEven(kGetListCropFull, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    func parseUserCropList (_ returnedDictionary: Dictionary<String, AnyObject>) {
        let json = JSON(returnedDictionary)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kGetCropListOfUserSuccessfully) {
            
            var returnedValue: [CCropType] = [CCropType]()
            if let t = json["data"].array {
                for i in 0..<t.count {
                    let cropTypeIDict = json["data"][i]
                    let cropType: CCropType = CCropType()
                    var listCrops: [CCrop] = [CCrop]()
                    
                    if let t = cropTypeIDict["id"].int {
                        cropType.id = t
                    }
                    
                    if let t = cropTypeIDict["name"].string {
                        cropType.name = t
                    }
                    if let t = cropTypeIDict["extendProp"]["parentcroptypeid"].int {
                        cropType.parentcroptypeid = t
                    }
                    
                    
                    if let t = cropTypeIDict["links"]["crops"].array {
                        for j in 0..<t.count {
                            let cropJDict = cropTypeIDict["links"]["crops"][j]
                            let crop: CCrop = CCrop()
                            
                            if let t = cropJDict["id"].int {
                                crop.id = t
                            }
                            if let t = cropJDict["farmid"].int {
                                crop.farmId = t
                            }
                            if let t = cropJDict["cropname"].string {
                                crop.cropname = t
                            }

                            if let t = cropJDict["varietyname"].string {
                                crop.varietyname = t
                            }

                            if let t = cropJDict["imagename"].string {
                                crop.imagename = t
                            }
                            if let t = cropJDict["groupname"].string {
                                crop.groupname = t
                            }
                            if let t = cropJDict["parentname"].string {
                                crop.parentname = t
                            }
                            if let t = cropJDict["croptypename"].string {
                                crop.croptypename = t
                            }
                            if let t = cropJDict["successionno"].int {
                                crop.successionno = t
                            }
                            if let t = cropJDict["successionid"].int {
                                crop.successionid = t
                            }
                            if let t = cropJDict["harvestunit"].string {
                                crop.harvestunit = t
                            }


                            //mastercrop
                            let masterCrop: CMasterCrop = CMasterCrop()
                            if let t = cropJDict["links"]["mastercrop"]["id"].int {
                                masterCrop.id = t
                            }
                            if let t = cropJDict["links"]["mastercrop"]["varietyname"].string {
                                masterCrop.varietyname = t
                            }
                            if let t = cropJDict["links"]["mastercrop"]["naturalname"].string {
                                masterCrop.naturalname = t
                            }
                            if let t = cropJDict["links"]["mastercrop"]["status"].string {
                                masterCrop.status = t
                            }
                            if let t = cropJDict["links"]["mastercrop"]["imagename"].string {
                                masterCrop.imagename = t
                            }
                            if let t = cropJDict["links"]["mastercrop"]["imagesource"].string {
                                masterCrop.imagesource = t
                            }
                            if let t = cropJDict["links"]["mastercrop"]["modifieddate"].double {
                                masterCrop.modifieddate = t
                            }
                            if let t = cropJDict["links"]["mastercrop"]["imagesource"].string {
                                masterCrop.imagesource = t
                            }
                            
                            crop.active = cropJDict["extendProp"]["active"].boolValue
                            if let t = cropJDict["extendProp"]["taskassociated"].int {
                                crop.taskassociated = t
                            }
                            
                            crop.masterCrop = masterCrop
                            
                            //get convertrates
                            if let tmpBB = cropJDict["links"]["convertrates"].array{
                                let groupConvertrates = tmpBB
                                
                                for j in 0..<groupConvertrates.count{
                                    let clsConvertrate: CConvertTrates = CConvertTrates()
                                    
                                    if let tmpCC = groupConvertrates[j]["id"].int {
                                        clsConvertrate.id = tmpCC
                                    }
                                    if let tmpCC = groupConvertrates[j]["fromunit"].string {
                                        clsConvertrate.fromunit = tmpCC
                                    }
                                    
                                    if let tmpCC = groupConvertrates[j]["tounit"].string {
                                        clsConvertrate.tounit = tmpCC
                                    }
                                    if let tmpCC = groupConvertrates[j]["value"].double {
                                        clsConvertrate.value = tmpCC
                                    }
                                    
                                    crop.listConvertrates.append(clsConvertrate)
                                    
                                }
                                
                                
                            }
                            //succession
                            var listSuccession: [CSuccession] = [CSuccession]()
                            
                            if let t = cropJDict["links"]["successions"].array {
                                for k in 0..<t.count {
                                    let successionK = cropJDict["links"]["successions"][k]
                                    let s: CSuccession = CSuccession()
                                    
                                    if let t = successionK["id"].int {
                                        s.id = t
                                    }
                                    if let t = successionK["cropid"].int {
                                        s.cropid = t
                                    }
                                    if let t = successionK["status"].string {
                                        s.status = t
                                    }
                                    if let t = successionK["dateplanted"].double {
                                        s.plantedDate = t
                                    }
                                    if let t = successionK["dateplantingcomplete"].double {
                                        s.plantingcompleteDate = t
                                    }
                                    if let t = successionK["datecompleted"].double {
                                        s.datecompleted = t
                                    }

                                    s.isfinished = successionK["isfinished"].boolValue
                                
                                    if let t = successionK["lineperbed"].int {
                                        s.lineperbed = t
                                    }
                                    
                                    if let t = successionK["harvestunit"].string {
                                        s.harvestunit = t
                                    }
                                    
                                    if let t = successionK["yieldunit"].string {
                                        s.yieldunit = t
                                    }
                                    
                                    if let t = successionK["numberofbeds"].int {
                                        s.numberofbeds = t
                                    }
                                    if let t = successionK["bedlength"].float {
                                        s.bedlength = t
                                    }
                                    if let t = successionK["successionno"].int {
                                        s.successionno = t
                                    }
                                    
                                    // Irrigation
                                    let irrgationComplete = successionK["links"]["irrigation"]
                                    let taskCLirrgation: SW_Task = SW_Task()
                                    
                                    if let t = irrgationComplete["taskdate"].double {
                                        taskCLirrgation.taskdate = t
                                    }
                                    
                                    let irrgationMethod: ClassTaskFeatureIrrigationMethod =  ClassTaskFeatureIrrigationMethod()
                                    
                                    if let t = irrgationComplete["links"]["method"]["irrigatemethod"].int {
                                        irrgationMethod.irrigatemethod = t
                                    }
                                    
                                    if let t = irrgationComplete["links"]["method"]["timeduration"].double {
                                        irrgationMethod.timeduration = t
                                    }
                                    
                                    if let t = irrgationComplete["links"]["method"]["overhead180"].double {
                                        irrgationMethod.overhead180 = t
                                    }
                                    if let t = irrgationComplete["links"]["method"]["overhead360"].double {
                                        irrgationMethod.overhead360 = t
                                    }
                                    if let t = irrgationComplete["links"]["method"]["min360"].double {
                                        irrgationMethod.min360 = t
                                    }
                                    if let t = irrgationComplete["links"]["method"]["min180"].double {
                                        irrgationMethod.min180 = t
                                    }
                                    
                                    taskCLirrgation.irrgationMethod = irrgationMethod
                                    
                                    s.irrigationComplete = taskCLirrgation
                                    
                                    
                                    //Harvest
                                    let taskComplete = successionK["links"]["harvest"]
                                    
                                    let taskCL: SW_Task = SW_Task()
                                    if let t =  taskComplete["taskdate"].double{
                                        taskCL.taskdate = t
                                    }
                                    
                                    if let t =  taskComplete["extendProp"]["harvestunit"].string {
                                        taskCL.harvestunit = t
                                    }
                                    
                                    if let t =  taskComplete["extendProp"]["harvestamount"].double {
                                        taskCL.harvestamount = t
                                    }
                                    
                                    s.taskComplete = taskCL
                                    
                                    //TODO: get more properties here...
                                    
                                    let locationExtendProp  = successionK["extendProp"]
                                    if let locationFormat = locationExtendProp["location"].string{
                                        s.locationformat = locationFormat
                                    }
                                    
                                    if let successionnames = locationExtendProp["successionname"].array {
                                        s.successionnamestring = successionnames[0].stringValue
                                    }
                                    
                                    var listField = [ClassFields]()
                                    let field: ClassFields = ClassFields()
                                    if let t = locationExtendProp["location"].string{
                                        var locationL = t
                                        if let totallocations = locationExtendProp["totallocations"].int {
                                            if (totallocations - 1  > 0){
                                                locationL = locationL + " +\(totallocations - 1) more"
                                            }
                                        }
                                        if let successionnames = locationExtendProp["successionname"].array {
                                           s.successionnamestring = "\(successionnames[0])"
 
                                        }
                                        field.name = locationL
                                        if let t = locationExtendProp["locationtype"].string {
                                           field.locationtype =  t
                                        }
  
                                    }
                                    
                                    
                                    
                                    if let t = locationExtendProp["finalharverttaskdate"].double {
                                        s.finalharverttaskdate =  t
                                    }
                                    
                                    if let tmpCC = locationExtendProp["finalharverttaskid"].int {
                                        s.finalharverttaskid = tmpCC
                                    }
                                    
                                     listField.append(field)
                                    s.locations = listField
                                    
                                    

                                    listSuccession.append(s)
                                }
                                
                                
                                crop.listSuccessions = listSuccession
                                
                            }
                            listCrops.append(crop)
                            
                            
                        }
                    }
                    cropType.crops = listCrops
                    
                    returnedValue.append(cropType)
                }
            }
            self.delegate?.CropControllerDelegate_returnedCropList!(returnedValue, message: message)
//            println("->end1 \(NSDate())")
        }
        else {
            self.delegate?.CropControllerDelegate_returnedCropList!(nil, message: "Crop list is empty")
            Utilities.evenAnswer(Utilities.getNameEven(kGetListCropBasicOfUser, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kGetListCropBasicOfUser))
        }
    }
    

    func handleReturnedSaveMasterCrop(_ resultInDictForm: Dictionary<String, AnyObject>) {
        let json = JSON(resultInDictForm)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kAddMasterCropSuccessfully) {
            let tmpDict = (resultInDictForm["data"] as! NSArray)
            
            var returnedValue: [CMasterCrop] = (try! MTLJSONAdapter.models(of: CMasterCrop.self, fromJSONArray: tmpDict as [AnyObject])) as! [CMasterCrop]
            
            if (self.cropTypeId <= 0) {
                var count = 0
                for i in 0..<returnedValue.count {
                    count += 1
                    if (((returnedValue[i] as CMasterCrop).iscroptypedefault) && ((returnedValue[i] as CMasterCrop).isparentcropdefault)) {
                        count = -1
                        self.delegate?.CropControllerDelegate_returnedAddedMasterCrop! (returnedValue[i], message: message)
                        break;
                    }
                }
                if (count == returnedValue.count) {
                    self.delegate?.CropControllerDelegate_returnedAddedMasterCrop! (returnedValue[0], message: message)
                }
                else {
                    // do nothing
                }
                
            }
            else {
                for i in 0..<returnedValue.count {
                    if (returnedValue[i].croptypeid == self.cropTypeId) {
                        self.delegate?.CropControllerDelegate_returnedAddedMasterCrop! (returnedValue[i], message: message)
                    }
                }
            }
        }
            
        else if (code == kMasterCropDuplicated) {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "This crop variety is already exist.")
            Utilities.evenAnswer(Utilities.getNameEven(KAddMasterCropByParent, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: KAddMasterCropByParent))
        }
        
        else {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Cannot add master crop.")
            Utilities.evenAnswer(Utilities.getNameEven(KAddMasterCropByParent, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: KAddMasterCropByParent))
        }
    }
    
    //MARK: - ======== Add variety =========
    func addMasterCropByParent (_ parentCropID: Int, farmID: Int, varietyName: String, categoryID: Int) {
        let url: String = "\(Utilities.getkServer())\(KAddMasterCropByParent)"
        
        //param dictionary
        let paraDict: NSMutableDictionary = NSMutableDictionary()
        paraDict.setValue("MASTER_CROP", forKey: "type")
        paraDict.setValue(varietyName, forKey: "varietyname")
        paraDict.setValue(categoryID, forKey: "categoryid")
        
        //extend crop dictionary
        let extendCropDict: NSMutableDictionary = NSMutableDictionary()
        extendCropDict.setValue(farmID, forKey: "farmid")
        extendCropDict.setValue(parentCropID, forKey: "parentcropid")
        
        paraDict.setValue(extendCropDict, forKey: "extendProp")
        
        let cws: CommunicatorWS = CommunicatorWS()
        cws.email = ClassGlobal.sharedInstance().tendUser.email
        //cws.password = ClassGlobal.sharedInstance().tendUser.password
//        cws.email = "q@tend.com"
//        cws.password = "123456"
        
        
//        print((paraDict as Dictionary).jsonString())
        
        cws.postJsonWS(url, param: paraDict, completeBlock: { (success, result) -> Void in
            if(success) {
                self.handleReturnedSaveMasterCrop(result as! Dictionary<String, AnyObject>)
            }
            else {
                self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Cannot parse returned JSON")
                Utilities.evenAnswer(Utilities.getNameEven(KAddMasterCropByParent, andResponse: kNoDataReturn), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: kNoDataReturn, andUrl: url))
            }
            }) { (error) -> Void in
                self.delegate?.CropControllerDelegate_returnedError!(error! as NSError, message: error!.localizedDescription)
                Utilities.evenAnswer(Utilities.getNameEven(KAddMasterCropByParent, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    //MARK: - update follow new change
    
    func handleReturnedSaveCrop(_ resultInDictForm: Dictionary<String, AnyObject>) {
        let json = JSON(resultInDictForm)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kSaveCropSuccess) {
            if let t = json["data"].array {
                var returnedValue: [CCrop] = [CCrop]()
                for i in 0..<t.count {
                    let c: CCrop = CCrop()
                    if let t = json["data"][i]["id"].int {
                        c.id = t
                    }
                    if let t = json["data"][i]["farmid"].int {
                        c.farmId = t
                    }
                    let mc: CMasterCrop = CMasterCrop()
                    if let t = json["data"][i]["links"]["mastercrop"]["id"].int {
                        mc.id = t
                    }
                    if let t = json["data"][i]["links"]["mastercrop"]["varietyname"].string {
                        mc.varietyname = t
                    }
                    if let t = json["data"][i]["links"]["mastercrop"]["naturalname"].string {
                        mc.naturalname = t
                    }
                    if let t = json["data"][i]["links"]["mastercrop"]["modifieddate"].double {
                        mc.modifieddate = t
                    }
                    if let t = json["data"][i]["links"]["mastercrop"]["status"].string {
                        mc.status = t
                    }
                    c.masterCrop = mc

                    let ct: CCropType = CCropType()

                    if let t = json["data"][i]["links"]["croptype"]["id"].int {
                        ct.id = t
                    }

                    if let t = json["data"][i]["links"]["croptype"]["name"].string {
                        ct.name = t
                    }

                    c.cropType = ct


                    returnedValue.append(c)
                }
                self.delegate?.CropControllerDelegate_returnedSavedCrop!(returnedValue, message: message)
            }
        }
        else if (code == "3_26_f") {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "This crop is already exist.")
        }
        else if (code == "3_9_f") {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Crop type not found.")
        }
        else {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Unknown Error.")
        }
    }

    func getIrrigationBySuccessions (_ successionIds: String) {
        

    }
    

    func getLaborBySuccessions (_ successionIds: String) {
        

    }
    
    

    
    
    func handleCommonReturned (_ resultInDictForm: Dictionary<String, AnyObject>, forScreen: Int) {
        
        let json = JSON(resultInDictForm)
        
        let code: String = json["meta"]["code"].stringValue
        var message = ""
        if let t: String = json["meta"]["message"].string {
            message = t
        }
        swiftlog("code: \(code), message: \(message)")
        
        switch (forScreen) {
        case 1:
            if (code == kGetYieldSuccessfully) {
                let dataDict = json["data"]
                let groupSuccession = CGroupSuccessions()
                groupSuccession.listSuccessions = [CSuccession]()
                
                if let t = dataDict["id"].int {
                    groupSuccession.id = t
                }
                if let t = dataDict["grouptype"].string {
                    groupSuccession.grouptype = t
                }
                groupSuccession.yields = CGroupType_Yields()
                if let t = dataDict["totalavg"].double {
                    groupSuccession.yields.totalAvg = t
                }
                if let t = dataDict["totalharvestamount"].double {
                    groupSuccession.yields.totalHarvestamount = t
                }
                if let t = dataDict["totalrowfeet"].double {
                    groupSuccession.yields.totalRowfeet = t
                }
                if let t = dataDict["unitDefault"].string {
                    groupSuccession.unitDefault = t
                }

                //data of chart
                if let _ = dataDict["links"]["grouptask"]["links"]["pointvalues"].array {
                    groupSuccession.chartData = (try! MTLJSONAdapter.models(of: PointValue.self, fromJSONArray: ((((resultInDictForm["data"] as! [AnyHashable: Any])["links"] as! [AnyHashable: Any])["grouptask"] as! [AnyHashable: Any])["links"] as! [AnyHashable: Any])["pointvalues"] as! [AnyObject])) as! [PointValue]
                }
                
                //copy từ đây
                if let listSuccessions = dataDict["listSuccessions"].array {
                    for i in 0..<listSuccessions.count {
                        let successionI = dataDict["listSuccessions"][i]
                        let succession: CSuccession = CSuccession()
                        if let t = successionI["id"].int {
                            succession.id = t
                        }
                        if let t = successionI["cropid"].int {
                            succession.cropid = t
                        }
                        if let t = successionI["status"].string {
                            succession.status = t
                        }
                        if let t = successionI["successionno"].int {
                            succession.successionno = t
                        }
                        if let t = successionI["createddate"].double {
                            succession.createddate = t
                        }
                        if let t = successionI["dateplantingcomplete"].double {
                            succession.plantingcompleteDate = t
                        }
                        if let t = successionI["harvestunit"].string {
                            succession.harvestunit = t
                        }
                        if let t = successionI["numberofbeds"].int {
                            succession.numberofbeds = t
                        }
                        if let t = successionI["bedlength"].float {
                            succession.bedlength = t
                        }
                        if let t = successionI["bedft"].float {
                            succession.bedft = t
                        }
                        
                        if let t = successionI["yieldunit"].string {
                            succession.yieldunit = t
                        }
                        
                        if let t = successionI["year"].int {
                            succession.year = t
                        }
                        if let t = successionI["extendProp"]["datesuccessiongroup"].double {
                            succession.datesuccessiongroup = t
                        }
                        if let t = successionI["extendProp"]["locationformat"].string {
                            succession.locationformat = t
                        }



                        //data -> listSuccessions -> [i] -> links -> lstWSTasks
                        
                        succession.listTasks = [Ent_Tasks]()
                        
                        if let lstWSTasks = successionI["links"]["lstWSTasks"].array {
                            
                            for j in 0..<lstWSTasks.count {
                                let lstWSTasksJ = successionI["links"]["lstWSTasks"][j]
                                let task: Ent_Tasks = Ent_Tasks()
                                if let t = lstWSTasksJ["id"].int {
                                    task.taskID = t as NSNumber!
                                }
                                if let t = lstWSTasksJ["farmid"].int {
                                    task.farmID = t as NSNumber!
                                }
                                if let t = (lstWSTasksJ["taskdate"].double) {
                                    task.taskDate = Date(timeIntervalSince1970: t/1000)
                                }
                                if let t = lstWSTasksJ["status"].string {
                                    task.status = t
                                }
                                
                                //lstWSTasks[j] -> links
                                let taskType: CTaskType = CTaskType()
                                if let t = lstWSTasksJ["links"]["tasktype"]["id"].int {
                                    taskType.id = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["name"].string {
                                    taskType.name = t
                                }
                                taskType.issystem = lstWSTasksJ["links"]["tasktype"]["issystem"].boolValue
                                if let t = lstWSTasksJ["links"]["tasktype"]["position"].int {
                                    taskType.position = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["status"].string {
                                    taskType.status = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["notes"].string {
                                    taskType.notes = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["sign"].string {
                                    taskType.sign = t
                                }
                                
                                task.taskType = taskType
                                

                                
                                //lstWSTasks[j] -> extendProp
                                if let t = lstWSTasksJ["extendProp"]["avg"].double {
                                    task.avg = t
                                }
                                
                                if let t = lstWSTasksJ["extendProp"]["harvestamount"].double {
                                    task.harvestamount = t
                                }
                                
                                if let t = lstWSTasksJ["extendProp"]["rowfeet"].int {
                                    task.rowfeet = t
                                }

                                if let t = lstWSTasksJ["extendProp"]["harvestunit"].string {
                                    task.harvestunit = t
                                }


                                succession.listTasks.append(task)
                            }
                            groupSuccession.listSuccessions.append(succession)
                        }
                    }
                }
                self.delegate?.CropControllerDelegate_returnedYields!(groupSuccession, message: message)
                
                //tới đây
            }
            else {
                self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Cannot get data of yields")
            }
            
        case 2:
            if (code == kGetIrrigationSuccessfully) {
                let dataDict = json["data"]
                let groupSuccession = CGroupSuccessions()
                
                if let t = dataDict["id"].int {
                    groupSuccession.id = t
                }
                if let t = dataDict["grouptype"].string {
                    groupSuccession.grouptype = t
                }
                groupSuccession.irrigation = CGroupType_Irrigation()
                if let t = dataDict["totaltimeduration"].int {
                    groupSuccession.irrigation.totaltimeduration = t
                }
                if let t = dataDict["totalgallons"].double {
                    groupSuccession.irrigation.totalgallons = t
                }
                if let t = dataDict["totalacreinches"].double {
                    groupSuccession.irrigation.totalacreinches = t
                }
                
                //copy từ đây
                if let listSuccessions = dataDict["listSuccessions"].array {
                    for i in 0..<listSuccessions.count {
                        let successionI = dataDict["listSuccessions"][i]
                        let succession: CSuccession = CSuccession()
                        if let t = successionI["id"].int {
                            succession.id = t
                        }
                        if let t = successionI["cropid"].int {
                            succession.cropid = t
                        }
                        if let t = successionI["status"].string {
                            succession.status = t
                        }
                        if let t = successionI["successionno"].int {
                            succession.successionno = t
                        }
                        if let t = successionI["createddate"].double {
                            succession.createddate = t
                        }
                        if let t = successionI["dateplantingcomplete"].double {
                            succession.plantingcompleteDate = t
                        }
                        if let t = successionI["harvestunit"].string {
                            succession.harvestunit = t
                        }
                        if let t = successionI["numberofbeds"].int {
                            succession.numberofbeds = t
                        }
                        if let t = successionI["bedlength"].float {
                            succession.bedlength = t
                        }
                        if let t = successionI["bedft"].float {
                            succession.bedft = t
                        }

                        if let t = successionI["year"].int {
                            succession.year = t
                        }
                        if let t = successionI["extendProp"]["datesuccessiongroup"].double {
                            succession.datesuccessiongroup = t
                        }
                        
                        if let t = successionI["extendProp"]["locationformat"].string {
                            succession.locationformat = t
                        }

                        if let t = successionI["extendProp"]["totalgallons"].int {
                            succession.totalgallons = t
                        }
                        
                        if let t = successionI["extendProp"]["totalacreinches"].double {
                            succession.totalacreinches = t
                        }

                        //data -> listSuccessions -> [i] -> links -> lstWSTasks

                        succession.listTasks = [Ent_Tasks]()

                        if let lstWSTasks = successionI["links"]["lstWSTasks"].array {

                            for j in 0..<lstWSTasks.count {
                                let lstWSTasksJ = successionI["links"]["lstWSTasks"][j]
                                let task: Ent_Tasks = Ent_Tasks()
                                if let t = lstWSTasksJ["id"].int {
                                    task.taskID = t as NSNumber
                                }
                                if let t = lstWSTasksJ["farmid"].int {
                                    task.farmID = t as NSNumber
                                }
                                if let t = (lstWSTasksJ["taskdate"].double) {
                                    task.taskDate = Date(timeIntervalSince1970: t/1000)
                                }
                                if let t = lstWSTasksJ["status"].string {
                                    task.status = t
                                }



                                //lstWSTasks[j] -> links
                                let taskType: CTaskType = CTaskType()
                                if let t = lstWSTasksJ["links"]["tasktype"]["id"].int {
                                    taskType.id = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["name"].string {
                                    taskType.name = t
                                }
                                taskType.issystem = lstWSTasksJ["links"]["tasktype"]["issystem"].boolValue
                                if let t = lstWSTasksJ["links"]["tasktype"]["position"].int {
                                    taskType.position = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["status"].string {
                                    taskType.status = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["notes"].string {
                                    taskType.notes = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["sign"].string {
                                    taskType.sign = t
                                }

                                task.taskType = taskType



                                if let t = lstWSTasksJ["extendProp"]["totalacreinches"].double {
                                    task.totalacreinches = t
                                }

                                if let t = lstWSTasksJ["extendProp"]["totalgallons"].double {
                                    task.totalgallons = t
                                }

                                succession.listTasks.append(task)
                            }
                            groupSuccession.listSuccessions.append(succession)
                        }
                    }
                }
                self.delegate?.CropControllerDelegate_returnedIrrigation!(groupSuccession, message: message)
                
                //tới đây
            }
            else {
                self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Cannot get data of irrigation")
            }
            
        case 3:
            if (code == kGetLaborSuccessfully) {
                let dataDict = json["data"]
                let groupSuccession = CGroupSuccessions()
                
                if let t = dataDict["id"].int {
                    groupSuccession.id = t
                }
                if let t = dataDict["grouptype"].string {
                    groupSuccession.grouptype = t
                }
                groupSuccession.labor = CGroupType_Labor()
                if let t = dataDict["totallaborcost"].double {
                    groupSuccession.labor.totallaborcost = t
                }
                if let t = dataDict["totallabortime"].double {
                    groupSuccession.labor.totallabortime = t
                }
                if let t = dataDict["totallabortimeperbed"].double {
                    groupSuccession.labor.totallabortimeperbed = t
                }
                
                //copy từ đây
                if let listSuccessions = dataDict["listSuccessions"].array {
                    for i in 0..<listSuccessions.count {
                        let successionI = dataDict["listSuccessions"][i]
                        let succession: CSuccession = CSuccession()
                        if let t = successionI["id"].int {
                            succession.id = t
                        }
                        if let t = successionI["cropid"].int {
                            succession.cropid = t
                        }
                        if let t = successionI["status"].string {
                            succession.status = t
                        }
                        if let t = successionI["successionno"].int {
                            succession.successionno = t
                        }
                        if let t = successionI["createddate"].double {
                            succession.createddate = t
                        }
                        if let t = successionI["dateplantingcomplete"].double {
                            succession.plantingcompleteDate = t
                        }
                        if let t = successionI["harvestunit"].string {
                            succession.harvestunit = t
                        }
                        if let t = successionI["numberofbeds"].int {
                            succession.numberofbeds = t
                        }
                        if let t = successionI["bedlength"].float {
                            succession.bedlength = t
                        }
                        if let t = successionI["bedft"].float {
                            succession.bedft = t
                        }

                        if let t = successionI["year"].int {
                            succession.year = t
                        }
                        
                        if let t = successionI["extendProp"]["datesuccessiongroup"].double {
                            succession.datesuccessiongroup = t
                        }
                        
                        if let t = successionI["extendProp"]["locationformat"].string {
                            succession.locationformat = t
                        }
                        
                        if let t = successionI["extendProp"]["labortime"].int {
                            succession.labortime = t
                        }
                        
                        if let t = successionI["extendProp"]["labortimeperbed"].int {
                            succession.labortimeperbed = t
                        }
                        
                        if let t = successionI["extendProp"]["laborcode"].int {
                            succession.laborcode = t
                        }

                        //data -> listSuccessions -> [i] -> links -> lstWSTasks

                        succession.listTasks = [Ent_Tasks]()

                        if let lstWSTasks = successionI["links"]["lstWSTasks"].array {

                            for j in 0..<lstWSTasks.count {
                                let lstWSTasksJ = successionI["links"]["lstWSTasks"][j]
                                let task: Ent_Tasks = Ent_Tasks()
                                if let t = lstWSTasksJ["id"].int {
                                    task.taskID = t as NSNumber!
                                }
                                if let t = lstWSTasksJ["farmid"].int {
                                    task.farmID = t as NSNumber!
                                }
                                if let t = (lstWSTasksJ["taskdate"].double) {
                                    task.taskDate = Date(timeIntervalSince1970: t/1000)
                                }
                                if let t = lstWSTasksJ["status"].string {
                                    task.status = t
                                }

                                //lstWSTasks[j] -> links
                                let taskType: CTaskType = CTaskType()
                                if let t = lstWSTasksJ["links"]["tasktype"]["id"].int {
                                    taskType.id = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["name"].string {
                                    taskType.name = t
                                }
                                taskType.issystem = lstWSTasksJ["links"]["tasktype"]["issystem"].boolValue
                                if let t = lstWSTasksJ["links"]["tasktype"]["position"].int {
                                    taskType.position = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["status"].string {
                                    taskType.status = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["notes"].string {
                                    taskType.notes = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["sign"].string {
                                    taskType.sign = t
                                }

                                task.taskType = taskType



                                //lstWSTasks[j] -> extendProp
                                if let t = lstWSTasksJ["extendProp"]["bedft"].double {
                                    task.bedft = t
                                }

                                if let t = lstWSTasksJ["extendProp"]["labortime"].double {
                                    task.labortime = t
                                }
                                if let t = lstWSTasksJ["extendProp"]["labortimeperbed"].double {
                                    task.labortimeperbed = t
                                }
                                if let t = lstWSTasksJ["extendProp"]["laborcode"].double {
                                    task.laborcode = t
                                }


                                succession.listTasks.append(task)
                            }
                            groupSuccession.listSuccessions.append(succession)
                        }
                    }
                }
                self.delegate?.CropControllerDelegate_returnedLabor!(groupSuccession, message: message)
                
                //tới đây
            }
            else {
                self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Cannot get data of labor")
            }
            
        case 4:
            if (code == kGetProfitSuccessfully) {
                let dataDict = json["data"]
                let groupSuccession = CGroupSuccessions()
                
                if let t = dataDict["id"].int {
                    groupSuccession.id = t
                }
                if let t = dataDict["grouptype"].string {
                    groupSuccession.grouptype = t
                }
                groupSuccession.profit = CGroupType_Profit()
                if let t = dataDict["totalLaborcost"].double {
                    groupSuccession.profit.totalLaborcost = t
                }
                if let t = dataDict["totalRevenue"].int {
                    groupSuccession.profit.totalRevenue = t
                }
                if let t = dataDict["totalProfit"].int {
                    groupSuccession.profit.totalProfit = t
                }
                
                //copy từ đây
                if let listSuccessions = dataDict["listSuccessions"].array {
                    for i in 0..<listSuccessions.count {
                        let successionI = dataDict["listSuccessions"][i]
                        let succession: CSuccession = CSuccession()
                        if let t = successionI["id"].int {
                            succession.id = t
                        }
                        if let t = successionI["cropid"].int {
                            succession.cropid = t
                        }
                        if let t = successionI["status"].string {
                            succession.status = t
                        }
                        if let t = successionI["successionno"].int {
                            succession.successionno = t
                        }
                        if let t = successionI["createddate"].double {
                            succession.createddate = t
                        }
                        if let t = successionI["dateplantingcomplete"].double {
                            succession.plantingcompleteDate = t
                        }
                        if let t = successionI["harvestunit"].string {
                            succession.harvestunit = t
                        }
                        if let t = successionI["numberofbeds"].int {
                            succession.numberofbeds = t
                        }
                        if let t = successionI["bedlength"].float {
                            succession.bedlength = t
                        }
                        if let t = successionI["bedft"].float {
                            succession.bedft = t
                        }

                        if let t = successionI["year"].int {
                            succession.year = t
                        }
                        if let t = successionI["extendProp"]["datesuccessiongroup"].double {
                            succession.datesuccessiongroup = t
                        }
                        if let t = successionI["extendProp"]["locationformat"].string {
                            succession.locationformat = t
                        }



                        //data -> listSuccessions -> [i] -> links -> lstWSTasks

                        succession.listTasks = [Ent_Tasks]()

                        if let lstWSTasks = successionI["links"]["lstWSTasks"].array {

                            for j in 0..<lstWSTasks.count {
                                let lstWSTasksJ = successionI["links"]["lstWSTasks"][j]
                                let task: Ent_Tasks = Ent_Tasks()
                                if let t = lstWSTasksJ["id"].int {
                                    task.taskID = t as NSNumber!
                                }
                                if let t = lstWSTasksJ["farmid"].int {
                                    task.farmID = t as NSNumber!
                                }
                                if let t = (lstWSTasksJ["taskdate"].double) {
                                    task.taskDate = Date(timeIntervalSince1970: t/1000)
                                }
                                if let t = lstWSTasksJ["status"].string {
                                    task.status = t
                                }

                                //lstWSTasks[j] -> links
                                let taskType: CTaskType = CTaskType()
                                if let t = lstWSTasksJ["links"]["tasktype"]["id"].int {
                                    taskType.id = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["name"].string {
                                    taskType.name = t
                                }
                                taskType.issystem = lstWSTasksJ["links"]["tasktype"]["issystem"].boolValue
                                if let t = lstWSTasksJ["links"]["tasktype"]["position"].int {
                                    taskType.position = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["status"].string {
                                    taskType.status = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["notes"].string {
                                    taskType.notes = t
                                }
                                if let t = lstWSTasksJ["links"]["tasktype"]["sign"].string {
                                    taskType.sign = t
                                }

                                task.taskType = taskType



                                //lstWSTasks[j] -> extendProp
                                if let t = lstWSTasksJ["extendProp"]["avg"].double {
                                    task.avg = t
                                }

                                if let t = lstWSTasksJ["extendProp"]["harvestamount"].double {
                                    task.harvestamount = t
                                }

                                if let t = lstWSTasksJ["extendProp"]["rowfeet"].int {
                                    task.rowfeet = t
                                }

                                succession.listTasks.append(task)
                            }
                            groupSuccession.listSuccessions.append(succession)
                        }
                    }
                }
                self.delegate?.CropControllerDelegate_returnedProfit!(groupSuccession, message: message)
                
                //tới đây
            }
            else {
                self.delegate?.CropControllerDelegate_returnedError!(nil, message: "Cannot get data of profit")
            }
            
        default:
            swiftlog("")
        }
        
    }
    

   
    func handleDeleteMasterCropResult (_ resultInDictForm: Dictionary<String, AnyObject>) {
        let json = JSON(resultInDictForm)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        
        if (code == kDeleteCropSuccess) {
            self.delegate?.CropControllerDelegate_returnedDeleteCropSuccess!()
        }
        else {
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: message)
            Utilities.evenAnswer(Utilities.getNameEven(kDeleteCrop, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kDeleteCrop))
        }
    }

    func deleteCrop (cropId: Int) {
        let url = "\(Utilities.getkServer())\(kDeleteCrop)?cropId=\(cropId)"

        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password

        communicationWS.postWS(url, param: nil, completeBlock: { (success, result) -> Void in
            self.handleDeleteMasterCropResult((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.CropControllerDelegate_returnedError!(error, message: "")
                Utilities.evenAnswer(Utilities.getNameEven(kDeleteCrop, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }

    func searchParentCropWithFilter (_ parentCropId: String, cropTypeId: String) {
        var url = ""
        if (cropTypeId == "") {
            url = "\(Utilities.getkServer())\(kSearchParentCropURL)?keysearch=&parentcropid=\(Int(parentCropId)!)"
        }
        else {            
            url = "\(Utilities.getkServer())\(kSearchParentCropURL)?keysearch=&parentcropid=\(Int(parentCropId)!)&croptypeid=\(Int(cropTypeId)!)"
        }
        let communicatorForSearch: CommunicatorForSearch = CommunicatorForSearch.sharedInstance
        
        
        communicatorForSearch.getWSWithSearchFeature(url, completeBlock: { (success, result) -> Void in
            self.handleCropSearchValue((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.CropControllerDelegate_returnedError!(error , message: (error?.localizedDescription)!)
                Utilities.evenAnswer(Utilities.getNameEven(kSearchParentCropURL, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
        
//        communicatorForSearch.testCallingWS()
    }



    func searchParentCrop (_ keysearch: String, categoryId: String, parentCropId: String) {
        //TODO:
        var url = ""
        if ((categoryId == "" || categoryId.length == 0) && (parentCropId == "" || parentCropId.length == 0)) {
             url = "\(Utilities.getkServer())\(kSearchParentCropURL)?keysearch=\(keysearch)"
        }
        else {
            if (categoryId == "" || categoryId.length == 0) {
                 url = "\(Utilities.getkServer())\(kSearchParentCropURL)?keysearch=\(keysearch)&parentcropid=\(Int(parentCropId)!)"
            }
            else if (parentCropId == "" || parentCropId.length == 0) {
                 url = "\(Utilities.getkServer())\(kSearchParentCropURL)?keysearch=\(keysearch)&categoryid=\(Int(categoryId)!)"
            }
            else {
                url = "\(Utilities.getkServer())\(kSearchParentCropURL)?keysearch=\(keysearch)&categoryid=\(Int(categoryId)!)&parentcropid=\(Int(parentCropId)!)"
            }
        }
        
        let communicatorForSearch: CommunicatorForSearch = CommunicatorForSearch.sharedInstance
        communicatorForSearch.email = ClassGlobal.sharedInstance().tendUser.email
        //communicatorForSearch.password = ClassGlobal.sharedInstance().tendUser.password
        
        communicatorForSearch.getWSWithSearchFeature(url, completeBlock: { (success, result) -> Void in
            self.handleCropSearchValue((result as? Dictionary<String, AnyObject>)!)
        }) { (error) -> Void in
            self.delegate?.CropControllerDelegate_returnedError!(nil, message: "")
            Utilities.evenAnswer(Utilities.getNameEven(kSearchParentCropURL, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    func handleCropSearchValue (_ result: Dictionary<String, AnyObject>) {
        
        let json = JSON(result)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kloadlistparentcropSuccessfully) {
            let tmpDict = result["data"] as! NSDictionary
            let h: CFakeMasterCrop = (try! MTLJSONAdapter.model(of: CFakeMasterCrop.self, fromJSONDictionary: tmpDict as! [AnyHashable: Any])) as! CFakeMasterCrop
            //TODO: nvhieu search parent crop successfully
            
            self.delegate?.CropControllerDelegate_returnedSearchParentCropListResults!(true, returnedValue: h)
        }
        else {
            //TODO: nvhieu search parent crop failed
            self.delegate?.CropControllerDelegate_returnedSearchParentCropListResults!(false, returnedValue: nil)
            Utilities.evenAnswer(Utilities.getNameEven(kSearchParentCropURL, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kSearchParentCropURL))
        }
        

    }

   
    
    func deleteCrop(_ dictionary:NSDictionary){
        let url = Utilities.getkServer() +  kDeleteCropSuccession
        
        let communicationWS:CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        
        communicationWS.postWS(url, param: dictionary, completeBlock: { (success, result) -> Void in
            self.delegate?.didEndDeleteCrop_Success!(result as? Dictionary<String, AnyObject>, message: "")
            }) { (error) -> Void in
                self.delegate?.didEndDeleteCrop_Fail!(error)
                 Utilities.evenAnswer(Utilities.getNameEven(kDeleteCropSuccession, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    func undodeleteCrop(_ dictionary:String){
        //var successionID: AnyObject? = dictionary["successionId"]
        let url = Utilities.getkServer() +  kUndoDeleteCropSuccession + "?successionId=\(dictionary)"
        
        let communicationWS:CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        
        communicationWS.postWS(url, param: dictionary as AnyObject, completeBlock: { (success, result) -> Void in
            self.delegate?.didEndUndoDeleteCrop_Success!(result as? Dictionary<String, AnyObject>, message: "")
            }) { (error) -> Void in
                self.delegate?.didEndUndoDeleteCrop_Fail!(error)
                 Utilities.evenAnswer(Utilities.getNameEven(kUndoDeleteCropSuccession, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    

    func getParentList (_ farmId: Int, categoryId: Int, getAll: Bool) {
        
        var url: String = ""
        let category = categoryId == -1 ? "":"\(categoryId)"
        if (getAll) {
            
            
            if(categoryId == 6){
                url = "\(Utilities.getkServer())\(kParentCropList)?noLocation=true&farmId=\(farmId)&getAll=true"
            }else{
                url = "\(Utilities.getkServer())\(kParentCropList)?categoryId=\(category)&farmId=\(farmId)&getAll=true"
            }
            print(url, terminator: "")
        }
        else {
            //No location
            if(categoryId == 6){
                url = "\(Utilities.getkServer())\(kParentCropList)?noLocation=true&farmId=\(farmId)&getAll=false"
            }else{
                url = "\(Utilities.getkServer())\(kParentCropList)?categoryId=\(category)&farmId=\(farmId)&getAll=false"
            }
            
        }
        
        
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            
            self.parseParentCropLists((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.CropControllerDelegate_returnedError!(error, message: "")
                Utilities.evenAnswer(Utilities.getNameEven(kParentCropList, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    func parseParentCropLists (_ result: Dictionary<String, AnyObject>) {
        let json = JSON(result)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kParentCropSuccess) {
            let tmpDict = result as NSDictionary
            let g: [CParentCrop] = (try! MTLJSONAdapter.models(of: CParentCrop.self, fromJSONArray: tmpDict["data"] as! [AnyObject])) as! [CParentCrop]
            self.delegate?.CropControllerDelegate_returnedParentCropListResults!(g)
        }
        else {
            self.delegate?.CropControllerDelegate_returnedParentCropListResults!(nil)
            Utilities.evenAnswer(Utilities.getNameEven(kParentCropList, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kParentCropList))
        }
    }
    
    // updated follow tend-2061
    func gruopSuccessionsByYear(_ returnedValue: [CSuccessionSimple]){
        //Gruop succession list with year
        let retVal: NSMutableDictionary = NSMutableDictionary()
        
        for succession in returnedValue {
            let endyear : String  = Utilities.timeFromTimeStamp(withFormat: succession.enddate, format: "yyyy")
            let startyear: String = Utilities.timeFromTimeStamp(withFormat: succession.startdate, format: "yyyy")
            
            // crop is a Biennial or Perennial
            var array: AnyObject?
            if (Int(endyear)! - Int(startyear)! > 0) {
                for y in stride(from: (Int(endyear)!), through: Int(startyear)!, by: -1) {
                    array = retVal.object(forKey: String(y)) as AnyObject?
                    if (array == nil) {
                        array = NSMutableArray()
                        retVal.setObject(array!, forKey: String(y) as NSCopying)
                    }
                    array?.add(succession)
                }
            }
                // crop is started and ended in the same year
            else {
                var array: AnyObject? = retVal.object(forKey: endyear) as AnyObject?
                if (array == nil) {
                    array = NSMutableArray()
                    retVal.setObject(array!, forKey: endyear as NSCopying)
                }
                array?.add(succession)
            }
            
        }
        if(self.delegate?.CropControllerDelegate_gruopDataSuccessionList != nil){
            self.delegate?.CropControllerDelegate_gruopDataSuccessionList!(retVal)
        }
    }
    
    func getcroplistfromparentcropid (_ farmId: Int, parentCropId: Int, getAll: Bool) {
        var url = ""
        if (getAll) {
            url = "\(Utilities.getkServer())\(kGetSuccessionListItem)?clientDate=\(Utilities.convertDateTime(toTimeStamp: Utilities.dateAtBeginningOfDay(for: Date())))&parentCropId=\(parentCropId)&farmId=\(farmId)&getAll=true"
        }
        else {
            url = "\(Utilities.getkServer())\(kGetSuccessionListItem)?clientDate=\(Utilities.convertDateTime(toTimeStamp: Utilities.dateAtBeginningOfDay(for: Date())))&parentCropId=\(parentCropId)&farmId=\(farmId)&getAll=false"
        }
    
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            
            self.parseCropListsItem((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.CropControllerDelegate_returnedError!(error, message: "")
                Utilities.evenAnswer(Utilities.getNameEven(kGetSuccessionListItem, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    // PhongLe
    func getCropListSuccessNearByFromParentCropId (_ radius: Int) {
        
        var url = Utilities.getkServer() +  kGetSuccessionNearBy + String(ClassGlobal.sharedInstance().tendUser.farmID)
        url += "&lat=\(TendAppController.shareInstance().currentGPS.latitude)&lng=\(TendAppController.shareInstance().currentGPS.longitude)&radius=\(radius)"

        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            
            self.parseCropListsItem((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.CropControllerDelegate_returnedError!(error, message: "")
                Utilities.evenAnswer(Utilities.getNameEven(kGetSuccessionNearBy, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }

    
    func parseCropListsItem (_ result: Dictionary<String, AnyObject>) {
        let json = JSON(result)
        
        let code: String = json["meta"]["code"].stringValue
//        var message: String = json["meta"]["message"].stringValue
        
        if (code == kSuccessionListSuccess) {
            let tmpDict = result as NSDictionary
            let g: [CSuccessionSimple] = (try! MTLJSONAdapter.models(of: CSuccessionSimple.self, fromJSONArray: tmpDict["data"] as! [AnyObject])) as! [CSuccessionSimple]
            
            self.delegate?.CropControllerDelegate_returnedSuccessionListItemResults!(g)
        }
        else {
            self.delegate?.CropControllerDelegate_returnedSuccessionListItemResults!(nil)
            Utilities.evenAnswer(Utilities.getNameEven(kGetSuccessionListItem, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kGetSuccessionListItem))
        }
    }
    

}
