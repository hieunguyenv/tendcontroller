//
//  Crop_SuccessionDataController.m
//  TendGrow
//
//  Created by hungnguyeniOS on 9/24/15.
//  Copyright (c) 2015 spiraledge.com. All rights reserved.
//

#import "Crop_SuccessionDataController.h"
#import "CMasterCropSuccession.h"

#define kTypeGruopT @"T"
#define kTypeGruopDS @"DS"
#define kTypeGruopBS @"BS"
#define kTypeGruopDSS @"DSS"

#define kDirectSow @"Direct Sow"
#define kDirectSowBanSwatch @"Direct Sow: Band/Swath"
#define kTransPlantFromPurchased @"Transplant from Purchased"
#define kGreenhouseSow @"Greenhouse Sow"
#define kBroadcast @"Broadcast Sow"
#define kDrill @"Drill Sow"

@implementation Crop_SuccessionDataController

-(SuccessionSeedingMethodType )seedingMethodTypeByeString:(NSString *)nameSeedingMethod{
    if ([nameSeedingMethod isEqualToString:kGreenhouseSow]) {
        return GREENHOUSE_SOW_S;
    }
    if ([nameSeedingMethod isEqualToString:kTransPlantFromPurchased]) {
        return TRANSPLANT_PURCHASED_S;
    }
    if ([nameSeedingMethod isEqualToString:kDirectSow]) {
        return DIRECT_SOW_S;
    }
    if ([nameSeedingMethod isEqualToString:kDirectSowBanSwatch]) {
        return DIRECT_SOW_BAND_SWATH;
    }
    if ([nameSeedingMethod isEqualToString:kBroadcast]) {
        return BROADCAST_SOWN;
    }
    if ([nameSeedingMethod isEqualToString:kDrill]) {
        return DRILL_SOWN;
    }
    return GREENHOUSE_SOW_S;
    
    
}

-(NSString*)seedingMethodTypeByeEnum:(SuccessionSeedingMethodType)plantingMethod{
    if ( plantingMethod == GREENHOUSE_SOW_S ) {
        return kGreenhouseSow;
    }
    if ( plantingMethod == TRANSPLANT_PURCHASED_S ) {
        return kTransPlantFromPurchased ;
    }
    if ( plantingMethod == DIRECT_SOW_S) {
        return kDirectSow;
    }
    if ( plantingMethod == DIRECT_SOW_BAND_SWATH) {
        return kDirectSowBanSwatch;
    }
    if ( plantingMethod == BROADCAST_SOWN) {
        return kBroadcast;
    }
    if ( plantingMethod == DRILL_SOWN) {
        return kDrill;
    }
    return kGreenhouseSow;
    
    
}

-(NSArray *)returnListSeedingMethod:(CMasterCropSuccession *)data{
    NSMutableArray * listSeedingMethodTemp=[NSMutableArray new];
    for (int i =0; i<data.links.groups.count; i++) {
        CMasterCropGroup * gropCombinationData=[data.links.groups objectAtIndex:i];
        NSString * nameGruop=[gropCombinationData groupname];
        if ([nameGruop isEqualToString:kTypeGruopT]) {
            [listSeedingMethodTemp addObject:kGreenhouseSow];
            [listSeedingMethodTemp addObject:kTransPlantFromPurchased];
            
        }
        if ([nameGruop isEqualToString:kTypeGruopDS]) {
            [listSeedingMethodTemp addObject:kDirectSow];
            [listSeedingMethodTemp addObject:kDirectSowBanSwatch];
            [listSeedingMethodTemp addObject:kTransPlantFromPurchased];
        }
        if ([nameGruop isEqualToString:kTypeGruopBS]) {
            [listSeedingMethodTemp addObject:kBroadcast];
        }
        if ([nameGruop isEqualToString:kTypeGruopDSS]) {
            [listSeedingMethodTemp addObject:kDrill];
        }
        
    }
    NSMutableArray * arrayFinal=[NSMutableArray new];
    for(id e in listSeedingMethodTemp)
    {
        if(![arrayFinal containsObject:e])
        {
            [arrayFinal addObject:e];
        }
    }
    return arrayFinal;
    
}
-(void)listGrowingMethod:(SuccessionSeedingMethodType)MethodType andData:(CMasterCropSuccession *)data andCompelete:(void (^)(NSArray *, NSArray *))complete{
    NSString * type;
    
    if (MethodType== GREENHOUSE_SOW_S) {
        //Get T
        type=kTypeGruopT;
        
    }
    else if ( MethodType == DRILL_SOWN){
        type = kTypeGruopDSS;
    }
    else if (MethodType == BROADCAST_SOWN){
        type = kTypeGruopBS;
    }
    else if(MethodType==DIRECT_SOW_S||MethodType==DIRECT_SOW_BAND_SWATH){
        //Get DS
        type=kTypeGruopDS;
    }else{
        //Get DS OR T
        type=[self checkGetDsOrT:data];
        
    }
    [self listCombinationDataWithType:type andTypeEnum:MethodType andCompelete:complete andData:data];
    
}
-(NSString *)checkGetDsOrT:(CMasterCropSuccession *)data {
    if (data.links.groups.count==2) {
        return kTypeGruopT;
    }else if([[[data.links.groups objectAtIndex:0] groupname] isEqualToString:kTypeGruopT]){
        return kTypeGruopT;
    }else{
        return kTypeGruopDS;
    }
}
-(void)listCombinationDataWithType:(NSString *)stringType andTypeEnum:(SuccessionSeedingMethodType)typeEnum andCompelete:(void(^)(NSArray * listGrowingMethodReturn , NSArray * listCombinationDataReturn))complete andData:(CMasterCropSuccession *)data{

    NSMutableArray * listGrowingMethodTemp=[NSMutableArray new];
    NSMutableArray * listCombinationDataTemp=[NSMutableArray new];
    for (int i =0; i<data.links.groups.count; i++) {
        CMasterCropGroup * gropCombinationData=[data.links.groups objectAtIndex:i];
        NSString * nameGruop=[gropCombinationData groupname];
        if ([nameGruop isEqualToString:stringType]) {
            for (int j=0; j<gropCombinationData.links.combinationdatas.count; j++) {
                CMasterCropCombinationData * combinationDataTemp =[gropCombinationData.links.combinationdatas objectAtIndex:j];
                if (combinationDataTemp.plantingmethodid == typeEnum) {
                    [listGrowingMethodTemp addObject:combinationDataTemp.growingmethod];
                    [listCombinationDataTemp addObject:combinationDataTemp];
                }
            }
        }
    }
    if (listCombinationDataTemp.count == 0) {
        for (int i =0; i<data.links.groups.count; i++) {
            CMasterCropGroup * gropCombinationData=[data.links.groups objectAtIndex:i];
            NSString * nameGruop=[gropCombinationData groupname];
            if ([nameGruop isEqualToString:stringType]) {
                for (int j=0; j<gropCombinationData.links.combinationdatas.count; j++) {
                    CMasterCropCombinationData * combinationDataTemp =[gropCombinationData.links.combinationdatas objectAtIndex:j];
                    if (combinationDataTemp.growingmethod != nil && ![combinationDataTemp.growingmethod isEqualToString:@""]) {
                        [listGrowingMethodTemp addObject:combinationDataTemp.growingmethod];
                    }
                    
                    [listCombinationDataTemp addObject:combinationDataTemp];
                    
                }
                
            }
        }
    }
    NSMutableArray * array2=[[NSMutableArray alloc]init];
    for (id obj in listGrowingMethodTemp)
    {
        if (![array2 containsObject:obj])
        {
            [array2 addObject: obj];
        }
    }
    
    listGrowingMethodTemp = array2;
    
    complete(listGrowingMethodTemp,listCombinationDataTemp);
}
-(SuccessionSeedingMethodType)getSuccessionDefaltGrowingMethod:(CMasterCropSuccession*)data{
    SuccessionSeedingMethodType defautGrowing = NONE_SEEDINGMETHOD;
    NSString* groupname = @"";
    int plantingamountid = 0;
    BOOL isHaveGrowing = false;
    double latedValue = 0;
    for (int i =0; i<data.links.groups.count; i++) {
        CMasterCropGroup * gropCombinationData=[data.links.groups objectAtIndex:i];
//        NSString * nameGruop=[gropCombinationData groupname];
        
        NSMutableArray * listLatest = [[NSMutableArray alloc] init];
        for (int j=0; j<gropCombinationData.links.combinationdatas.count; j++) {
            CMasterCropCombinationData * combinationDataTemp =[gropCombinationData.links.combinationdatas objectAtIndex:j];
            
            if (combinationDataTemp.latest) {
                [listLatest addObject:combinationDataTemp];
                
                isHaveGrowing = true;
            }
            
        }
        
        if (listLatest.count > 0) {
            if (listLatest.count == 1) {
                CMasterCropCombinationData * temp = [listLatest objectAtIndex:0];
                if (temp.modifieddate > latedValue) {
                    groupname = [gropCombinationData groupname];
                    latedValue = temp.modifieddate;
                    plantingamountid = temp.plantingmethodid;
                }
            } else {
                BOOL isHaveMastercrop = false;
                NSMutableArray * listMastercrop = [[NSMutableArray alloc] init];
                for (int j = 0; j < listLatest.count; j++) {
                    CMasterCropCombinationData * temp = [listLatest objectAtIndex:j];
                    if ([temp.mastercropname isEqualToString: data.varietyname]) {
                        [listMastercrop addObject:temp];
                        isHaveMastercrop = true;
                    }
                }
                
                if (listMastercrop.count > 0) {
                    if (listMastercrop.count == 1) {
                        CMasterCropCombinationData * temp = [listMastercrop objectAtIndex:0];
                        if (temp.modifieddate > latedValue) {
                            groupname = [gropCombinationData groupname];
                            plantingamountid = temp.plantingmethodid;
                            latedValue = temp.modifieddate;
                        }
                        
                    } else {
                        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifieddate" ascending:NO];
                        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                        listMastercrop = [listMastercrop sortedArrayUsingDescriptors:sortDescriptors];
                        CMasterCropCombinationData * temp = [listMastercrop objectAtIndex:0];
                        if (temp.modifieddate > latedValue) {
                            groupname = [gropCombinationData groupname];
                            plantingamountid = temp.plantingmethodid;
                            latedValue = temp.modifieddate;
                        }
                    }
                }
                
                if (!isHaveMastercrop) {
                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifieddate" ascending:NO];
                    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                    listLatest = [listLatest sortedArrayUsingDescriptors:sortDescriptors];
                    CMasterCropCombinationData * temp = [listLatest objectAtIndex:0];
                    if (temp.modifieddate > latedValue) {
                        groupname = [gropCombinationData groupname];
                        plantingamountid = temp.plantingmethodid;
                        latedValue = temp.modifieddate;
                    }
                }
                
            }
        }

        if (isHaveGrowing == false) {
            for (int j=0; j<gropCombinationData.links.combinationdatas.count; j++) {
                CMasterCropCombinationData * combinationDataTemp =[gropCombinationData.links.combinationdatas objectAtIndex:j];
                if (combinationDataTemp.iscroptypedefault == true ) {
                    groupname = [gropCombinationData groupname];
                    plantingamountid = combinationDataTemp.plantingmethodid;
                    break;
                }
                
            }
        }

    }
    
    if (![groupname isEqualToString:@""] && plantingamountid != 0) {
        defautGrowing = (SuccessionSeedingMethodType)plantingamountid;
    }
    return defautGrowing;
}

@end
