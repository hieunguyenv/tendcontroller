//
//  ConvertSuccessionDataToCombidationGroup.swift
//  TendGrow
//
//  Created by Nguyen Trung on 4/8/16.
//  Copyright (c) 2016 spiraledge.com. All rights reserved.
//

import UIKit

class ConvertSuccessionDataToCombidationGroup: NSObject {
    func convertSuccessionToCombidation(_ successionData: CSuccession) -> CMasterCropCombinationData {
        let mastercombidationdata:CMasterCropCombinationData = CMasterCropCombinationData()
        mastercombidationdata.id = 0
        mastercombidationdata.combinationkeyid = successionData.combinationkey
        mastercombidationdata.growingmethod = successionData.extendProp.growingmethodname
        mastercombidationdata.harvestunit = successionData.harvestunit
        mastercombidationdata.rowspacinglow = successionData.rowspacinglow
        mastercombidationdata.inrowspacinglow = successionData.inrowspacinglow
        mastercombidationdata.inrowspacinghigh = successionData.inrowspacinghigh
        mastercombidationdata.inrowspacing = successionData.inrowspacing
        mastercombidationdata.rowspacinghigh = successionData.rowspacinghigh
        mastercombidationdata.rowsperbed = successionData.rowperbed
        mastercombidationdata.rowperbed = successionData.rowperbed
        mastercombidationdata.rowfeet = successionData.rowfeet
        mastercombidationdata.flatsize = successionData.flatsize
        mastercombidationdata.numofflats = Float(successionData.numofflats)
        mastercombidationdata.seedingratevalue = Double(successionData.seedingratevalue)//seedingrate
        mastercombidationdata.bedwidth = successionData.bedwidth
        mastercombidationdata.seedspercell = Int32(successionData.seedpercell)
        mastercombidationdata.numofplants = successionData.numofplants
        mastercombidationdata.adjusteddtmlow = Int32(successionData.adjusteddtmlow)
        mastercombidationdata.pelletedoption = Int32(successionData.pelletedoption)
        mastercombidationdata.mastercropid = Int32(successionData.extendProp.mastercropid)
        mastercombidationdata.seedpercellhigh = Int32(successionData.seedpercellhigh)
        mastercombidationdata.daytomaturityll = Int32(successionData.daytomaturityll)
        mastercombidationdata.daytotransplant = Int32(successionData.daytotransplant)
        mastercombidationdata.daystotransplantdatefromghl = Int32(successionData.daystotransplantdatefromghl)
        mastercombidationdata.daystotransplantdatefromghh = Int32(successionData.daystotransplantdatefromghh)
        mastercombidationdata.daystomaturityadjtransplantl = Int32(successionData.daystomaturityadjtransplantl)
        mastercombidationdata.daystomaturityadjtransplanth = Int32(successionData.daystomaturityadjtransplanth)
        mastercombidationdata.daystomaturityadjdirectsowingl = Int32(successionData.daystomaturityadjdirectsowingl)
        mastercombidationdata.daystomaturityadjdirectsowingh = Int32(successionData.daystomaturityadjdirectsowingh)
        mastercombidationdata.driplineperbed = Int32(successionData.driplineperbed)
        mastercombidationdata.plantingmethodid = Int32(successionData.extendProp.plantingid)
        mastercombidationdata.plantingmethodname = successionData.extendProp.plantingname
        mastercombidationdata.numoftrees = Int32(successionData.numoftrees)
        mastercombidationdata.harvestseasonid = Int32(successionData.harvestseasonid)
        mastercombidationdata.plantingyear = Int32(successionData.year)
        mastercombidationdata.seedingratelow = successionData.seedingratelow
        mastercombidationdata.seedingratehigh = successionData.seedingratehigh
        mastercombidationdata.bandsseedingrateperfoot = successionData.bandsseedingrateperfoot
        mastercombidationdata.seedingrateunitid = Int32(successionData.seedingrateunitid)
        mastercombidationdata.seedingrateunitname = successionData.seedingrateunitname
        mastercombidationdata.plantingamount = Float(successionData.plantingamount)
        mastercombidationdata.plantingamountunit = successionData.plantingamountunit
        mastercombidationdata.saleprice = successionData.saleprice
        mastercombidationdata.salepriceunit = successionData.salepriceunit
        mastercombidationdata.yieldrate = successionData.yieldrate
     
        mastercombidationdata.numofflatsunitid = Int32(successionData.numofflatsunitid)
        mastercombidationdata.seedtype = successionData.seedtype
        mastercombidationdata.harvestwindow = Int32(successionData.daytomaturityll)
        mastercombidationdata.daymaturityaverage = Int32(successionData.adjusteddtmlow)
        mastercombidationdata.averagedaysgreenhouse = Int32(successionData.daytotransplant)
        mastercombidationdata.retailpricedefault = Float(successionData.saleprice)
        mastercombidationdata.retailpricedefaultunit = successionData.salepriceunit
        
        return mastercombidationdata
    }
}
