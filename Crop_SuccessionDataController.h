//
//  Crop_SuccessionDataController.h
//  TendGrow
//
//  Created by hungnguyeniOS on 9/24/15.
//  Copyright (c) 2015 spiraledge.com. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CMasterCropSuccession;

@interface Crop_SuccessionDataController : NSObject
-(NSArray *)returnListSeedingMethod:(CMasterCropSuccession *)data;
-(void)listGrowingMethod:(SuccessionSeedingMethodType )MethodType andData:(CMasterCropSuccession*)data andCompelete:(void(^)(NSArray * listGrowingMethodReturn , NSArray * listCombinationDataReturn))complete;
-(SuccessionSeedingMethodType )seedingMethodTypeByeString:(NSString *)nameSeedingMethod;
-(SuccessionSeedingMethodType)getSuccessionDefaltGrowingMethod:(CMasterCropSuccession*)data;
-(NSString*)seedingMethodTypeByeEnum:(SuccessionSeedingMethodType)plantingMethod;
@end
