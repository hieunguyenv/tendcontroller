//
//  SuccessionController.swift
//  TendGrow
//
//  Created by Nguyen Trung on 3/25/16.
//  Copyright (c) 2016 spiraledge.com. All rights reserved.
//

import UIKit

@objc protocol SuccessionControllerDelegate {
    
    @objc optional func didEndCallAPISuccessionControllerFail(_ error:NSError?)
    @objc optional func didEndGetHistoryDescription(_ error:NSError?)
    @objc optional func didEndgetCombinationDataSuccession(_ result:NSDictionary, masterCropDto:CMasterCropSuccession, isSuccess:Bool)
    @objc optional func didEndgetHistoryDescription(_ result:NSArray, isSuccess:Bool)
    @objc optional func didEndgetListUnitParentCrop(_ result:NSArray, isSuccess:Bool)
    @objc optional func didEndGetConvertRateDataSuccession(_ result:NSDictionary?, listConvertRate:[CConvertTrates]?, isSuccess:Bool)
    @objc optional func didEndGetOrchardLocation(_ result:NSDictionary?, orchardLocation:DetailClassField?, isSuccess:Bool)
    @objc optional func didEndSaveTrackingSuccession(_ result:NSDictionary?, error:NSError?)
    @objc optional func didEndGetTrackingSuccession(_ clsTracking:ClassTrackingSuccession?, error:NSError?)
    @objc optional func didEndGetSuccessionDetailCrop(_ result: NSDictionary?, cropSuccession: CSuccession?, isSuccess: Bool, error: NSError?)
    @objc optional func didEndAddSuccession(_ result: NSDictionary?, isSuccess: Bool, error: NSError?)
}
class SuccessionController: NSObject {
    weak var delegate: SuccessionControllerDelegate? = nil
    
    func loadCombinationDataSuccession(_ parentcroptypeid:Int, mastercropid:Int, croptypeid:Int, parentcropid:Int, categoryid:Int){
        var url: String = "\(Utilities.getkServer())\(kGetCombinationDataByCropSuccession)?farmid=\(ClassGlobal.sharedInstance().tendUser.farmID)"
        if categoryid == CategoryEnum.fruits.rawValue || categoryid == CategoryEnum.trees_.rawValue || categoryid == CategoryEnum.berries.rawValue || categoryid == CategoryEnum.vines.rawValue {
//            url = "\(kServer)\(kGetCombinationDataByCropSuccessionForFruits)?farmid=\(ClassGlobal.sharedInstance().tendUser.farmID)"
            url = "\(Utilities.getkServer())\(kGetCombinationDataByCropSuccession)?farmid=\(ClassGlobal.sharedInstance().tendUser.farmID)"
        }
        if mastercropid != 0{
            url = url + "&mastercropid=\(mastercropid)"
            if croptypeid != 0{
                url = url + "&croptypeid=\(croptypeid)"
            }
            if parentcropid != 0{
                url = url + "&parentcropid=\(parentcropid)"
            }
        }
        else {
//            if parentcroptypeid != 0{
//                url = url + "&parentcroptypeid=\(parentcroptypeid)"
//            }
            
            if croptypeid != 0{
                url = url + "&croptypeid=\(croptypeid)"
            }
            if parentcropid != 0{
                url = url + "&parentcropid=\(parentcropid)"
            }
        }
                        
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            self.parseLoadCombidationData((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.didEndCallAPISuccessionControllerFail!(error)
                Utilities.evenAnswer(Utilities.getNameEven(kGetCombinationDataByCropSuccession, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    func getHistoryDescription(_ parentcropid: Int){
        let url: String = "\(Utilities.getkServer())mobile/succession/onewordlist?parentCropId=\(parentcropid)"
        
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            self.parseHistoryDescription((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.didEndGetHistoryDescription!(error)
                Utilities.evenAnswer(Utilities.getNameEven("onewordlist", andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    func parseHistoryDescription (_ returned: Dictionary<String, AnyObject>) {
        let json = JSON(returned)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        print("code: \(code), message: \(message)", terminator: "")
        
        if (code == "7_11_s") {
            let tmpDict = returned["data"] as! NSArray
            self.delegate?.didEndgetHistoryDescription!(tmpDict, isSuccess: true)
        }
        else {
            self.delegate?.didEndGetHistoryDescription!(nil)
            Utilities.evenAnswer(Utilities.getNameEven("onewordlist", andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: "onewordlist"))
        }
    }
    
    func getListUnitPatentCrop(_ parentcropid: Int){
        let url: String = "\(Utilities.getkServer())\(kWGetListUnitParentCrop)?farmId=\(ClassGlobal.sharedInstance().tendUser.farmID)&parentcropid=\(parentcropid)"
        
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            self.parseListUnitParentCrop((result as? Dictionary<String, AnyObject>)!)
        }) { (error) -> Void in
            self.delegate?.didEndGetHistoryDescription!(error)
            Utilities.evenAnswer(Utilities.getNameEven(kWGetListUnitParentCrop, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    func parseListUnitParentCrop (_ returned: Dictionary<String, AnyObject>) {
        let json = JSON(returned)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        print("code: \(code), message: \(message)", terminator: "")
        
        if (code == "13_1_s") {
            let tmpDict = returned["data"] as! NSArray
            self.delegate?.didEndgetListUnitParentCrop!(tmpDict, isSuccess: true)
        }
        else {
            self.delegate?.didEndGetHistoryDescription!(nil)
            Utilities.evenAnswer(Utilities.getNameEven(kWGetListUnitParentCrop, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kWGetListUnitParentCrop))
        }
    }
    
    func getSuccessionDetail(_ successionid: Int) {
        
        let url: String = Utilities.getkServer() + kGetSuccessionDetail + "?successionid=" + "\(successionid)" + "&farmid=" + "\(ClassGlobal.sharedInstance().tendUser.farmID)"
        
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            self.parseSuccessionDetail((result as? Dictionary<String, AnyObject>)!)
        }) { (error) -> Void in
            self.delegate?.didEndGetSuccessionDetailCrop!(nil, cropSuccession: nil, isSuccess: false, error: error)
            Utilities.evenAnswer(Utilities.getNameEven(kGetSuccessionDetail, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
        
    }
    
    func parseSuccessionDetail (_ returned: Dictionary<String, AnyObject>) {
        let json = JSON(returned)
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        print("code: \(code), message: \(message)", terminator: "")
        let tmpDict = returned["data"] as! NSDictionary
        if (code == kGetCropSuccessionById) {
            let combinationSuccessionDetail:CSuccession = (try! MTLJSONAdapter.model(of: CSuccession.self, fromJSONDictionary: tmpDict as! [AnyHashable: Any])) as! CSuccession
            self.delegate?.didEndGetSuccessionDetailCrop!(tmpDict, cropSuccession: combinationSuccessionDetail, isSuccess: true, error: nil)
        }
        else {
            self.delegate?.didEndGetSuccessionDetailCrop!(tmpDict, cropSuccession: nil, isSuccess: false, error: nil)
            Utilities.evenAnswer(Utilities.getNameEven(kGetSuccessionDetail, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kGetSuccessionDetail))
        }
    }
    
    func addSuccession(_ dictForSave: NSDictionary) {
        let url: String = Utilities.getkServer() + kAddSuccession
        
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.postJsonWS(url, param: dictForSave, completeBlock: { (success, result) -> Void in
            if(success) {
                let json = JSON(result!)
                let code: String = json["meta"]["code"].stringValue
                if code == kAddSuccessionSuccess {
                    self.delegate?.didEndAddSuccession!(result, isSuccess: true, error: nil)
                } else {
                    self.delegate?.didEndAddSuccession!(result, isSuccess: false, error: nil)
                    Utilities.evenAnswer(Utilities.getNameEven(kAddSuccession, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: url))
                }
            }
            else {
                self.delegate?.didEndAddSuccession!(result, isSuccess: false, error: nil)
                Utilities.evenAnswer(Utilities.getNameEven(kAddSuccession, andResponse: kNoDataReturn), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: kNoDataReturn, andUrl: url))
            }
        }) { (error) -> Void in
            self.delegate?.didEndAddSuccession!(nil, isSuccess: false, error: error)
            Utilities.evenAnswer(Utilities.getNameEven(kAddSuccession, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    
    //MARK: - parse result of parent crop list
    func parseLoadCombidationData (_ returned: Dictionary<String, AnyObject>) {
        let json = JSON(returned)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        print("code: \(code), message: \(message)", terminator: "")
        
        if (code == kLoadCombinationSuccessionSuccess) {
            let tmpDict = returned["data"] as! NSDictionary
//            let combinationSuccession: CMasterCropSuccession = (try! MTLJSONAdapter.modelOfClass(CMasterCropSuccession.self, fromJSONDictionary: tmpDict as [NSObject : AnyObject])) as! CMasterCropSuccession
            let combinationSuccession: CMasterCropSuccession?
            
//            combinationSuccession = (try! MTLJSONAdapter.modelOfClass(CMasterCropSuccession.self, fromJSONDictionary: tmpDict as [NSObject : AnyObject])) as? CMasterCropSuccession
//            self.delegate?.didEndgetCombinationDataSuccession!(tmpDict, masterCropDto: combinationSuccession!, isSuccess: true)
            do {
                combinationSuccession = try MTLJSONAdapter.model(of: CMasterCropSuccession.self, fromJSONDictionary: tmpDict as [NSObject : AnyObject]!) as? CMasterCropSuccession
                self.delegate?.didEndgetCombinationDataSuccession!(tmpDict, masterCropDto: combinationSuccession!, isSuccess: true)
            }
            catch _{
                combinationSuccession = CMasterCropSuccession()
                self.delegate?.didEndgetCombinationDataSuccession!(tmpDict, masterCropDto: combinationSuccession!, isSuccess: false)
            }
        }
            
        else {
            self.delegate?.didEndCallAPISuccessionControllerFail!(nil)
            Utilities.evenAnswer(Utilities.getNameEven(kGetCombinationDataByCropSuccession, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kGetCombinationDataByCropSuccession))
        }
    }
    
    func getconvertrate(_ parentcropid:Int){
        let url: String = "\(Utilities.getkServer())\(kGetConvertRateByParentCrop)?parentcropid=\(parentcropid)"
        print("getconvertrate url: \(url)", terminator: "")
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            self.parseConvertRateData((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.didEndGetConvertRateDataSuccession!(nil, listConvertRate: nil, isSuccess: false)
                Utilities.evenAnswer(Utilities.getNameEven(kGetConvertRateByParentCrop, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
        
    }
    //MARK: - parse result of parent crop list
    func parseConvertRateData (_ returned: Dictionary<String, AnyObject>) {
        let json = JSON(returned)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        swiftlog("code: \(code), message: \(message)")
        
        if (code == kloadlistparentcropSuccessfully) {
            let listConvertRate:[CConvertTrates] =  (try! MTLJSONAdapter.models(of: CConvertTrates.self, fromJSONArray: returned["data"] as! [AnyObject]))  as! [CConvertTrates]
            self.delegate?.didEndGetConvertRateDataSuccession!(returned["data"] as? NSDictionary, listConvertRate: listConvertRate, isSuccess: true)
        }
            
        else {
            self.delegate?.didEndGetConvertRateDataSuccession!(nil, listConvertRate: nil, isSuccess: false)
            Utilities.evenAnswer(Utilities.getNameEven(kGetConvertRateByParentCrop, andResponse: "\(code)" ), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kGetConvertRateByParentCrop))
        }
    }

    func getTreeLocationFromTree(_ treeLocation:CLLocationCoordinate2D){
        let url: String = "\(Utilities.getkServer())\(kGetOrchardFromTreeLocation)?lat=\(treeLocation.latitude)&lng=\(treeLocation.longitude)"
//        http://alpha-growapi.tend.ag/mobile/location/getbylnglat?lat=10.7828156262164&lng=106.704035326838
        print("getLocationMap url: \(url)", terminator: "")
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            self.parseOrchardLocation((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.didEndGetOrchardLocation!(nil, orchardLocation: nil, isSuccess: false)
                Utilities.evenAnswer(Utilities.getNameEven(kGetOrchardFromTreeLocation, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    //MARK: - parse result of parent crop list
    func parseOrchardLocation (_ returned: Dictionary<String, AnyObject>) {
        let json = JSON(returned)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        print("code: \(code), message: \(message)", terminator: "")
        
        if (code == kGetBlockInformationSuccess) {
            let tmpDict = returned["data"] as! NSDictionary
            let orchard:DetailClassField = (try! MTLJSONAdapter.model(of: DetailClassField.self, fromJSONDictionary: tmpDict as! [AnyHashable: Any])) as! DetailClassField
            
            self.delegate?.didEndGetOrchardLocation!(returned["data"] as? NSDictionary, orchardLocation: orchard, isSuccess: true)
        }
            
        else {
            self.delegate?.didEndGetOrchardLocation!(nil, orchardLocation: nil, isSuccess: false)
            Utilities.evenAnswer(Utilities.getNameEven(kGetOrchardFromTreeLocation, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kGetOrchardFromTreeLocation))
        }
    }
    func updateTrackingSuccession(_ param:NSDictionary){
        let url: String = "\(Utilities.getkServer())\(kSaveTrackingSuccession)"
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.postJsonWS(url, param: param, completeBlock: { (success, result) -> Void in
            self.delegate?.didEndSaveTrackingSuccession!(result, error: nil)
        }) { (error) -> Void in
            self.delegate?.didEndSaveTrackingSuccession!(nil, error: error)
            Utilities.evenAnswer(Utilities.getNameEven(kSaveTrackingSuccession, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
        
    }
    func getTrackingSuccession(_ successionid:Int){
        let url: String = "\(Utilities.getkServer())\(kGetTrackingSuccession)?successionId=\(successionid)"
        print("getTrackingSuccession url: \(url)")
        let communicationWS: CommunicatorWS = CommunicatorWS()
        communicationWS.email = ClassGlobal.sharedInstance().tendUser.email
        //communicationWS.password = ClassGlobal.sharedInstance().tendUser.password
        communicationWS.getWS(url, completeBlock: { (success, result) -> Void in
            self.parseTrackingSuccession((result as? Dictionary<String, AnyObject>)!)
            }) { (error) -> Void in
                self.delegate?.didEndGetTrackingSuccession!(nil, error: error)
                Utilities.evenAnswer(Utilities.getNameEven(kGetTrackingSuccession, andResponse: "\(error?.localizedDescription ?? "")"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(error?.localizedDescription ?? "")", andUrl: url))
        }
    }
    //MARK: - parse result of parent crop list
    func parseTrackingSuccession (_ returned: Dictionary<String, AnyObject>) {
        let json = JSON(returned)
        
        let code: String = json["meta"]["code"].stringValue
        let message: String = json["meta"]["message"].stringValue
        print("code: \(code), message: \(message)", terminator: "")
        
        if (code == kGetSuccessionTimelineSuccess) {
            let tmpDict = returned["data"] as! NSDictionary
            let clsTracking:ClassTrackingSuccession = (try! MTLJSONAdapter.model(of: ClassTrackingSuccession.self, fromJSONDictionary: tmpDict as! [AnyHashable: Any])) as! ClassTrackingSuccession
            self.delegate?.didEndGetTrackingSuccession!(clsTracking, error: nil)
        }
            
        else {
            self.delegate?.didEndGetTrackingSuccession!(nil, error: nil)
            Utilities.evenAnswer(Utilities.getNameEven(kGetTrackingSuccession, andResponse: "\(code)"), andAttribute: Utilities.createDicEvent(#function, andFileName: #file, andInfoMsg: kGetDataFail, andResponse: "\(code)", andUrl: kGetTrackingSuccession))
        }
    }
}
