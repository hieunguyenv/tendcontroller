//
//  Crop_SuccessionController.m
//  TendGrow
//
//  Created by hungnguyeniOS on 9/23/15.
//  Copyright (c) 2015 spiraledge.com. All rights reserved.
//

#if alphaTend
#import "TendGrow-Swift.h"
#else
#import "TendGrow_Pro-Swift.h"
#endif
#import "ClassGlobal.h"
#import "Crop_SuccessionController.h"
#import "AT_TableViewController.h"

#define kValue1  1
#define kValue2  2
#define kValue3  3

@interface Crop_SuccessionController(){
    NSMutableArray * dataShows;
    NSMutableArray* listMethod;
    NSMutableArray * listCropSuccessionSeleted;
    
    NSString * parentCropName;
    NSArray * listSeedSource;
    NSArray * listNonPelleted;
    NSArray * listAnnual;
    NSArray * listHarvestSeason;
    NSArray * listPlatingYear;
    //Value caculator
    int bedLengthPlantCL;
    double bedWitdPlantCL;
    double bedWidthEffect;
    
    int rowPerBedPlantCL;
    double inRowSpacingPlantCL;
    double rowFeetPlantCl;
    
    int traySizePlantCL;
    int seedPerCellPlantCL;
    int numberOfTraysPlantCL;
    int numberOfPlantCL;
    int numberOfBedPlantCl;
    
    double totalBedInField;
    
    NSArray * listDripLinesPerBed;
    NSArray * listUnit;
    
    
    double dateIntervalSeedingDate;
    double dateIntervalDayToTransplant;
    double dateIntervalDayToFirstHarvest;
    double dateIntervalDayToLastHarvest;
    
    double numberDayToTransplant;
    double numberDayToFirstHarvest;
    double numberDayToLastHarvest;
    
    NSString * locationName;
    
    int rowpacinglow;
    
    NSString* growingmethodname;
    
}
@end
@implementation Crop_SuccessionController

-(void)createListObjectiveAddSuccession:(NSString *)nameCrop{
    
    parentCropName= nameCrop;
    [self createObjctive];
    
    
    if (self.clsSuccessionDetail.id == 0) {
        growingmethodname = self.combinationDataForShow.growingmethod;
        
        dataShows=[NSMutableArray arrayWithArray:[self listDataShowTop]];
        if (self.categoryId == 3) {
            [dataShows addObjectsFromArray:[self createListFistLoadForFruits]];
        }
        else{
            [dataShows addObjectsFromArray:[self createListFistLoad]];
        }
        
        if (self.listLocationSelected.count >0) {
           [self setDataWhenSelectedLocation:[NSArray arrayWithArray:self.listLocationSelected] andAtIndex:2];
        }
    }
    else{
        growingmethodname = self.clsSuccessionDetail.extendProp.growingmethodname;
        
        dataShows=[NSMutableArray arrayWithArray:[self listDataShowTop]];
        [self updateTitleCrop];
        // draw location
        if (self.listLocationSelected.count > 0) {
            [self setDataWhenSelectedLocation:[NSArray arrayWithArray:self.listLocationSelected] andAtIndex:2];
        }
        if (self.categoryId == 3) {
            [dataShows addObjectsFromArray:[self createListSuccessionDetailForFruit]];
        }
        else{
            [dataShows addObjectsFromArray:[self createListSuccessionDetail]];
        }
        
    }
    
    if ([_eventHandler respondsToSelector:@selector(didEndReturnListObjectTaskPlant:)]) {
        [_eventHandler didEndReturnListObjectTaskPlant:dataShows];
    }
}
-(void)clearDataShow{
    [dataShows removeAllObjects];
}
-(void)createObjctive{
    
    dateIntervalSeedingDate = 0;
    dateIntervalDayToTransplant = 0;
    dateIntervalDayToFirstHarvest = 0;
    dateIntervalDayToLastHarvest = 0;
    
    numberDayToTransplant = 0;
    numberDayToFirstHarvest = 0;
    numberDayToLastHarvest = 0;
    
    
    
    bedLengthPlantCL     = 0;
    
    rowPerBedPlantCL     = 0;
    inRowSpacingPlantCL  = 0;
    rowFeetPlantCl       = 0;
    
    rowpacinglow = 0;
    
    traySizePlantCL      = 0;
    seedPerCellPlantCL   = 0;
    numberOfTraysPlantCL = 0;
    
    numberOfPlantCL      = 0;
     numberOfBedPlantCl=0;

    bedWitdPlantCL = [ClassGlobal sharedInstance].setting.bedwidth;
    bedWidthEffect = [ClassGlobal sharedInstance].setting.effectivebedwidth;
    
    
    NSDateFormatter *dateformat=[[NSDateFormatter alloc]init];
    [dateformat setDateFormat:@"MM-dd-yyyy"];
    NSString *dateStr=[dateformat stringFromDate:[Utilities currentDateZeroTime]];
    
    NSDate *datetype=[dateformat dateFromString:dateStr];
    
    dateIntervalSeedingDate = [[Utilities convertDateTimeToTimeStamp:datetype] doubleValue];
    if (self.clsSuccessionDetail.id > 0) {
        dateIntervalSeedingDate = self.clsSuccessionDetail.startdate;
        numberDayToTransplant = self.clsSuccessionDetail.daytotransplant;
        numberDayToFirstHarvest = self.clsSuccessionDetail.adjusteddtmlow;
        numberDayToLastHarvest = self.clsSuccessionDetail.daytomaturityll;
        [self caculatorDate:ACTION_AT_ADD_SEEDINGDATE];
        
        self.plantingMethodFirstLoad = self.clsSuccessionDetail.extendProp.plantingname;
        if (self.clsSuccessionDetail.extendProp.plantingid == 1) {
            self.successionSeedingMethoTypeCL =  GREENHOUSE_SOW_S;
        }
        else if (self.clsSuccessionDetail.extendProp.plantingid == 5){
            self.successionSeedingMethoTypeCL =  TRANSPLANT_PURCHASED_S;
        }
        else if (self.clsSuccessionDetail.extendProp.plantingid == 14){
            self.successionSeedingMethoTypeCL =  DIRECT_SOW_S;
        }
        else if (self.clsSuccessionDetail.extendProp.plantingid == 2){
            self.successionSeedingMethoTypeCL =  DIRECT_SOW_BAND_SWATH;
        }
//        self.combinationDataForShow = self.clsSuccessionDetail.links.combinationdata;
        
        rowPerBedPlantCL = self.clsSuccessionDetail.rowperbed;
        inRowSpacingPlantCL = self.clsSuccessionDetail.inrowspacing;
        rowFeetPlantCl = self.clsSuccessionDetail.rowfeet;
        numberOfPlantCL = self.clsSuccessionDetail.numofplants;
        
    }
    
    
     listUnit=[NSArray arrayWithObjects:@"Pounds",@"Bunches",@"Each/Count",@"Pints" , nil];
    listDripLinesPerBed=[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"Variable", nil];
    listSeedSource=[NSArray arrayWithObjects:@"American Organic",@"Annie's Heirlooms",@"Baker Creek",@"Bountiful Gardens",@"Burpee",@"Eden Brothers",@"Fedco Seeds",@"Fruition Seeds",@"Gurney's Seed",@"Harris Seeds",@"Heirloom Seeds",@"High Mowing Seeds",@"Holiday Seed Company",@"Hudson Valley Seed Library",@"Johnny's Selected Seeds",@"Kitazawa Seed",@"Natural Gardening",@"Organic Seed Supply",@"Osborne Seed Company",@"Park Seed",@"Peaceful Valley",@"Redwood Seeds",@"Renee's Garden",@"Restoration Seeds",@"Sakata",@"San Diego Seed Company",@"Saved Seed From Another Farm",@"Saved Seed From My Farm",@"Seed Co-op",@"Seed Savers Exchange",@"Seeds of Change",@"Seedway",@"Siegers Seed Company",@"Siskiyou Seeds",@"Southern Exposure Seed Exchange",@"Sunrise Seed",@"Sustainable Seed Company",@"Territorial Seed",@"The Good Seed Company",@"Tomato Fest",@"Tomato Growers",@"Uprising Seeds",@"Victory Seeds",@"West Coast Seeds", nil];
    listNonPelleted =[NSArray arrayWithObjects:@"Non-pelleted",@"Pelleted", nil];
    listAnnual = [NSArray arrayWithObjects:@"Annual",@"Perennial", nil];
    
    listHarvestSeason = [NSArray arrayWithObjects:@"Very Early-Season", @"Early-Season", @"Early Mid-Season", @"Mid-Season", @"Late Mid-Season", @"Early Late-Season", @"Late-Season", @"Very Late-Season", @"Everbearing", nil];
    
    
    NSMutableArray* arrayss = [NSMutableArray new];
    int year = (int)([Utilities currentDateZeroTime]).year;
    for (int i = year ; i>= 1900; i--) {
        [arrayss addObject:[NSString stringWithFormat:@"%i",i]];
    }
    listPlatingYear = [NSArray arrayWithArray:arrayss];
    
    growingmethodname = @"";
}
-(NSArray *)returnListLocation{
    return self.listLocationSelected;
}
-(NSArray *)returnListLocationGreenHouse{
    return self.listLocationSelectedGreenHouse;
}
-(void)updateTitleCrop{
    for (int i = 0; i< dataShows.count; i++) {
        CellForShow* cellShow = [dataShows objectAtIndex:i];
        if (cellShow.typeAction_AT == ACTION_AN_INFO) {
            
            NSDate* minDate = [Utilities dateFromTimestamp:dateIntervalSeedingDate];
            NSDate* maxDate = [Utilities dateFromTimestamp:dateIntervalDayToLastHarvest];
            
            NSString* strinMinDate = [Utilities stringShortDateFromTimestamp:dateIntervalSeedingDate];
            if (!minDate.isThisYear) {
                strinMinDate = [Utilities stringFullDateFromTimestamp:dateIntervalSeedingDate];
            }
            NSString* strinMaxDate = [Utilities stringShortDateFromTimestamp:dateIntervalDayToLastHarvest];
            if (!maxDate.isThisYear) {
                strinMaxDate = [Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest];
            }
            
            
            NSString * stringDate=[NSString stringWithFormat:@"%@ - %@",strinMinDate,strinMaxDate];
            NSString * subString=[parentCropName substringWithRange:NSMakeRange(0, 2)];
            NSDictionary * dictShow=[[NSDictionary alloc] initWithObjects:@[parentCropName,subString,stringDate] forKeys:@[@"name",@"acronym",@"date"]];
            [cellShow setDictionaryShow_AT:dictShow];
        }
    }
    
}
-(void)updateTitleCrop:(double)minTimeStamp maxTimeStamp:(double)maxTimeStamp{
    // update infor
    for (int i = 0; i< dataShows.count; i++) {
        CellForShow* cellShow = [dataShows objectAtIndex:i];
        if (cellShow.typeAction_AT == ACTION_AN_INFO) {
            
            NSDate* minDate = [Utilities dateFromTimestamp:minTimeStamp];
            NSDate* maxDate = [Utilities dateFromTimestamp:maxTimeStamp];
            
            NSString* strinMinDate = [Utilities stringShortDateFromTimestamp:minTimeStamp];
            if (!minDate.isThisYear) {
                strinMinDate = [Utilities stringFullDateFromTimestamp:minTimeStamp];
            }
            NSString* strinMaxDate = [Utilities stringShortDateFromTimestamp:maxTimeStamp];
            if (!maxDate.isThisYear) {
                strinMaxDate = [Utilities stringFullDateFromTimestamp:maxTimeStamp];
            }
            
            
            NSString * stringDate=[NSString stringWithFormat:@"%@ - %@",strinMinDate,strinMaxDate];
            NSString * subString=[parentCropName substringWithRange:NSMakeRange(0, 2)];
            NSDictionary * dictShow=[[NSDictionary alloc] initWithObjects:@[parentCropName,subString,stringDate] forKeys:@[@"name",@"acronym",@"date"]];
            [cellShow setDictionaryShow_AT:dictShow];
        }
    }
}
-(void)caculatorDate:(AddTaskTypeAction)type{
    switch (type) {
        case ACTION_AT_ADD_SEEDINGDATE:
        {
            //update date of first harvert and last harvest
            dateIntervalDayToTransplant = numberDayToTransplant*86400000 + dateIntervalSeedingDate;
            dateIntervalDayToFirstHarvest = numberDayToFirstHarvest* 86400000 + dateIntervalDayToTransplant;
            dateIntervalDayToLastHarvest = numberDayToLastHarvest* 86400000 + dateIntervalDayToFirstHarvest;
        }
            break;
        case ACTION_AT_ADD_DAYTOTRANSPLANT:
        {
            dateIntervalDayToFirstHarvest = numberDayToFirstHarvest* 86400000 + dateIntervalDayToTransplant;
            dateIntervalDayToLastHarvest = numberDayToLastHarvest* 86400000 + dateIntervalDayToFirstHarvest;
        }
            break;
        case ACTION_AT_ADD_FIRST_HARVEST:{
            // update last harvest
            dateIntervalDayToLastHarvest = numberDayToLastHarvest* 86400000 + dateIntervalDayToFirstHarvest;
        }
            break;
        default:
            break;
    }
}
-(void)changed:(int)kValue{
    
//    if (self.clsSuccessionDetail.id > 0) {
//        rowPerBedPlantCL = self.clsSuccessionDetail.rowperbed;
//    }
//    else{
//        rowPerBedPlantCL = self.combinationDataForShow.rowsperbed;
//    }

    //In Row Spacing Change Not related
    switch (kValue) {
        case kValue1:{
            rowFeetPlantCl = rowPerBedPlantCL *numberOfBedPlantCl * bedLengthPlantCL;
            
            if (inRowSpacingPlantCL==0) {
                numberOfPlantCL=0;
            }else{
                if (rowFeetPlantCl==0) {
                    numberOfPlantCL=0;
                }else{
                    numberOfPlantCL = (rowFeetPlantCl * 12)/ inRowSpacingPlantCL;
                }
            }
        }
            break;
        case kValue2:{
            
            rowFeetPlantCl = rowPerBedPlantCL *numberOfBedPlantCl * bedLengthPlantCL;
            
            if (rowFeetPlantCl==0) {
                numberOfPlantCL=0;
            }else{
                if (inRowSpacingPlantCL==0) {
                    numberOfPlantCL=0;
                }else{
                    numberOfPlantCL = (rowFeetPlantCl * 12)/ inRowSpacingPlantCL;
                }
                
                
            }

        }
            break;
        case kValue3:{
            if (rowFeetPlantCl==0) {
                numberOfPlantCL=0;
            }else{
                numberOfPlantCL = (rowFeetPlantCl * 12)/ inRowSpacingPlantCL;
            }
            
        }
            break;
            
        default:
            break;
    }
}

-(BOOL)isMoveLinesOfDripPerBedToShowMoreSection{
//    if ((self.successionSeedingMethoTypeCL == DIRECT_SOW_BAND_SWATH)||(self.successionSeedingMethoTypeCL == TRANSPLANT_GREENHOUSE)||(self.successionSeedingMethoTypeCL == TRANSPLANT_FIELD)||(self.successionSeedingMethoTypeCL == TRANSPLANT_PURCHASED_S)||(self.successionSeedingMethoTypeCL == DIRECT_SOW_MULTIPLE_SEEDS_PER_HOLE)||(self.successionSeedingMethoTypeCL == DIRECT_SOW_S)) {
//        return YES;
//    }
//    return NO;
    //Show all case
    return YES;
}






-(void)refreshThreeFourColumnWhenChangeLocation{
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        if (cellShow.typeCell_AT==CELL_AT_THREECOLUMN_V2) {
            [self changed:kValue2];
            NSMutableDictionary * dictValue;
            
            dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithInt:rowPerBedPlantCL],[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2",@"value3"]];
            
            
            [cellShow setNsmutabledictSelected_AT:dictValue];
            [self refreshNumberOfPlans];
        }
    }
}
-(void)refreshNumberOfPlans{
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        if (cellShow.typeAction_AT==ACTION_AT_ADD_NUMBEROFPLANS) {
            cellShow.valueSelected_AT=[NSString stringWithFormat:@"%d",numberOfPlantCL];
        }
    }
}

#pragma mark - Create List Fist Load
-(NSArray *)listDataShowTop{
 NSMutableArray * successionShow=[[NSMutableArray alloc] init];
    for (int i =0; i<3; i++) {
        CellForShow * cellShow=[[CellForShow alloc] init];
        switch (i) {
            case 0:{
                if (self.clsSuccessionDetail.id > 0){
                    [cellShow setTypeCell_AT:CELL_AT_CROPLOCATION];
                    NSDictionary * dictShowCrop=[[NSDictionary alloc] initWithObjects:@[parentCropName,self.varietyNameCL,@"Crop",self.imageVarietyNameCL] forKeys:@[@"valueCropType",@"valueShow",@"typeShow",@"image"]];
                    
                    [cellShow setDictionaryShow_AT:dictShowCrop];
                }
                else{
                    [cellShow setTypeCell_AT:CELL_AT_NAME];
                    NSString * dateBelowName=@"";
                    [cellShow setTypeAction_AT:ACTION_AN_INFO];
                    //Logic date (Seeding date - Seeding date + day harvest duration)
                    NSString * subString=[parentCropName substringWithRange:NSMakeRange(0, 2)];
                    NSDictionary * dictShow=[[NSDictionary alloc] initWithObjects:@[parentCropName,subString,dateBelowName] forKeys:@[@"name",@"acronym",@"date"]];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                
                
            }
                
                break;
            case 1:{
                if (self.clsSuccessionDetail.id > 0){
                    continue;
                }
                [cellShow setTypeCell_AT:CELL_AT_CROP];
                if (self.clsSuccessionDetail.id == 0) {
                    NSString * stringEdit = @"YES";
                    cellShow.isHaveData_AT = true;
                    if (self.isPopulate) {
                        stringEdit = @"NO";
                        cellShow.isHaveData_AT = false;
                    }
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjects:@[self.varietyNameCL,self.imageVarietyNameCL,stringEdit] forKeys:@[@"valueShow",@"image",@"showEdit"]];
                    [cellShow setTypeAction_AT:ACTION_AT_ADD_CROP];
                    [cellShow setDictionaryShow_AT:dictShow];
                    
                }
                else{
                   
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjects:@[self.varietyNameCL,self.imageVarietyNameCL,@"YES"] forKeys:@[@"valueShow",@"image",@"showEdit"]];
                    [cellShow setTypeAction_AT:ACTION_AT_ADD_CROP];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
            }
                
                break;
            case 2:{
                [cellShow setTypeCell_AT:CELL_AT_LOCATION];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjects:@[@"Select Location"] forKeys:@[@"valueShow"]];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LOCATION];
                [cellShow setTypeMethod_AT:TYPE_PLAN_METHOD];
                
                [cellShow setIsShowLineBottom:true];
            }
                
                break;
            
            default:
                continue;
                break;
        }
         [successionShow addObject:cellShow];
        
    }
    return successionShow;
}
-(void)reloadDataWhenChangeDate{
    
    for (int i=0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        
        switch (cellShow.typeAction_AT) {
            case ACTION_AT_ADD_DAYTOTRANSPLANT:{
                
                NSString*stringReturn = [Utilities stringFullDateFromTimestamp:dateIntervalDayToTransplant];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Transplant",@"itemLeft",stringReturn,@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Transplant",numberDayToTransplant],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
                
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToTransplant],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToTransplant],@"days", nil]];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToTransplant]];
                
            }
                
                break;
            case ACTION_AT_ADD_FIRST_HARVEST:{
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"First Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToFirstHarvest],@"days", nil]];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest]];
                
                if (self.categoryId == 5  && [growingmethodname isEqualToString:@"Cover Crop"]) {
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Take-Down Date",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                    [self updateTitleCrop:dateIntervalSeedingDate maxTimeStamp:dateIntervalDayToFirstHarvest];
                }
                
            }
                
                break;
            case ACTION_AT_ADD_LAST_HARVEST:{
                NSString*stringReturn = [Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",stringReturn,@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest]];
                if (self.categoryId == 5) {
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
            }
                
                break;
            case ACTION_AN_INFO:{
                if (dateIntervalDayToLastHarvest==0 || ([growingmethodname isEqualToString:@"Cover Crop"])) {
                    continue;
                }
                NSString * stringDate=[NSString stringWithFormat:@"%@ - %@",[Utilities stringShortDateFromTimestamp:dateIntervalSeedingDate],[Utilities stringShortDateFromTimestamp:dateIntervalDayToLastHarvest]];
                NSString * subString=[parentCropName substringWithRange:NSMakeRange(0, 2)];
                NSDictionary * dictShow=[[NSDictionary alloc] initWithObjects:@[parentCropName,subString,stringDate] forKeys:@[@"name",@"acronym",@"date"]];
                [cellShow setDictionaryShow_AT:dictShow];
                
            }
                
                break;
                
            default:
                break;
        }
        
    }
}
-(NSArray*)createListFistLoadForFruits{
    NSMutableArray * successionShow=[[NSMutableArray alloc] init];
    for (int  i=0; i<25; i++) {
        CellForShow * cellShow=[[CellForShow alloc] init];
        switch (i) {
            case 0:{
                
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_HARVERT_SEASION];
                NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Season",@"itemLeft",@"Very Early-Season",@"itemRight", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setValueSelected_AT:@"1"];
                [cellShow setIsShowLineBottom:true];
                if (self.combinationDataForShow.harvestseasonid != 0) {
                    NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Season",@"itemLeft",[listHarvestSeason objectAtIndex:self.combinationDataForShow.harvestseasonid - 1],@"itemRight", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                    [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%i", self.combinationDataForShow.harvestseasonid]];
                }
            }
                
                
                break;
            case 1:{
                [cellShow setTypeCell_AT:CELL_AT_PRUNING_SEASON];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PRUNING_SEASON];
                [cellShow setIsShowLineBottom:true];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"winter",@"0",@"summer", nil];
                [cellShow setDictionaryShow_AT:dictShow];
            }
                break;
            case 2:{
                numberDayToFirstHarvest = 0;
                dateIntervalDayToFirstHarvest = dateIntervalSeedingDate;
                NSString * stringDate=[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_FIRST_HARVEST];
                
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"First Harvest",@"itemLeft",stringDate,@"itemRight",@"hide",@"statusArrowImg", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsHaveData_AT:true];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest]];
                [cellShow setIsShowLineBottom:true];
                
            }
                break;
            case 3:{
                numberDayToLastHarvest = self.combinationDataForShow.daytomaturityll;
                dateIntervalDayToLastHarvest = dateIntervalDayToFirstHarvest + numberDayToLastHarvest* 86400000;
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LAST_HARVEST];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest]];
                [cellShow setIsHaveData_AT:true];
                [cellShow setIsShowLineBottom:true];
                
                [self updateTitleCrop:dateIntervalDayToFirstHarvest maxTimeStamp:dateIntervalDayToLastHarvest];
                
                
            }
                break;
            case 4:{
                
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PLANTING_YEAR];
                NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Year",@"itemLeft", [listPlatingYear objectAtIndex:0],@"itemRight", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setValueSelected_AT:[listPlatingYear objectAtIndex:0]];
                [cellShow setIsShowLineBottom:false];
                if (self.combinationDataForShow.plantingyear != 0) {
                    NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Year",@"itemLeft", [NSString stringWithFormat:@"%i",self.combinationDataForShow.plantingyear],@"itemRight", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                    [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%i",self.combinationDataForShow.plantingyear]];
                    [cellShow setIsHaveData_AT:true];
                }
                
            }
                
                
                break;
                
            case 5:{
                // Check seeding type and return value
                inRowSpacingPlantCL = [self.combinationDataForShow inrowspacing];
                rowFeetPlantCl = [self.combinationDataForShow rowspacinglow];
                
                [cellShow setTypeCell_AT:CELL_AT_TWOCOLUMN];
                NSDictionary * dictShow;
                NSMutableDictionary * dictValue;
                
                dictShow=[[NSDictionary alloc] initWithObjects:@[@"In-row Spacing",@"Between-row Spacing",@"1"] forKeys:@[@"column1",@"column2",@"punctuationmark"]];
                
                dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2"]];
                
                
                [cellShow setNsmutabledictSelected_AT:dictValue];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setPlantingMethod:self.successionSeedingMethoTypeCL];
            }
                break;
            case 6:{
                //Hide field if band swath
                
                [cellShow setTypeCell_AT:CELL_AT_SHOWNUMBEROFPLANTS];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_NUMBER_TREES];
                [cellShow setIsShowLineBottom:true];
                NSString * numberOfPlant;
                if (self.combinationDataForShow.numoftrees==0) {
                    numberOfPlant=@"0" ;
                }else{
                    numberOfPlant=[NSString stringWithFormat:@"%i",self.combinationDataForShow.numoftrees] ;
                    [cellShow setIsHaveData_AT:true];
                }
                
                [cellShow setValueSelected_AT:numberOfPlant];
                
            }
                
                break;
              
            
            case 7:{
                [cellShow setTypeCell_AT:CELL_AT_HIDEMORE];
                [cellShow setTypeAction_AT:ACTION_AT_HIDEMORE];
                [cellShow setValueSelected_AT:@"1"];
                [cellShow setIsShowLineBottom:false];
            }
                break;
            case 12:{
                
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeCell_Hide_Show:CELL_AT_ADD_TEXTFIELD];
                [cellShow setTypeMethod_AT:CELL_AT_SHOW_HIDE];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_ROOTSTOCK];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Rootstock",@"fieldText", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottomFull:true];
                
            }
                
                break;
            case 13:{
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeCell_Hide_Show:CELL_AT_ADD_TEXTFIELD];
                [cellShow setTypeMethod_AT:CELL_AT_SHOW_HIDE];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_ROOTSTOCK_SUPPLIER];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Rootstock Supplier",@"fieldText", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottomFull:false];
            }
                
                
                break;
            
            case 17:{
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"NOTES"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
            }
                break;
            case 18:{
                [cellShow setTypeCell_AT:CELL_AT_ADD_ITEM_VIEW_ITEM];
                [cellShow setTypeAction_AT:ACTION_ADD_ITEM_VIEW_ITEM];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Add a Note",@"leftitem", @"View all Notes",@"rightitem", nil];
                [cellShow setDictionaryShow_AT:dictShow];
            }
                break;
            case 19:{
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"PHOTOS"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
            }
                break;
            case 20:{
                [cellShow setTypeCell_AT:CELL_AT_TAKEPHOTO];
                
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PHOTO];
                
            }
                
                
                break;
                
            default:
                continue;
                break;
        }
        [successionShow addObject:cellShow];
        
    }
    return successionShow;
}
-(NSArray*)createListFistLoad{
    NSMutableArray * successionShow=[[NSMutableArray alloc] init];
    for (int  i=0; i<25; i++) {
         CellForShow * cellShow=[[CellForShow alloc] init];
        switch (i) {
            case 0:{
                
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PLANTINGMETHOD];
                NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Method",@"itemLeft",self.plantingMethodFirstLoad,@"itemRight", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
            }
                
                
                break;
            case 1:{
                if (self.successionSeedingMethoTypeCL!=GREENHOUSE_SOW_S) {
                    continue;
                }
                
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                ClassFields * field=[self.listLocationSelectedGreenHouse objectAtIndex:0];
//                NSDictionary * dictShow;
//                if (![field.name isEqualToString:@""] || field.name.length > 0) {
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Greenhouse",@"itemLeft",field.name,@"itemRight", nil];
//                }
//                else {
//                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Greenhouse",@"itemLeft",@"Select Greenhouse",@"itemRight", nil];
//                }
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LOCATIONGREENHOUSE];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
                
            }
                break;
            
            case 2:{

                NSString * growingMethod=[self.combinationDataForShow growingmethod];
                if (!growingMethod||growingMethod.length==0) {
                    continue;
                }
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Growing Method",@"itemLeft",growingMethod,@"itemRight", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_GROWINGMETHOD];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsHaveData_AT:true];
                [cellShow setValueSelected_AT:growingMethod];
                [cellShow setIsShowLineBottom:true];
            }

                
                break;
            case 3:{
                
                NSString * stringDate=[Utilities stringFullDateFromTimestamp:dateIntervalSeedingDate];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Date",@"itemLeft",stringDate,@"itemRight",@"hide",@"statusArrowImg", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_SEEDINGDATE];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsHaveData_AT:true];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalSeedingDate]];
                [cellShow setIsShowLineBottom:true];
                numberDayToTransplant = 0;
                dateIntervalDayToTransplant = dateIntervalSeedingDate;
                
            }
                
                break;
            
            case 4:{
                // Direct sow hide this cell
                if (self.successionSeedingMethoTypeCL!=GREENHOUSE_SOW_S) {
                    continue;
                }
                
                numberDayToTransplant = self.combinationDataForShow.daystotransplantdatefromghl;
                dateIntervalDayToTransplant = dateIntervalSeedingDate + numberDayToTransplant*86400000;
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Transplant",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToTransplant],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Transplant",numberDayToTransplant],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToTransplant],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToTransplant],@"days", nil]];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_DAYTOTRANSPLANT];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToTransplant]];
                [cellShow setIsHaveData_AT:true];
                [cellShow setIsShowLineBottom:true];
                
            }

                
                break;
            case 5:{
                // Greenhouse sow hide this cell
                numberDayToFirstHarvest = self.combinationDataForShow.adjusteddtmlow;
                dateIntervalDayToFirstHarvest = (double)(numberDayToFirstHarvest * 86400000) + dateIntervalDayToTransplant ;
                
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"First Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_FIRST_HARVEST];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToFirstHarvest],@"days", nil]];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest]];
                [cellShow setIsHaveData_AT:true];
                [cellShow setIsShowLineBottom:true];
                
                if (self.categoryId == 5 && [growingmethodname isEqualToString:@"Cover Crop"]) {
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Take-Down Date",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                    [self updateTitleCrop:dateIntervalSeedingDate maxTimeStamp:dateIntervalDayToFirstHarvest];
                }
                
            }

                
                break;
            case 6:{
                if ([growingmethodname isEqualToString:@"Cover Crop"]) {
                    continue;
                }
                
                // Greenhouse sow hide this cell
                numberDayToLastHarvest = self.combinationDataForShow.daytomaturityll;
                dateIntervalDayToLastHarvest = numberDayToLastHarvest * 86400000 + dateIntervalDayToFirstHarvest;
                
                
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LAST_HARVEST];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest]];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                [cellShow setIsHaveData_AT:true];
                if (self.categoryId == 5) {
                    [cellShow setIsShowLineBottom:true];
                }
                
                
                [self updateTitleCrop];
            }

                
                break;
                
//            case 7:{
//                if (self.categoryId == 5 && ![growingmethodname isEqualToString:@"Cover Crop"]) {
//                    [cellShow setTypeCell_AT:Cell_NameType_ID];
//                    NSString* harvestunit = self.combinationDataForShow.harvestunit;
//                    if ([harvestunit isEqualToString:@""]) {
//                        NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Unit",@"itemLeft",@"Pound",@"itemRight", nil];
//                        [cellShow setDictionaryShow_AT:dictShow];
//                        [cellShow setValueSelected_AT:@"Pound"];
//                    }
//                    else{
//                        NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Unit",@"itemLeft",self.combinationDataForShow.harvestunit,@"itemRight", nil];
//                        [cellShow setDictionaryShow_AT:dictShow];
//                        [cellShow setValueSelected_AT:self.combinationDataForShow.harvestunit];
//                    }
//                    [cellShow setTypeAction_AT:ACTION_AT_ADD_HARVEST_UNIT];
//                    
//                    [cellShow setIsHaveData_AT:true];
//                    
//                    [cellShow setIsShowLineBottom:true];
//                }
//                else
//                {
//                    continue;
//                }
//            }
//                break;

            case 8:{
                
                if ([self isMoveLinesOfDripPerBedToShowMoreSection] || self.categoryId != 5) {
                    continue;
                }
                
                NSString * numeberDrip=@"";
                if (self.combinationDataForShow.driplineperbed !=0) {
                    numeberDrip = [NSString stringWithFormat:@"%i", self.combinationDataForShow.driplineperbed] ;
                }else{
                    numeberDrip=[NSString stringWithFormat:@"%d",(int)[ClassGlobal sharedInstance].setting.driplineperbed];
                    if ( [ClassGlobal sharedInstance].setting.driplineperbed == 0) {
                        numeberDrip = @"Variable";
                    }
                }
                [cellShow setTypeCell_AT:Cell_NameType3_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LINEOFDRIP];
                [cellShow setIsShowCell:YES];
                
                if ([numeberDrip isEqualToString:@"Variable"]) {
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",numeberDrip,@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:@"0"];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                else{
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",numeberDrip,@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:numeberDrip];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                
                
                
            }

                break;
            case 9:{
                if (self.categoryId != 5) {
                    continue;
                }
                [cellShow setTypeCell_AT:Cell_NameType3_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_SEEDING_RATE];
                NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",@"0 lbs/acre",@"itemRight",@"0.87",@"alpha", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                
                NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"valueRate",@"lbs/acre",@"unitRate",@"1",@"unitRateId", nil];
                [cellShow setDictSelected_AT:dictSelected_AT];
                
                [cellShow setIsShowCell:true];
                if (self.successionSeedingMethoTypeCL == BROADCAST_SOWN) {
                    [cellShow setIsShowLineBottomFull:true];
                }
            }
                break;
                
            case 11:{
                if (self.categoryId == 5  && self.successionSeedingMethoTypeCL == DRILL_SOWN) {
                    // Check seeding type and return value
                    inRowSpacingPlantCL = [self.combinationDataForShow inrowspacing];
                    rowFeetPlantCl = [self.combinationDataForShow rowspacinglow];
                    
                    [cellShow setTypeCell_AT:CELL_AT_TWOCOLUMN];
                    NSDictionary * dictShow;
                    NSMutableDictionary * dictValue;
                    
                    dictShow=[[NSDictionary alloc] initWithObjects:@[@"In-row Spacing",@"Between-row Spacing",@"0"] forKeys:@[@"column1",@"column2",@"punctuationmark"]];
                    
                    dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2"]];
                    
                    
                    [cellShow setNsmutabledictSelected_AT:dictValue];
                    [cellShow setDictionaryShow_AT:dictShow];
                    [cellShow setPlantingMethod:self.successionSeedingMethoTypeCL];
                    
                }
                else{
                    continue;
                }
                
            }
                break;
            case 12:{
                if (self.categoryId == 5) {
                    continue;
                }
                // Check seeding type and return value
                inRowSpacingPlantCL = [self.combinationDataForShow inrowspacing];
                rowPerBedPlantCL = [self.combinationDataForShow rowsperbed];
                if (self.successionSeedingMethoTypeCL == DIRECT_SOW_BAND_SWATH) {
                    inRowSpacingPlantCL = 0;
                }
                [self changed:kValue1];
                
                [cellShow setTypeCell_AT:CELL_AT_THREECOLUMN_V2];
                NSDictionary * dictShow;
                NSMutableDictionary * dictValue;
            
                dictShow=[[NSDictionary alloc] initWithObjects:@[@"Rows per Bed",@"In-row Spacing",@"Row Feet"] forKeys:@[@"column1",@"column2",@"column3"]];
                
                
                
                dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithInt:rowPerBedPlantCL],[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2",@"value3"]];
                
                
                [cellShow setNsmutabledictSelected_AT:dictValue];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setPlantingMethod:self.successionSeedingMethoTypeCL];
                
            }

                
                break;
            case 13:{
                //Hide field if band swath
                if (self.successionSeedingMethoTypeCL == DIRECT_SOW_BAND_SWATH || self.categoryId == 5) {
                    continue;
                }
                [cellShow setTypeCell_AT:CELL_AT_SHOWNUMBEROFPLANTS];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_NUMBEROFPLANS];
                [cellShow setIsShowLineBottom:true];
                NSString * numberOfPlant;
                if (numberOfPlantCL==0) {
                    numberOfPlant=@"0" ;
                }else{
                    numberOfPlant=[NSString stringWithFormat:@"%d",numberOfPlantCL] ;
                }
               
                [cellShow setValueSelected_AT:numberOfPlant];
                
            }

                
                break;
            case 14:{
                [cellShow setTypeCell_AT:CELL_AT_HIDEMORE];
                [cellShow setTypeAction_AT:ACTION_AT_HIDEMORE];
                [cellShow setValueSelected_AT:@"1"];
                [cellShow setIsShowLineBottom:false];
            }
                break;
            case 15:{
                if (![self isMoveLinesOfDripPerBedToShowMoreSection] || self.categoryId == 5) {
                    continue;
                }
                
                NSString * numeberDrip=@"";
                if (self.combinationDataForShow.driplineperbed !=0) {
                    numeberDrip = [NSString stringWithFormat:@"%i", self.combinationDataForShow.driplineperbed] ;
                }else{
                    numeberDrip=[NSString stringWithFormat:@"%d",(int)[ClassGlobal sharedInstance].setting.driplineperbed];
                    if ( [ClassGlobal sharedInstance].setting.driplineperbed == 0) {
                        numeberDrip = @"Variable";
                    }
                }
                [cellShow setTypeCell_AT:Cell_NameType3_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LINEOFDRIP];
                [cellShow setIsShowLineBottom:YES];
                [cellShow setTypeMethod_AT:CELL_AT_SHOW_HIDE];
                [cellShow setIsShowCell:NO];
                if ([numeberDrip isEqualToString:@"Variable"]) {
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",numeberDrip,@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:@"0"];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                else{
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",numeberDrip,@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:numeberDrip];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
            }
                
                break;
            case 16:{
                if (self.categoryId == 5) {
                    continue;
                }
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeAction_AT:ACTION_AT_ACTIONSEEDSOURCE];
                [cellShow setIsShowCell:NO];
                NSDictionary * dictShow;
                dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Seed Source",@"selectType", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
            }

                
                break;
            case 17:{
                if (self.combinationDataForShow.pelletedoption==0) {
                    continue;
                }
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeAction_AT:ACTION_AT_ACTIONPELLETED];
                [cellShow setIsShowCell:NO];
                NSDictionary * dictShow;
                dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Non-pelleted",@"selectType", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
            }

                
                break;
            case 18:{
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeAction_AT:ACTION_AT_ACTIONANNUAL];
                [cellShow setIsShowCell:NO];
                NSDictionary * dictShow;
                dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Annual",@"selectType", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
            }

                
                break;
            case 19:{
                if (self.categoryId == 5) {
                    continue;
                }
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeCell_Hide_Show:CELL_AT_ADDTASK_CHECKBOX];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_CHECKBOX];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Growing in a Hoophouse",@"checkBox", nil];
                [cellShow setIsShowCell:NO];
                [cellShow setValueSelected_AT:@"0"];
                [cellShow setIsHaveData_AT:YES];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:false];
            }

                
                break;
            case 20:{
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"NOTES"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
            }
                break;
            case 21:{
                [cellShow setTypeCell_AT:CELL_AT_ADD_ITEM_VIEW_ITEM];
                [cellShow setTypeAction_AT:ACTION_ADD_ITEM_VIEW_ITEM];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Add a Note",@"leftitem", @"View all Notes",@"rightitem", nil];
                [cellShow setDictionaryShow_AT:dictShow];
            }
                break;
            case 22:{
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"PHOTOS"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
            }
                break;
            case 23:{
                [cellShow setTypeCell_AT:CELL_AT_TAKEPHOTO];
                
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PHOTO];
                
            }

                
                break;
                
            default:
                continue;
                break;
        }
        [successionShow addObject:cellShow];
        
    }
    return successionShow;
}
-(NSArray*)createListSuccessionDetail{
    NSMutableArray * successionShow=[[NSMutableArray alloc] init];
    for (int  i=0; i<26; i++) {
        
        switch (i) {
            case 0:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PLANTINGMETHOD];
                NSDictionary *dictShow;
                if (self.clsSuccessionDetail.extendProp.plantingid == 16) {
                    dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Method",@"itemLeft",@"Broadcast Sow", @"itemRight", nil];
                }
                else if (self.clsSuccessionDetail.extendProp.plantingid == 15){
                    dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Method",@"itemLeft",@"Drill Sow", @"itemRight", nil];
                }
                else{
                    dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Method",@"itemLeft",self.plantingMethodFirstLoad,@"itemRight", nil];
                }
                
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 1:{
                if (self.successionSeedingMethoTypeCL!=GREENHOUSE_SOW_S) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Greenhouse",@"itemLeft",@"Select Greenhouse",@"itemRight", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LOCATIONGREENHOUSE];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
                
                if (self.listLocationSelectedGreenHouse.count > 0) {
                    ClassFields * field=[self.listLocationSelectedGreenHouse objectAtIndex:0];
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Greenhouse",@"itemLeft",field.name,@"itemRight", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                [successionShow addObject:cellShow];
            }
                break;
                
            case 2:{
                
                NSString * growingMethod=self.clsSuccessionDetail.extendProp.growingmethodname;
                if (!growingMethod||growingMethod.length==0) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Growing Method",@"itemLeft",growingMethod,@"itemRight", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_GROWINGMETHOD];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsHaveData_AT:true];
                [cellShow setValueSelected_AT:growingMethod];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 3:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                NSString * stringDate=[Utilities stringFullDateFromTimestamp:dateIntervalSeedingDate];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Date",@"itemLeft",stringDate,@"itemRight",@"hide",@"statusArrowImg", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_SEEDINGDATE];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsHaveData_AT:true];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalSeedingDate]];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
            }
                
                break;
                
            case 4:{
                // Direct sow hide this cell
                if (self.successionSeedingMethoTypeCL!=GREENHOUSE_SOW_S) {
                    numberDayToTransplant = 0;
                    dateIntervalDayToTransplant = dateIntervalSeedingDate;
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Transplant",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToTransplant],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Transplant",numberDayToTransplant],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToTransplant],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToTransplant],@"days", nil]];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_DAYTOTRANSPLANT];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToTransplant]];
                [cellShow setIsHaveData_AT:true];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 5:{
                // Greenhouse sow hide this cell
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"First Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_FIRST_HARVEST];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToFirstHarvest],@"days", nil]];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest]];
                [cellShow setIsHaveData_AT:true];
                [cellShow setIsShowLineBottom:true];
                if (self.categoryId == 5  && [growingmethodname isEqualToString:@"Cover Crop"]) {
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Take-Down Date",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                
                
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 6:{
                if ([growingmethodname isEqualToString:@"Cover Crop"]) {
                    continue;
                }
                // Greenhouse sow hide this cell
                
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LAST_HARVEST];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest]];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                [cellShow setIsHaveData_AT:true];
                if (self.categoryId == 5) {
                    [cellShow setIsShowLineBottom:true];
                }
                
                [self updateTitleCrop];
                [successionShow addObject:cellShow];
            }
                
                
                break;
//            case 7:{
//                if (self.categoryId == 5 && self.successionSeedingMethoTypeCL == DRILL_SOWN && ![growingmethodname isEqualToString:@"Cover Crop"]) {
//                    CellForShow * cellShow=[[CellForShow alloc] init];
//                    [cellShow setTypeCell_AT:Cell_NameType_ID];
//                    NSString* harvestunit = self.clsSuccessionDetail.harvestunit;
//                    if ([harvestunit isEqualToString:@""]) {
//                        NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Unit",@"itemLeft",@"Pound",@"itemRight", nil];
//                        [cellShow setDictionaryShow_AT:dictShow];
//                        [cellShow setValueSelected_AT:@"Pound"];
//                    }
//                    else{
//                        NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Unit",@"itemLeft",harvestunit,@"itemRight", nil];
//                        [cellShow setDictionaryShow_AT:dictShow];
//                        [cellShow setValueSelected_AT:harvestunit];
//                    }
//                    [cellShow setTypeAction_AT:ACTION_AT_ADD_GROWINGMETHOD];
//                    
//                    [cellShow setIsHaveData_AT:true];
//                    
//                    [cellShow setIsShowLineBottom:true];
//                    [successionShow addObject:cellShow];
//                }
//                
//            }
//                break;
            case 8:{
                if ([self isMoveLinesOfDripPerBedToShowMoreSection] || self.categoryId != 5) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType3_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LINEOFDRIP];
                [cellShow setIsShowCell:YES];
                
                if (self.clsSuccessionDetail.lineperbed == 0) {
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",@"Variable",@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:@"0"];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                else{
                    NSString * numeberDrip=[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.lineperbed];
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",numeberDrip,@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:numeberDrip];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                [successionShow addObject:cellShow];
                
            }
                
                
                break;
            case 9:{
                if (self.categoryId != 5) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType3_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_SEEDING_RATE];
                
                NSNumberFormatter* formatnumber = [[NSNumberFormatter alloc] init];
                formatnumber.numberStyle = NSNumberFormatterDecimalStyle;
                NSNumber* number = [NSNumber numberWithDouble:self.clsSuccessionDetail.seedingratevalue] ;
                if (self.clsSuccessionDetail.seedingratevalueunitid != 0) {
                    switch (self.clsSuccessionDetail.seedingratevalueunitid) {
                        case 1:{
                            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:number.stringValue,@"valueRate",@"lbs/acre",@"unitRate",[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.seedingratevalueunitid],@"unitRateId", nil];
                            [cellShow setDictSelected_AT:dictSelected_AT];
                            
                            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",[NSString stringWithFormat:@"%@ %@",[formatnumber stringFromNumber:[[NSNumber alloc]initWithDouble:number.stringValue.doubleValue]],@"lbs/acre"],@"itemRight",@"0.87",@"alpha", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            break;
                        }
                        case 2:{
                            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:number.stringValue,@"valueRate",@"lbs/sq ft",@"unitRate",[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.seedingratevalueunitid],@"unitRateId", nil];
                            [cellShow setDictSelected_AT:dictSelected_AT];
                            
                            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",[NSString stringWithFormat:@"%@ %@",[formatnumber stringFromNumber:[[NSNumber alloc]initWithDouble:number.stringValue.doubleValue]],@"lbs/sq ft"],@"itemRight",@"0.87",@"alpha", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            break;
                        }
                        case 3:{
                            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:number.stringValue,@"valueRate",@"lbs/1,000 sq ft",@"unitRate",[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.seedingratevalueunitid],@"unitRateId", nil];
                            [cellShow setDictSelected_AT:dictSelected_AT];
                            
                            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",[NSString stringWithFormat:@"%@ %@",[formatnumber stringFromNumber:[[NSNumber alloc]initWithDouble:number.stringValue.doubleValue]],@"lbs/1,000 sq ft"],@"itemRight",@"0.87",@"alpha", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            break;
                        }
                        case 4:{
                            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:number.stringValue,@"valueRate",@"oz/acre",@"unitRate",[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.seedingratevalueunitid],@"unitRateId", nil];
                            [cellShow setDictSelected_AT:dictSelected_AT];
                            
                            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",[NSString stringWithFormat:@"%@ %@",[formatnumber stringFromNumber:[[NSNumber alloc]initWithDouble:number.stringValue.doubleValue]],@"oz/acre"],@"itemRight",@"0.87",@"alpha", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            break;
                        }
                        case 5:{
                            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:number.stringValue,@"valueRate",@"oz/sq ft",@"unitRate",[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.seedingratevalueunitid],@"unitRateId", nil];
                            [cellShow setDictSelected_AT:dictSelected_AT];
                            
                            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",[NSString stringWithFormat:@"%@ %@",[formatnumber stringFromNumber:[[NSNumber alloc]initWithDouble:number.stringValue.doubleValue]],@"oz/sq ft"],@"itemRight",@"0.87",@"alpha", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            break;
                        }
                        case 6:{
                            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:number.stringValue,@"valueRate",@"oz/1,000 sq ft",@"unitRate",[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.seedingratevalueunitid],@"unitRateId", nil];
                            [cellShow setDictSelected_AT:dictSelected_AT];
                            
                            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",[NSString stringWithFormat:@"%@ %@",[formatnumber stringFromNumber:[[NSNumber alloc]initWithDouble:number.stringValue.doubleValue]],@"oz/1,000 sq ft"],@"itemRight",@"0.87",@"alpha", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            break;
                        }
                        
                        default:{
                            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"valueRate",@"lbs/acre",@"unitRate",@"1",@"unitRateId", nil];
                            [cellShow setDictSelected_AT:dictSelected_AT];
                            
                            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",@"0 lbs/acre",@"itemRight",@"0.87",@"alpha", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            break;
                        }
                    }
                }
                else{
                    NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"valueRate",@"lbs/acre",@"unitRate",@"1",@"unitRateId", nil];
                    [cellShow setDictSelected_AT:dictSelected_AT];
                    
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",@"0 lbs/acre",@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                
                
                [cellShow setIsShowCell:true];
                if (self.successionSeedingMethoTypeCL == BROADCAST_SOWN) {
                    [cellShow setIsShowLineBottomFull:true];
                }
                [successionShow addObject:cellShow];
            }
                break;
            case 11:{
                if (self.categoryId == 5  && self.successionSeedingMethoTypeCL == DRILL_SOWN) {
                    // Check seeding type and return value
                    inRowSpacingPlantCL = [self.clsSuccessionDetail inrowspacing];
                    rowFeetPlantCl = [self.clsSuccessionDetail rowspacinglow];
                    
                    CellForShow * cellShow=[[CellForShow alloc] init];
                    [cellShow setTypeCell_AT:CELL_AT_TWOCOLUMN];
                    NSDictionary * dictShow;
                    NSMutableDictionary * dictValue;
                    
                    dictShow=[[NSDictionary alloc] initWithObjects:@[@"In-row Spacing",@"Between-row Spacing",@"0"] forKeys:@[@"column1",@"column2",@"punctuationmark"]];
                    
                    dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2"]];
                    
                    
                    [cellShow setNsmutabledictSelected_AT:dictValue];
                    [cellShow setDictionaryShow_AT:dictShow];
                    [cellShow setPlantingMethod:self.successionSeedingMethoTypeCL];
                    [successionShow addObject:cellShow];
                    
                }
                
            }
                break;
                
            case 12:{
                if (self.categoryId == 5) {
                    continue;
                }
                // Check seeding type and return value
                if (self.successionSeedingMethoTypeCL == DIRECT_SOW_BAND_SWATH) {
                    inRowSpacingPlantCL = 0;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_THREECOLUMN_V2];
                NSDictionary * dictShow;
                NSMutableDictionary * dictValue;
                
                dictShow=[[NSDictionary alloc] initWithObjects:@[@"Rows per Bed",@"In-row Spacing",@"Row Feet"] forKeys:@[@"column1",@"column2",@"column3"]];
                
                dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithInt:rowPerBedPlantCL],[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2",@"value3"]];
                
                
                [cellShow setNsmutabledictSelected_AT:dictValue];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setPlantingMethod:self.successionSeedingMethoTypeCL];
                [successionShow addObject:cellShow];
                
                
            }
                
                
                break;
            case 13:{
                if (self.successionSeedingMethoTypeCL == DIRECT_SOW_BAND_SWATH || self.categoryId == 5) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_SHOWNUMBEROFPLANTS];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_NUMBEROFPLANS];
                [cellShow setIsShowLineBottom:true];
                NSString * numberOfPlant = [NSString stringWithFormat:@"%i",numberOfPlantCL];
                [cellShow setValueSelected_AT:numberOfPlant];
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 14:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDEMORE];
                [cellShow setTypeAction_AT:ACTION_AT_HIDEMORE];
                [cellShow setValueSelected_AT:@"1"];
                [successionShow addObject:cellShow];
            }
                break;
            case 15:{
                if (![self isMoveLinesOfDripPerBedToShowMoreSection] || self.categoryId == 5) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType3_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LINEOFDRIP];
                [cellShow setIsShowLineBottom:YES];
                [cellShow setIsShowCell:NO];
                [cellShow setTypeMethod_AT:CELL_AT_SHOW_HIDE];
                if (self.clsSuccessionDetail.lineperbed == 0) {
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",@"Variable",@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:@"0"];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                else{
                    NSString * numeberDrip=[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.lineperbed];
                    NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",numeberDrip,@"itemRight",@"0.87",@"alpha", nil];
                    [cellShow setValueSelected_AT:numeberDrip];
                    [cellShow setDictionaryShow_AT:dictShow];
                }
                [successionShow addObject:cellShow];
            }
                
                break;
            case 16:{
                if (self.categoryId == 5) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeAction_AT:ACTION_AT_ACTIONSEEDSOURCE];
                [cellShow setIsShowCell:NO];
                NSDictionary * dictShow;
                if (self.clsSuccessionDetail.seedsource && ![self.clsSuccessionDetail.seedsource isEqualToString:@""]) {
                    dictShow =[NSDictionary dictionaryWithObjectsAndKeys:self.clsSuccessionDetail.seedsource,@"selectType", nil];
                    cellShow.valueSelected_AT = [NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.seedsourceid];
                    cellShow.isHaveData_AT = true;
                }
                else{
                    dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Seed Source",@"selectType", nil];
                
                }
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 17:{
                if (self.clsSuccessionDetail.pelletedoption==0) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeAction_AT:ACTION_AT_ACTIONPELLETED];
                [cellShow setIsShowCell:NO];
                NSDictionary * dictShow;
                if (self.clsSuccessionDetail.seedtype && ![self.clsSuccessionDetail.seedtype isEqualToString:@""]) {
                    dictShow=[NSDictionary dictionaryWithObjectsAndKeys:self.clsSuccessionDetail.seedtype,@"selectType", nil];
                }
                else{
                    dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Non-pelleted",@"selectType", nil];
                }
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 18:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeAction_AT:ACTION_AT_ACTIONANNUAL];
                [cellShow setIsShowCell:NO];
                NSDictionary * dictShow;
                if (self.clsSuccessionDetail.annualperennial && ![self.clsSuccessionDetail.annualperennial isEqualToString:@""]) {
                    dictShow=[NSDictionary dictionaryWithObjectsAndKeys:self.clsSuccessionDetail.annualperennial,@"selectType", nil];
                }
                else{
                    dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Annual",@"selectType", nil];
                }
                
                [cellShow setDictionaryShow_AT:dictShow];
                if (self.categoryId != 5) {
                    [cellShow setIsShowLineBottom:true];
                }
                
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 19:{
                if (self.categoryId == 5) {
                    continue;
                }
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeCell_Hide_Show:CELL_AT_ADDTASK_CHECKBOX];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_CHECKBOX];
                if (self.clsSuccessionDetail.growinginahoophouse) {
                    [cellShow setValueSelected_AT:@"1"];
                }
                else{
                    [cellShow setValueSelected_AT:@"0"];
                }
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Growing in a Hoophouse",@"checkBox", nil];
                [cellShow setIsShowCell:NO];
                
                [cellShow setIsHaveData_AT:YES];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsShowLineBottom:false];
                [successionShow addObject:cellShow];
            }
                
                
                break;
            case 20:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"NOTES"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
                [successionShow addObject:cellShow];
            }
                
                break;
            case 21:{
                if (self.clsSuccessionDetail.links.notes.count > 0) {
                    for (int j = 0; j < self.clsSuccessionDetail.links.notes.count; j ++) {
                        CellForShow * cellShow=[[CellForShow alloc] init];
                        [cellShow setTypeCell_AT:CELL_AT_ADD_NOTE_ITEM];
                        [cellShow setTypeAction_AT:ACTION_AT_ADD_NOTE];
                        cellShow.note = (SW_Note*)[self.clsSuccessionDetail.links.notes objectAtIndex:j];
                        [successionShow addObject:cellShow];
                    }
                }
                
            }
                
                break;
            case 22:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_ADD_ITEM_VIEW_ITEM];
                [cellShow setTypeAction_AT:ACTION_ADD_ITEM_VIEW_ITEM];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Add a Note",@"leftitem", @"View all Notes",@"rightitem", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [successionShow addObject:cellShow];
            }
                
                break;
            case 23:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"PHOTOS"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
                [successionShow addObject:cellShow];
            }
                
                break;
            case 24:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                if (self.clsSuccessionDetail.links.images.count > 0) {
                    cellShow.listImages = [NSMutableArray new];
                    for (int i = 0; i < self.clsSuccessionDetail.links.images.count; i++) {
                        CImage *cImage = self.clsSuccessionDetail.links.images[i];
                        CImage *image = [CImage new];
                        image.id = cImage.id;
                        image.url = cImage.url;
                        [cellShow.listImages addObject:image];
                    }
                    cellShow.totalCurrentImage = (int)cellShow.listImages.count;
                }
                [cellShow setTypeCell_AT:CELL_AT_TAKEPHOTO];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PHOTO];
                
                [successionShow addObject:cellShow];
            }
                
                
                break;
                
            default:
                continue;
                break;
        }
        
        
    }
    return successionShow;
}
-(NSArray*)createListSuccessionDetailForFruit{
    NSMutableArray * successionShow=[[NSMutableArray alloc] init];
    for (int  i=0; i<26; i++) {
        switch (i) {
            case 0:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_HARVERT_SEASION];
                NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Season",@"itemLeft",[listHarvestSeason objectAtIndex:(self.clsSuccessionDetail.harvestseasonid - 1)],@"itemRight", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.harvestseasonid]];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
                
            }
                
                
                break;
            case 1:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_PRUNING_SEASON];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PRUNING_SEASON];
                [cellShow setIsShowLineBottom:true];
                NSString* winterStr = self.clsSuccessionDetail.pruningwinter?@"1":@"0";
                NSString* summerStr = self.clsSuccessionDetail.pruningsummer?@"1":@"0";
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:winterStr,@"winter",summerStr,@"summer", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [successionShow addObject:cellShow];
            }
                break;
            case 2:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                numberDayToFirstHarvest = 0;
                dateIntervalDayToFirstHarvest = self.clsSuccessionDetail.dateplanted;
                NSString * stringDate=[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_FIRST_HARVEST];
                
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"First Harvest",@"itemLeft",stringDate,@"itemRight",@"hide",@"statusArrowImg", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setIsHaveData_AT:true];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest]];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
            }
                break;
            case 3:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                numberDayToLastHarvest = self.clsSuccessionDetail.daytomaturityll;
                dateIntervalDayToLastHarvest =  numberDayToLastHarvest* 86400000 + dateIntervalDayToFirstHarvest  ;
                [cellShow setTypeCell_AT:Cell_NameType2_ID];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_LAST_HARVEST];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest]];
                [cellShow setIsHaveData_AT:true];
                [cellShow setIsShowLineBottom:true];
                [successionShow addObject:cellShow];
                
                
            }
                break;
            case 4:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:Cell_NameType_ID];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PLANTING_YEAR];
                NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Year",@"itemLeft", [NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.plantingyear],@"itemRight", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.plantingyear]];
                [cellShow setIsShowLineBottom:false];
                [successionShow addObject:cellShow];
            }
                
                
                break;
                
            case 5:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                inRowSpacingPlantCL = [self.clsSuccessionDetail inrowspacing];
                rowFeetPlantCl = [self.clsSuccessionDetail rowspacinglow];
                
                [cellShow setTypeCell_AT:CELL_AT_TWOCOLUMN];
                NSDictionary * dictShow;
                NSMutableDictionary * dictValue;
                
                dictShow=[[NSDictionary alloc] initWithObjects:@[@"In-row Spacing",@"Between-row Spacing",@"1"] forKeys:@[@"column1",@"column2",@"punctuationmark"]];
                
                dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2"]];
                
                
                [cellShow setNsmutabledictSelected_AT:dictValue];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setPlantingMethod:self.successionSeedingMethoTypeCL];
                [successionShow addObject:cellShow];
            }
                break;
            case 6:{
                //Hide field if band swath
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_SHOWNUMBEROFPLANTS];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_NUMBER_TREES];
                [cellShow setIsShowLineBottom:true];
                if (self.clsSuccessionDetail.numoftrees == 0) {
                    [cellShow setValueSelected_AT:@"0"]; ;
                }else{
                    [cellShow setIsHaveData_AT:true];
                    [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%i",(int)self.clsSuccessionDetail.numoftrees]];
                }
                
                
                [successionShow addObject:cellShow];
            }
                
                break;
                
                
            case 7:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDEMORE];
                [cellShow setTypeAction_AT:ACTION_AT_HIDEMORE];
                [cellShow setValueSelected_AT:@"1"];
                [cellShow setIsShowLineBottom:false];
                [successionShow addObject:cellShow];
            }
                break;
            case 12:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeCell_Hide_Show:CELL_AT_ADD_TEXTFIELD];
                [cellShow setTypeMethod_AT:CELL_AT_SHOW_HIDE];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_ROOTSTOCK];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Rootstock",@"fieldText", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                if (![self.clsSuccessionDetail.rootstock isEqualToString:@""]) {
                    [cellShow setValueSelected_AT:self.clsSuccessionDetail.rootstock];
                    [cellShow setIsHaveData_AT:true];
                }
                [cellShow setIsShowLineBottomFull:true];
                [successionShow addObject:cellShow];
            }
                
                break;
            case 13:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_HIDE_SHOW];
                [cellShow setTypeCell_Hide_Show:CELL_AT_ADD_TEXTFIELD];
                [cellShow setTypeMethod_AT:CELL_AT_SHOW_HIDE];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_ROOTSTOCK_SUPPLIER];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Rootstock Supplier",@"fieldText", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                if (![self.clsSuccessionDetail.rootstocksupplier isEqualToString:@""]) {
                    [cellShow setValueSelected_AT:self.clsSuccessionDetail.rootstocksupplier];
                    [cellShow setIsHaveData_AT:true];
                }
                [cellShow setIsShowLineBottomFull:false];
                [successionShow addObject:cellShow];
            }
                
                
                break;
                
            case 17:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"NOTES"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
                [successionShow addObject:cellShow];
            }
                
                break;
            case 18:{
                if (self.clsSuccessionDetail.links.notes.count > 0) {
                    for (int j = 0; j < self.clsSuccessionDetail.links.notes.count; j ++) {
                        CellForShow * cellShow=[[CellForShow alloc] init];
                        [cellShow setTypeCell_AT:CELL_AT_ADD_NOTE_ITEM];
                        [cellShow setTypeAction_AT:ACTION_AT_ADD_NOTE];
                        cellShow.note = (SW_Note*)[self.clsSuccessionDetail.links.notes objectAtIndex:j];
                        [successionShow addObject:cellShow];
                    }
                }
                
            }
                
                break;
            case 19:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AT_ADD_ITEM_VIEW_ITEM];
                [cellShow setTypeAction_AT:ACTION_ADD_ITEM_VIEW_ITEM];
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Add a Note",@"leftitem", @"View all Notes",@"rightitem", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [successionShow addObject:cellShow];
            }
                
                break;
            case 20:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                [cellShow setTypeCell_AT:CELL_AN_HEADER_ADD_CROP_LOCATION];
                NSDictionary * dict=[NSDictionary dictionaryWithObjects:@[@"PHOTOS"] forKeys:@[@"name"]];
                [cellShow setDictionaryShow_AT:dict];
                [cellShow setTypeMethod_AT:CELL_AN_ADD_TASK];
                [cellShow setIsShowLineTop:YES];
                [successionShow addObject:cellShow];
            }
                
                break;
            case 21:{
                CellForShow * cellShow=[[CellForShow alloc] init];
                if (self.clsSuccessionDetail.links.images.count > 0) {
                    cellShow.listImages = [NSMutableArray new];
                    for (int i = 0; i < self.clsSuccessionDetail.links.images.count; i++) {
                        CImage *cImage = self.clsSuccessionDetail.links.images[i];
                        CImage *image = [CImage new];
                        image.id = cImage.id;
                        image.url = cImage.url;
                        [cellShow.listImages addObject:image];
                    }
                    cellShow.totalCurrentImage = (int)cellShow.listImages.count;
                }
                [cellShow setTypeCell_AT:CELL_AT_TAKEPHOTO];
                [cellShow setTypeAction_AT:ACTION_AT_ADD_PHOTO];
                
                [successionShow addObject:cellShow];
            }
                
                
                break;
                
            default:
                continue;
                break;
        }
    }
    return successionShow;
}
#pragma mark - Handler Click action
-(void)didSelectedAtIndex:(int)index{
    
    CellForShow * cellShow=[dataShows objectAtIndex:index];
    if (cellShow.typeCell_AT != CELL_AT_HIDEMORE && cellShow.typeCell_AT != CELL_AN_HEADER_ADD_CROP_LOCATION && cellShow.typeCell_AT != CELL_AT_PRUNING_SEASON && cellShow.typeCell_AT != CELL_AT_ADD_NOTE_ITEM) {
        self.isSelectItem = true;
    }
    
    switch (cellShow.typeAction_AT) {
        case ACTION_AT_ADD_CROP:{
            if (self.isPopulate) {
                return;
            }
            if (self.clsSuccessionDetail.id == 0) {
                if ([_eventHandler respondsToSelector:@selector(showListCropVariety:)]) {
                    [_eventHandler showListCropVariety:index];
                }
            }
            
        }
            
            break;
        case ACTION_AT_ADD_LOCATION:{
            if ([_eventHandler respondsToSelector:@selector(showListLocation:andIdType:andisGH:andStartDate:andEndDate:)]) {
                int type=2;
                if (self.successionSeedingMethoTypeCL ==GREENHOUSE_SOW_S) {
                    [_eventHandler showListLocation:index andIdType:type andisGH:NO andStartDate:dateIntervalDayToTransplant andEndDate:dateIntervalDayToLastHarvest];
                }
                else{
                    [_eventHandler showListLocation:index andIdType:type andisGH:NO andStartDate:dateIntervalSeedingDate andEndDate:dateIntervalDayToLastHarvest];
                }
                
            }
        }
            
            break;
//        case ACTION_AT_ACTIONEDITLOCATION:{
//            if ([_eventHandler respondsToSelector:@selector(showListLocation:andIdType:andisGH:andStartDate:andEndDate:)]) {
//                int type=2;
//                if (self.successionSeedingMethoTypeCL ==Greenhouse_Sow_S) {
//                    [_eventHandler showListLocation:index andIdType:type andisGH:NO andStartDate:dateIntervalDayToTransplant andEndDate:dateIntervalDayToLastHarvest];
//                }
//                else{
//                    [_eventHandler showListLocation:index andIdType:type andisGH:NO andStartDate:dateIntervalSeedingDate andEndDate:dateIntervalDayToLastHarvest];
//                }
//                
//            }
//        }
//            break;
        case ACTION_AT_ADD_SEEDINGDATE:{
            NSDate* date = [Utilities dateFromTimestamp:dateIntervalSeedingDate];
            if ([_eventHandler respondsToSelector:@selector(showDatePicker:atIndex:mimDate:maxDate:)]) {
                [_eventHandler showDatePicker:date atIndex:index mimDate:nil maxDate:nil];
            }
        }
            
            break;
        case ACTION_AT_ADD_PLANTINGMETHOD:{
            if (self.clsSuccessionDetail.id == 0) {
                if ([_eventHandler respondsToSelector:@selector(showPickerPlantingMethod:andId:)]) {
                    [_eventHandler showPickerPlantingMethod:index andId:[[cellShow dictionaryShow_AT] valueForKey:@"itemRight"]];
                }
            }
            
        }
            
            break;
        case ACTION_AT_ADD_GROWINGMETHOD:{
            if (self.clsSuccessionDetail.id == 0) {
                if ([_eventHandler respondsToSelector:@selector(showPickerGrowingMethod:andId:)]) {
                    [_eventHandler showPickerGrowingMethod:index andId:[[cellShow dictionaryShow_AT] valueForKey:@"itemRight"]];
                }
            }
            
        }
            
            break;
            
        case ACTION_AT_ADD_DAYTOTRANSPLANT:{
            
            NSDate* minDate = [Utilities dateFromTimestamp:dateIntervalSeedingDate ]  ; //- 86400000
            
            NSDate* date = [Utilities dateFromTimestamp:dateIntervalDayToTransplant];
            
            if ([_eventHandler respondsToSelector:@selector(showDatePicker:atIndex:mimDate:maxDate:)]) {
                [_eventHandler showDatePicker:date atIndex:index mimDate:minDate maxDate:nil];
            }
        }
            
            break;
        case ACTION_AT_ADD_FIRST_HARVEST:{
            NSDate* minDate = [Utilities dateFromTimestamp:dateIntervalDayToTransplant];
            NSDate* date = [Utilities dateFromTimestamp:dateIntervalDayToFirstHarvest];
            if (self.categoryId == 3) {
                minDate = nil;
            }
            
            if ([_eventHandler respondsToSelector:@selector(showDatePicker:atIndex:mimDate:maxDate:)]) {
                [_eventHandler showDatePicker:date atIndex:index mimDate:minDate maxDate:nil];
            }
        }
            break;
        case ACTION_AT_ADD_LAST_HARVEST:{
            NSDate* date = [Utilities dateFromTimestamp:dateIntervalDayToLastHarvest];
            
            NSDate* minDate = [Utilities dateFromTimestamp:dateIntervalDayToFirstHarvest];
            if ([_eventHandler respondsToSelector:@selector(showDatePicker:atIndex:mimDate:maxDate:)]) {
                [_eventHandler showDatePicker:date atIndex:index mimDate:minDate maxDate:nil];
            }
        }
            break;
        case ACTION_AT_ADD_LINEOFDRIP:{
            if ([cellShow.valueSelected_AT isEqualToString:@"0"]) {
                if ([_eventHandler respondsToSelector:@selector(showPicker:andStringSelect:andStringSelect:)]) {
                    
                    [_eventHandler showPicker:index andStringSelect:@"Variable" andStringSelect:listDripLinesPerBed];
                    
                }
            }
            else{
                if ([_eventHandler respondsToSelector:@selector(showPicker:andStringSelect:andStringSelect:)]) {
                    
                    [_eventHandler showPicker:index andStringSelect:cellShow.valueSelected_AT andStringSelect:listDripLinesPerBed];
                    
                }
            }
        }
            
            break;
        case ACTION_AT_ADD_NUMBEROFPLANS:{
            
        }
            
            break;
        case ACTION_AT_HIDEMORE:{
            if ([cellShow.valueSelected_AT isEqualToString:@"1"]) {
                [self showAndHideMore:YES];
                [cellShow setValueSelected_AT:@"0"];
            }else{
                [self showAndHideMore:NO];
                [cellShow setValueSelected_AT:@"1"];
            }
            [self reloadWithNewData];
        }
            
            break;
        case ACTION_AT_ACTIONSEEDSOURCE:{
            if ([_eventHandler respondsToSelector:@selector(showPicker:andStringSelect:andStringSelect:)]) {
                [_eventHandler showPicker:index andStringSelect:[cellShow.dictionaryShow_AT valueForKey:@"selectType"] andStringSelect:listSeedSource];
            }
        }
            
            break;
        case ACTION_AT_ACTIONPELLETED:{
            if ([_eventHandler respondsToSelector:@selector(showPicker:andStringSelect:andStringSelect:)]) {
                [_eventHandler showPicker:index andStringSelect:cellShow.valueSelected_AT andStringSelect:listNonPelleted];
            }
        }
               break;
        case ACTION_AT_ACTIONANNUAL:{
            if ([_eventHandler respondsToSelector:@selector(showPicker:andStringSelect:andStringSelect:)]) {
                [_eventHandler showPicker:index andStringSelect:cellShow.valueSelected_AT andStringSelect:listAnnual];
            }
        }
            
         
            break;
        case ACTION_AT_ADD_CHECKBOX:{
            cellShow.isHaveData_AT = true;
            if ([cellShow.valueSelected_AT isEqualToString:@"0"]) {
                cellShow.valueSelected_AT = @"1";
            }
            else{
                cellShow.valueSelected_AT = @"0";
            }
            [self reloadWithNewData];
        }
            
            
            break;
        case ACTION_AT_ADD_PHOTO:{
            
        }
            
            
            break;
        case ACTION_AT_ADD_LOCATIONGREENHOUSE:{
            if ([_eventHandler respondsToSelector:@selector(showListLocation:andIdType:andisGH:andStartDate:andEndDate:)]) {
                int type=1;
                if (self.successionSeedingMethoTypeCL ==GREENHOUSE_SOW_S) {
                    [_eventHandler showListLocation:index andIdType:type andisGH:YES andStartDate:dateIntervalSeedingDate andEndDate:dateIntervalDayToTransplant];
                }
                else{
                    [_eventHandler showListLocation:index andIdType:type andisGH:YES andStartDate:dateIntervalSeedingDate andEndDate:dateIntervalDayToLastHarvest];
                }
                
            }
        }
            break;
        case ACTION_AT_ADD_NOTE:{
            if ([_eventHandler respondsToSelector:@selector(editNote:andNoteId:)]) {
                [_eventHandler editNote:index andNoteId:(int)cellShow.note.id];
                
            }
        }
            break;
        case ACTION_HARVERT_SEASION:{
            if ([_eventHandler respondsToSelector:@selector(showPicker:andStringSelect:andStringSelect:)]) {
                NSString* nameStringOflist = [listHarvestSeason objectAtIndex:cellShow.valueSelected_AT.intValue - 1] ;
                [_eventHandler showPicker:index andStringSelect:nameStringOflist andStringSelect:listHarvestSeason];
                
            }
        }
            break;
        case ACTION_AT_ADD_PLANTING_YEAR:{
            if ([_eventHandler respondsToSelector:@selector(showPicker:andStringSelect:andStringSelect:)]) {
                [_eventHandler showPicker:index andStringSelect:cellShow.valueSelected_AT andStringSelect:listPlatingYear];
                
            }
        }
            break;
        case ACTION_AT_ADD_SEEDING_RATE:{
            
            if ([_eventHandler respondsToSelector:@selector(showSeedingRate:atValue:andRateId:andSeedingRateLow:andSeedingRateHigh:)]) {
                if (self.clsSuccessionDetail.id > 0) {
                    [_eventHandler showSeedingRate:index atValue:[cellShow.dictSelected_AT valueForKey:@"valueRate"] andRateId:[cellShow.dictSelected_AT valueForKey:@"unitRateId"] andSeedingRateLow:self.clsSuccessionDetail.seedingratelow andSeedingRateHigh:self.clsSuccessionDetail.seedingratehigh];
                }
                else{
                    [_eventHandler showSeedingRate:index atValue:[cellShow.dictSelected_AT valueForKey:@"valueRate"] andRateId:[cellShow.dictSelected_AT valueForKey:@"unitRateId"] andSeedingRateLow:self.combinationDataForShow.seedingratelow andSeedingRateHigh:self.combinationDataForShow.seedingratehigh];
                    
                }
                
                
            }
            
        }
            break;
               default:
            break;
    }
    
    
}
-(void)showAndHideMore:(BOOL)isShow{
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        if (cellShow.typeCell_AT==CELL_AT_HIDE_SHOW||cellShow.typeMethod_AT ==CELL_AT_SHOW_HIDE) {
            cellShow.isShowCell=isShow;
        }
    }
}
#pragma mark - Handler Set data for tableview

-(void)setDataForIndex:(int)atIndex withData:(NSString *)valueString andDict:(NSDictionary *)valueDict andStringReturn:(NSString *)stringReturn{
    self.isSelectItem = true;
    CellForShow * cellShow=[dataShows objectAtIndex:atIndex];
    [cellShow setValueSelected_AT:valueString];
    [cellShow setDictSelected_AT:valueDict];
    switch (cellShow.typeAction_AT) {
        case ACTION_AT_ADD_CROP:{
        
        }
            
            break;
        case ACTION_AT_ADD_LOCATION:{

        }
            
            break;
        case ACTION_HARVERT_SEASION:{
            NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Season",@"itemLeft", stringReturn,@"itemRight", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            [cellShow setValueSelected_AT:valueString];
            [self reloadWithNewData];
        }
            break;
        case ACTION_AT_ADD_SEEDINGDATE:{
            //update all
            NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Date",@"itemLeft",stringReturn,@"itemRight",@"hide",@"statusArrowImg", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            [cellShow setIsHaveData_AT:true];
            
            dateIntervalSeedingDate = [valueString doubleValue];
            [self caculatorDate:ACTION_AT_ADD_SEEDINGDATE];
            [self reloadDataWhenChangeDate];
            if (self.categoryId != 5 || ![growingmethodname isEqualToString:@"Cover Crop"]) {
                [self updateTitleCrop];
            }
            
            

            [self reloadWithNewData];
        }
            
            break;
        case ACTION_AT_ADD_PLANTINGMETHOD:{

        }
            
            break;
        case ACTION_AT_ADD_GROWINGMETHOD:{
            
        }
            
            break;
//        case ACTION_AT_ADD_HARVEST_UNIT:{
//            [cellShow setIsHaveData_AT:YES];
//            
//            NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Harvest Unit",@"itemLeft",stringReturn,@"itemRight", nil];
//            [cellShow setDictionaryShow_AT:dictShow];
//            
//            
//            [self reloadWithNewData];
//        }
//            
//            break;
        case ACTION_AT_ADD_DAYTOTRANSPLANT:{
            
            dateIntervalDayToTransplant = [valueString doubleValue];
            
            NSDate* date = [Utilities dateFromTimestamp: dateIntervalDayToTransplant];
            NSDate* seedingDate = [Utilities dateFromTimestamp:dateIntervalSeedingDate];
            numberDayToTransplant = (int)[Utilities daysBetweenDate:seedingDate andDate:date];
            
            NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Transplant",@"itemLeft",stringReturn,@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Transplant",numberDayToTransplant],@"itemRightBottom", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            
            [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:valueString,@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToTransplant],@"days", nil]];
            [self caculatorDate:ACTION_AT_ADD_DAYTOTRANSPLANT];
            [self reloadDataWhenChangeDate];
            [self updateTitleCrop];
            [self reloadWithNewData];
            
            
            
        }
            
            break;
        case ACTION_AT_ADD_FIRST_HARVEST:{
            dateIntervalDayToFirstHarvest = [valueString doubleValue];
            if (self.categoryId == 3) {
                NSString * stringDate=[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest];
                
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"First Harvest",@"itemLeft",stringDate,@"itemRight",@"hide",@"statusArrowImg", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToFirstHarvest]];
                [self caculatorDate:ACTION_AT_ADD_FIRST_HARVEST];
                
                
                
                // calculate last harvest
                for (int i=0; i<dataShows.count; i++) {
                    CellForShow * cellShow=[dataShows objectAtIndex:i];
                    
                    switch (cellShow.typeAction_AT) {
                        
                        case ACTION_AT_ADD_LAST_HARVEST:{
                            
                            NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                            [cellShow setDictionaryShow_AT:dictShow];
                            [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                            [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest]];
                            
                            
                        }
                            
                            break;
                            
                        default:
                            break;
                    }
                    
                }
                [self updateTitleCrop:dateIntervalDayToFirstHarvest maxTimeStamp:dateIntervalDayToLastHarvest];
            }
            else{
                NSDate* date = [Utilities dateFromTimestamp:dateIntervalDayToFirstHarvest];
                
                
                NSDate* dateTransplant = [Utilities dateFromTimestamp:dateIntervalDayToTransplant];
                numberDayToFirstHarvest = (int)[Utilities daysBetweenDate:dateTransplant andDate:date];
                
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"First Harvest",@"itemLeft",stringReturn,@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:valueString,@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToFirstHarvest],@"days", nil]];
                
                // update last har
                [self caculatorDate:ACTION_AT_ADD_FIRST_HARVEST];
                
                if (self.categoryId == 5  && [growingmethodname isEqualToString:@"Cover Crop"]) {
                    NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Take-Down Date",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToFirstHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f Days to Maturity",numberDayToFirstHarvest],@"itemRightBottom", nil];
                    [cellShow setDictionaryShow_AT:dictShow];
                    
                    [self reloadDataWhenChangeDate];
                    [self updateTitleCrop:dateIntervalSeedingDate maxTimeStamp:dateIntervalDayToFirstHarvest];
                }
                else{
                    [self reloadDataWhenChangeDate];
                    [self updateTitleCrop];
                }
                
                
                
                
                
            }
            
            
            [self reloadWithNewData];
        }
            
            break;
        case ACTION_AT_ADD_LAST_HARVEST: {
            
            dateIntervalDayToLastHarvest = [valueString doubleValue];
            NSDate* date = [Utilities dateFromTimestamp:dateIntervalDayToLastHarvest];
            NSDate* firstHarvest = [Utilities dateFromTimestamp:dateIntervalDayToFirstHarvest];
            numberDayToLastHarvest = (int)[Utilities daysBetweenDate:firstHarvest andDate:date];
            
            if (self.categoryId == 3) {
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",[Utilities stringFullDateFromTimestamp:dateIntervalDayToLastHarvest],@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest],@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                [cellShow setValueSelected_AT:[NSString stringWithFormat:@"%.0f",dateIntervalDayToLastHarvest]];
                
                [self updateTitleCrop:dateIntervalDayToFirstHarvest maxTimeStamp:dateIntervalDayToLastHarvest];
            }
            else{
                
                
                NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Last Harvest",@"itemLeft",stringReturn,@"itemRightTop",[NSString stringWithFormat:@"%.0f-Day Harvest Window",numberDayToLastHarvest],@"itemRightBottom", nil];
                [cellShow setDictionaryShow_AT:dictShow];
                [cellShow setDictSelected_AT:[NSDictionary dictionaryWithObjectsAndKeys:valueString,@"timetamp",[NSString stringWithFormat:@"%.0f",numberDayToLastHarvest],@"days", nil]];
                
                [self reloadDataWhenChangeDate];
                
                [self updateTitleCrop];
            }
            
            
            [self reloadWithNewData];
            
            
        }
            break;
        case ACTION_AT_ADD_LINEOFDRIP:{
            [cellShow setIsHaveData_AT:YES];

            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Lines of drip per bed",@"itemLeft",stringReturn,@"itemRight",@"0.87",@"alpha", nil];
            if ([stringReturn isEqualToString:@"Variable"]) {
                [cellShow setValueSelected_AT:@"0"];
            }
            else{
                [cellShow setValueSelected_AT:stringReturn];
            }
            [cellShow setDictionaryShow_AT:dictShow];
            
            
            
            
            [self reloadWithNewData];
        }
            
            break;
        case ACTION_AT_ADD_NUMBEROFPLANS:{
            
        }
            
            break;
        case ACTION_AT_HIDEMORE:{
            
        }
            
            break;
        case ACTION_AT_ACTIONSEEDSOURCE:{
            [cellShow setIsHaveData_AT:YES];
            NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:stringReturn,@"selectType", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            
            [self reloadWithNewData];
        }
            
            break;
        case ACTION_AT_ACTIONPELLETED:{
            [cellShow setIsHaveData_AT:YES];
            NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:stringReturn,@"selectType", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            [cellShow setValueSelected_AT:stringReturn];
            [self reloadWithNewData];
        }
            break;
        case ACTION_AT_ACTIONANNUAL:{
            [cellShow setIsHaveData_AT:YES];
            NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:stringReturn,@"selectType", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            [cellShow setValueSelected_AT:stringReturn];
            [self reloadWithNewData];
        }
            
            break;
        
            
        case ACTION_AT_ADD_PHOTO:{
            
        }
            break;
        case ACTION_AT_ADD_PRUNING_SEASON:{
            [cellShow setDictionaryShow_AT:valueDict];
        }
            break;
        case ACTION_AT_ADD_PLANTING_YEAR:{
            NSDictionary * dictShow =[NSDictionary dictionaryWithObjectsAndKeys:@"Planting Year",@"itemLeft", stringReturn,@"itemRight", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            [cellShow setValueSelected_AT:stringReturn];
            [cellShow setIsHaveData_AT:true];
            [self reloadWithNewData];
        }
            break;
        case ACTION_AT_ADD_ROOTSTOCK:{
            [cellShow setIsHaveData_AT:true];
        }
            break;
        case ACTION_AT_ADD_ROOTSTOCK_SUPPLIER:{
            [cellShow setIsHaveData_AT:true];
        }
            break;
        case ACTION_AT_ADD_NUMBER_TREES:{
            [cellShow setIsHaveData_AT:true];
        }
            break;
        case ACTION_AT_ADD_SEEDING_RATE:{
            NSNumberFormatter* formatnumber = [[NSNumberFormatter alloc] init];
            formatnumber.numberStyle = NSNumberFormatterDecimalStyle;
            NSDictionary *dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Seeding Rate",@"itemLeft",[NSString stringWithFormat:@"%@ %@",[formatnumber stringFromNumber:[[NSNumber alloc]initWithDouble:valueString.doubleValue]],stringReturn ],@"itemRight",@"0.87",@"alpha", nil];
            [cellShow setDictionaryShow_AT:dictShow];
            
            NSDictionary *dictSelected_AT=[NSDictionary dictionaryWithObjectsAndKeys:valueString,@"valueRate",stringReturn,@"unitRate",[valueDict valueForKey:@"unitRateId"],@"unitRateId", nil];
            [cellShow setDictSelected_AT:dictSelected_AT];
            
            
            
            [self reloadWithNewData];
        }
            break;
        default:
            break;
    }
    

}
#pragma mark - Handler Change Value From TableView
-(BOOL)isHaveSelectGreeHouse{
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        if (cellShow.typeAction_AT==ACTION_AT_ADD_LOCATIONGREENHOUSE) {
            return YES;
        }
    }
    return NO;
}

-(void)setDataWhenChangeSeedingMethod:(SuccessionSeedingMethodType )newMethod andIndex:(int)atIndex withData:(NSString *)valueString andDict:(NSDictionary *)valueDict andStringReturn:(NSString *)stringReturn{
    self.isSelectItem = true;
    int removeAtIndex;
    int removeNumber;
    removeAtIndex=atIndex;
    removeNumber= atIndex;
    NSRange  rangeRemove=NSMakeRange(removeAtIndex, dataShows.count-removeNumber);
    [dataShows removeObjectsInRange:rangeRemove];
    
    growingmethodname = self.combinationDataForShow.growingmethod;
    if (self.categoryId == 3) {
        [dataShows addObjectsFromArray:[self createListFistLoadForFruits]];
    }
    else{
        [dataShows addObjectsFromArray:[self createListFistLoad]];
    }

    
    [self reloadDataWhenChangeDate];
    
    [self reloadWithNewData];
    

}
-(void)setDataWhenChangeGrowingMethod:(int)atIndex withData:(NSString *)valueString{
    self.isSelectItem = true;
    int removeAtIndex;
    int removeNumber;
    if ([self isHaveSelectGreeHouse]) {
        removeAtIndex=atIndex-2;
        removeNumber= atIndex-2;
    }else{
        removeAtIndex=atIndex-1;
        removeNumber= atIndex-1;
    }
    NSRange  rangeRemove=NSMakeRange(removeAtIndex, dataShows.count-removeNumber);
    [dataShows removeObjectsInRange:rangeRemove];
    
    growingmethodname = self.combinationDataForShow.growingmethod;
    
    if (self.categoryId == 3) {
        [dataShows addObjectsFromArray:[self createListFistLoadForFruits]];
    }
    else{
        [dataShows addObjectsFromArray:[self createListFistLoad]];
    }
    
    
    [self reloadWithNewData];


}
-(int)removeCellWithType{
    BOOL isGetFirstIndex = false;
    int firstIndex = 0;
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        if (cellShow.typeMethod_AT==TYPE_PLAN_METHOD) {
            if (!isGetFirstIndex) {
                firstIndex = i;
                
            }
            [dataShows removeObject:cellShow];
            i--;
        }
    }
    return firstIndex;
    
}
-(int)indexWithTypeAction:(AddTaskTypeAction)typeAction{
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        if (cellShow.typeAction_AT==typeAction) {
            return i;
        }
    }
    return -1;
    
}
-(void)setDataWhenSelectedLocationGH:(NSArray *)listField andAtIndex:(int)index{
    self.isSelectItem = true;
    if (self.successionSeedingMethoTypeCL!=GREENHOUSE_SOW_S || self.categoryId == 3) {
        return;
    }
    self.listLocationSelectedGreenHouse =[NSMutableArray arrayWithArray:listField];
    for (int i =0; i<listField.count; i++) {
        ClassFields * field=[listField objectAtIndex:i];
        NSString * nameField =@"";
        nameField= field.name;
        CellForShow * cellShow=[dataShows objectAtIndex:index];
        NSDictionary * dictShow=[NSDictionary dictionaryWithObjectsAndKeys:@"Greenhouse",@"itemLeft",nameField,@"itemRight", nil];
        [cellShow setDictionaryShow_AT:dictShow];
    }
    [self reloadWithNewData];
    
}
-(void)numberOfBedPlantCL:(NSArray *)listField{
    
    for (int i =0 ; i<listField.count; i++) {
        ClassFields * field=[listField objectAtIndex:i];
        for (int j=0; j<field.listGroup.count; j++) {
            ClassGroups * gruop=[field.listGroup objectAtIndex:j];
            for (int k=0; k<gruop.listBeds.count; k++) {
                
                ClassBeds * bed=[gruop.listBeds objectAtIndex:k];
                if (!bed.isavailable) {
                    continue;
                }
                numberOfBedPlantCl++;
                
            }
            
        }
    }

}
-(void)setDataWhenSelectedLocation:(NSArray *)listField andAtIndex:(int)index{
    BOOL isChangeLocationFromEmpty = false;
    self.listLocationSelected =[[NSMutableArray alloc] initWithArray:listField];
    if (self.listLocationSelected.count == 0) {
        index = [self removeCellWithType];
        CellForShow * cellShow=[[CellForShow alloc] init];
        [cellShow setTypeCell_AT:CELL_AT_LOCATION];
        NSDictionary * dictShow=[NSDictionary dictionaryWithObjects:@[@"Select Location"] forKeys:@[@"valueShow"]];
        [cellShow setDictionaryShow_AT:dictShow];
        [cellShow setTypeAction_AT:ACTION_AT_ADD_LOCATION];
        [cellShow setTypeMethod_AT:TYPE_PLAN_METHOD];
        [dataShows insertObject:cellShow atIndex:index];
        isChangeLocationFromEmpty = false;
    }
    
    totalBedInField=0;
   
    
    if (listField.count>0) {
        ClassFields * classField=[self.listLocationSelected objectAtIndex:0];
        bedLengthPlantCL=(int)[classField bedlength];
        totalBedInField=[[listField objectAtIndex:0] bedsperblock];
        index = [self removeCellWithType];
        
    }else{
        [self reloadWithNewData];
        return;
        
    }
    numberOfBedPlantCl=0;
    //Parse
    [self numberOfBedPlantCL:listField];
    
    NSMutableArray* stringNameLocation = [NSMutableArray new];
    for (int i =0; i<listField.count; i++) {
        ClassFields * field=[listField objectAtIndex:i];
        NSString * nameField =@"";
        nameField= field.name;
        if ([field.locationtype isEqualToString:@"GREENHOUSE"]) {
//            [self addCellAtIndex:index andStringLocation:nameField andCheckEdit:checkEdit];
            [stringNameLocation addObject:nameField];
        }else{
            int totalbed = 0;
            for (int j = 0; j<field.listGroup.count; j++) {
                ClassGroups* clsGroup = [field.listGroup objectAtIndex:j];
                totalbed += clsGroup.listBeds.count;
                
            }
            if (totalbed == field.bedsperblock) {
//                checkEdit++;
//                [self addCellAtIndex:index andStringLocation:nameField andCheckEdit:checkEdit];
//                index++;
                [stringNameLocation addObject:nameField];
            }
            else{
                for (int j=0; j<field.listGroup.count; j++) {
                    ClassGroups * group=[field.listGroup objectAtIndex:j];
                    NSString * location=[NSString stringWithFormat:@"%@, %@",nameField,group.groupname];
                    if ([group.groupname isEqualToString:@"Ungrouped Beds"]) {
                        location=[NSString stringWithFormat:@"%@",nameField];
                    }
                    if (group.bedsperblock==group.listBeds.count&&group.id!=-1) {
//                        checkEdit++;
//                        [self addCellAtIndex:index andStringLocation:location andCheckEdit:checkEdit];
//                        index++;
                        [stringNameLocation addObject:location];
                    }else{
                        if (group.listBeds.count > 0) {
                            int oldposition = 0;
                            int newposition = 0;
                            NSString* bedName = @"";
                            for (int k=0; k<group.listBeds.count; k++) {
                                
                                ClassBeds * bed=[group.listBeds objectAtIndex:k];
                                if (oldposition == 0) {
                                    oldposition = (int)bed.position;
                                    bedName = [NSString stringWithFormat:@"%i",oldposition];
                                }
                                else{
                                    if ((oldposition+1) != (int)bed.position) {
                                        if (newposition == 0) {
                                            oldposition = (int)bed.position;
                                            bedName = [NSString stringWithFormat:@"%@, %i",bedName,oldposition];
                                        }
                                        else{
                                            oldposition = (int)bed.position;
                                            bedName = [NSString stringWithFormat:@"%@-%i, %i",bedName,newposition,oldposition];
                                        }
                                        newposition = 0;
                                    }
                                    else{
                                        newposition = (int)bed.position;
                                        oldposition = (int)bed.position;
                                    }
                                }
                                
                            }
                            if (newposition != 0) {
                                bedName = [NSString stringWithFormat:@"%@-%i",bedName,newposition];
                            }
                            location=[NSString stringWithFormat:@"%@, %@%@",location,group.listBeds.count>1? @"Beds ":@"Bed ",bedName];
                        }
                        
//                        checkEdit++;
//                        [self addCellAtIndex:index andStringLocation:location andCheckEdit:checkEdit];
//                        index++;
                        [stringNameLocation addObject:location];
                    }
                }
            
            }
        }
        
        
    }
    if (stringNameLocation.count > 0) {
        
        for (int i = 0; i< stringNameLocation.count; i++) {
            BOOL checkEdit = false;
            BOOL isShowLineBottom = false;
            if (i == 0) {
                checkEdit = true;
            }
            else {
                checkEdit = false;
            }
            if (i == stringNameLocation.count - 1) {
                isShowLineBottom = true;
            }
            locationName = [stringNameLocation objectAtIndex:0];
            if (stringNameLocation.count>1) {
                locationName =[NSString stringWithFormat:@"%@ +%i more",locationName, (int)stringNameLocation.count-1];
            }
            [self addCellAtIndex:(index+i) andStringLocation:[stringNameLocation objectAtIndex:i] andCheckEdit:checkEdit andShowLineBottom:isShowLineBottom];
        }
    }
    
    // check user edit
    if (isChangeLocationFromEmpty == false || (rowFeetPlantCl == 0 || inRowSpacingPlantCL == 0 || rowFeetPlantCl == 0 )) {
        [self refreshThreeFourColumnWhenChangeLocation];
    }
    
    
    [self reloadWithNewData];
    
}

-(void)addCellAtIndex:(int)atIndex andStringLocation:(NSString *)nameField andCheckEdit:(BOOL)checkEdit andShowLineBottom:(BOOL)isShow{
    CellForShow * cellShow=[[CellForShow alloc] init];
    [cellShow setTypeCell_AT:CELL_AT_LOCATION];
    [cellShow setTypeAction_AT:ACTION_AT_ADD_LOCATION];
    [cellShow setTypeMethod_AT:TYPE_PLAN_METHOD];
    [cellShow setIsHaveData_AT:false];
    if (checkEdit) {
        [cellShow setIsHaveData_AT:true];
    }
    NSDictionary * dictShow=[[NSDictionary alloc] initWithObjects:@[nameField] forKeys:@[@"valueShow"]];
    [cellShow setDictionaryShow_AT:dictShow];
    [cellShow setIsShowLineBottom:isShow];
    [dataShows insertObject:cellShow atIndex:atIndex];
  

}
-(void)setDataWhenEditCombination:(NSString *)keyEdit andString:(NSString *)stringEdit andAtIndex:(int)atIndex{
    self.isSelectItem = true;
    CellForShow * cellShow=[dataShows objectAtIndex:atIndex];
    if (cellShow.typeCell_AT==CELL_AT_THREECOLUMN_V2) {
        
        if ([keyEdit isEqualToString:@"value1"]) {
            rowPerBedPlantCL = [stringEdit intValue];
            [self updateData:kValue1];
            
        }else if([keyEdit isEqualToString:@"value2"]){
            inRowSpacingPlantCL=[stringEdit doubleValue];
            [self updateData:kValue2];
        }else{
            rowFeetPlantCl=[stringEdit doubleValue];
            [self updateData:kValue3];
        }
        
        NSMutableDictionary * dictValue=[NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithInt:rowPerBedPlantCL],[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2",@"value3"]];
        [cellShow setNsmutabledictSelected_AT:dictValue];
        [self refreshNumberOfPlans];
        
    }
    else if (cellShow.typeCell_AT==CELL_AT_SHOWNUMBEROFPLANTS) {
        cellShow.valueSelected_AT = stringEdit;
        cellShow.isHaveData_AT = true;
    }
    else if (cellShow.typeCell_AT == CELL_AT_TWOCOLUMN){
        if ([keyEdit isEqualToString:@"value1"]) {
            inRowSpacingPlantCL = [stringEdit doubleValue];
            
            
        }else if([keyEdit isEqualToString:@"value2"]){
            rowFeetPlantCl=[stringEdit doubleValue];
            
        }
        
        NSMutableDictionary * dictValue = [NSMutableDictionary dictionaryWithObjects:@[[NSNumber numberWithDouble:inRowSpacingPlantCL],[NSNumber numberWithDouble:rowFeetPlantCl]] forKeys:@[@"value1",@"value2"]];
        [cellShow setNsmutabledictSelected_AT:dictValue];
//        [self refreshNumberOfPlans];
    }
    [self reloadWithNewData];

}
-(void)updateData:(int)kValue{

    switch (kValue) {
        case kValue1:
            rowFeetPlantCl = rowPerBedPlantCL *numberOfBedPlantCl * bedLengthPlantCL;
            if (inRowSpacingPlantCL==0) {
                numberOfPlantCL=0;
            }else{
                if (rowFeetPlantCl==0 || inRowSpacingPlantCL == 0) {
                    numberOfPlantCL=0;
                }else{
                    numberOfPlantCL = (rowFeetPlantCl * 12)/ inRowSpacingPlantCL;
                }
            }
            
            break;
        case kValue2:
            rowFeetPlantCl = rowPerBedPlantCL *numberOfBedPlantCl * bedLengthPlantCL;
            if (inRowSpacingPlantCL==0) {
                numberOfPlantCL=0;
            }else{
                if (rowFeetPlantCl==0 || inRowSpacingPlantCL == 0) {
                    numberOfPlantCL=0;
                }else{
                    numberOfPlantCL = (int)((rowFeetPlantCl * 12)/ inRowSpacingPlantCL);
                }
            }
            break;
        case kValue3:
            if (rowFeetPlantCl==0 || inRowSpacingPlantCL == 0) {
                numberOfPlantCL=0;
            }else{
                numberOfPlantCL = (rowFeetPlantCl * 12)/ inRowSpacingPlantCL;
            }
            break;
        default:
            break;
    }
}
-(void)reloadWithNewData{
    if ([_eventHandler respondsToSelector:@selector(reloadTableViewWithNewData:)]) {
        [_eventHandler reloadTableViewWithNewData:dataShows];
    }
}

-(void)showImage:(UIImage*)img atIndex:(int)index{
    self.isSelectItem = true;
    CellForShow * noteShow=[dataShows objectAtIndex:index];
    if (noteShow.totalCurrentImage == 0) {
        noteShow.img1 = img;
        noteShow.totalCurrentImage = 1;
    }
    else if (noteShow.totalCurrentImage == 1){
        noteShow.img2 = img;
        noteShow.totalCurrentImage = 2;
    }
    else if (noteShow.totalCurrentImage == 2){
        noteShow.img3 = img;
        noteShow.totalCurrentImage = 3;
    }
    else if (noteShow.totalCurrentImage == 3){
        noteShow.img4 = img;
        noteShow.totalCurrentImage = 4;
    }
    else if (noteShow.totalCurrentImage == 4){
        noteShow.img5 = img;
        noteShow.totalCurrentImage = 5;
    }
    
    
    [self reloadWithNewData];
}

-(NSMutableArray*)getPhoto {
    for (int i=0; i<dataShows.count; i++) {
        CellForShow * noteShow=[dataShows objectAtIndex:i];
        AddTaskTypeAction typeAction = [noteShow typeAction_AT];
        
        switch (typeAction) {
            case ACTION_AT_ADD_PHOTO:
            {
                if (noteShow.totalCurrentImage > 0) {
                    NSMutableArray* arrayList = [NSMutableArray new];
                    if (noteShow.img1) {
                        [arrayList addObject:noteShow.img1];
                    }
                    if (noteShow.img2) {
                        [arrayList addObject:noteShow.img2];
                    }
                    if (noteShow.img3) {
                        [arrayList addObject:noteShow.img3];
                    }
                    if (noteShow.img4) {
                        [arrayList addObject:noteShow.img4];
                    }
                    if (noteShow.img5) {
                        [arrayList addObject:noteShow.img5];
                    }
                    return arrayList;
                }
            }
                break;
            default:
                break;
        }
    }
    return nil;
}

-(int)returnIdSubTaskType{
    int idReturn=0;
    switch (self.successionSeedingMethoTypeCL) {
        case GREENHOUSE_SOW_S:
            idReturn= 1;
            break;
        case DIRECT_SOW_S:
            idReturn= 14;
            break;
        case DIRECT_SOW_BAND_SWATH:
            idReturn= 2;
            break;
        case TRANSPLANT_PURCHASED_S:
            idReturn= 5;
            break;
            
        default:
            break;
    }
    return idReturn;
}
-(NSArray *)listDictLocation{
    NSMutableArray * dictLocations=[NSMutableArray new];
    NSMutableArray * idLocations=[NSMutableArray new];
    for (int i =0; i<self.listLocationSelected.count; i++) {
        ClassFields * field=[self.listLocationSelected objectAtIndex:i];
        if ([field.locationtype isEqualToString:@"GREENHOUSE"]) {
            [idLocations addObject:[NSNumber numberWithInteger:field.id]];
        }
        for (int j=0;j<field.listGroup.count; j++) {
            ClassGroups * block=[field.listGroup objectAtIndex:j];
            for (int k=0; k<block.listBeds.count; k++) {
                ClassBeds * bed =[block.listBeds objectAtIndex:k];
//                if (bed.isAvailable) {
//                    [idLocations addObject:[NSNumber numberWithInteger:bed.id]];
//                }
                [idLocations addObject:[NSNumber numberWithInteger:bed.id]];
            }
        }
    }
    for (int j =0 ; j<idLocations.count; j++) {
        NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:[idLocations objectAtIndex:j],@"id", nil];
        [dictLocations addObject:dict];
    }
    if (self.listLocationSelectedGreenHouse.count>0) {
        ClassFields * field  = [self.listLocationSelectedGreenHouse objectAtIndex:0];
        NSDictionary * dict=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:field.id],@"id", nil];
        [dictLocations addObject:dict];
    }
    
    return dictLocations;


}

-(CSuccession *) successionReturn {
    CSuccession *retVal = [CSuccession new];
    NSDictionary * dictSave =[self dictReturn];
    NSMutableArray * listLocation =[NSMutableArray new];
    ClassFields * clsField=[[ClassFields alloc] init];
    if (locationName != nil) {       
        clsField.name = locationName ;
    }
    [listLocation addObject:clsField];
    retVal.locations = listLocation;
    double plantDate = [[[[[dictSave valueForKey:@"links"] valueForKey:@"successions"] objectAtIndex:0] valueForKey:@"dateplanted"] doubleValue];
    retVal.plantedDate = plantDate;
    retVal.numberofbeds = numberOfBedPlantCl;
    retVal.bedlength = bedLengthPlantCL;

    
    
    return retVal;
}


-(NSDictionary *)dictReturn{
    NSMutableDictionary * dictLinks=[NSMutableDictionary new];
    NSMutableDictionary * dictSuccession=[NSMutableDictionary new];
    NSMutableDictionary * dictLinksSuccession=[NSMutableDictionary new];
    
    //[dictSuccession setValue:[Utilities convertDateTimeToTimeStamp:[NSDate date]] forKey:@"dateplanted"];
    //Set masterCrop
    [dictLinks setValue:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:[self.masterCropId intValue]] forKey:@"id"] forKey:@"mastercrop"];
    //
    NSDictionary* seedsourceDic;
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * taskShow=[dataShows objectAtIndex:i];
        switch (taskShow.typeAction_AT) {
            case ACTION_AT_ADD_SEEDINGDATE:
                [dictSuccession setValue:[NSNumber numberWithDouble:[[taskShow valueSelected_AT] doubleValue]] forKey:@"startdate"];
                [dictSuccession setValue:[NSNumber numberWithDouble:[[taskShow valueSelected_AT] doubleValue]] forKey:@"dateplanted"];
                
                
                break;
            case ACTION_AT_ADD_DAYTOTRANSPLANT:
            {
                [dictSuccession setValue:[NSNumber numberWithInteger:[[taskShow.dictSelected_AT valueForKey:@"days"] integerValue]] forKey:@"daytotransplant"];
            }
                break;
            case ACTION_AT_ADD_FIRST_HARVEST:{
                if (self.categoryId == 3) {
                    [dictSuccession setValue:[NSNumber numberWithDouble:[[taskShow valueSelected_AT] doubleValue]] forKey:@"startdate"];
                    [dictSuccession setValue:[NSNumber numberWithDouble:[[taskShow valueSelected_AT] doubleValue]] forKey:@"dateplanted"];
                }
                else{
                    [dictSuccession setValue:[NSNumber numberWithInteger:[[taskShow.dictSelected_AT valueForKey:@"days"] integerValue]] forKey:@"adjusteddtmlow"];
                }
                
            }
                break;
            case ACTION_AT_ADD_LAST_HARVEST:{
                [dictSuccession setValue:[NSNumber numberWithInteger:[[taskShow.dictSelected_AT valueForKey:@"days"] integerValue]] forKey:@"daytomaturityll"];
            }
                break;
            case ACTION_AT_ADD_LINEOFDRIP:{
                [dictSuccession setValue:[NSNumber numberWithInt:[[taskShow valueSelected_AT] intValue]] forKey:@"lineperbed"];
                
            }
                break;
            case ACTION_AT_ADD_NUMBEROFPLANS:{
                NSString * stringNumberOfPlant = taskShow.valueSelected_AT;
                if (!stringNumberOfPlant) {
                    stringNumberOfPlant=@"0";
                }
                [dictSuccession setValue:[NSNumber numberWithFloat:[stringNumberOfPlant floatValue]] forKey:@"numofplants"];
            }
                break;
            case ACTION_AT_ADD_CHECKBOX:{
                 [dictSuccession setValue:[NSNumber numberWithBool:[[taskShow valueSelected_AT] boolValue]] forKey:@"growinginahoophouse"];
            }
                break;
            case ACTION_AT_ACTIONANNUAL:{
                [dictSuccession setValue:[[taskShow dictionaryShow_AT] valueForKey:@"selectType"] forKey:@"annualperennial"];
            }
                break;
            case ACTION_AT_ACTIONSEEDSOURCE:{
                if (![taskShow.valueSelected_AT isEqualToString:@"0"]) {
                    int indexData = [taskShow.valueSelected_AT intValue];
                    seedsourceDic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:indexData],@"id", nil];

                    
                }
                
            }
                break;
            case ACTION_AT_ACTIONPELLETED:{
                [dictSuccession setValue:[[taskShow dictionaryShow_AT] valueForKey:@"selectType"] forKey:@"seedtype"];
            }
                break;
            case ACTION_HARVERT_SEASION:{
                [dictSuccession setValue:[taskShow valueSelected_AT] forKey:@"harvestseasonid"];
                
            }
                break;
            case ACTION_AT_ADD_PRUNING_SEASON:{
                NSString* winterStr = [[taskShow dictionaryShow_AT] valueForKey:@"winter"] ;
                if ([winterStr isEqualToString:@"0"]) {
                    [dictSuccession setValue:[NSNumber numberWithBool:false] forKey:@"pruningwinter"];
                }
                else{
                    [dictSuccession setValue:[NSNumber numberWithBool:true] forKey:@"pruningwinter"];
                }
                
                NSString* summerStr = [[taskShow dictionaryShow_AT] valueForKey:@"summer"] ;
                if ([summerStr isEqualToString:@"0"]) {
                    [dictSuccession setValue:[NSNumber numberWithBool:false] forKey:@"pruningsummer"];
                }
                else{
                    [dictSuccession setValue:[NSNumber numberWithBool:true] forKey:@"pruningsummer"];
                }
                
            }
                break;
            case ACTION_AT_ADD_PLANTING_YEAR:{
                [dictSuccession setValue:[taskShow valueSelected_AT] forKey:@"plantingyear"];
            }
                break;
            case ACTION_AT_ADD_NUMBER_TREES:{
                if (taskShow.isHaveData_AT) {
                    [dictSuccession setValue:[NSNumber numberWithInt:taskShow.valueSelected_AT.intValue]  forKey:@"numoftrees"];
                }
            }
                break;
            case ACTION_AT_ADD_ROOTSTOCK:{
                if (taskShow.isHaveData_AT) {
                    [dictSuccession setValue:taskShow.valueSelected_AT forKey:@"rootstock"];
                }
            }
                break;
            case ACTION_AT_ADD_ROOTSTOCK_SUPPLIER:{
                if (taskShow.isHaveData_AT) {
                    [dictSuccession setValue:taskShow.valueSelected_AT forKey:@"rootstocksupplier"];
                }
            }
                break;
            case ACTION_AT_ADD_SEEDING_RATE:{
                double seedingrate = ([NSString stringWithFormat:@"%@", [taskShow.dictSelected_AT valueForKey:@"valueRate"]] ).doubleValue;
                [dictSuccession setValue:[[NSNumber alloc] initWithDouble:seedingrate] forKey:@"seedingratevalue"];
                [dictSuccession setValue:[[NSNumber alloc]initWithInt:[[taskShow.dictSelected_AT valueForKey:@"unitRateId"] intValue]] forKey:@"seedingratevalueunitid"];
                
            }
                break;
            default:
                break;
        }
    }
    //Add default
    NSDictionary * dictDeafualt=[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:self.setDefault] forKey:@"setdefault"];
    [dictSuccession setValue:dictDeafualt forKey:@"extendProp"];
    
    // Dict links combination data
    if (self.clsSuccessionDetail.id > 0) {
        [dictSuccession setValue:[NSNumber numberWithInt:(int)self.clsSuccessionDetail.id] forKey:@"id"];
    }
    if (self.categoryId == 3 || (self.categoryId == 5  && self.successionSeedingMethoTypeCL != BROADCAST_SOWN)) {
        [dictSuccession setValue:[NSNumber numberWithDouble:inRowSpacingPlantCL] forKey:@"inrowspacing"];
        [dictSuccession setValue:[NSNumber numberWithDouble:rowFeetPlantCl] forKey:@"rowspacinglow"];
        
    }
    else if (self.categoryId == 5  && self.successionSeedingMethoTypeCL == BROADCAST_SOWN){
    
    }
    else{
        [dictSuccession setValue:[NSNumber numberWithInt:rowPerBedPlantCL] forKey:@"rowperbed"];
        [dictSuccession setValue:[NSNumber numberWithDouble:inRowSpacingPlantCL] forKey:@"inrowspacing"];
        [dictSuccession setValue:[NSNumber numberWithDouble:rowFeetPlantCl] forKey:@"rowfeet"];
    }
    
    if (self.clsSuccessionDetail.id > 0) {

        [dictSuccession setValue:self.clsSuccessionDetail.combinationkey forKey:@"combinationkey"];
    }
    else{
        
        NSDictionary * dictIdCombinationData=[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:self.combinationDataForShow.id] forKey:@"id"];
        NSDictionary * dictCombinationKey=[NSDictionary dictionaryWithObject:self.combinationDataForShow.combinationkeyid forKey:@"combinationkeyid"];
        
        NSMutableDictionary * dictCombinationData=[NSMutableDictionary new];
        [dictCombinationData addEntriesFromDictionary:dictIdCombinationData];
        [dictCombinationData addEntriesFromDictionary:dictCombinationKey];
        [dictLinksSuccession setValue:dictCombinationData forKey:@"combinationdata"];
    }
    
   
    
    [dictLinksSuccession setValue:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:self.successionSeedingMethoTypeCL] forKey:@"id"] forKey:@"subtasktype"];
    if (seedsourceDic) {
        [dictLinksSuccession setValue:seedsourceDic forKey:@"seedsource"];
    }
    
    [dictSuccession setValue:dictLinksSuccession forKey:@"links"];
    [dictLinks setValue:@[dictSuccession] forKey:@"successions"];
    [dictLinksSuccession setValue:[self listDictLocation] forKey:@"locations"];
    
    
    
    //Note
    NSDictionary * dictReturn;
    if (self.clsSuccessionDetail.id > 0) {
        dictReturn=[[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithInteger:[[[ClassGlobal sharedInstance] tendUser] farmID]],dictLinks,self.croptypeId,[NSNumber numberWithInt:(int)self.clsSuccessionDetail.id],[NSNumber numberWithInt:self.categoryId]] forKeys:@[@"farmid",@"links",@"croptypeid",@"id",@"categoryid"]];
    }
    else{
        dictReturn=[[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithInteger:[[[ClassGlobal sharedInstance] tendUser] farmID]],dictLinks,self.croptypeId, [NSNumber numberWithInt:self.categoryId]] forKeys:@[@"farmid",@"links",@"croptypeid",@"categoryid"]];
    }
    return dictReturn;
}


-(void)validateRequiredField:(void(^)(BOOL success, int index))complete{
    for (int i =0; i<dataShows.count; i++) {
        CellForShow * cellShow=[dataShows objectAtIndex:i];
        if (cellShow.typeAction_AT==ACTION_AT_ADD_SEEDINGDATE) {
            if (!cellShow.isHaveData_AT) {
                complete(NO,ACTION_AT_ADD_SEEDINGDATE);
                return;
            }
        }
        if (cellShow.typeAction_AT==ACTION_AT_ADD_LAST_HARVEST) {
            if ([cellShow.valueSelected_AT isEqualToString:@"0"]) {
                complete(NO,ACTION_AT_ADD_LAST_HARVEST);
                return;
            }
        }
        if (cellShow.typeAction_AT==ACTION_AT_ADD_FIRST_HARVEST) {
            if ([cellShow.valueSelected_AT isEqualToString:@"0"]) {
                complete(NO,ACTION_AT_ADD_FIRST_HARVEST);
                return;
            }

        }
        
    }
    complete(YES,0);
}

-(void)removeNoteId:(int)noteid{
    for (int i  = 0; i < dataShows.count; i++) {
        CellForShow* cellDrip = [dataShows objectAtIndex:i];
        if (cellDrip.typeCell_AT == CELL_AT_ADD_NOTE_ITEM && cellDrip.note.id == noteid) {
            [dataShows removeObjectAtIndex:i];
            break;
        }
        
    }
    [self reloadWithNewData];
}
@end
